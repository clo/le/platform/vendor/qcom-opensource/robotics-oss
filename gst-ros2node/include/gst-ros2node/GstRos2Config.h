/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_CONFIG_H__
#define __GST_ROS2_CONFIG_H__

#include "gst-ros2node/GstRos2Common.h"

#define GSTROS2_CONFIG_DIRECTORY "/data/misc/ros2/"

typedef struct _GstRos2CamConfig
{
    uint32_t    pipelineId;
    bool        preStart;
    std::string cameraSource;
    int32_t     cameraId;
    uint32_t    width;
    uint32_t    height;
    uint32_t    stride;
    uint32_t    slice;
    std::string format;
    uint8_t     fps;
    uint8_t     memSave;
    int32_t     publishFreq;
    uint8_t     latencyType;
    uint8_t     logLevel;
    //int32_t     dumpFreq;
    //int32_t     dumpNum;
} GstRos2CamConfig;

typedef enum _GstRos2Cmd
{
    GstRos2CmdQuery          = 0,
    GstRos2CmdSet            = 1,
    GstRos2CmdStop           = 2,
    GstRos2CmdDump           = 3,
    GstRos2CmdExit           = 4,
    GstRos2CmdNop            = 5,
    GstRos2CmdMax,
} GstRos2Cmd;

typedef enum _GstRos2Param
{
    // set params
    PIPELINE_ID = 0,
    PRE_START,
    CAMEREA_SOURCE,
    CAMERA_ID,
    WIDHT,
    HEIGHT,
    FORMAT,
    FPS,
    MEM_SAVE,
    PUBLISH_FREQ,
    LATENCY_TYPE,
    LOG_LEVEL,
    // dump params
    DUMP_FREQ,
    DUMP_NUM,
    RESULT,
    GSTROS2PARAM_MAX,
} GstRos2Param;

typedef struct _GstRos2Result
{
    GstRos2Cmd cmd;
    uint32_t   pipelineId;
    bool       success;
} GstRos2Result;

class GstRos2Config
{
public:
    GstRos2Config(GstRos2CamConfig *camCfg);
    ~GstRos2Config();
    static GstRos2Config* CreateInstance(GstRos2CamConfig *camCfg);
    void Destroy();
    GstRos2CamConfig* GetCameraConfig() { return &m_camCfg; }
    uint32_t GetPipelineId() { return m_camCfg.pipelineId; }
public:
    static int32_t PreLoadConfigure(GstRos2Config** cfg, uint32_t index);
    static std::string ConstructResult(std::string &key, std::string &order, std::string &result);
    static void ParseKey(std::string &orderIn, std::string &key, std::string &orderOut);
    static void ParseOrder(std::string &order, std::string &key, std::string &cmd, std::string &param);
    static GstRos2Cmd ParseCmd(std::string &cmd);
    static GstRos2Result ParseResult(std::string &cmd, std::string &param);
    static int32_t ParseCfg(GstRos2Config** cfg, std::string &param);
    static int32_t GetIntParamFromStr(std::string &cmd, char *const paramStr);
    static int32_t GetPipelineId(std::string cmd);
    static int32_t GetMemSaveMode(std::string cmd);
    static int32_t GetDumpFreq(std::string cmd);
    static int32_t GetDumpNum(std::string cmd);
    static char *const cmdToken[GstRos2CmdMax];
    static char *const paramToken[GSTROS2PARAM_MAX];

private:
    int32_t Init(GstRos2CamConfig *camCfg);
    void Deinit();
    void PrintConfig();

public:
    GstRos2CamConfig m_camCfg;
};

#endif
