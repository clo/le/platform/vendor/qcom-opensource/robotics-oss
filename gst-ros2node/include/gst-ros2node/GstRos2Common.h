/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_COMMON_H__
#define __GST_ROS2_COMMON_H__

#include <functional>

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <tinyxml2.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>

#include <linux/un.h>
#include <linux/in.h>
#include <linux/ion.h>
#include <linux/msm_ion.h>

//ROS2 headers
#include <rclcpp/rclcpp.hpp>
#include <std_msgs/msg/string.hpp>
#include <sensor_msgs/image_encodings.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <image_transport/image_transport.h>

#ifdef ENABLE_LIBLOG
#include <log/log.h>
#ifdef  LOG_TAG
#undef  LOG_TAG
#endif

#define LOG_NDEBUG 0
#define LOG_TAG "GstRos2"

#else
#define ALOGE(fmt, args...) do {} while (0)
#define ALOGW(fmt, args...) do {} while (0)
#define ALOGI(fmt, args...) do {} while (0)
#define ALOGD(fmt, args...) do {} while (0)
#endif

#define STRINGIZE(arg) #arg
#define STRINGIZE_MACRO(arg) STRINGIZE(arg)

#define GSTROS2_ERR(fmt, args...) do {                              \
    ALOGE("%s %d:" fmt, __FUNCTION__, __LINE__, ##args);            \
    GstRos2Log("[ERROR] %s() " STRINGIZE_MACRO(__LINE__) ": " fmt,  \
               __FUNCTION__,                                        \
               ##args);                                             \
} while (0)

#define GSTROS2_WARN(fmt, args...) do {                             \
    ALOGW("%s %d:" fmt, __FUNCTION__, __LINE__, ##args);            \
    GstRos2Log("[WARN ] %s() " STRINGIZE_MACRO(__LINE__) ": " fmt,  \
                __FUNCTION__,                                       \
               ##args);                                             \
} while (0)

#define GSTROS2_INFO(fmt, args...) do {                             \
    ALOGI("%s %d:" fmt, __FUNCTION__, __LINE__, ##args);            \
    GstRos2Log("[INFO ] %s() " STRINGIZE_MACRO(__LINE__) ": " fmt,  \
               __FUNCTION__,                                        \
               ##args);                                             \
} while (0)

#define GSTROS2_DBG(fmt, args...) do {                              \
    ALOGD("%s %d:" fmt, __FUNCTION__, __LINE__, ##args);            \
    GstRos2Log("[DEBUG] %s() " STRINGIZE_MACRO(__LINE__) ": " fmt,  \
               __FUNCTION__,                                        \
               ##args);                                             \
} while (0)

#define GSTROS2_PRINT(fmt, args...) do {                            \
    printf(fmt, ##args);                                            \
} while (0)

#define MAX_CAMERA_NUM             (5)
#define MAX_GST_ROS2_PIPELINES     MAX_CAMERA_NUM
#define MAX_CLIENTS_PER_PIPELIEN   (5)

#define QMMFSRC                "qtiqmmfsrc"
#define V4L2SRC                "v4l2src"
#define GSTROS2_WORKDIR        "/data/misc/ros2"

#define GstRos2Max(a,b)       ((a) >= (b)? (a):(b))

typedef enum _GstRos2State
{
    GstRos2StateUninitialize = 0,
    GstRos2StateConfigured   = 1,
    GstRos2StateStart        = 2,
    GstRos2StateStop         = 3,
    GstRos2StateMax          = 4,
} GstRos2State;

typedef enum _GstRos2Fmt
{
    GstRos2Fmt_INVALID  = 0,
    GstRos2Fmt_YUV      = 1,
    GstRos2Fmt_JPEG     = 2,
    GstRos2Fmt_RGB      = 3,
    GstRos2Fmt_RAW      = 4,
    GstRos2Fmt_MONO     = 5,
} GstRos2Fmt;

typedef enum _GstRos2MemSaveMode
{
    GstRos2MemSaveDisable      = 0,
    GstRos2MemSaveBySharedMem  = 1,
} GstRos2MemSaveMode;

typedef enum _GstRos2LatencyType
{
    GstRos2LatencyShutter      = 0,
    GstRos2LatencyDistribution = 1,
} GstRos2LatencyType;

typedef struct _GstRos2FrameInfo
{
    std::string  cameraSource;
    uint32_t     pipelineId;
    int32_t      cameraId;
    uint32_t     frameId;
    uint64_t     size;
    uint32_t     width;
    uint32_t     height;
    uint32_t     stride;
    uint32_t     slice;
    std::string  format;
    int64_t      timestamp; //ns
    int64_t      latency;   //ns
    uint8_t      latencyType;
    uint8_t      memSave;
    int32_t      fd;
} GstRos2FrameInfo;

typedef struct _GstRos2Frame
{
    GstRos2FrameInfo   info;
    uint8_t           *data;
} GstRos2Frame;

typedef struct _pthreadInfo{
    pthread_t                pid;
    pthread_attr_t           attr;
    std::mutex               mutex;
    std::condition_variable  cond;
    volatile bool            isRunning;
    void                    *prvData;
} PthreadInfo;

static inline int32_t gettid()
{
    return (int32_t)syscall(SYS_gettid);
}

static void GstRos2Log(const char* pFormat, ...)
{
    char logText[512];
    va_list args;
    va_start(args, pFormat);
    vsnprintf(logText, sizeof(logText), pFormat, args);
    va_end(args);

    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);
    struct tm* pCurrentTime = localtime((time_t *)&spec.tv_sec);

    if (pCurrentTime != NULL)
    {
        printf("%02d-%02d %02d:%02d:%02d:%09ld %5d %5d %s\n",
            (pCurrentTime->tm_mon + 1),
            pCurrentTime->tm_mday,
            pCurrentTime->tm_hour,
            pCurrentTime->tm_min,
            pCurrentTime->tm_sec,
            spec.tv_nsec,
            getpid(),
            gettid(),
            logText);
    }
    else
    {
        printf("%5d %5d %s\n",
            getpid(),
            gettid(),
            logText);
    }
}

static inline GstRos2Fmt GetFormatType(std::string format)
{
    if (format == "NV12" || format == "YUY2")
    {
        return GstRos2Fmt_YUV;
    }
    else if(format == "JPEG")
    {
        return GstRos2Fmt_JPEG;
    }
    else if (format == "rgb8")
    {
        return GstRos2Fmt_RGB;
    }
    else if (format == "mono8")
    {
        return GstRos2Fmt_MONO;
    }
    else if (format.npos != format.find("RAW"))
    {
        return GstRos2Fmt_RAW;
    }

    return GstRos2Fmt_INVALID;
}

static inline const char* GetFileType(GstRos2Fmt format)
{
    if (format == GstRos2Fmt_YUV)
    {
        return "yuv";
    }
    else if (format == GstRos2Fmt_RAW)
    {
        return "raw";
    }
    else if (format == GstRos2Fmt_JPEG)
    {
        return "jpeg";
    }
    else if (format == GstRos2Fmt_RGB)
    {
        return "rgb";
    }
    else if (format == GstRos2Fmt_MONO)
    {
        return "mono8";
    }

    return "unknown";
}

static inline uint32_t ALIGN(uint32_t operand, uint32_t alignment)
{
    uint32_t remainder = (operand % alignment);

    return (0 == remainder) ? operand : operand - remainder + alignment;
}

static void GetAlignment(
    bool isQmmfsrc,   std::string format,
    uint32_t  width,  uint32_t height,
    uint32_t *stride, uint32_t *slice)
{
    if (stride == NULL || slice == NULL)
    {
        return;
    }

    GstRos2Fmt fmt = GetFormatType(format);
    switch (fmt)
    {
        case GstRos2Fmt_YUV:
        {
            /// 512 Alignment hardcode for kona venius buffer
            *stride = isQmmfsrc? ALIGN(width,  512) : width;
            *slice  = isQmmfsrc? ALIGN(height, 512) : height;

            break;
        }
        case GstRos2Fmt_MONO:
        {
            *stride = isQmmfsrc? ALIGN(width,  512) : width;
            *slice  = isQmmfsrc? ALIGN(height, 512) : height;
            break;
        }
        case GstRos2Fmt_RGB:
        {
            *stride = width;
            *slice  = height;
            break;
        }
        default:
        {
            *stride = width;
            *slice  = height;
            break;
        }
    }
}

#define S2NS(s) (s * 1000000000LL)
static void showFPS(GstRos2Frame *frame)
{
    static volatile int64_t   g_lastFpstime[MAX_GST_ROS2_PIPELINES];
    static volatile int64_t   g_totalLatency[MAX_GST_ROS2_PIPELINES];
    static volatile uint32_t  g_frameCount[MAX_GST_ROS2_PIPELINES];

    uint32_t pipelineId = frame->info.pipelineId;
    if (pipelineId >= MAX_GST_ROS2_PIPELINES)
    {
        return;
    }

    struct timespec time;
    time.tv_sec = time.tv_nsec = 0;
    clock_gettime(CLOCK_MONOTONIC, &time);
    int64_t now = S2NS(time.tv_sec) + time.tv_nsec;

    if (frame->info.frameId == 0 || g_frameCount[pipelineId] == 0)
    {
        /// reset
        g_lastFpstime[pipelineId]    = now;
        g_totalLatency[pipelineId]   = 0;
        g_frameCount[pipelineId]     = 0;
    }

    g_frameCount[pipelineId]++;

    volatile int64_t diff = now - g_lastFpstime[pipelineId];

    g_totalLatency[pipelineId] += frame->info.latency;

    if (diff > S2NS(5))
    {
        double fps = (((double)(g_frameCount[pipelineId])) *
                     (double)(S2NS(1))) / (double)diff;

        int64_t latency = g_totalLatency[pipelineId] / g_frameCount[pipelineId] / 1000;
        GSTROS2_DBG("Pipeline %u, Camera %d, fmt %s, w x h[%u x %u], str x sli[%u x %u], "
            "frameId %u, frameCnt %u, FPS: %.4lf, ts %ld ns, latency %ld us \n",
            frame->info.pipelineId,
            frame->info.cameraId,
            frame->info.format.c_str(),
            frame->info.width,
            frame->info.height,
            frame->info.stride,
            frame->info.slice,
            frame->info.frameId,
            g_frameCount[pipelineId],
            fps,
            frame->info.timestamp,
            latency
        );

        g_lastFpstime[pipelineId]  = now;
        g_totalLatency[pipelineId] = 0;
        g_frameCount[pipelineId]   = 0;
    }
}

static void DumpFrameToFile(GstRos2Frame *frame, const char* name)
{
    uint8_t * bufData = frame->data;
    int32_t   bufSize = frame->info.size;

    char fname[256];
    snprintf(fname, sizeof(fname), "%s/%s_wh[%dx%d]_ali[%dx%d]_id[%d-%d-%d].%s",
        GSTROS2_WORKDIR,
        name,
        frame->info.width,
        frame->info.height,
        frame->info.stride,
        frame->info.slice,
        frame->info.pipelineId,
        frame->info.cameraId,
        frame->info.frameId,
        GetFileType(GetFormatType(frame->info.format)));

    GSTROS2_PRINT("%s\n", fname);

    FILE *fd = fopen (fname, "wb");
    fwrite(bufData, bufSize, 1, fd);
    fclose(fd);
    fd = NULL;
}

#endif
