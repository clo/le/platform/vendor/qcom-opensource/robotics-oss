/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_SHARED_MEM_H__
#define __GST_ROS2_SHARED_MEM_H__

#include "gst-ros2node/GstRos2Common.h"
#include "gst-ros2node/GstRos2Socket.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define GST_ROS2_SHM_TAB_MASTER_PID_INDEX (0)
#define GST_ROS2_MAX_NUM_FD               (32)
#define GST_ROS2_SHM_TAB_WIDTH            (MAX_CLIENTS_PER_PIPELIEN + 1)
#define GST_ROS2_SHM_TAB_HEIGHT           (GST_ROS2_MAX_NUM_FD)

typedef enum _GstRos2ShmType
{
    GstRos2ShmTypeInvalid     = 0,
    GstRos2ShmTypeTab         = 1,
    GstRos2ShmTypeBuffer      = 2,
} GstRos2ShmType;

typedef struct _GstRos2ShmInfo
{
    int32_t               type;
    int32_t               fd;
    uint64_t              size;
    #ifndef TARGET_ION_ABI_VERSION
    ion_user_handle_t     handle;
    #endif
} GstRos2ShmInfo;

typedef struct _GstRos2Shm
{
    GstRos2ShmInfo        info;
    void*                 data;
} GstRos2Shm;

typedef struct _GstRos2ShTabHeader
{
    int32_t               id;            /// pipeline id
    int32_t               numActivePids; /// record pids of all processes
    int32_t               numActiveFds;  /// record master shared fds
} GstRos2ShTabHeader;

typedef struct _GstRos2ShTab
{
    GstRos2ShTabHeader    header;
    int32_t               pidList[GST_ROS2_SHM_TAB_WIDTH];
    GstRos2Shm            mapList[GST_ROS2_SHM_TAB_HEIGHT][GST_ROS2_SHM_TAB_WIDTH];
} GstRos2ShTab;

typedef struct _GstRos2SharedMemCreateInfo
{
    int32_t            pipelineId;
    bool               isMaster;
} GstRos2SharedMemCreateInfo;

class GstRos2SharedTable;
class GstRos2SharedMem
{
public:
    GstRos2SharedMem(GstRos2SharedMemCreateInfo* info);
    ~GstRos2SharedMem();

    static GstRos2SharedMem* CreateInstance(GstRos2SharedMemCreateInfo* info);
    void    Destroy();
    int32_t PublishShm(GstRos2Shm *shm);
    int32_t GetShm(GstRos2Shm *shm);
    int32_t Flush();

public:
    static int32_t MapShm(GstRos2Shm *shm);
    static int32_t UnmapShm(GstRos2Shm *shm);
    static bool    IsValidShm(GstRos2Shm *shm);
    static bool    IsValidShm(GstRos2Shm *shm, GstRos2ShmType type);
    static bool    CompareShm(GstRos2Shm *shmSrc, GstRos2Shm *shmDst);

public:
    static int32_t g_ionFd;

private:
    int32_t IonOpen();
    void    IonClose();
    int32_t Init(GstRos2SharedMemCreateInfo* info);
    void    Deinit();
    void    SyncCb(int32_t msg, int32_t msgData);

private:
    GstRos2SharedTable          *m_pShTab;
    GstRos2Socket               *m_pSocket;
    int32_t                      m_id;
    int32_t                      m_pid;
    bool                         m_isMaster;
    static std::vector<int32_t>  g_idList;
    std::mutex                   m_lock;
};

class GstRos2SharedTable
{
public:
    GstRos2SharedTable(GstRos2SharedMemCreateInfo* info);
    ~GstRos2SharedTable();
    static GstRos2SharedTable* CreateInstance(GstRos2SharedMemCreateInfo* info);
    void   Destroy();

    int32_t CreateShTab(GstRos2Shm *tab);
    int32_t ReleaseShTab();
    int32_t WriteShmToTab(GstRos2Shm *shm, int32_t masterFd);
    int32_t ReadShmFromTab(GstRos2Shm *shm, int32_t masterFd, uint64_t masterSize);
    void    PrintShTab();
    int32_t MatchPidFromTab(int32_t pid);
    int32_t MatchFdFromTab(int32_t pidIdx, int32_t fd);
    int32_t InsertPidToTab(int32_t pid);
    int32_t RemovePidFromTab(int32_t pid);
    int32_t GetActiveNumPids();
    int32_t GetActiveNumFds();
    std::vector<int32_t> GetUnmapPid(int32_t masterFd);
    int32_t Flush();
    GstRos2Shm * GetShTab() { return &m_shTab; }
private:
    int32_t Init(GstRos2SharedMemCreateInfo* info);
    void    Deinit();

private:
    GstRos2Shm                              m_shTab;
    int32_t                                 m_id;
    int32_t                                 m_pid;
    bool                                    m_isMaster;
    std::mutex                              m_lock;

private:
    static std::map<int32_t, GstRos2Shm*>   g_shTabList;
};

#endif
