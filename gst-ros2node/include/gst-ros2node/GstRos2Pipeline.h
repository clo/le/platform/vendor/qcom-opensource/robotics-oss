/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_PIPELINE_H__
#define __GST_ROS2_PIPELINE_H__

#include <gst/gst.h>
#include <gst/allocators/allocators.h>
#include "gst-ros2node/GstRos2Common.h"
#include "gst-ros2node/GstRos2Config.h"
#include "gst-ros2node/GstRos2Msg.h"

//#define USE_APPSINK 1
#define GET_REFCNT(e) (((GObject *)e)->ref_count)

class GstRos2Pipeline
{
public:
    GstRos2Pipeline(GstRos2Config* cfg);
    ~GstRos2Pipeline();
    static GstRos2Pipeline* CreateInstance(GstRos2Config* cfg);
    void Destroy();
    void RegisterPublish(GstRos2MsgPublishFrameFunc publish);
    bool UpdateParams(GstRos2Config* cfg);

private:
    int32_t Init(GstRos2Config* cfg);
    void Deinit();
    void SetLogLevel(GstRos2CamConfig *camCfg);
    int32_t Start();
    void Stop();
    int32_t ProcessData(GstBuffer *buffer);
    int64_t GetGstTime();

    static GstPadProbeReturn DataCb(GstPad *pad, GstPadProbeInfo *probeInfo, gpointer userData);
    static GstFlowReturn SampleCb (GstElement *sink, gpointer userData);

    static gboolean BusCb(GstBus * bus, GstMessage * msg, gpointer userData);

    GstRos2MsgPublishFrameFunc m_pPublish;

    GMutex      m_mutex;
    GCond       m_cond;
    GstElement *m_pPipeline;
    GstElement *m_pSrc;
    GstElement *m_pFilter0;
    GstElement *m_pPostProcess;
    GstElement *m_pFilter1;
    GstElement *m_pQueue;
    GstElement *m_pSink;
    GstPad     *m_pSinkPad;
    //GMainLoop  *loop;
    GstRos2Config   *m_pConfig;
    int32_t          m_curFrameId;
    GstClockTime     m_baseTime;
};

#endif
