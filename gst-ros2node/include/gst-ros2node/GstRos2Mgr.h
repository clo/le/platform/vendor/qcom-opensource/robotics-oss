/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_MGR_H__
#define __GST_ROS2_MGR_H__

#include "gst-ros2node/GstRos2Common.h"
#include "gst-ros2node/GstRos2Config.h"
#include "gst-ros2node/GstRos2Msg.h"
#include "gst-ros2node/GstRos2PipelineMgr.h"

typedef struct _GstRos2MgrCreateInfo
{
    rclcpp::Node::SharedPtr ros2Node;
} GstRos2MgrCreateInfo;

class GstRos2Mgr
{
public:
    GstRos2Mgr(GstRos2MgrCreateInfo *info);
    ~GstRos2Mgr();
    static GstRos2Mgr* CreateInstance(GstRos2MgrCreateInfo *info);
    void Destroy();
    void Run();
    //GstRos2State GetState(int32_t id);

private:
    int32_t Init(GstRos2MgrCreateInfo *info);
    void Deinit();
    void MsgNotify(GstRos2MsgPkg msg);
    void ReportResult(std::string &result);
    int32_t Setup(GstRos2Config* cfg);

    GstRos2PipelineMgr       *m_pGstRos2PipelineMgr;
    GstRos2Msg               *m_pGstRos2Msg;
    GstRos2Config            *m_pConfig[MAX_GST_ROS2_PIPELINES];
    GstRos2State              m_state[MAX_GST_ROS2_PIPELINES];
    rclcpp::Node::SharedPtr   m_ros2Node;
    std::mutex                m_lock;
};

#endif