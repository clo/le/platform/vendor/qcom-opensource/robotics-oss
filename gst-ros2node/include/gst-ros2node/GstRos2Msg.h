/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_MSG_H__
#define __GST_ROS2_MSG_H__

#include "gst-ros2node/GstRos2Common.h"
#include "gst-ros2node/GstRos2SharedMem.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//#define ENABLE_IMAGE_TRANSPORT 1
#ifdef ENABLE_IMAGE_TRANSPORT
    typedef const sensor_msgs::msg::Image::ConstSharedPtr&  Ros2ImageMsgType;
#else
    typedef const sensor_msgs::msg::Image::SharedPtr       Ros2ImageMsgType;
#endif

// Topic for settings from clients to master
#define GST_ROS2_TOPIC_CFG "GstRos2/config"
// Topic for message from master to  clients
#define GST_ROS2_TOPIC_RES "GstRos2/result"
// Topic for camera images from master to clients
#define GST_ROS2_TOPIC_IMG "GstRos2/img_%u"

#define GST_ROS2_FRAME_ID  "%s cam[%u_%u] id[%u]"

#define GST_ROS2_IMG_QUEUE_DEPTH (10)
#define GST_ROS2_MSG_QUEUE_DEPTH (10)

typedef enum _GstRos2MsgDirection
{
    GstRos2MsgDirectionPublish = 0,
    GstRos2MsgDirectionListen  = 1,
} GstRos2MsgDirection;

typedef enum _GstRos2MsgType
{
    GstRos2MsgNone        = 0,
    GstRos2MsgStr         = 1 << 0,
    GstRos2MsgImg         = 1 << 1,
    GstRos2MsgSharedImg   = 1 << 2,
} GstRos2MsgType;

typedef struct _GstRos2MsgPkg
{
    GstRos2MsgType   type;
    void            *data;
} GstRos2MsgPkg;

typedef struct _GstRos2MsgTopic
{
    GstRos2MsgDirection   direction;
    uint32_t              idx;
    GstRos2MsgType        type;
    const char            topicName[32];
} GstRos2MsgTopic;

typedef struct _GstRos2MsgCreateInfo
{
    rclcpp::Node::SharedPtr    ros2Node;
    int64_t                    timeNsOffset;
} GstRos2MsgCreateInfo;

typedef std::function<void(GstRos2MsgPkg msg)> GstRos2MsgNotifyCb;
typedef std::function<void(GstRos2Frame *frame, uint32_t topicIdx)> GstRos2MsgPublishFrameFunc;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GstRos2MsgPublisher
{
public:
    GstRos2MsgPublisher(GstRos2MsgCreateInfo *info);
    ~GstRos2MsgPublisher();
    static GstRos2MsgPublisher* CreateInstance(GstRos2MsgCreateInfo *info);
    void Destroy();
    int32_t AdvertiseTopic(GstRos2MsgTopic topic);
    int32_t PublishImage(GstRos2Frame *frame);
    int32_t PublishSentence(std::string &sentence);
private:
    int32_t Init(GstRos2MsgCreateInfo *info);
    void Deinit();
    void ConvertFrame2Msg(GstRos2Frame *frame, sensor_msgs::msg::Image::UniquePtr& imgMsg, GstRos2Shm &shm);
    int32_t PublishShmBuffer(GstRos2Shm *shm);
    void GetBasicImgInfo(GstRos2Frame *frame, sensor_msgs::msg::Image::UniquePtr& imgMsg);

    rclcpp::Node::SharedPtr                                       m_pRos2Node;
    image_transport::ImageTransport                              *m_pImageTransport;
    image_transport::Publisher                                    m_itImgPublisher;
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr         m_pImgPublisher;
    rclcpp::Publisher<std_msgs::msg::String>::SharedPtr           m_pStrPublisher;
    int64_t                                                       m_timeNsOffset;
    //GstRos2MsgTopic                                               m_topic;
    GstRos2SharedMem                                             *m_pShm;
    std::mutex                                                    m_lock;
    uint32_t                                                      m_frameId;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GstRos2MsgListener
{
public:
    GstRos2MsgListener(GstRos2MsgCreateInfo *info);
    ~GstRos2MsgListener();
    static GstRos2MsgListener* CreateInstance(GstRos2MsgCreateInfo *info);
    void Destroy();
    int32_t RegisterMsgNotify(GstRos2MsgTopic topic, GstRos2MsgNotifyCb notify);
private:
    int32_t Init(GstRos2MsgCreateInfo *info);
    void Deinit();
    void ImageNotify(Ros2ImageMsgType imgMsg);
    static void ImageNotify(Ros2ImageMsgType imgMsg, void* userData);
    static void SentenceNotify(const std_msgs::msg::String::SharedPtr msg, void* userData);
    void ConvertImgMsg2Frame(Ros2ImageMsgType imgMsg, GstRos2Frame *frame);
    int32_t GetShmBuffer(GstRos2Frame *frame);

    rclcpp::Node::SharedPtr                                   m_pRos2Node;
    image_transport::ImageTransport                          *m_pImageTransport;
    image_transport::Subscriber                               m_itImgSubscriber;
    rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr  m_pImgSubscriber;
    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr    m_pStrSubscriper;
    GstRos2MsgNotifyCb                                        m_pNotify;
    GstRos2SharedMem                                         *m_pShm;
    std::mutex                                                m_lock;
    //GstRos2MsgTopic                                           m_topic;
    uint32_t                                                  m_frameId;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GstRos2Msg
{
public:
    GstRos2Msg(GstRos2MsgCreateInfo *info);
    ~GstRos2Msg();
    static GstRos2Msg* CreateInstance(GstRos2MsgCreateInfo *info);
    void Destroy();
    static int64_t CalLatency(int64_t ts);
    static int64_t GetCurRosTime();
    void SyncTimestamp(int64_t gstTime);
    int32_t AdvertiseTopic(GstRos2MsgTopic topic);
    int32_t RegisterMsgNotify(GstRos2MsgTopic topic, GstRos2MsgNotifyCb notify);
    int32_t CancelImgTopic(GstRos2MsgTopic topic);
    void PublishImage(GstRos2Frame *frame, int32_t topicIdx);
    void PublishSentence(std::string &sentence, int32_t topicIdx);

private:
    int32_t Init(GstRos2MsgCreateInfo *info);
    void Deinit();

    GstRos2MsgCreateInfo                m_createInfo;
    /// for sentence
    GstRos2MsgPublisher                *m_pGstRos2MsgPublisher;
    GstRos2MsgListener                 *m_pGstRos2MsgListener;
    /// for images
    GstRos2MsgPublisher                *m_pGstRos2ImgPublisher[MAX_GST_ROS2_PIPELINES];
    GstRos2MsgListener                 *m_pGstRos2ImgListener[MAX_GST_ROS2_PIPELINES];
    std::mutex                          m_lock;
};

#endif
