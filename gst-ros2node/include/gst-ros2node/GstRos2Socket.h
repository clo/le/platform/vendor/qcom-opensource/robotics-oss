/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_SOCKET_H__
#define __GST_ROS2_SOCKET_H__

#include "gst-ros2node/GstRos2Common.h"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define INVALID_SOCKET         (-1)
#define GSTROS2_SERVER_NAME    GSTROS2_WORKDIR"/shm%d"
#define GSTROS2_CLIENT_NAME    GSTROS2_WORKDIR"/shm%d_client%d"

typedef enum _GstRos2SocketSyncMsg
{
    GstRos2SocketSyncConnect      = 0,
    GstRos2SocketSyncDisconnect   = 1,
} GstRos2SocketSyncMsg;

typedef std::function<void(int32_t msg, int32_t msgData)> GstRos2SocketSyncCb;

typedef enum _GstRos2SocketType
{
    GstRos2SocketDirectionServer = 0,
    GstRos2SocketDirectionClient = 1,
} GstRos2SocketType;

typedef struct _GstRos2SocketCreateInfo
{
    int32_t                   pipelineId;
    GstRos2SocketType         type;
    GstRos2SocketSyncCb       pSyncCb;
} GstRos2SocketCreateInfo;

class GstRos2Socket
{
public:
    GstRos2Socket(GstRos2SocketCreateInfo *info);
    virtual ~GstRos2Socket();
    static GstRos2Socket* CreateInstance(GstRos2SocketCreateInfo *info);
    virtual void Destroy();
    virtual int32_t SendFd(std::vector<int32_t> &fdList, int32_t sendFd);
    virtual int32_t RecvFd(int32_t *recvFd, int32_t *mapFd);
    virtual int32_t Setup(int pipelineId);
    std::vector<int32_t> GetClientFdFromPid(std::vector<int32_t> &pidList);
    bool CheckFdInClientMap(int32_t fd);

protected:
    virtual int32_t Init(GstRos2SocketCreateInfo *info);
    virtual void Deinit();

protected:
    GstRos2SocketType              m_type;
    int32_t                        m_pid;
    int32_t                        m_pipelineId;
    GstRos2SocketSyncCb            m_pSyncCb;
    std::map<int32_t, int32_t>     m_clientMap;
    std::mutex                     m_lock;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GstRos2SocketServer : public GstRos2Socket
{
public:
    GstRos2SocketServer(GstRos2SocketCreateInfo *info);
    virtual ~GstRos2SocketServer();
    virtual int32_t SendFd(std::vector<int32_t> &fdList, int32_t sendFd);

private:
    virtual int32_t Init(GstRos2SocketCreateInfo *info);
    virtual void Deinit();
    virtual int32_t Setup(int pipelineId);
    static void* DetectThread(void *data);

private:
    struct sockaddr_un             m_serverUn;
    int32_t                        m_serverFd;
    PthreadInfo                    m_thread;
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GstRos2SocketClient : public GstRos2Socket
{
public:
    GstRos2SocketClient(GstRos2SocketCreateInfo *info);
    virtual ~GstRos2SocketClient();
    virtual int32_t RecvFd(int32_t *recvFd, int32_t *mapFd);

private:
    virtual int32_t Init(GstRos2SocketCreateInfo *info);
    virtual void Deinit();
    virtual int32_t Setup(int pipelineId);

private:
    struct sockaddr_un          m_clientUn;
    int32_t                     m_clientFd;
};

#endif
