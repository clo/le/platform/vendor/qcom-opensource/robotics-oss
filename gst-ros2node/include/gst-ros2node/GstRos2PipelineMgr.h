/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef __GST_ROS2_PIPELINE_MGR_H__
#define __GST_ROS2_PIPELINE_MGR_H__

#include "gst-ros2node/GstRos2Common.h"
#include "gst-ros2node/GstRos2Config.h"
#include "gst-ros2node/GstRos2Pipeline.h"
#include "gst-ros2node/GstRos2Msg.h"

typedef struct _GstRos2PipelineMgrCreateInfo
{
    GstRos2MsgPublishFrameFunc mPubilshFrameFunction;
} GstRos2PipelineMgrCreateInfo;

class GstRos2PipelineMgr
{
public:
    GstRos2PipelineMgr(GstRos2PipelineMgrCreateInfo *info);
    ~GstRos2PipelineMgr();
    static GstRos2PipelineMgr* CreateInstance(GstRos2PipelineMgrCreateInfo *info);
    void Destroy();
    int32_t Setup(GstRos2Config* cfg);
    bool UpdateParams(GstRos2Config* cfg);
    int64_t GetGstTime();
    int32_t DestroyPipeline(uint32_t id);
    int32_t DestroyAllPipelines();
private:
    int32_t Init(GstRos2PipelineMgrCreateInfo *info);
    void Deinit();
    GstRos2Pipeline *m_pGstPipeline[MAX_GST_ROS2_PIPELINES];
    GstRos2MsgPublishFrameFunc m_pPublish;
};

#endif