/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <signal.h>
#include "gst-ros2node/GstRos2Common.h"
#include "gst-ros2node/GstRos2Config.h"
#include "gst-ros2node/GstRos2Msg.h"

static const char g_usageStr[] =
"------------------------------------------------------------------------------------------------\n"
"Usage: gst-ros2test                                                                             \n"
"                                                                                                \n"
"------------------------------------------------------------------------------------------------\n"
"set:(start or update camera)                                                                    \n"
"[Ex]                                                                                            \n"
"set:pipelineId=0,cameraSource=v4l2src,cameraId=0,width=1920,height=1080,format=YUY2,fps=5       \n"
"set:pipelineId=1,cameraSource=qtiqmmfsrc,cameraId=0,width=1920,height=1080,format=NV12,fps=30   \n"
"set:pipelineId=2,cameraSource=qtiqmmfsrc,cameraId=1,width=1280,height=720,format=NV12,fps=30    \n"
"set:pipelineId=3,cameraSource=qtiqmmfsrc,cameraId=2,width=1928,height=1208,format=NV12,fps=30   \n"
"set:pipelineId=4,cameraSource=qtiqmmfsrc,cameraId=3,width=1928,height=1208,format=NV12,fps=30   \n"
"[params]                                                                                        \n"
"      @pipelineId:   Pipeline Index                                                             \n"
"                     - N (0 ~ MAX:4)                                                            \n"
"      @cameraSource: Type of image source plugin                                                \n"
"                     - v4l2src (/dev/videoX is decided by cameraId)                             \n"
"                     - qtiqmmfsrc                                                               \n"
"      @cameraId:     Camera Index                                                               \n"
"                     - N (0 ~ MAX:4)                                                            \n"
"      @width:        Image width                                                                \n"
"                     - N (pixels)                                                               \n"
"      @height:       Image height                                                               \n"
"                     - N (pixels)                                                               \n"
"      @format:       Image format                                                               \n"
"                     - YUY2                                                                     \n"
"                     - NV12                                                                     \n"
"                     - mono8 (no blanking, suitable for rviz2)                                  \n"
"                     - rgb8  (no blanking, suitable for rviz2)                                  \n"
"                     - RAW[bpp]/[color_filter]                                                  \n"
"                       RAW10/bggr                                                               \n"
"                       RAW10/rggb                                                               \n"
"                       RAW10/gbrg                                                               \n"
"                       RAW10/grbg                                                               \n"
"                       RAW10/mono                                                               \n"
"      @fps:          Frames Per Second                                                          \n"
"                     - N (fps)                                                                  \n"
"      @memSave:      Memory save mode for image publish (optional)                              \n"
"                     - 0 (disable, default)                                                     \n"
"                     - 1 (enable inter-process shared memory)                                   \n"
"      @publishFreq:  Image publish per [publishFreq]  (optional)                                \n"
"                     - 0 (disable publish)                                                      \n"
"                     - N (>=1)                                                                  \n"
"      @latencyType:  Select latency calculation type (optional)                                 \n"
"                     - 0 (shutter latency)                                                      \n"
"                     - 1 (distribute latency)                                                   \n"
"      @logLevel:     GST log level (optional)                                                   \n"
"                     - N (1 ~ 7)                                                                \n"
"------------------------------------------------------------------------------------------------\n"
"stop:(stop streaming)                                                                           \n"
"[Ex]                                                                                            \n"
"stop:pipelineId=0                                                                               \n"
"[params]                                                                                        \n"
"      @pipelineId:   Pipeline Index                                                             \n"
"                     - N (0 ~ MAX:4)                                                            \n"
"------------------------------------------------------------------------------------------------\n"
"dump:(dump images in test application)                                                          \n"
"[Ex]                                                                                            \n"
"dump: pipelineId=0,dumpFreq=1,dumpNum=3                                                         \n"
"[params]                                                                                        \n"
"      @pipelineId:   Pipeline Index                                                             \n"
"                     - N (0 ~ MAX:4)                                                            \n"
"      @dumpFreq:     Image dump per [dumpFreq] (optional)                                       \n"
"                     - N (>=1)                                                                  \n"
"      @dumpNum:      Numbers of frames to dump                                                  \n"
"                     - N (>=1)                                                                  \n"
"------------------------------------------------------------------------------------------------\n"
"exit:(exit application)                                                                         \n"
"[Ex]                                                                                            \n"
"exit                                                                                            \n"
"------------------------------------------------------------------------------------------------\n"
;

class GstRos2Client
{
public:
    GstRos2Client(rclcpp::Node::SharedPtr ros2Node);
    ~GstRos2Client();
    static GstRos2Client* CreateInstance(rclcpp::Node::SharedPtr ros2Node);
    void Destroy();
    void MsgNotify(GstRos2MsgPkg msg);
    GstRos2Msg* GetRos2Msg() { return m_pRos2Msg; }
    static void* PublishThread(void *data);
    int32_t Run();

private:
    int32_t Init(rclcpp::Node::SharedPtr ros2Node);
    void    Deinit();
    void    ProcessResultMsg(GstRos2MsgPkg msg);
    void    ProcessImage(GstRos2Frame *frame);
    bool    ProcessInputCmd(GstRos2Msg* ros2Msg, std::string &order);

private:
    rclcpp::Node::SharedPtr m_pRos2Node;
    GstRos2Msg*             m_pRos2Msg;
    PthreadInfo             m_publishThread;
    int32_t                 m_dumpFreq[MAX_GST_ROS2_PIPELINES];
    int32_t                 m_dumpNum[MAX_GST_ROS2_PIPELINES];
    int32_t                 m_imgCnt[MAX_GST_ROS2_PIPELINES];
};

GstRos2Client::GstRos2Client(rclcpp::Node::SharedPtr ros2Node)
{
    m_pRos2Node = ros2Node;
    memset(m_dumpFreq, 1, sizeof(m_dumpFreq));
    memset(m_dumpNum, 0, sizeof(m_dumpNum));
    memset(m_imgCnt, 0, sizeof(m_imgCnt));
}

GstRos2Client::~GstRos2Client()
{
    Deinit();
}

int32_t GstRos2Client::Init(rclcpp::Node::SharedPtr ros2Node)
{
    GstRos2MsgCreateInfo info;
    info.ros2Node              = ros2Node;
    GstRos2Msg* ros2Msg        = GstRos2Msg::CreateInstance(&info);
    if (ros2Msg == NULL)
    {
        GSTROS2_ERR("Create GstRos2Msg failed.");
        return -EINVAL;
    }

    m_pRos2Msg = ros2Msg;

    GstRos2MsgTopic topicResult =
    {
        GstRos2MsgDirectionListen,
        0,
        GstRos2MsgStr,
        GST_ROS2_TOPIC_RES
    };

    std::function<void(GstRos2MsgPkg msg)> notifyCb =
        [&] (GstRos2MsgPkg msg)
        {
            this->MsgNotify(msg);
        };

    int32_t rc = ros2Msg->RegisterMsgNotify(topicResult, notifyCb);

    return rc;
}

void GstRos2Client::Deinit()
{
    m_publishThread.isRunning = false;
    // need extra '\n' to unblock std::getline if enable pthread_join
    //pthread_join(publishThread.pid, NULL);

    if (m_pRos2Msg)
    {
        m_pRos2Msg->Destroy();
        m_pRos2Msg = NULL;
    }
}

GstRos2Client* GstRos2Client::CreateInstance(rclcpp::Node::SharedPtr ros2Node)
{
    GstRos2Client* gstRos2Client = NULL;

    gstRos2Client = new GstRos2Client(ros2Node);
    if (gstRos2Client == NULL)
    {
        return gstRos2Client;
    }

    if (gstRos2Client->Init(ros2Node) < 0)
    {
        gstRos2Client->Destroy();
        gstRos2Client = NULL;
    }

    return  gstRos2Client;
}

void GstRos2Client::Destroy()
{
    delete this;
}

void GstRos2Client::ProcessImage(GstRos2Frame *frame)
{
    if (frame == NULL ||
        frame->info.pipelineId >= MAX_GST_ROS2_PIPELINES)
    {
        GSTROS2_ERR("invalid params");
        return;
    }

    int32_t id = frame->info.pipelineId;

    showFPS(frame);

    m_imgCnt[id]++;
    if ((m_imgCnt[id] % m_dumpFreq[id] == 0) &&
        (m_dumpNum[id] > 0))
    {
        m_dumpNum[id]--;
        DumpFrameToFile(frame, "Test");
    }
}

void GstRos2Client::ProcessResultMsg(GstRos2MsgPkg msg)
{
    GstRos2Result res;
    std::string key, cmd, param;
    std::string resStr((const char *)msg.data);

    GstRos2Config::ParseOrder(resStr, key, cmd, param);

    res = GstRos2Config::ParseResult(cmd, param);

    if (key == "")
    {
        GSTROS2_INFO("Recv broadcast: %s", msg.data);
        if (res.cmd == GstRos2CmdStop &&
                 res.success == true)
        {
            // un-registerMsgNotify
            GstRos2Msg* ros2Msg = GetRos2Msg();

            int32_t id = res.pipelineId;;
            if (id >= 0 && id < MAX_GST_ROS2_PIPELINES)
            {
                GstRos2MsgTopic topicImg =
                {
                    GstRos2MsgDirectionListen,
                    (uint32_t)id,
                    GstRos2MsgImg,
                };

                ros2Msg->RegisterMsgNotify(topicImg, NULL);
            }
        }

    }
    else if (atoi(key.c_str()) == (int32_t)getpid())
    {
        GSTROS2_INFO("Recv: %s", msg.data);

        if (res.cmd == GstRos2CmdSet &&
            res.success == true)
        {
            std::unique_lock<std::mutex> lock(m_publishThread.mutex);
            m_publishThread.cond.notify_one();
        }
    }
    else
    {
        GSTROS2_DBG("Ingore msg: %s", msg.data);
    }
}

void GstRos2Client::MsgNotify(GstRos2MsgPkg msg)
{
    if (msg.data == NULL)
    {
        GSTROS2_ERR("invalid params");
        return;
    }

    switch (msg.type)
    {
        case GstRos2MsgStr:
        {
            ProcessResultMsg(msg);

            break;
        }
        case GstRos2MsgImg:
        {
            ProcessImage((GstRos2Frame *)msg.data);

            break;
        }
        default:
        {
            GSTROS2_ERR("MsgNotify: invalid type %d", msg.type);
            break;
        }
    }
}

bool GstRos2Client::ProcessInputCmd(GstRos2Msg* ros2Msg, std::string &order)
{
    std::unique_lock<std::mutex> lock(m_publishThread.mutex);

    std::string key  = std::to_string(getpid());
    bool keepRunning = true;
    std::string cmd;
    std::string message = "[" + key + "]" + order;

    if (ros2Msg == NULL)
    {
        keepRunning = false;
        return keepRunning;
    }

    if (order == "")
    {
        return keepRunning;
    }

    std::string::size_type pos = order.find(':');
    cmd = order.substr(0, pos);
    switch (GstRos2Config::ParseCmd(cmd))
    {
        case GstRos2CmdDump:
        {
            int32_t id = GstRos2Config::GetPipelineId(order);
            if (id < 0 || id >= MAX_GST_ROS2_PIPELINES)
            {
                for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
                {
                    m_dumpFreq[i] = 1;
                    m_dumpNum[i] += 3;
                }
                break;
            }

            m_dumpFreq[id]  = GstRos2Config::GetDumpFreq(order);
            if (m_dumpFreq[id] <= 0)
            {
                m_dumpFreq[id] = 1;
            }
            m_dumpNum[id]  += GstRos2Config::GetDumpNum(order);

            GSTROS2_DBG("dumpFreq[%d]: %d, dumpNum[%d]: %d",
                id, m_dumpFreq[id],
                id, m_dumpNum[id]);

            break;
        }
        case GstRos2CmdExit:
        {
            keepRunning = false;

            /// unlock rclcpp::spin
            raise(SIGINT);
            break;
        }
        case GstRos2CmdSet:
        {
            /// 1. setup master first
            uint8_t tryMaxCnt = 3;
            bool ack = false;

            while(ack == false && tryMaxCnt > 0)
            {
                ros2Msg->PublishSentence(message, 0);
                if (m_publishThread.cond.wait_for(lock, std::chrono::seconds(2)) == std::cv_status::timeout)
                {
                    tryMaxCnt--;
                    GSTROS2_WARN("Wait ack timeout, retrying");
                }
                else
                {
                    ack = true;
                    GSTROS2_INFO("ros2node setup done.");
                    break;
                }
            }

            if (ack == false)
            {
                GSTROS2_ERR("No response from ros2node!");
            }

            /// 2. Then setup client
            int32_t id = GstRos2Config::GetPipelineId(order);
            if (id >= 0 && id < MAX_GST_ROS2_PIPELINES)
            {
                GstRos2MsgTopic topicImg =
                {
                    GstRos2MsgDirectionListen,
                    (uint32_t)id,
                    GstRos2MsgImg,
                };

                if (GstRos2Config::GetMemSaveMode(order) == GstRos2MemSaveBySharedMem)
                {
                    topicImg.type = GstRos2MsgSharedImg;
                }

                snprintf((char*)topicImg.topicName, sizeof(topicImg.topicName),
                    GST_ROS2_TOPIC_IMG, topicImg.idx);

                std::function<void(GstRos2MsgPkg msg)> imageCb =
                    [&] (GstRos2MsgPkg msg)
                    {
                        this->MsgNotify(msg);
                    };

                ros2Msg->RegisterMsgNotify(topicImg, imageCb);

                m_imgCnt[id] = 0;
            }

            break;
        }
        case GstRos2CmdStop:
        {
            // un-registerMsgNotify
            int32_t id = GstRos2Config::GetPipelineId(order);
            if (id >= 0 && id < MAX_GST_ROS2_PIPELINES)
            {
                GstRos2MsgTopic topicImg =
                {
                    GstRos2MsgDirectionListen,
                    (uint32_t)id,
                    GstRos2MsgImg,
                };

                ros2Msg->RegisterMsgNotify(topicImg, NULL);
            }

            ros2Msg->PublishSentence(message, 0);

            break;
        }
        case GstRos2CmdQuery:
        case GstRos2CmdNop:
        default:
        {
            ros2Msg->PublishSentence(message, 0);

            break;
        }

    }
    return keepRunning;
}

void* GstRos2Client::PublishThread(void *data)
{
    GSTROS2_DBG("start");

    PthreadInfo *pme = (PthreadInfo*) (data);
    GstRos2Client* ros2Client = (GstRos2Client*) pme->prvData;
    GstRos2Msg* ros2Msg = ros2Client->GetRos2Msg();
    if (NULL == ros2Msg)
    {
        GSTROS2_ERR("Get ros2Msg failed");
        return NULL;
    }

    GstRos2MsgTopic topicConfig =
    {
        GstRos2MsgDirectionPublish,
        0,
        GstRos2MsgStr,
        GST_ROS2_TOPIC_CFG
    };
    ros2Msg->AdvertiseTopic(topicConfig);

    /// To avoid ros msg dropping,
    /// add delay to wait topic advertise done.
    usleep(300 * 1000);

    std::string order;
    while (true == pme->isRunning)
    {
        printf("Please enter cmd:  \n");
        if (!std::getline(std::cin, order))
        {
            GSTROS2_ERR("Failed to getline!");
            continue;
        }

        pme->isRunning = ros2Client->ProcessInputCmd(ros2Msg, order);
    }

    GSTROS2_DBG("exit");

    return NULL;
}

int32_t GstRos2Client::Run()
{
    m_publishThread.isRunning = true;
    m_publishThread.prvData = (void*) this;

    int32_t rc = pthread_create(&m_publishThread.pid,
                             NULL,
                             GstRos2Client::PublishThread,
                             (void *)&m_publishThread);
    if (rc)
    {
        GSTROS2_ERR("create thread failed!");
    }

    return rc;
}

int32_t main (int32_t argc, char **argv)
{
    if (getopt(argc, argv, "h") == 'h')
    {
        GSTROS2_PRINT("%s", g_usageStr);
        exit(0);
    }

    rclcpp::init(argc, argv);
    rclcpp::Node::SharedPtr ros2Node = rclcpp::Node::make_shared("GstRos2Test");

    GSTROS2_INFO("GST Ros2 Test APP is Running...");

    GstRos2Client * ros2Client = GstRos2Client::CreateInstance(ros2Node);
    if (ros2Client == NULL)
    {
        GSTROS2_ERR("Create ros2Client failed");
        return -EINVAL;
    }

    ros2Client->Run();

    // spin will block until work comes in, execute work as it becomes available, and keep blocking.
    // It will only be interrupted by Ctrl-C
    rclcpp::spin(ros2Node);

    GSTROS2_INFO("exiting");
    if (ros2Client)
    {
        ros2Client->Destroy();
        ros2Client = NULL;
    }

    rclcpp::shutdown();

    return 0;
}
