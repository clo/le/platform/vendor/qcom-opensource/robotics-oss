/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2PipelineMgr.h"

GstRos2PipelineMgr::GstRos2PipelineMgr(GstRos2PipelineMgrCreateInfo *info)
{
    for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        m_pGstPipeline[i] = NULL;
    }
}

GstRos2PipelineMgr::~GstRos2PipelineMgr()
{
    Deinit();
}

GstRos2PipelineMgr* GstRos2PipelineMgr::CreateInstance(GstRos2PipelineMgrCreateInfo *info)
{
    static GstRos2PipelineMgr* g_gstRos2PipelineMgr = NULL;

    if (g_gstRos2PipelineMgr || info == NULL)
    {
        return g_gstRos2PipelineMgr;
    }

    g_gstRos2PipelineMgr = new GstRos2PipelineMgr(info);
    if (g_gstRos2PipelineMgr == NULL)
    {
        return g_gstRos2PipelineMgr;
    }

    if (g_gstRos2PipelineMgr->Init(info))
    {
        g_gstRos2PipelineMgr->Destroy();
        g_gstRos2PipelineMgr = NULL;
    }

    return  g_gstRos2PipelineMgr;
}

void GstRos2PipelineMgr::Destroy()
{
    delete this;
}

int32_t GstRos2PipelineMgr::Init(GstRos2PipelineMgrCreateInfo *info)
{
    if (info == NULL)
    {
        return -EINVAL;
    }

    GSTROS2_DBG("GST init start");

    gst_init(0, NULL);

    GSTROS2_DBG("GST init start done");

    m_pPublish = info->mPubilshFrameFunction;
    return 0;
}

void GstRos2PipelineMgr::Deinit()
{
    gst_deinit();
}

int64_t GstRos2PipelineMgr::GetGstTime()
{
    GstClock        *clock = gst_system_clock_obtain();
    GstClockTime  gstTime  = gst_clock_get_time(clock);
    gst_object_unref(clock);
    return (int64_t)gstTime;
}

int32_t GstRos2PipelineMgr::Setup(GstRos2Config* cfg)
{
    int32_t rc = 0;

    if (cfg == NULL ||
        cfg->GetPipelineId() >= MAX_GST_ROS2_PIPELINES)
    {
        return -EINVAL;
    }
    uint32_t pipelineId = cfg->GetPipelineId();

    if (m_pGstPipeline[pipelineId])
    {
        m_pGstPipeline[pipelineId]->Destroy();
        m_pGstPipeline[pipelineId] = NULL;
    }

    m_pGstPipeline[pipelineId] = GstRos2Pipeline::CreateInstance(cfg);
    if (!m_pGstPipeline[pipelineId])
    {
        return -EINVAL;
    }

    m_pGstPipeline[pipelineId]->RegisterPublish(m_pPublish);
    return rc;
}

bool GstRos2PipelineMgr::UpdateParams(GstRos2Config* cfg)
{
    bool needRestart = false;

    if (cfg == NULL ||
        cfg->GetPipelineId() >= MAX_GST_ROS2_PIPELINES)
    {
        GSTROS2_ERR("Invalid cfg!");
        return needRestart;
    }
    uint32_t pipelineId = cfg->GetPipelineId();

    if (m_pGstPipeline[pipelineId])
    {
        needRestart = m_pGstPipeline[pipelineId]->UpdateParams(cfg);
    }

    return needRestart ;
}

int32_t GstRos2PipelineMgr::DestroyPipeline(uint32_t id)
{
    if (id >= MAX_GST_ROS2_PIPELINES)
    {
        return -EINVAL;
    }

    if (m_pGstPipeline[id])
    {
        m_pGstPipeline[id]->Destroy();
        m_pGstPipeline[id] = NULL;
    }

    return 0;
}

int32_t GstRos2PipelineMgr::DestroyAllPipelines()
{
    for(int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        if (m_pGstPipeline[i])
        {
            /// disable all notify first
            m_pGstPipeline[i]->RegisterPublish(NULL);
        }
    }

    for(int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        if (m_pGstPipeline[i])
        {
            m_pGstPipeline[i]->Destroy();
            m_pGstPipeline[i] = NULL;
        }
    }
    return 0;
}
