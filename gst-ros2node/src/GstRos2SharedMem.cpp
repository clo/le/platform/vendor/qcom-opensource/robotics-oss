/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2SharedMem.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int32_t GstRos2SharedMem::g_ionFd = -1;
std::vector<int32_t> GstRos2SharedMem::g_idList;

GstRos2SharedMem::GstRos2SharedMem(GstRos2SharedMemCreateInfo* info)
{
    m_pSocket          = NULL;
    m_pShTab           = NULL;
}

GstRos2SharedMem::~GstRos2SharedMem()
{
    Deinit();
}

GstRos2SharedMem* GstRos2SharedMem::CreateInstance(GstRos2SharedMemCreateInfo* info)
{
    GstRos2SharedMem* g_GstRos2SharedMem = NULL;

    g_GstRos2SharedMem = new GstRos2SharedMem(info);
    if (g_GstRos2SharedMem == NULL)
    {
        return g_GstRos2SharedMem;
    }

    if (g_GstRos2SharedMem->Init(info) < 0)
    {
        g_GstRos2SharedMem->Destroy();
        g_GstRos2SharedMem = NULL;
    }

    return  g_GstRos2SharedMem;
}

void GstRos2SharedMem::Destroy()
{
    delete this;
}

int32_t GstRos2SharedMem::Init(GstRos2SharedMemCreateInfo* info)
{
    {
        std::lock_guard<std::mutex> lock(m_lock);

        if (info == NULL)
        {
            return -EINVAL;
        }

        m_pid          = getpid();
        m_id           = info->pipelineId;
        m_isMaster     = info->isMaster;

        int32_t rc = IonOpen();
        if (rc > 0)
        {
            GstRos2SharedMem::g_idList.push_back(m_id);
        }

        m_pShTab = GstRos2SharedTable::CreateInstance(info);
        if (m_pShTab == NULL)
        {
            GSTROS2_ERR("Create GstRos2SharedTable failed");
            return -EINVAL;
        }

        std::function<void(int32_t msg, int32_t msgData)> notifyCb =
            [&] (int32_t msg, int32_t msgData)
            {
                this->SyncCb(msg, msgData);
            };

        GstRos2SocketCreateInfo socketInfo;
        socketInfo.pipelineId = m_id;
        socketInfo.type       =
            (m_isMaster ? GstRos2SocketDirectionServer : GstRos2SocketDirectionClient);
        socketInfo.pSyncCb    = notifyCb;

        m_pSocket = GstRos2Socket::CreateInstance(&socketInfo);
        if (m_pSocket == NULL)
        {
            GSTROS2_ERR("Create socket failed");
            return -EINVAL;
        }
    }

    /// Need unlock first as Setup will acquire lock.
    if (m_pSocket->Setup(m_id))
    {
        GSTROS2_ERR("Socket setup failed");
        return -EPERM;
    }

    return 0;
}

void GstRos2SharedMem::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_id >= 0)
    {
        std::vector<int32_t>::iterator iter =
            std::find(GstRos2SharedMem::g_idList.begin(), GstRos2SharedMem::g_idList.end(), m_id);

        if(iter != GstRos2SharedMem::g_idList.end())
        {
            GstRos2SharedMem::g_idList.erase(iter);
        }
    }

    /// Clearing shm tab should be ahead of socket disconnect,
    /// To avoid invaild communication in the process of client stopping.

    if (m_pShTab)
    {
        m_pShTab->Destroy();
        m_pShTab = NULL;
    }

    if (GstRos2SharedMem::g_idList.empty())
    {
        IonClose();
    }

    if (m_pSocket)
    {
        m_pSocket->Destroy();
        m_pSocket = NULL;
    }
}

int32_t GstRos2SharedMem::IonOpen()
{
    if (g_ionFd > 0)
    {
        return g_ionFd;
    }

    g_ionFd = open("/dev/ion", O_RDONLY | O_CLOEXEC);
    if (g_ionFd < 0)
    {
        GSTROS2_ERR("open /dev/ion failed!");
    }
    else
    {
        GSTROS2_DBG("open /dev/ion success!");
    }

    return g_ionFd;
}

void GstRos2SharedMem::IonClose()
{
    int32_t rc = 0;
    if (g_ionFd > 0)
    {
        close(g_ionFd);
    }

    g_ionFd = -1;
}

int32_t GstRos2SharedMem::PublishShm(GstRos2Shm *shm)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (m_pSocket == NULL  ||
        m_pShTab == NULL   ||
        m_isMaster != true ||
        shm == NULL        ||
        shm->info.fd <= 0)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    if (m_pShTab->ReadShmFromTab(shm, shm->info.fd, shm->info.size))
    {
        if (m_pShTab->WriteShmToTab(shm, shm->info.fd))
        {
            GSTROS2_ERR("failed to update tab, type %d", shm->info.type);
            return -EINVAL;
        }
    }

    std::vector<int32_t> pidList = m_pShTab->GetUnmapPid(shm->info.fd);
    if (pidList.empty())
    {
        /// Client fd already in tab
        return 0;
    }

    std::vector<int32_t> ClientList  = m_pSocket->GetClientFdFromPid(pidList);
    if (ClientList.empty())
    {
        GSTROS2_ERR("failed to GetClientFdFromPid");
        return -EDQUOT;
    }

    if (m_pSocket->SendFd(ClientList, shm->info.fd))
    {
        return -EINVAL;
    }

    return 0;
}

int32_t GstRos2SharedMem::GetShm(GstRos2Shm *shm)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (m_pSocket == NULL  ||
        m_pShTab == NULL   ||
        m_isMaster == true ||
        shm == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    if (m_pShTab->ReadShmFromTab(shm, shm->info.fd, shm->info.size) == 0)
    {
        /// Already in map table
        return 0;
    }

    int32_t masterFd = -1;
    if (m_pSocket->RecvFd(&masterFd, &shm->info.fd) <= 0)
    {
        GSTROS2_ERR("RecvFd failed: original fd %d", shm->info.fd);

        m_pShTab->PrintShTab();

        return -EINVAL;
    }

    int32_t rc = 0;
    if (GstRos2SharedMem::MapShm(shm))
    {
        GSTROS2_ERR("map failed");
        rc = -EINVAL;
    }

    if (!rc)
    {
        if (m_pShTab->WriteShmToTab(shm, masterFd))
        {
            GSTROS2_ERR("unable to update tab, type %d", shm->info.type);
            m_pShTab->PrintShTab();
            rc = -EINVAL;
        }
    }

    if (rc)
    {
        /// error handler
        if (shm->data)
        {
            if (GstRos2SharedMem::UnmapShm(shm))
            {
                GSTROS2_ERR("UnmapShm failed");
            }
        }
        else if (shm->info.fd > 0)
        {
            close(shm->info.fd);
            shm->info.fd = -1;
        }
    }

    return rc;
}

int32_t GstRos2SharedMem::MapShm(GstRos2Shm *shm)
{
    if (g_ionFd <= 0      ||
        shm == NULL       ||
        shm->info.fd <= 0 ||
        shm->info.size == 0)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    shm->data = mmap(NULL, shm->info.size, PROT_READ | PROT_WRITE, MAP_SHARED, shm->info.fd, 0);
    if (MAP_FAILED == shm->data)
    {
        GSTROS2_ERR("Failed to map fd %d, size %lu : %d[%s]!",
            shm->info.fd, shm->info.size, -errno, strerror(errno));
        return -ENOMEM;
    }

    GSTROS2_DBG("Map fd %d, data %p, size %lu", shm->info.fd, shm->data, shm->info.size);

    return 0;
}

int32_t GstRos2SharedMem::UnmapShm(GstRos2Shm *shm)
{
    if (g_ionFd <= 0 || shm == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    if (shm->data && shm->info.fd > 0)
    {
        if (munmap((void *)shm->data, shm->info.size) < 0)
        {
          GSTROS2_ERR("unable to unmap buffer[%d], data %p, size %lu, err:%d[%s]",
                 shm->info.fd, shm->data, shm->info.size, errno, strerror(errno));
          return errno;
        }

        GSTROS2_DBG("fd %d, data %p, size %lu",
            shm->info.fd, shm->data, shm->info.size);

        if (shm->info.fd > 0)
        {
            close(shm->info.fd);
            shm->info.fd   = -1;
        }

        shm->data      = NULL;
        shm->info.type = GstRos2ShmTypeInvalid;
        shm->info.size = 0;
    }

    return 0;
}

bool GstRos2SharedMem::IsValidShm(GstRos2Shm *shm)
{
    if (shm == NULL                             ||
        shm->info.type == GstRos2ShmTypeInvalid ||
        shm->info.fd <= 0                       ||
        shm->info.size == 0                     ||
        shm->data == NULL)
    {
        //GSTROS2_DBG("%p %d %d %lu %p", shm, shm->info.type, shm->info.fd, shm->info.size, shm->data);
        return false;
    }

    return true;
}

bool GstRos2SharedMem::IsValidShm(GstRos2Shm *shm, GstRos2ShmType type)
{
    if (shm == NULL            ||
        shm->info.type != type ||
        shm->info.fd <= 0      ||
        shm->info.size == 0    ||
        shm->data == NULL)
    {
        return false;
    }

    if (shm->info.type == GstRos2ShmTypeTab)
    {
        GstRos2ShTab *tabData = (GstRos2ShTab *) shm->data;
        if (tabData == NULL                                        ||
            tabData->header.id < 0                                 ||
            tabData->header.numActivePids > GST_ROS2_SHM_TAB_WIDTH ||
            tabData->header.numActiveFds  > GST_ROS2_SHM_TAB_HEIGHT)
        {
            GSTROS2_DBG("invalid shm tab %p: type %d, fd %d, size %lu, data %p, id %d, numActivePids %d, numActiveFds %d",
                shm, shm->info.type, shm->info.fd, shm->info.size, shm->data,
                tabData->header.id, tabData->header.numActivePids, tabData->header.numActiveFds);
            return false;
        }
    }
    return true;
}

bool GstRos2SharedMem::CompareShm(GstRos2Shm *shmSrc, GstRos2Shm *shmDst)
{
    if (shmSrc == NULL || shmDst == NULL)
    {
        GSTROS2_ERR("invalid shm");
        return false;
    }

    if (shmSrc->data      != shmDst->data      ||
        shmSrc->info.fd   != shmDst->info.fd   ||
        shmSrc->info.size != shmDst->info.size ||
        shmSrc->info.type != shmDst->info.type)
    {
        return false;
    }

    return true;
}

int32_t GstRos2SharedMem::Flush()
{
    if (!m_pShTab)
    {
        GSTROS2_ERR("invalid shm tab");
    }

    return m_pShTab->Flush();
}

void GstRos2SharedMem::SyncCb(int32_t msg, int32_t msgData)
{
    switch (msg)
    {
        case GstRos2SocketSyncConnect:
        {
            int32_t pid = msgData;

            if (m_pShTab == NULL)
            {
                GSTROS2_ERR("invalid m_pShTab");
                return;
            }
            GstRos2Shm *tab = m_pShTab->GetShTab();

            if (m_isMaster)
            {
                if (!GstRos2SharedMem::IsValidShm(tab, GstRos2ShmTypeTab))
                {
                    GSTROS2_ERR("invalid tab");
                    return;
                }

                /// insert client pid
                if(m_pShTab->InsertPidToTab(pid) < 0)
                {
                    GSTROS2_ERR("failed to insert master pid %d", m_pid);
                    return;
                }

                /// publish shm tab
                if (PublishShm(tab))
                {
                    GSTROS2_ERR("PublishShm failed");
                    return;
                }

                GSTROS2_INFO("GstRos2SocketSyncConnect: Publish tab successfully");
            }
            else
            {
                if (GetShm(tab))
                {
                    GSTROS2_ERR("GetShm failed");
                    return;
                }

                if (!GstRos2SharedMem::IsValidShm(tab, GstRos2ShmTypeTab))
                {
                    GSTROS2_ERR("invalid shm table");
                    return;
                }

                GSTROS2_DBG("Get shm table success!");
            }

            break;
        }
        case GstRos2SocketSyncDisconnect:
        {
            int32_t pid = msgData;
            GSTROS2_INFO("client pid %d disconnected", pid);
            break;
        }
        default:
        {
            break;
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
std::map<int32_t, GstRos2Shm*> GstRos2SharedTable::g_shTabList;

GstRos2SharedTable::GstRos2SharedTable(GstRos2SharedMemCreateInfo* info)
{
    m_shTab.info.type  = GstRos2ShmTypeTab;
    m_shTab.info.fd    = -1;
    m_shTab.info.size  = sizeof(GstRos2ShTab);
    m_shTab.data       = NULL;
}

GstRos2SharedTable::~GstRos2SharedTable()
{
    Deinit();
}

GstRos2SharedTable* GstRos2SharedTable::CreateInstance(GstRos2SharedMemCreateInfo* info)
{
    GstRos2SharedTable* gstRos2SharedTable = NULL;

    gstRos2SharedTable = new GstRos2SharedTable(info);
    if (gstRos2SharedTable == NULL)
    {
        return gstRos2SharedTable;
    }

    if (gstRos2SharedTable->Init(info) < 0)
    {
        gstRos2SharedTable->Destroy();
        gstRos2SharedTable = NULL;
    }

    return  gstRos2SharedTable;
}

void GstRos2SharedTable::Destroy()
{
    delete this;
}

int32_t GstRos2SharedTable::Init(GstRos2SharedMemCreateInfo* info)
{
    if (info == NULL)
    {
        return -EINVAL;
    }

    m_pid          = getpid();
    m_id           = info->pipelineId;
    m_isMaster     = info->isMaster;

    if (m_isMaster)
    {
        if (CreateShTab(&m_shTab))
        {
            GSTROS2_ERR("CreateShTab failed");
            return -ENOMEM;
        }

        if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
        {
            GSTROS2_ERR("invalid tab");
            return -EINVAL;;
        }
    }

    return 0;
}

void GstRos2SharedTable::Deinit()
{
    if (GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;

        PrintShTab();

        /// 1. find matched pid
        int32_t pidIdx = MatchPidFromTab(m_pid);
        if (pidIdx < 0)
        {
            GSTROS2_ERR("Not found matched pid");
            return;
        }

        /// 2. remove pid
        if (RemovePidFromTab(m_pid))
        {
            GSTROS2_ERR("remove pid %d failed", m_pid);
        }

        /// 3. unmap fds
        for (int32_t fdIdx = 0; fdIdx < GST_ROS2_SHM_TAB_HEIGHT; fdIdx++)
        {
            if (!GstRos2SharedMem::IsValidShm(&tabData->mapList[fdIdx][pidIdx],
                 (GstRos2ShmType)tabData->mapList[fdIdx][pidIdx].info.type))
            {
                continue;
            }

            if (tabData->mapList[fdIdx][pidIdx].info.type == GstRos2ShmTypeBuffer)
            {
                if (m_isMaster)
                {
                    tabData->mapList[fdIdx][pidIdx].info.fd   = -1;
                    tabData->mapList[fdIdx][pidIdx].info.size = 0;
                    tabData->mapList[fdIdx][pidIdx].data      = NULL;
                    continue;
                }

                if (GstRos2SharedMem::UnmapShm(&tabData->mapList[fdIdx][pidIdx]))
                {
                    GSTROS2_ERR("UnmapShm failed: pidIdx %d fdIdx %d",
                        pidIdx, fdIdx);
                    continue;
                }
            }
            else
            {
                /// GstRos2ShmTypeTab will be unmap later
                tabData->mapList[fdIdx][pidIdx].info.fd   = -1;
                tabData->mapList[fdIdx][pidIdx].info.size = 0;
                tabData->mapList[fdIdx][pidIdx].data      = NULL;
            }
        }

        /// 4. clear header
        if (m_isMaster)
        {
            tabData->header.numActivePids = 0;
            tabData->header.numActiveFds  = 0;
        }

        /// 5. release shared tab
        ReleaseShTab();
    }
}

int32_t GstRos2SharedTable::CreateShTab(GstRos2Shm *tab)
{
    int32_t rc = 0;

    if (GstRos2SharedMem::g_ionFd <= 0 || tab == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    struct ion_allocation_data allocData;
    memset(&allocData, 0, sizeof(allocData));

    allocData.len          = sizeof(GstRos2ShTab);
    allocData.flags        = ION_FLAG_CACHED;
    allocData.heap_id_mask = ION_HEAP(ION_SYSTEM_HEAP_ID);
    #ifndef TARGET_ION_ABI_VERSION
    {
        allocData.align        = 4096;
    }
    #endif

    rc = ioctl(GstRos2SharedMem::g_ionFd, ION_IOC_ALLOC, &allocData);
    if (rc < 0)
    {
        GSTROS2_ERR("ION_IOC_ALLOC failed %s", strerror(errno));
        return rc;
    }

    #ifndef TARGET_ION_ABI_VERSION
    {
        struct ion_fd_data ionFdData;
        memset(&ionFdData, 0, sizeof(ionFdData));
        ionFdData.handle = allocData.handle;

        rc = ioctl(g_ionFd, ION_IOC_SHARE, &ionFdData);
        if (rc < 0)
        {
            GSTROS2_ERR("ION_IOC_SHARE failed %s", strerror(errno));
            return rc;
        }
        tab->info.fd     = ionFdData.fd;
        tab->info.handle      = ionFdData.handle;
    }
    #else
    {
        tab->info.fd = allocData.fd;
    }
    #endif
    tab->info.size   = allocData.len;
    //GSTROS2_DBG("ION FD %d len %d", shmMap->info.fd, shmMap->info.size);

    if (GstRos2SharedMem::MapShm(tab))
    {
        GSTROS2_ERR("Failed to map fd %d, size %lu : %d[%s]!",
            tab->info.fd, tab->info.size, -errno, strerror(errno));
        return -ENOMEM;
    }

    {
        /// init with master pid
        GstRos2ShTab *tabData     = (GstRos2ShTab *) m_shTab.data;
        memset(m_shTab.data, 0, sizeof(GstRos2ShTab));
        tabData->header.id        = m_id;
        if(InsertPidToTab(m_pid) < 0)
        {
            GSTROS2_ERR("failed to insert master pid %d", m_pid);
        }
    }

    GSTROS2_DBG("Allocated shm table success!");

    if (WriteShmToTab(tab, tab->info.fd))
    {
        GSTROS2_ERR("failed to update tab, type %d", tab->info.type);
        return -EINVAL;
    }

    return rc;
}

int32_t GstRos2SharedTable::ReleaseShTab()
{
    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_ERR("invalid shm tab");
        return -EINVAL;
    }

    if (GstRos2SharedMem::UnmapShm(&m_shTab))
    {
        GSTROS2_ERR("unmap table failed");
        return -EINVAL;
    }

    #ifndef TARGET_ION_ABI_VERSION
    if(m_isMaster)
    {
        ioctl(g_ionFd, ION_IOC_FREE, m_shTab.info.handle);
    }
    #endif

    m_shTab.data = NULL;

    return 0;
}

int32_t GstRos2SharedTable::WriteShmToTab(GstRos2Shm *shm, int32_t masterFd)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (shm == NULL)
    {
        GSTROS2_ERR("invalid shm");
        return -EINVAL;
    }

    if (masterFd < 0)
    {
        GSTROS2_ERR("invalid masterFd");
        return -EINVAL;
    }

    if (!GstRos2SharedMem::IsValidShm(shm, (GstRos2ShmType)shm->info.type))
    {
        GSTROS2_ERR("invalid type %d", shm->info.type);
        return -EINVAL;
    }

    if (shm->info.type != GstRos2ShmTypeTab &&
       !GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_ERR("invalid tab");
        return -EINVAL;
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;

    int32_t pidIdx = MatchPidFromTab(m_pid);
    if (pidIdx < 0)
    {
        GSTROS2_ERR("Not found matched pid");
        return -ENODATA;
    }

    int32_t fdIdx = -1;
    fdIdx = MatchFdFromTab(GST_ROS2_SHM_TAB_MASTER_PID_INDEX, masterFd);
    if (fdIdx < 0)
    {
        if (m_isMaster)
        {
            fdIdx = MatchFdFromTab(GST_ROS2_SHM_TAB_MASTER_PID_INDEX, 0);
            if (fdIdx < 0 || fdIdx >= GST_ROS2_MAX_NUM_FD)
            {
                GSTROS2_ERR("Invalid fdIdx");
                return -ENODATA;
            }
        }
        else
        {
            GSTROS2_ERR("Not found matched master fd %d", masterFd);
            return -ENODATA;
        }
    }

    GstRos2Shm *matchedShm = &tabData->mapList[fdIdx][pidIdx];
    if ((!GstRos2SharedMem::IsValidShm(matchedShm)) ||
        (!GstRos2SharedMem::CompareShm(matchedShm, shm)))
    {
        GSTROS2_INFO("Update shm(%d): pidIdx %d, fdIdx %d, fd %d",
            m_id, pidIdx, fdIdx, shm->info.fd);

        *matchedShm = (*shm);
    }

    if (m_isMaster)
    {
        tabData->header.numActiveFds++;

        GSTROS2_DBG("masterFd %d, numActiveFds %d",
            masterFd, tabData->header.numActiveFds);
    }

    return 0;
}

int32_t GstRos2SharedTable::ReadShmFromTab(GstRos2Shm *shm, int32_t masterFd, uint64_t masterSize)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_INFO("Shm table is to be setuped");
        return -EINVAL;
    }

    if (shm == NULL)
    {
        GSTROS2_ERR("invalid shm");
        return -EINVAL;
    }

    if (masterFd < 0)
    {
        GSTROS2_ERR("invalid masterFd");
        return -EINVAL;
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;

    /// 1. match pid
    int32_t pidIdx = MatchPidFromTab(m_pid);
    if (pidIdx < 0)
    {
        GSTROS2_ERR("Not found matched pid");
        return -ENODATA;
    }

    /// 2. match fd
    int32_t fdIdx   = MatchFdFromTab(GST_ROS2_SHM_TAB_MASTER_PID_INDEX, masterFd);
    if (fdIdx < 0)
    {
        GSTROS2_DBG("Not found matched master fd %d", masterFd);
        return -ENODATA;
    }

    /// 3. Get mapped fd & vaddr
    GstRos2Shm *matchedShm = &tabData->mapList[fdIdx][pidIdx];
    if ((!GstRos2SharedMem::IsValidShm(matchedShm)) ||
        (matchedShm->info.size != masterSize))
    {
        GSTROS2_DBG("Not found expected shm (fd %d) in client list", masterFd);
        return -ENODATA;
    }

    memcpy(shm, &tabData->mapList[fdIdx][pidIdx], sizeof(GstRos2Shm));

    //GSTROS2_DBG("Fd(%d->%d) already in map list: pidIdx(%d), fdIdx(%d), vaddr(%p)",
    //    masterFd, shm->info.fd, pidIdx, fdIdx, shm->data);

    return 0;
}

void GstRos2SharedTable::PrintShTab()
{
    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        return;
    }

    GSTROS2_PRINT("Dumping Shared Table... \n");

    GstRos2ShTab *tabData     = (GstRos2ShTab *) m_shTab.data;

    GSTROS2_PRINT("[Header]\n");
    GSTROS2_PRINT("id %d\n"
                  "numActivePids %d\n"
                  "numActiveFds %d\n",
                  tabData->header.id,
                  tabData->header.numActivePids,
                  tabData->header.numActiveFds);

    GSTROS2_PRINT("[Pid]\n");
    for (int32_t i = 0; i < GST_ROS2_SHM_TAB_WIDTH; i++)
    {
        if (tabData->pidList[i] > 0)
        {
            GSTROS2_PRINT("%-21d|", tabData->pidList[i]);
        }
    }
    GSTROS2_PRINT("\n");

    GSTROS2_PRINT("[Memory List]\n");
    for (int32_t i = 0; i < GST_ROS2_SHM_TAB_HEIGHT; i++)
    {
        if (tabData->mapList[i][GST_ROS2_SHM_TAB_MASTER_PID_INDEX].info.fd <= 0 ||
            tabData->mapList[i][GST_ROS2_SHM_TAB_MASTER_PID_INDEX].data == NULL)
        {
            continue;
        }

        for (int32_t j = 0; j < GST_ROS2_SHM_TAB_WIDTH; j++)
        {
            if (tabData->pidList[j] > 0)
            {
                GSTROS2_PRINT("%-4d@%-16lx|",
                    tabData->mapList[i][j].info.fd, (int64_t)tabData->mapList[i][j].data);
            }
        }
        GSTROS2_PRINT("\n");
    }
}

int32_t GstRos2SharedTable::MatchPidFromTab(int32_t pid)
{
    int32_t pidIdx  = -1;

    if (pid <= 0)
    {
        GSTROS2_ERR("invalid pid");
        return pidIdx;
    }

    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_ERR("invalid shm tab");
        return pidIdx;
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;

    int32_t *pPidList = (int32_t *)&(tabData->pidList);
    for (int32_t i = 0; i < GST_ROS2_SHM_TAB_WIDTH; i++)
    {
        if (pPidList[i] == pid)
        {
            pidIdx = i;
            break;
        }
    }
    return pidIdx;
}

int32_t GstRos2SharedTable::MatchFdFromTab(int32_t pidIdx, int32_t fd)
{
    int32_t fdIdx   = -1;

    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_ERR("invalid shm tab");
        return fdIdx;
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;

    for (int32_t i = 0; i < GST_ROS2_SHM_TAB_HEIGHT; i++)
    {
        if (fd <= 0 && tabData->mapList[i][pidIdx].info.fd <= 0)
        {
            /// return free fdIdx
            fdIdx = i;
            break;
        }

        if (tabData->mapList[i][pidIdx].info.fd == fd)
        {
            fdIdx = i;
            break;
        }
    }

    return fdIdx;
}

int32_t GstRos2SharedTable::InsertPidToTab(int32_t pid)
{
    std::lock_guard<std::mutex> lock(m_lock);

    int32_t pidIdx        = -1;
    if (pid <= 0)
    {
        GSTROS2_ERR("invalid pid %d", pid);
        return pidIdx;
    }

    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_ERR("invalid shm tab");
        return pidIdx;
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;;
    for (uint32_t i = 0; i < GST_ROS2_SHM_TAB_WIDTH; i ++)
    {
        if (tabData->pidList[i] <= 0)
        {
            tabData->pidList[i] = pid;
            pidIdx              = i;
            break;
        }
        else if (tabData->pidList[i] == pid)
        {
            GSTROS2_ERR("pid %d already in shm tab", pid);
            pidIdx = -1;
            return pidIdx;
        }
    }

    if (pidIdx < 0)
    {
        GSTROS2_ERR("exceed limit!");
        return -EDQUOT;
    }

    tabData->header.numActivePids++;

    for (int32_t i = 0; i < GST_ROS2_SHM_TAB_HEIGHT; i++)
    {
        memset((void *)&tabData->mapList[i][pidIdx], 0, sizeof(GstRos2Shm));
    }

    return pidIdx;
}

int32_t GstRos2SharedTable::RemovePidFromTab(int32_t pid)
{
    std::lock_guard<std::mutex> lock(m_lock);

    int32_t pidIdx  = MatchPidFromTab(pid);
    if (pidIdx < 0)
    {
        return -EINVAL;
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;
    tabData->pidList[pidIdx] = -1;
    tabData->header.numActivePids--;

    return 0;
}

int32_t GstRos2SharedTable::GetActiveNumPids()
{
    GstRos2ShTab *tabData = (GstRos2ShTab *) (m_shTab.data);
    if (tabData == NULL)
    {
        return -1;
    }
    return tabData->header.numActivePids;
}

int32_t GstRos2SharedTable::GetActiveNumFds()
{
    GstRos2ShTab *tabData = (GstRos2ShTab *) (m_shTab.data);
    if (tabData == NULL)
    {
        return -1;
    }
    return tabData->header.numActiveFds;
}

std::vector<int32_t> GstRos2SharedTable::GetUnmapPid(int32_t masterFd)
{
    std::vector<int32_t> pidList;

    if (!GstRos2SharedMem::IsValidShm(&m_shTab, GstRos2ShmTypeTab))
    {
        GSTROS2_ERR("invalid shm tab");
        return pidList;
    }

    int32_t tabFdIdx = MatchFdFromTab(GST_ROS2_SHM_TAB_MASTER_PID_INDEX, m_shTab.info.fd);
    if (tabFdIdx < 0)
    {
        GSTROS2_ERR("Not found tab fd %d", m_shTab.info.fd);
    }

    int32_t fdIdx = MatchFdFromTab(GST_ROS2_SHM_TAB_MASTER_PID_INDEX, masterFd);
    if (fdIdx < 0)
    {
        GSTROS2_ERR("Not found masterFd %d", masterFd);
    }

    GstRos2ShTab *tabData = (GstRos2ShTab *) m_shTab.data;
    for (int32_t i = (GST_ROS2_SHM_TAB_MASTER_PID_INDEX + 1); i < GST_ROS2_SHM_TAB_WIDTH; i++)
    {
        if (tabData->pidList[i] <= 0)
        {
            continue;
        }

        if ((masterFd != m_shTab.info.fd) &&
            (!GstRos2SharedMem::IsValidShm(&tabData->mapList[tabFdIdx][i])))
        {
            /// client tab is to be setup.
            continue;
        }

        if (!GstRos2SharedMem::IsValidShm(
                &tabData->mapList[fdIdx][i]))
        {
            pidList.push_back(tabData->pidList[i]);
        }
    }

    return pidList;
}

int32_t GstRos2SharedTable::Flush()
{
    int32_t rc = 0;

    GstRos2ShTab *tabData = (GstRos2ShTab *) (m_shTab.data);
    if (tabData == NULL)
    {
        return -EINVAL;
    }

    GSTROS2_DBG("Before Flush");

    PrintShTab();

    GstRos2Shm *shm = NULL;
    int32_t numActiveFds = GetActiveNumFds();

    for (int32_t fdIdx = 0; fdIdx < numActiveFds; fdIdx++)
    {
        if (tabData->mapList[fdIdx][GST_ROS2_SHM_TAB_MASTER_PID_INDEX].info.type == GstRos2ShmTypeTab)
        {
            continue;
        }

        int32_t pidIdx = MatchPidFromTab(m_pid);
        if (pidIdx < 0)
        {
            return -EINVAL;
        }

        shm = &tabData->mapList[fdIdx][pidIdx];

        if (GstRos2SharedMem::IsValidShm(shm, GstRos2ShmTypeBuffer))
        {
            GSTROS2_DBG("Clearing fd %d", tabData->mapList[fdIdx][pidIdx].info.fd);

            if (!m_isMaster)
            {
                /// remove shm in tab
                rc = GstRos2SharedMem::UnmapShm(shm);
                if (rc)
                {
                    GSTROS2_ERR("UnmapShm failed: pidIdx %d, fdIdx %d, fd %d, size %lu, data %p",
                        pidIdx, fdIdx, shm->info.fd, shm->info.size, shm->data);
                }
            }

            shm->info.type = GstRos2ShmTypeInvalid;
        }

        bool flushDone = true;
        for (int32_t pidIdx = 0; pidIdx < GST_ROS2_SHM_TAB_WIDTH; pidIdx++)
        {
            shm = &tabData->mapList[fdIdx][pidIdx];
            if (GstRos2SharedMem::IsValidShm(shm, GstRos2ShmTypeBuffer))
            {
                flushDone = false;
                break;
            }
        }

        if (flushDone)
        {
            /// clear master shm after flush done
            shm = &tabData->mapList[fdIdx][GST_ROS2_SHM_TAB_MASTER_PID_INDEX];
            shm->info.type = GstRos2ShmTypeInvalid;
            shm->info.fd   = -1;
            shm->info.size = 0;
            shm->data      = NULL;

            tabData->header.numActiveFds--;

            GSTROS2_DBG("Flush shm tab done for fdIdx %d, numActiveFds %d",
                fdIdx, tabData->header.numActiveFds);
        }
    }

    GSTROS2_DBG("After Flush");

    PrintShTab();

    return rc;
}
