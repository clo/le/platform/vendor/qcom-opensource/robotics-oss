/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2Mgr.h"

GstRos2Mgr::GstRos2Mgr(GstRos2MgrCreateInfo *info)
{
    m_ros2Node            = NULL;
    m_pGstRos2PipelineMgr = NULL;
    m_pGstRos2Msg         = NULL;
    for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        m_pConfig[i] = NULL;
    }
    memset(m_state, 0 , sizeof(m_state));
}

GstRos2Mgr::~GstRos2Mgr()
{
    Deinit();
}

GstRos2Mgr* GstRos2Mgr::CreateInstance(GstRos2MgrCreateInfo *info)
{
    static GstRos2Mgr* gstRos2Mgr = NULL;

    if (gstRos2Mgr || info == NULL)
    {
        return gstRos2Mgr;
    }

    gstRos2Mgr = new GstRos2Mgr(info);
    if (gstRos2Mgr == NULL)
    {
        return gstRos2Mgr;
    }

    if (gstRos2Mgr->Init(info))
    {
        gstRos2Mgr->Destroy();
        gstRos2Mgr = NULL;
    }

    return  gstRos2Mgr;
}

void GstRos2Mgr::Destroy()
{
    delete this;
}

int32_t GstRos2Mgr::Init(GstRos2MgrCreateInfo *info)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (info == NULL)
    {
        return -EINVAL;
    }

    m_ros2Node = info->ros2Node;

    {
        GstRos2MsgCreateInfo info;
        memset(&info, 0, sizeof(GstRos2MsgCreateInfo));

        info.ros2Node                 = m_ros2Node;
        m_pGstRos2Msg                 = GstRos2Msg::CreateInstance(&info);
    }

    if (m_pGstRos2Msg)
    {
        std::function<void(GstRos2Frame *frame, uint32_t topicIdx)> PublishImage =
            [&] (GstRos2Frame *frame, uint32_t topicIdx) { m_pGstRos2Msg->PublishImage(frame, topicIdx); };

        GstRos2PipelineMgrCreateInfo info;
        info.mPubilshFrameFunction = PublishImage;

        m_pGstRos2PipelineMgr = GstRos2PipelineMgr::CreateInstance(&info);
    }

    if (m_pGstRos2PipelineMgr && m_pGstRos2Msg)
    {
        m_pGstRos2Msg->SyncTimestamp(m_pGstRos2PipelineMgr->GetGstTime());
    }

    return 0;
}

void GstRos2Mgr::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);

    std::string resStr("gst-ros2node is destroying...");
    ReportResult(resStr);

    // 1. stop pipeline
    if (m_pGstRos2PipelineMgr)
    {
        m_pGstRos2PipelineMgr->DestroyAllPipelines();
        m_pGstRos2PipelineMgr->Destroy();
        m_pGstRos2PipelineMgr = NULL;
    }

    // 2. stop ros msg
    if (m_pGstRos2Msg)
    {
        m_pGstRos2Msg->Destroy();
        m_pGstRos2Msg = NULL;
    }

    for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        if (m_pConfig[i])
        {
            m_pConfig[i]->Destroy();
            m_pConfig[i] = NULL;
        }
    }
}

void GstRos2Mgr::MsgNotify(GstRos2MsgPkg msg)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (msg.data == NULL)
    {
        GSTROS2_ERR("invalid params");
        return;
    }

    std::string result;
    bool success = true;

    const char *string = (const char *)msg.data;
    GSTROS2_INFO("Received Msg: %s", string);

    std::string order(string);
    std::string key, cmd, param;

    GstRos2Config::ParseOrder(order, key, cmd, param);

    switch (GstRos2Config::ParseCmd(cmd))
    {
        case GstRos2CmdSet:
        {
            int32_t rc = 0;
            GstRos2Config *cfg = NULL;
            if(GstRos2Config::ParseCfg(&cfg, param) || cfg == NULL)
            {
                success = false;
                result = "invalid_params";

                break;
            }

            int32_t pipelineId = cfg->GetPipelineId();

            if (m_state[pipelineId] == GstRos2StateUninitialize ||
                m_state[pipelineId] == GstRos2StateConfigured   ||
                m_state[pipelineId] == GstRos2StateStop)
            {
                rc = Setup(cfg);
                if (!rc)
                {
                    m_state[pipelineId] = GstRos2StateStart;
                }
            }
            else if (m_state[pipelineId] == GstRos2StateStart)
            {
                bool needRestart = m_pGstRos2PipelineMgr->UpdateParams(cfg);
                if (needRestart)
                {
                    rc = Setup(cfg);
                }
                else
                {
                    /// add delay to avoid ack message missing
                    usleep(5 * 1000);
                }
            }

            if (rc)
            {
                success = false;
                result = "setup_failed";

                break;
            }

            // save config
            m_pConfig[pipelineId]->Destroy();
            m_pConfig[pipelineId] = cfg;

            break;
        }
        case GstRos2CmdStop:
        {
            int32_t pipelineId = GstRos2Config::GetPipelineId(param);
            if (pipelineId < 0 || pipelineId >= MAX_GST_ROS2_PIPELINES)
            {
                success = false;
                result = "invalid_params";

                break;
            }

            int32_t rc = m_pGstRos2PipelineMgr->DestroyPipeline(pipelineId);

            GstRos2MsgTopic topicImg =
            {
                GstRos2MsgDirectionPublish,
                (uint32_t) pipelineId,
                GstRos2MsgImg
            };

            rc |= m_pGstRos2Msg->CancelImgTopic(topicImg);

            if (rc)
            {
                success = false;
                result = "stop_failed";

                break;
            }

            m_state[pipelineId] = GstRos2StateStop;

            // Reset key to NULL for sending msg to all clients if stop
            key = "";

            break;
        }
        case GstRos2CmdQuery:
        default:
        {
            success = false;
            result = "unsupported_cmd";

            break;
        }
    }

    if (success)
    {
        result = "OK";
    }

    std::string res = GstRos2Config::ConstructResult(key, order, result);
    ReportResult(res);
}

void GstRos2Mgr::ReportResult(std::string &result)
{
    //GSTROS2_INFO("%s", result);
    if (m_pGstRos2Msg == NULL)
    {
        GSTROS2_ERR("invalid params");
        return;
    }
    m_pGstRos2Msg->PublishSentence(result, 0);
}

int32_t GstRos2Mgr::Setup(GstRos2Config* cfg)
{
    int32_t rc = 0;
    if (cfg == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;;
    }

    GstRos2MsgTopic topicImg =
    {
        GstRos2MsgDirectionPublish,
        cfg->GetPipelineId(),
        GstRos2MsgImg
    };

    int32_t memSave = cfg->GetCameraConfig()->memSave;
    if (memSave == GstRos2MemSaveBySharedMem)
    {
        topicImg.type = GstRos2MsgSharedImg;
    }

    snprintf((char*)topicImg.topicName, sizeof(topicImg.topicName),
        GST_ROS2_TOPIC_IMG, topicImg.idx);
    rc = m_pGstRos2Msg->AdvertiseTopic(topicImg);
    if (rc)
    {
        GSTROS2_ERR("AdvertiseTopic failed, rc = %d", rc);
        return rc;
    }

    rc = m_pGstRos2PipelineMgr->Setup(cfg);
    if (rc)
    {
        GSTROS2_ERR("Setup failed, rc = %d", rc);
        return rc;
    }

    return rc;
}

void GstRos2Mgr::Run()
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (m_pGstRos2PipelineMgr == NULL || m_pGstRos2Msg == NULL)
    {
        return;
    }

    int32_t rc = 0;

    for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        rc = GstRos2Config::PreLoadConfigure(&m_pConfig[i], i);
        if (rc || m_pConfig[i] == NULL)
        {
            continue;
        }

        GstRos2CamConfig *camCfg = m_pConfig[i]->GetCameraConfig();
        if (camCfg->preStart)
        {
            m_state[i] = GstRos2StateConfigured;
        }

        if (m_state[camCfg->pipelineId] == GstRos2StateConfigured)
        {
            rc = Setup(m_pConfig[i]);
            if (!rc)
            {
                m_state[i] = GstRos2StateStart;
            }
        }
    }

    std::function<void(GstRos2MsgPkg msg)> notifyCb = [&] (GstRos2MsgPkg msg) { this->MsgNotify(msg); };

    GstRos2MsgTopic topicL =
    {
        GstRos2MsgDirectionListen,
        0,
        GstRos2MsgStr,
        GST_ROS2_TOPIC_CFG
    };
    m_pGstRos2Msg->RegisterMsgNotify(topicL, notifyCb);

    GstRos2MsgTopic topicP =
    {
        GstRos2MsgDirectionPublish,
        0,
        GstRos2MsgStr,
        GST_ROS2_TOPIC_RES
    };

    m_pGstRos2Msg->AdvertiseTopic(topicP);

}
