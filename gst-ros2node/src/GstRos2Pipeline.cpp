/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2PipelineMgr.h"

GstRos2Pipeline::GstRos2Pipeline(GstRos2Config *cfg)
{
    m_pPipeline    = NULL;
    m_pSrc         = NULL;
    m_pFilter0     = NULL;
    m_pPostProcess = NULL;
    m_pFilter1     = NULL;
    m_pQueue       = NULL;
    m_pSink        = NULL;
    m_pSinkPad     = NULL;
    m_pConfig      = cfg;
    m_curFrameId   = 0;
    m_baseTime     = 0;
}

GstRos2Pipeline::~GstRos2Pipeline()
{

}

GstRos2Pipeline* GstRos2Pipeline::CreateInstance(GstRos2Config *cfg)
{
    GstRos2Pipeline* gstRos2Pipeline = new GstRos2Pipeline(cfg);

    if (gstRos2Pipeline == NULL)
    {
        return gstRos2Pipeline;
    }

    if (gstRos2Pipeline->Init(cfg))
    {
        gstRos2Pipeline->Destroy();
        gstRos2Pipeline = NULL;
    }

    return gstRos2Pipeline;
}

void GstRos2Pipeline::Destroy()
{
    Deinit();
    delete this;
}

int32_t GstRos2Pipeline::Init(GstRos2Config *cfg)
{
    if (cfg == NULL)
    {
        GSTROS2_ERR("Error: set cfg first!");
        return -EINVAL;
    }

    g_mutex_init(&m_mutex);
    g_cond_init (&m_cond);

    g_mutex_lock (&m_mutex);
    GstRos2CamConfig *camCfg = cfg->GetCameraConfig();
    bool isQmmfsrc    = false;
    bool needPostPro  = false;

    /// create pipeline
    char gstName[32];
    snprintf(gstName, sizeof(gstName),
        "pipeline_%u", camCfg->pipelineId);
    m_pPipeline = gst_pipeline_new (gstName);

    snprintf(gstName, sizeof(gstName),
        "%s_%u", camCfg->cameraSource.c_str(), camCfg->pipelineId);
    m_pSrc      = gst_element_factory_make (camCfg->cameraSource.c_str(), gstName);

    snprintf(gstName, sizeof(gstName),
        "filter0_%u", camCfg->pipelineId);
    m_pFilter0   = gst_element_factory_make ("capsfilter", gstName);

    #ifdef USE_APPSINK
    snprintf(gstName, sizeof(gstName),
        "queue_%u", camCfg->pipelineId);
    m_pQueue    = gst_element_factory_make ("queue", gstName);

    snprintf(gstName, sizeof(gstName),
        "appsink_%u", camCfg->pipelineId);
    m_pSink     = gst_element_factory_make ("appsink", gstName);
    #else
    snprintf(gstName, sizeof(gstName),
        "ros2sink_%u", camCfg->pipelineId);
    m_pSink     = gst_element_factory_make ("ros2sink", gstName);
    #endif

    if (m_pPipeline == NULL ||
        m_pSrc == NULL ||
        m_pFilter0 == NULL ||
        m_pSink == NULL)
    {
        GSTROS2_ERR("Invalid params: m_pPipeline %p, m_pSrc %p, m_pFilter0 %p, m_pSink %p",
            m_pPipeline, m_pSrc, m_pFilter0, m_pSink);
        g_mutex_unlock (&m_mutex);
        return -EINVAL;
    }

    /// Set Args for source
    if (camCfg->cameraSource == QMMFSRC)
    {
        isQmmfsrc = true;
        snprintf(gstName, sizeof(gstName),
            "qmmfsrc%u", camCfg->pipelineId);

        g_object_set (G_OBJECT (m_pSrc),
                      "name", gstName,
                      "camera", camCfg->cameraId,
                      NULL
                      );
        GSTROS2_DBG(QMMFSRC":%s", gstName);
    }
    else if (camCfg->cameraSource == V4L2SRC)
    {
        char videoNode[16];
        snprintf(videoNode, sizeof(videoNode),
            "/dev/video%d", camCfg->cameraId);

        g_object_set (G_OBJECT (m_pSrc),
                "device", videoNode,
                NULL
                );

        GSTROS2_DBG(V4L2SRC":%s", videoNode);
    }

    /// Set Args for capsfilter
    char capString[256];
    if (isQmmfsrc && camCfg->format.find("RAW") != camCfg->format.npos)
    {
        /// RAW format
        int32_t bpp = 0;
        char format[16];

        sscanf(camCfg->format.c_str(), "RAW%d/%s", &bpp, format);

        if (bpp <= 0)
        {
            GSTROS2_ERR("Invalid format");
            g_mutex_unlock (&m_mutex);
            return -EINVAL;
        }

        if (strcmp(format, "bggr") &&
            strcmp(format, "rggb") &&
            strcmp(format, "gbrg") &&
            strcmp(format, "grbg") &&
            strcmp(format, "mono"))
        {
            GSTROS2_ERR("Invalid format for filter");
            g_mutex_unlock (&m_mutex);
            return -EINVAL;
        }

        snprintf(capString, sizeof(capString),
            "video/x-bayer,"
            "width=%d,"
            "height=%d,"
            "framerate=%d/1,"
            "format=%s,"
            "bpp=(string)%d",
            camCfg->width,
            camCfg->height,
            camCfg->fps,
            format,
            bpp);
    }
    else
    {
        /// Non RAW format
        const char *format;
        if (camCfg->format == "mono8" ||
            camCfg->format == "rgb8")
        {
            if (isQmmfsrc)
            {
                format = "NV12";
            }
            else
            {
                format = "YUY2";
            }
        }
        else
        {
            format = camCfg->format.c_str();
        }

        snprintf(capString, sizeof(capString),
            "video/x-raw,"
            "width=%d,"
            "height=%d,"
            "framerate=%d/1,"
            "format=%s",
            camCfg->width,
            camCfg->height,
            camCfg->fps,
            format);
    }

    GSTROS2_DBG("capString0: %s", capString);
    gst_util_set_object_arg (G_OBJECT (m_pFilter0), "caps", capString);

    if (isQmmfsrc && camCfg->format == "rgb8")
    {
        needPostPro = true;

        snprintf(gstName, sizeof(gstName),
            "postProcess_%u", camCfg->pipelineId);
        m_pPostProcess   = gst_element_factory_make ("qtivtransform", gstName);

        snprintf(gstName, sizeof(gstName),
            "filter1_%u", camCfg->pipelineId);
        m_pFilter1   = gst_element_factory_make ("capsfilter", gstName);
        gst_util_set_object_arg (G_OBJECT (m_pFilter1), "caps", "video/x-raw,format=RGB");
    }

    #ifdef USE_APPSINK
    snprintf(gstName, sizeof(gstName),
        "appsink_%u", camCfg->pipelineId);
    g_object_set (G_OBJECT (m_pSink),
                  "name", gstName,
                  "emit-signals", true,
                  NULL
                  );
    #endif

    /// link
    if (needPostPro == true &&
        m_pPostProcess != NULL &&
        m_pFilter1 != NULL)
    {
        #ifdef USE_APPSINK
        gst_bin_add_many (GST_BIN (m_pPipeline), m_pSrc, m_pFilter0, m_pPostProcess, m_pFilter1, m_pQueue, m_pSink, NULL);
        gst_element_link_many (m_pSrc, m_pFilter0, m_pQueue, m_pPostProcess, m_pFilter1, m_pSink, NULL);
        #else
        gst_bin_add_many (GST_BIN (m_pPipeline), m_pSrc, m_pFilter0, m_pPostProcess, m_pFilter1, m_pSink, NULL);
        gst_element_link_many (m_pSrc, m_pFilter0, m_pPostProcess, m_pFilter1, m_pSink, NULL);
        #endif
    }
    else
    {
        #ifdef USE_APPSINK
        gst_bin_add_many (GST_BIN (m_pPipeline), m_pSrc, m_pFilter0, m_pQueue, m_pSink, NULL);
        gst_element_link_many (m_pSrc, m_pFilter0, m_pQueue, m_pSink, NULL);
        #else
        gst_bin_add_many (GST_BIN (m_pPipeline), m_pSrc, m_pFilter0, m_pSink, NULL);
        gst_element_link_many (m_pSrc, m_pFilter0, m_pSink, NULL);
        #endif
    }

    /// debug level
    SetLogLevel(camCfg);

    /// set bus cb
    GstBus *bus = GST_ELEMENT_BUS (m_pPipeline);
    gst_bus_add_watch(bus, BusCb, this);
    gst_object_unref (bus);

    /// set data cb
    #ifdef  USE_APPSINK
    g_signal_connect (m_pSink, "new-sample", G_CALLBACK (SampleCb), this);
    #else
    char padName[16];
    snprintf(padName, sizeof(padName), "video_%d", 0); // hardcode
    m_pSinkPad = gst_element_get_static_pad(m_pSink, padName);
    if (m_pSinkPad == NULL)
    {
        GSTROS2_ERR ("Failed to get pad");
        g_mutex_unlock (&m_mutex);
        return -EPERM;
    }

    GSTROS2_DBG("Got pad: %s", padName);
    gst_pad_add_probe(m_pSinkPad, GST_PAD_PROBE_TYPE_BUFFER, DataCb, this, NULL);
    #endif

    if (Start())
    {
        GSTROS2_ERR ("Start pipeline failed");
        return -EPERM;
    }

    m_baseTime = gst_element_get_base_time(m_pSrc);
    GSTROS2_DBG("Gst base time: %lu", m_baseTime);

    GSTROS2_INFO("Pipiline started: %d", camCfg->pipelineId);

    g_mutex_unlock (&m_mutex);
    return 0;
}

void GstRos2Pipeline::Deinit()
{
    /// stop pipeline
    g_mutex_lock (&m_mutex);

    Stop();

    g_mutex_unlock (&m_mutex);

    g_mutex_clear (&m_mutex);
    g_cond_clear (&m_cond);

    if (m_pPipeline && (GET_REFCNT(m_pPipeline) > 0))
    {
        gst_object_unref (m_pPipeline);
        m_pPipeline = NULL;
    }

    if (m_pSrc && (GET_REFCNT(m_pSrc) > 0))
    {
        gst_object_unref (m_pSrc);
        m_pSrc = NULL;
    }

    if (m_pFilter0 && (GET_REFCNT(m_pFilter0) > 0))
    {
        gst_object_unref (m_pFilter0);
        m_pFilter0 = NULL;
    }

    if (m_pPostProcess && (GET_REFCNT(m_pPostProcess) > 0))
    {
        gst_object_unref (m_pPostProcess);
        m_pPostProcess = NULL;
    }

    if (m_pFilter1 && (GET_REFCNT(m_pFilter1) > 0))
    {
        gst_object_unref (m_pFilter1);
        m_pFilter1 = NULL;
    }

    if (m_pQueue && (GET_REFCNT(m_pQueue) > 0))
    {
        gst_object_unref (m_pQueue);
        m_pQueue = NULL;
    }

    if (m_pSink && (GET_REFCNT(m_pSink) > 0))
    {
        gst_object_unref (m_pSink);
        m_pSink = NULL;
    }

    if (m_pSinkPad && (GET_REFCNT(m_pSinkPad) > 0))
    {
        gst_object_unref (m_pSinkPad);
        m_pSinkPad = NULL;
    }

    GSTROS2_INFO("Pipiline: %d stopped", m_pConfig->GetPipelineId());
}

void GstRos2Pipeline::SetLogLevel(GstRos2CamConfig *camCfg)
{
    if (camCfg && camCfg->logLevel <= GST_LEVEL_TRACE)
    {
        gst_debug_set_default_threshold (GST_LEVEL_FIXME); //3 by default
        gst_debug_set_threshold_for_name(camCfg->cameraSource.c_str(), (GstDebugLevel)camCfg->logLevel);
        gst_debug_set_colored (FALSE);
    }
}

int32_t GstRos2Pipeline::Start()
{
    /// start pipeline
    gst_element_set_state(m_pPipeline, GST_STATE_PLAYING);
    /* wait until it's up and running or failed */
    if (gst_element_get_state(m_pPipeline, NULL, NULL, -1) == GST_STATE_CHANGE_FAILURE) {
        GSTROS2_ERR ("Failed to Go into PLAYING state");
        g_mutex_unlock (&m_mutex);
        return -EPERM;
    }

    return 0;
}

void GstRos2Pipeline::Stop()
{
    int32_t rc = 0;
    uint32_t pipelineId = m_pConfig->GetPipelineId();
    GSTROS2_INFO("Pipiline: %d stopping", pipelineId);

    gst_element_send_event (m_pPipeline, gst_event_new_eos ());

    gst_element_set_state (m_pPipeline, GST_STATE_PAUSED);
    rc = gst_element_get_state (m_pPipeline, NULL, NULL, -1);
    if (rc == GST_STATE_CHANGE_FAILURE)
    {
        GSTROS2_ERR("Pipiline: %d set GST_STATE_PAUSED failed", pipelineId);
    }
    else
    {
        GSTROS2_DBG("Pipiline: %d set GST_STATE_PAUSED %d", pipelineId, rc);
    }

    /* iterate mainloop to process pending stuff */
    while (g_main_context_iteration (NULL, FALSE));

    gst_element_set_state (m_pPipeline, GST_STATE_READY);
    rc = gst_element_get_state (m_pPipeline, NULL, NULL, -1);
    if (rc == GST_STATE_CHANGE_FAILURE)
    {
        GSTROS2_ERR("Pipiline: %d set GST_STATE_READY failed", pipelineId);
    }
    else
    {
        GSTROS2_DBG("Pipiline: %d set GST_STATE_READY %d", pipelineId, rc);
    }

    gst_element_set_state (m_pPipeline, GST_STATE_NULL);
    rc = gst_element_get_state (m_pPipeline, NULL, NULL, -1);
    if (rc == GST_STATE_CHANGE_FAILURE)
    {
        GSTROS2_ERR("Pipiline: %d set GST_STATE_NULL failed", pipelineId);
    }
    else
    {
        GSTROS2_DBG("Pipiline: %d set GST_STATE_NULL %d", pipelineId, rc);
    }
}

bool GstRos2Pipeline::UpdateParams(GstRos2Config* cfg)
{
    int32_t rc = 0;
    bool needRestart = false;

    if (cfg == NULL || m_pConfig == NULL)
    {
        GSTROS2_ERR("Error: set cfg first!");
        return needRestart;
    }

    g_mutex_lock (&m_mutex);
    GstRos2CamConfig *camCfgNew = cfg->GetCameraConfig();
    GstRos2CamConfig *camCfgOld = m_pConfig->GetCameraConfig();

    if (camCfgNew->cameraSource != camCfgOld->cameraSource)
    {
        GSTROS2_INFO("cameraSource changed %s for pipeline %u",
            camCfgNew->cameraSource.c_str(), camCfgNew->pipelineId);
        needRestart = true;
    }

    if (camCfgNew->format != camCfgOld->format)
    {
        GSTROS2_INFO("format changed %s for pipeline %u",
            camCfgNew->format.c_str(), camCfgNew->pipelineId);
        needRestart = true;
    }

    if (camCfgNew->cameraId != camCfgOld->cameraId)
    {
        GSTROS2_INFO("cameraId changed %d for pipeline %u", camCfgNew->cameraId, camCfgNew->pipelineId);
        needRestart = true;
    }

    if (camCfgNew->width != camCfgOld->width       ||
        camCfgNew->height != camCfgOld->height     ||
        camCfgNew->stride != camCfgOld->stride     ||
        camCfgNew->slice != camCfgOld->slice)
    {
        GSTROS2_INFO("size changed for pipeline %u", camCfgNew->pipelineId);
        needRestart = true;
    }

    if (camCfgNew->fps != camCfgOld->fps)
    {
        /// now fps is not supported for dynamic change in gst plugin.
        GSTROS2_INFO("fps changed %d for pipeline %u", camCfgNew->fps, camCfgNew->pipelineId);
        needRestart = true;
    }

    if (camCfgNew->memSave != camCfgOld->memSave)
    {
        GSTROS2_INFO("memSave changed %d for pipeline %u", camCfgNew->memSave, camCfgNew->pipelineId);
        needRestart = true;
    }

    if (camCfgNew->logLevel != camCfgOld->logLevel)
    {
        /// debug level
        SetLogLevel(camCfgNew);
    }

    m_pConfig = cfg;

    GSTROS2_INFO("needRestart %d for pipeline %u", needRestart, camCfgNew->pipelineId);
    g_mutex_unlock (&m_mutex);

    return needRestart ;
}

void GstRos2Pipeline::RegisterPublish(GstRos2MsgPublishFrameFunc publish)
{
    m_pPublish = publish;
}

int64_t GstRos2Pipeline::GetGstTime()
{
    GstClock        *clock = gst_system_clock_obtain();
    GstClockTime  gstTime  = gst_clock_get_time(clock);
    gst_object_unref(clock);
    return (int64_t)gstTime;
}

int32_t GstRos2Pipeline::ProcessData(GstBuffer *buffer)
{
    if (buffer == NULL)
    {
        return -EINVAL;
    }

    GstMapInfo        mapInfo;
    memset(&mapInfo, 0, sizeof(mapInfo));

    GstRos2Frame      frame;
    memset(&frame, 0, sizeof(GstRos2Frame));

    GstRos2FrameInfo* frameInfo = &frame.info;
    GstRos2CamConfig *camCfg = m_pConfig->GetCameraConfig();

    GstMemory *memory = gst_buffer_get_memory(buffer, 0);
    if (memory == NULL)
    {
        GSTROS2_ERR("Failed to get memory for pipeline %d", camCfg->pipelineId);
        g_mutex_unlock (&m_mutex);
        return -EINVAL;
    }

    if (camCfg->memSave == GstRos2MemSaveDisable)
    {
        if (!gst_memory_map(memory, &mapInfo, GST_MAP_READ))
        {
            GSTROS2_ERR("Failed to map buffers for pipeline %d", camCfg->pipelineId);
            gst_memory_unref(memory);
            return -EINVAL;
        }
        frame.data               = mapInfo.data;
    }
    else
    {
        frame.data               = (uint8_t *)0xFD;
    }

    frameInfo->cameraSource  = camCfg->cameraSource;
    frameInfo->pipelineId    = camCfg->pipelineId;
    frameInfo->cameraId      = camCfg->cameraId;
    frameInfo->frameId       = m_curFrameId;
    frameInfo->width         = camCfg->width;
    frameInfo->height        = camCfg->height;
    frameInfo->stride        = camCfg->stride;
    frameInfo->slice         = camCfg->slice;
    frameInfo->format        = camCfg->format;

    if (GetFormatType(frameInfo->format) == GstRos2Fmt_MONO)
    {
        frameInfo->size      = frameInfo->stride * frameInfo->slice;
    }
    else
    {
        frameInfo->size      = gst_memory_get_sizes(memory, NULL, NULL);
    }

    // calculate shutter ts in GST time
    frameInfo->timestamp     = (int64_t)(GST_BUFFER_PTS(buffer) + m_baseTime);
    frameInfo->latencyType   = camCfg->latencyType;
    frameInfo->latency       = 0;

    frameInfo->memSave       = camCfg->memSave;
    frameInfo->fd = -1;
    if (gst_is_fd_memory(memory))
    {
        frameInfo->fd = gst_fd_memory_get_fd(memory);
    }

#if 0
    frameInfo->latency       = GetGstTime() - frameInfo->timestamp;
    GSTROS2_DBG("pipelineId %d, frameId %d, fd %d, w x h[%d x %d], format %s, size %lu, ts %ld(ns), latency %ld(us)",
        frameInfo->pipelineId,
        frameInfo->frameId,
        frameInfo->fd,
        frameInfo->width,
        frameInfo->height,
        frameInfo->format.c_str(),
        frameInfo->size,
        frameInfo->timestamp,
        (frameInfo->latency / 1000));
#endif

   // DumpFrameToFile(&frame, "Node");

    if (m_pPublish &&
        camCfg->publishFreq > 0 &&
        m_curFrameId % camCfg->publishFreq == 0)
    {
        m_pPublish(&frame, frameInfo->pipelineId);
    }

    if (camCfg->memSave == GstRos2MemSaveDisable)
    {
        gst_memory_unmap(memory, &mapInfo);
    }

    if (GET_REFCNT(memory) > 0)
    {
        gst_memory_unref(memory);
    }

    m_curFrameId++;

    return 0;
}

GstPadProbeReturn GstRos2Pipeline::DataCb(GstPad *pad, GstPadProbeInfo *probeInfo, gpointer userData)
{
    GstPadProbeReturn rc = GST_PAD_PROBE_OK;
    GstRos2Pipeline *pipeline = (GstRos2Pipeline *) userData;
    if (pipeline == NULL)
    {
        return GST_PAD_PROBE_DROP;
    }

    g_mutex_lock (&pipeline->m_mutex);
    //TODO: check pipeline status here

    GstBuffer *buffer = (GstBuffer*) probeInfo->data;
    if (pipeline->ProcessData(buffer))
    {
        rc = GST_PAD_PROBE_DROP;
    }

    g_mutex_unlock (&pipeline->m_mutex);
    return rc;
}

GstFlowReturn GstRos2Pipeline::SampleCb (GstElement *sink, gpointer userData)
{
    GstSample *sample = NULL;
    GstFlowReturn rc = GST_FLOW_OK;
    GstRos2Pipeline *pipeline = (GstRos2Pipeline *) userData;

    if (sink == NULL || pipeline == NULL)
    {
        return GST_FLOW_ERROR;
    }

    g_mutex_lock (&pipeline->m_mutex);
    g_signal_emit_by_name (sink, "pull-sample", &sample);
    if (sample == NULL) {
        GSTROS2_ERR ("Pulled sample is NULL!");
        g_mutex_unlock (&pipeline->m_mutex);
        return GST_FLOW_ERROR;
    }

    GstBuffer *buffer = gst_sample_get_buffer (sample);
    if (pipeline->ProcessData(buffer))
    {
        rc = GST_FLOW_ERROR;
    }

    gst_sample_unref (sample);
    g_mutex_unlock (&pipeline->m_mutex);
    return rc;
}

gboolean GstRos2Pipeline::BusCb (GstBus *bus, GstMessage *msg, gpointer userData)
{
    GstRos2Pipeline *pipeline = (GstRos2Pipeline *) userData;
    if (pipeline == NULL)
    {
        return TRUE;
    }

    switch (GST_MESSAGE_TYPE (msg)) {
        case GST_MESSAGE_ERROR:{
            GError *err = NULL;
            gchar *dbg;
            GSTROS2_ERR ("GST_MESSAGE_ERROR !");
            gst_message_parse_error (msg, &err, &dbg);
            gst_object_default_error (msg->src, err, dbg);
            g_clear_error (&err);
            g_free (dbg);
            //g_main_loop_quit (loop);
            break;
        }
        default:
            break;
    }
    return TRUE;
}
