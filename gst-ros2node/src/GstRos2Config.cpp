/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2Config.h"

GstRos2Config::GstRos2Config(GstRos2CamConfig *camCfg)
{

}

GstRos2Config::~GstRos2Config()
{
    Deinit();
}

GstRos2Config* GstRos2Config::CreateInstance(GstRos2CamConfig *camCfg)
{
    GstRos2Config* cfg = NULL;
    if (cfg == NULL)
    {
        cfg = new GstRos2Config(camCfg);
    }

    if (cfg == NULL)
    {
        return cfg;
    }

    if (cfg->Init(camCfg))
    {
        cfg->Destroy();
        cfg = NULL;
    }

    return cfg;
}

void GstRos2Config::Destroy()
{
    delete this;
}

int32_t GstRos2Config::Init(GstRos2CamConfig *camCfg)
{
    if (camCfg == NULL)
    {
        return -EINVAL;
    }

    m_camCfg.pipelineId    = camCfg->pipelineId;
    m_camCfg.preStart      = camCfg->preStart;
    m_camCfg.cameraSource  = camCfg->cameraSource;
    m_camCfg.cameraId      = camCfg->cameraId;
    m_camCfg.width         = camCfg->width;
    m_camCfg.height        = camCfg->height;
    m_camCfg.stride        = camCfg->stride;
    m_camCfg.slice         = camCfg->slice;
    m_camCfg.format        = camCfg->format;
    m_camCfg.memSave       = camCfg->memSave;
    m_camCfg.fps           = camCfg->fps;
    m_camCfg.publishFreq   = camCfg->publishFreq;
    m_camCfg.latencyType   = camCfg->latencyType;
    m_camCfg.logLevel      = camCfg->logLevel;

    return 0;
}

void GstRos2Config::Deinit()
{

}

char *const GstRos2Config::cmdToken[GstRos2CmdMax] = {
    [GstRos2CmdQuery]                  = (char *const)"query",
    [GstRos2CmdSet]                    = (char *const)"set",
    [GstRos2CmdStop]                   = (char *const)"stop",
    [GstRos2CmdDump]                   = (char *const)"dump",
    [GstRos2CmdExit]                   = (char *const)"exit",
    [GstRos2CmdNop]                    = (char *const)"",
};

char *const GstRos2Config::paramToken[GSTROS2PARAM_MAX] =
{
    [PIPELINE_ID]              = (char *const)"pipelineId",
    [PRE_START]                = (char *const)"preStart",
    [CAMEREA_SOURCE]           = (char *const)"cameraSource",
    [CAMERA_ID]                = (char *const)"cameraId",
    [WIDHT]                    = (char *const)"width",
    [HEIGHT]                   = (char *const)"height",
    [FORMAT]                   = (char *const)"format",
    [FPS]                      = (char *const)"fps",
    [MEM_SAVE]                 = (char *const)"memSave",
    [PUBLISH_FREQ]             = (char *const)"publishFreq",
    [LATENCY_TYPE]             = (char *const)"latencyType",
    [LOG_LEVEL]                = (char *const)"logLevel",
    [DUMP_FREQ]                = (char *const)"dumpFreq",
    [DUMP_NUM]                 = (char *const)"dumpNum",
    [RESULT]                   = (char *const)"result",
};

void GstRos2Config::PrintConfig()
{
    GSTROS2_INFO( "\n"
                  "pipelineId:   %u \n"
                  "preStart:     %d \n"
                  "cameraSource: %s \n"
                  "cameraId:     %d \n"
                  "width:        %u \n"
                  "height:       %u \n"
                  "stride:       %u \n"
                  "slice:        %u \n"
                  "format:       %s \n"
                  "fps:          %d \n"
                  "memSave:      %d \n"
                  "publishFreq:  %d \n"
                  "latencyType:  %d \n"
                  "logLevel:     %d \n",
        m_camCfg.pipelineId,
        m_camCfg.preStart,
        m_camCfg.cameraSource.c_str(),
        m_camCfg.cameraId,
        m_camCfg.width,
        m_camCfg.height,
        m_camCfg.stride,
        m_camCfg.slice,
        m_camCfg.format.c_str(),
        m_camCfg.fps,
        m_camCfg.memSave,
        m_camCfg.publishFreq,
        m_camCfg.latencyType,
        m_camCfg.logLevel);
}

int32_t GstRos2Config::PreLoadConfigure(GstRos2Config** cfg, uint32_t index)
{
    int32_t  rc              = 0;
    const char*  temp    = NULL;
    bool isQmmfSrc       = false;
    GstRos2CamConfig camCfg;

    if (cfg == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    char fileName[256];
    snprintf(fileName, sizeof(fileName),
        GSTROS2_CONFIG_DIRECTORY"config_%d.xml", index);

    tinyxml2::XMLDocument doc;

    rc = doc.LoadFile(fileName);
    if(rc)
    {
        return -ENOENT;
    }

    GSTROS2_INFO("Found pre-configiure for pipeline %d: %s", index, fileName);

    tinyxml2::XMLElement *info = doc.RootElement();
    if (info == NULL)
    {
        GSTROS2_ERR("invaild paramters");
        return -EINVAL;
    }

    temp = info->FirstChildElement(paramToken[PIPELINE_ID])->GetText();
    if (temp)
    {
        camCfg.pipelineId = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[PRE_START])->GetText();
    if (temp)
    {
        camCfg.preStart = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[CAMEREA_SOURCE])->GetText();
    if (temp)
    {
        camCfg.cameraSource = temp;
        if (camCfg.cameraSource == QMMFSRC)
        {
              isQmmfSrc = true;
        }
    }

    temp = info->FirstChildElement(paramToken[CAMERA_ID])->GetText();
    if (temp)
    {
        camCfg.cameraId = atoi(temp);
        if (camCfg.cameraId < 0 ||
            camCfg.cameraId >= MAX_CAMERA_NUM)
        {
            GSTROS2_ERR("invaild cameraId %d", camCfg.cameraId);
            return -EINVAL;
        }
    }

    temp = info->FirstChildElement(paramToken[WIDHT])->GetText();
    if (temp)
    {
        camCfg.width = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[HEIGHT])->GetText();
    if (temp)
    {
        camCfg.height = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[FORMAT])->GetText();
    if (temp)
    {
        camCfg.format = temp;
    }

    temp = info->FirstChildElement(paramToken[FPS])->GetText();
    if (temp)
    {
        camCfg.fps = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[MEM_SAVE])->GetText();
    if (temp)
    {
        camCfg.memSave = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[PUBLISH_FREQ])->GetText();
    if (temp)
    {
        camCfg.publishFreq = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[LATENCY_TYPE])->GetText();
    if (temp)
    {
        camCfg.latencyType = atoi(temp);
    }

    temp = info->FirstChildElement(paramToken[LOG_LEVEL])->GetText();
    if (temp)
    {
        camCfg.logLevel = atoi(temp);
    }
    else
    {
        camCfg.logLevel = 3;
    }

    GetAlignment(isQmmfSrc, camCfg.format,
                 camCfg.width,    camCfg.height,
                 &camCfg.stride, &camCfg.slice);

    if (*cfg == NULL)
    {
        *cfg = GstRos2Config::CreateInstance(&camCfg);
    }

    if (*cfg)
    {
        (*cfg)->PrintConfig();
    }

    return 0;
}

std::string GstRos2Config::ConstructResult(std::string &key, std::string &order, std::string &result)
{
    std::string resStr;

    resStr = order + "," + paramToken[RESULT] + "=" + result;

    if (key != "")
    {
        resStr = "[" + key + "]" + resStr;
    }

    return resStr;
}

void GstRos2Config::ParseKey(std::string &orderIn, std::string &key, std::string &orderOut)
{
    /// Parse Key
    std::string::size_type pos_s = orderIn.find('[');
    std::string::size_type pos_e = orderIn.find(']');

    if (pos_s != orderIn.npos && pos_e != orderIn.npos)
    {
        key      = orderIn.substr(pos_s + 1, pos_e - 1);
        orderOut = orderIn.substr(pos_e + 1, orderIn.size());
    }
    else
    {
        orderOut = orderIn;
    }
}

void GstRos2Config::ParseOrder(std::string &order, std::string &key, std::string &cmd, std::string &param)
{
    ParseKey(order, key, order);

    /// Parse Cmds
    std::string::size_type pos = order.find(':');
    cmd = order.substr(0, pos);
    param = order.substr(pos+1, order.size());
}

GstRos2Cmd GstRos2Config::ParseCmd(std::string &cmd)
{
    char *cmdStr = (char *)cmd.c_str();
    if (cmdStr == NULL)
    {
        GSTROS2_ERR("invalid params");
        return GstRos2CmdNop;
    }

    char* value = NULL;

    GSTROS2_DBG("Cmd: %s", cmdStr);
    char cmdId = getsubopt(&cmdStr, cmdToken, &value);
    switch(cmdId)
    {
        case GstRos2CmdQuery:
        case GstRos2CmdSet:
        case GstRos2CmdStop:
        case GstRos2CmdDump:
        case GstRos2CmdExit:
        {
            //GSTROS2_DBG("cmdId: %d", cmdId);
            break;
        }
        default:
        {
            cmdId = GstRos2CmdNop;
            break;
        }
    }
    return (GstRos2Cmd)cmdId;
}

GstRos2Result GstRos2Config::ParseResult(std::string &cmd, std::string &param)
{
    GstRos2Result res;
    memset(&res, 0, sizeof(res));

    res.cmd = ParseCmd(cmd);

    char *resStr = (char *)param.c_str();
    if (resStr == NULL)
    {
        GSTROS2_ERR("invalid params");
        return res;
    }

    char* value = NULL;
    while (*resStr != '\0')
    {
        switch (getsubopt(&resStr, paramToken, &value))
        {
            case PIPELINE_ID:
            {
                res.pipelineId = atoi(value);

                if (res.pipelineId >= MAX_GST_ROS2_PIPELINES)
                {
                    GSTROS2_ERR("invalid PIPELINE_ID");
                    return res;
                }

                break;
            }
            case RESULT:
            {
                if (!strcmp(value, "OK"))
                {
                    res.success = true;
                }
                else
                {
                    res.success = false;
                }
                break;
            }
            default:
            {
                break;
            }
        }
    }

    return res;
}

int32_t GstRos2Config::ParseCfg(GstRos2Config **cfg, std::string &param)
{
    int32_t rc         = 0;
    bool isQmmfSrc = false;
    GstRos2CamConfig camCfg;

    char *cfgStr = (char *)param.c_str();
    if (cfgStr == NULL || cfg == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    // init config
    {
        camCfg.pipelineId    =  0;
        camCfg.cameraSource  =  "v4l2src";
        camCfg.cameraId      =  0;
        camCfg.width         =  1920;
        camCfg.height        =  1080;
        camCfg.fps           =  5;
        camCfg.memSave       =  0;
        camCfg.publishFreq   =  1;
        camCfg.latencyType   =  0;
        camCfg.logLevel      =  3;
    }

    GSTROS2_INFO("ParseCfg: %s", cfgStr);
    char* value = NULL;
    while (*cfgStr != '\0')
    {
        switch (getsubopt(&cfgStr, paramToken, &value))
        {
            case PIPELINE_ID:
            {
                camCfg.pipelineId = atoi(value);
                if (camCfg.pipelineId >= MAX_GST_ROS2_PIPELINES)
                {
                    GSTROS2_ERR("invalid PIPELINE_ID");
                    return -EINVAL;
                }

                GSTROS2_DBG("pipelineId: %d", camCfg.pipelineId);
                break;
            }
            case CAMEREA_SOURCE:
            {
                camCfg.cameraSource = (const char *)(value);
                if (camCfg.cameraSource == QMMFSRC)
                {
                      isQmmfSrc = true;
                }

                GSTROS2_DBG("cameraSource: %s", camCfg.cameraSource.c_str());
                break;
            }
            case CAMERA_ID:
            {
                camCfg.cameraId = atoi(value);
                if (camCfg.cameraId < 0 ||
                    camCfg.cameraId >= MAX_CAMERA_NUM)
                {
                    GSTROS2_ERR("invalid CAMERA_ID");
                    return -EINVAL;
                }

                GSTROS2_DBG("cameraId: %d", camCfg.cameraId);
                break;
            }
            case WIDHT:
            {
                camCfg.width = atoi(value);

                GSTROS2_DBG("width: %d", camCfg.width);
                break;
            }
            case HEIGHT:
            {
                camCfg.height = atoi(value);

                GSTROS2_DBG("height: %d", camCfg.height);
                break;
            }
            case FORMAT:
            {
                camCfg.format = (const char *)(value);
                GSTROS2_DBG("format: %s", camCfg.format.c_str());
                break;
            }
            case FPS:
            {
                camCfg.fps = atoi(value);
                GSTROS2_DBG("fps: %d", camCfg.fps);
                break;
            }
            case MEM_SAVE:
            {
                camCfg.memSave = atoi(value);
                GSTROS2_DBG("memSave: %d", camCfg.memSave);
                break;
            }
            case PUBLISH_FREQ:
            {
                camCfg.publishFreq = atoi(value);
                GSTROS2_DBG("publishFreq: %d", camCfg.publishFreq);
                break;
            }
            case LATENCY_TYPE:
            {
                camCfg.latencyType = atoi(value);
                GSTROS2_DBG("latencyType: %d", camCfg.latencyType);
                break;
            }
            case LOG_LEVEL:
            {
                camCfg.logLevel = atoi(value);
                GSTROS2_DBG("logLevel: %d", camCfg.logLevel);
                break;
            }
            case PRE_START:
            case DUMP_FREQ:
            case DUMP_NUM:
            case RESULT:
            {
                break;
            }
            default:
            {
                return -EINVAL;
            }
        }
    }

    GetAlignment(isQmmfSrc, camCfg.format,
                 camCfg.width,    camCfg.height,
                 &camCfg.stride, &camCfg.slice);

    if (*cfg == NULL)
    {
        *cfg = GstRos2Config::CreateInstance(&camCfg);
    }

    if (*cfg)
    {
        (*cfg)->PrintConfig();
    }

    return rc;
}

int32_t GstRos2Config::GetIntParamFromStr(std::string &cmd, char *const paramStr)
{
    int32_t param = -1;
    char* value   = NULL;
    std::string::size_type pos_s = cmd.find(paramStr);
    if (pos_s != cmd.npos)
    {
        char *const token[] =
        {
            [0] = paramStr,
        };

        std::string tempStr = cmd.substr(pos_s, cmd.npos);
        char *substr = (char *) tempStr.c_str();

        if (getsubopt(&substr, token, &value) == 0)
        {
            param = atoi(value);
        }

        GSTROS2_DBG("%s: %d", paramStr, param);
    }
    return param;
}

int32_t GstRos2Config::GetPipelineId(std::string cmd)
{
    return GstRos2Config::GetIntParamFromStr(cmd, paramToken[PIPELINE_ID]);
}

int32_t GstRos2Config::GetMemSaveMode(std::string cmd)
{
    return GstRos2Config::GetIntParamFromStr(cmd, paramToken[MEM_SAVE]);
}

int32_t GstRos2Config::GetDumpFreq(std::string cmd)
{
    return GstRos2Config::GetIntParamFromStr(cmd, paramToken[DUMP_FREQ]);
}

int32_t GstRos2Config::GetDumpNum(std::string cmd)
{
    return GstRos2Config::GetIntParamFromStr(cmd, paramToken[DUMP_NUM]);
}
