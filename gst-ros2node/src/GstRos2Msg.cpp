/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2Msg.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GstRos2MsgPublisher::GstRos2MsgPublisher(GstRos2MsgCreateInfo *info)
{
    m_pImageTransport      = NULL;
    m_pImgPublisher        = NULL;
    m_pStrPublisher        = NULL;
    m_pRos2Node            = NULL;
    m_timeNsOffset         = 0;
    m_pShm                 = NULL;
    m_frameId              = 0;

    //memset(&m_topic, 0, sizeof(m_topic));
}

GstRos2MsgPublisher::~GstRos2MsgPublisher()
{
    Deinit();
}

GstRos2MsgPublisher* GstRos2MsgPublisher::CreateInstance(GstRos2MsgCreateInfo *info)
{
    GstRos2MsgPublisher* gstRos2MsgPublisher = new GstRos2MsgPublisher(info);

    if (gstRos2MsgPublisher == NULL)
    {
        return gstRos2MsgPublisher;
    }

    if (gstRos2MsgPublisher->Init(info))
    {
        gstRos2MsgPublisher->Destroy();
        gstRos2MsgPublisher = NULL;
    }

    return  gstRos2MsgPublisher;
}

void GstRos2MsgPublisher::Destroy()
{
    delete this;
}

int32_t GstRos2MsgPublisher::Init(GstRos2MsgCreateInfo *info)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (info == NULL)
    {
        return -EINVAL;
    }

    m_pRos2Node    = info->ros2Node;
    m_timeNsOffset = info->timeNsOffset;

    return 0;
}

void GstRos2MsgPublisher::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (m_pShm)
    {
        m_pShm->Destroy();
        m_pShm = NULL;
    }

    if (m_pImageTransport)
    {
        delete m_pImageTransport;
        m_pImageTransport = NULL;
        m_itImgPublisher.shutdown();
    }
}

int32_t GstRos2MsgPublisher::AdvertiseTopic(GstRos2MsgTopic topic)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (topic.direction != GstRos2MsgDirectionPublish)
    {
        GSTROS2_ERR("invalid direction");
        return -EINVAL;
    }

    GSTROS2_INFO("idx %d, type %d, topicName %s",
        topic.idx, topic.type, topic.topicName);

    switch (topic.type)
    {
        case GstRos2MsgImg:
        case GstRos2MsgSharedImg:
        {
            #ifdef ENABLE_IMAGE_TRANSPORT
            {
                if (m_pImageTransport)
                {
                    delete m_pImageTransport;
                    m_pImageTransport = NULL;
                }

                m_pImageTransport = new image_transport::ImageTransport(m_pRos2Node);
                if (m_pImageTransport)
                {
                    m_itImgPublisher = m_pImageTransport->advertise(topic.topicName, GST_ROS2_IMG_QUEUE_DEPTH);
                }
            }
            #else
            {
                auto qos = rclcpp::QoS(rclcpp::KeepLast(GST_ROS2_IMG_QUEUE_DEPTH), rmw_qos_profile_default);
                //qos.history(RMW_QOS_POLICY_HISTORY_KEEP_ALL);

                // TODO: drop frames if using with SensorDataQoS
                //auto qos = rclcpp::SensorDataQoS();
                m_pImgPublisher =
                    m_pRos2Node->create_publisher<sensor_msgs::msg::Image>(topic.topicName, qos);
            }
            #endif

            if (topic.type == GstRos2MsgSharedImg)
            {
                if (m_pShm == NULL)
                {
                    GstRos2SharedMemCreateInfo info;
                    info.pipelineId = topic.idx;
                    info.isMaster   = true;
                    m_pShm = GstRos2SharedMem::CreateInstance(&info);
                    if (m_pShm == NULL)
                    {
                        GSTROS2_ERR("Create GstRos2SharedMem failed");
                        return -EINVAL;
                    }
                }
            }
            else
            {
                if (m_pShm)
                {
                    m_pShm->Destroy();
                    m_pShm = NULL;
                }
            }

            break;
        }
        case GstRos2MsgStr:
        {
            if (m_pStrPublisher == NULL)
            {
                // publish msg info
                auto qos = rclcpp::QoS(rclcpp::KeepAll(), rmw_qos_profile_default);
                m_pStrPublisher =
                    m_pRos2Node->create_publisher<std_msgs::msg::String>(topic.topicName, qos);
            }
            break;
        }
        default:
        {
            break;
        }
    }

    //m_topic = topic;

    return 0;
}

void GstRos2MsgPublisher::GetBasicImgInfo(GstRos2Frame *frame, sensor_msgs::msg::Image::UniquePtr& imgMsg)
{
    imgMsg->header.stamp.sec      = (int32_t)(frame->info.timestamp / 1000000000);
    imgMsg->header.stamp.nanosec  = (uint32_t)(frame->info.timestamp % 1000000000);

    char frameIdStr[48];
    snprintf((char *)frameIdStr, sizeof(frameIdStr),
        GST_ROS2_FRAME_ID,
        frame->info.cameraSource.c_str(),
        frame->info.pipelineId,
        frame->info.cameraId,
        frame->info.frameId);

    imgMsg->header.frame_id       = (const char*)frameIdStr;
    imgMsg->width                 = frame->info.width;
    imgMsg->height                = frame->info.height;
    imgMsg->is_bigendian          = false;
    imgMsg->step                  = frame->info.stride;
    imgMsg->encoding              = frame->info.format;
}

void GstRos2MsgPublisher::ConvertFrame2Msg(GstRos2Frame *frame, sensor_msgs::msg::Image::UniquePtr& imgMsg, GstRos2Shm &shm)
{
    if (frame == NULL || imgMsg == NULL)
    {
        return;
    }

    GetBasicImgInfo(frame, imgMsg);

    if (frame->info.memSave == GstRos2MemSaveDisable)
    {
        switch (GetFormatType(frame->info.format))
        {
            case GstRos2Fmt_MONO:
            {
                /// remove blanking
                imgMsg->step = imgMsg->width;
                for (uint32_t h = 0; h < frame->info.height; h++)
                {
                    uint8_t *curData = frame->data + frame->info.stride * h;
                    imgMsg->data.insert(imgMsg->data.end(), curData, curData + frame->info.width);
                }
                break;
            }
            default:
            {
                imgMsg->data.insert(imgMsg->data.end(), frame->data, frame->data + frame->info.size);
                break;
            }
        }
    }
    else if (frame->info.memSave == GstRos2MemSaveBySharedMem)
    {
        shm.info.type = GstRos2ShmTypeBuffer;
        shm.info.fd   = frame->info.fd;
        shm.info.size = frame->info.size;
        shm.data      = frame->data;
        imgMsg->data.insert(imgMsg->data.begin(), (uint8_t *)(&shm), (uint8_t *)(&shm) + sizeof(GstRos2Shm));
    }

    #if 0
    GSTROS2_DBG("frame_id %s, encoding %s, size %lu, ts %d(s)+%u(ns)",
        imgMsg->header.frame_id.c_str(),
        imgMsg->encoding.c_str(),
        imgMsg->data.size(),
        imgMsg->header.stamp.sec,
        imgMsg->header.stamp.nanosec);
    #endif
}

int32_t GstRos2MsgPublisher::PublishShmBuffer(GstRos2Shm *shm)
{
    if (m_pShm == NULL || shm == NULL)
    {
        return -EINVAL;
    }

    return m_pShm->PublishShm(shm);
}

int32_t GstRos2MsgPublisher::PublishImage(GstRos2Frame *frame)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (frame == NULL)
    {
        return -EINVAL;
    }

    if (frame->info.latencyType == GstRos2LatencyDistribution)
    {
        frame->info.timestamp = GstRos2Msg::GetCurRosTime();
        frame->info.latency   = 0;
    }
    else
    {
        // Update timestamp with offset
        frame->info.timestamp  = frame->info.timestamp + m_timeNsOffset;
        frame->info.latency    = GstRos2Msg::CalLatency(frame->info.timestamp);
    }

    sensor_msgs::msg::Image::UniquePtr imgMsg(new sensor_msgs::msg::Image());
    GstRos2Shm shm;

    ConvertFrame2Msg(frame, imgMsg, shm);

    showFPS(frame);

    #ifdef ENABLE_IMAGE_TRANSPORT
    m_itImgPublisher.publish(std::move(imgMsg));
    #else
    if (m_pImgPublisher)
    {
        m_pImgPublisher->publish(std::move(imgMsg));
    }
    #endif

    if (m_pShm)
    {
        if (frame->info.frameId == 0 && m_frameId != 0)
        {
            GSTROS2_INFO("Trigger flush for pipeline restart");
            m_pShm->Flush();
        }

        if (PublishShmBuffer(&shm))
        {
            GSTROS2_ERR("PublishSharedMem failed");
        }
    }

    m_frameId = frame->info.frameId;

    return 0;
}

int32_t GstRos2MsgPublisher::PublishSentence(std::string &sentence)
{
    std::lock_guard<std::mutex> lock(m_lock);

    std_msgs::msg::String msg;
    msg.data = sentence;

    if(m_pStrPublisher)
    {
        GSTROS2_INFO("%s", msg.data.c_str());
        m_pStrPublisher->publish(msg);
    }
    return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GstRos2MsgListener::GstRos2MsgListener(GstRos2MsgCreateInfo *info)
{
    m_pImageTransport       = NULL;
    m_pImgSubscriber        = NULL;
    m_pStrSubscriper        = NULL;
    m_pNotify               = NULL;
    m_pRos2Node             = NULL;
    m_pShm                  = NULL;
    m_frameId               = 0;

    //memset(&m_topic, 0, sizeof(m_topic));
}

GstRos2MsgListener::~GstRos2MsgListener()
{
    Deinit();
}

GstRos2MsgListener* GstRos2MsgListener::CreateInstance(GstRos2MsgCreateInfo *info)
{
    GstRos2MsgListener* gstRos2MsgListener = new GstRos2MsgListener(info);

    if (gstRos2MsgListener == NULL)
    {
        return gstRos2MsgListener;
    }

    if (gstRos2MsgListener->Init(info))
    {
        gstRos2MsgListener->Destroy();
        gstRos2MsgListener = NULL;
    }

    return  gstRos2MsgListener;
}

void GstRos2MsgListener::Destroy()
{
    delete this;
}

int32_t GstRos2MsgListener::Init(GstRos2MsgCreateInfo *info)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (info == NULL)
    {
        return -EINVAL;
    }

    m_pRos2Node = info->ros2Node;
    return 0;
}

void GstRos2MsgListener::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (m_pShm)
    {
        m_pShm->Destroy();
        m_pShm = NULL;
    }

    if (m_pImageTransport)
    {
        delete m_pImageTransport;
        m_pImageTransport = NULL;
    }

    m_itImgSubscriber.shutdown();
}

void GstRos2MsgListener::ConvertImgMsg2Frame(Ros2ImageMsgType imgMsg, GstRos2Frame *frame)
{
    if (frame == NULL || imgMsg == NULL)
    {
        return;
    }

    char src[16];
    sscanf(imgMsg->header.frame_id.c_str(),
        GST_ROS2_FRAME_ID,
        src,
        &frame->info.pipelineId,
        &frame->info.cameraId,
        &frame->info.frameId);

    frame->info.cameraSource   = src;
    frame->info.size           = (uint32_t) imgMsg->data.size();
    frame->info.width          = imgMsg->width;
    frame->info.height         = imgMsg->height;
    frame->info.format         = imgMsg->encoding.c_str();

    //frame->info.stride       = imgMsg->step;
    GetAlignment((frame->info.cameraSource == QMMFSRC),
                 frame->info.format,
                 frame->info.width,   frame->info.height,
                 &frame->info.stride, &frame->info.slice);

    frame->info.timestamp    = (int64_t)(imgMsg->header.stamp.sec) * 1000000000 +
                               (int64_t)(imgMsg->header.stamp.nanosec);

    frame->data = (uint8_t*) imgMsg->data.data();

    #if 0
    GSTROS2_DBG(GST_ROS2_FRAME_ID
        " format %s, w x h[%u x %u], str x sli [%u x %u], size %lu, ts %d(s)+%u(ns)",
        frame->info.cameraSource.c_str(),
        frame->info.pipelineId,
        frame->info.cameraId,
        frame->info.frameId,
        frame->info.format.c_str(),
        frame->info.width,
        frame->info.height,
        frame->info.stride,
        frame->info.slice,
        frame->info.size,
        imgMsg->header.stamp.sec,
        imgMsg->header.stamp.nanosec);
    #endif
}

int32_t GstRos2MsgListener::GetShmBuffer(GstRos2Frame *frame)
{
    if (m_pShm == NULL || frame == NULL)
    {
        return -EINVAL;
    }

    GstRos2Shm shm = *(GstRos2Shm *)(frame->data);
    if(m_pShm->GetShm(&shm) || shm.data == NULL)
    {
        return -EINVAL;
    }

    frame->data      = (uint8_t *)shm.data;
    frame->info.size = shm.info.size;

    return 0;
}

void GstRos2MsgListener::ImageNotify(Ros2ImageMsgType imgMsg)
{
    GstRos2MsgListener::ImageNotify(imgMsg, this);
}

void GstRos2MsgListener::ImageNotify(Ros2ImageMsgType imgMsg, void* userData)
{
    if (userData == NULL)
    {
        GSTROS2_ERR("invalid params");
        return;
    }

    GstRos2MsgListener *listener = (GstRos2MsgListener *)userData;

    std::lock_guard<std::mutex> lock(listener->m_lock);

    if (listener->m_pNotify == NULL)
    {
        return;
    }

    GstRos2Frame frame;
    memset(&frame, 0 , sizeof(GstRos2Frame));

    listener->ConvertImgMsg2Frame(imgMsg, &frame);

    if (listener->m_pShm)
    {
        if (frame.info.frameId == 0 && listener->m_frameId != 0)
        {
            GSTROS2_INFO("Trigger flush for pipeline restart");
            if (listener->m_pShm->Flush())
            {
                GSTROS2_ERR("flush failed");
                return;
            }
        }

        if (listener->GetShmBuffer(&frame))
        {
            GSTROS2_ERR("GetShmBuffer failed");
            return;
        }
    }

    frame.info.latency = GstRos2Msg::CalLatency(frame.info.timestamp);

    {
        GstRos2MsgPkg msgPkg;
        msgPkg.type    = GstRos2MsgImg;
        msgPkg.data    = (void*)&frame;
        (listener->m_pNotify)(msgPkg);
    }

    listener->m_frameId = frame.info.frameId;
}

void GstRos2MsgListener::SentenceNotify(const std_msgs::msg::String::SharedPtr msg, void* userData)
{
    if (userData == NULL)
    {
        GSTROS2_ERR("invalid params");
        return;
    }

    GstRos2MsgListener *listener = (GstRos2MsgListener *)userData;

    std::lock_guard<std::mutex> lock(listener->m_lock);

    if (listener->m_pNotify)
    {
        GstRos2MsgPkg msgPkg;
        msgPkg.type = GstRos2MsgStr;
        msgPkg.data = (void*)msg->data.c_str();
        (listener->m_pNotify)(msgPkg);
    }
}

int32_t GstRos2MsgListener::RegisterMsgNotify(GstRos2MsgTopic topic, GstRos2MsgNotifyCb notify)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (topic.direction != GstRos2MsgDirectionListen)
    {
        GSTROS2_ERR("invalid direction");
        return -EINVAL;
    }

    GSTROS2_INFO("idx %d, type %d, topicName %s",
        topic.idx, topic.type, topic.topicName);

    switch (topic.type)
    {
        case GstRos2MsgImg:
        case GstRos2MsgSharedImg:
        {
            void *cb = (void*) this;
            std::function<void(Ros2ImageMsgType imgMsg)> imgCb =
                [&, cb] (Ros2ImageMsgType imgMsg)
                {
                    GstRos2MsgListener::ImageNotify(imgMsg, cb);
                };

            // Subscripe camera image
            #ifdef ENABLE_IMAGE_TRANSPORT
            if (m_pImageTransport == NULL && m_itImgSubscriber == NULL)
            {
                m_pImageTransport = new image_transport::ImageTransport(m_pRos2Node);
                if (m_pImageTransport)
                {
                    //m_itImgSubscriber = m_pImageTransport->subscribe(
                    //     topic.topicName, 10, &GstRos2MsgListener::ImageNotify, this, NULL);

                    m_itImgSubscriber = m_pImageTransport->subscribe(
                        topic.topicName, GST_ROS2_IMG_QUEUE_DEPTH, imgCb, NULL);
                }
            }
            #else
            if (m_pImgSubscriber == NULL)
            {
                auto qos = rclcpp::QoS(rclcpp::KeepLast(GST_ROS2_IMG_QUEUE_DEPTH), rmw_qos_profile_default);
                //qos.history(RMW_QOS_POLICY_HISTORY_KEEP_ALL);

                // TO Check: drop frames if using with SensorDataQoS
                //auto qos = rclcpp::SensorDataQoS();
                m_pImgSubscriber =
                    m_pRos2Node->create_subscription<sensor_msgs::msg::Image>(topic.topicName, qos, imgCb);
            }
            #endif

            if (topic.type == GstRos2MsgSharedImg)
            {
                if (m_pShm != NULL)
                {
                    m_pShm->Destroy();
                    m_pShm = NULL;
                }

                GstRos2SharedMemCreateInfo info;
                info.pipelineId = topic.idx;
                info.isMaster   = false;
                m_pShm = GstRos2SharedMem::CreateInstance(&info);
                if (m_pShm == NULL)
                {
                    GSTROS2_ERR("Create GstRos2SharedMem failed");
                    return -EINVAL;
                }
            }

            break;
        }
        case GstRos2MsgStr:
        {
            // Subscripe camera cfg
            void *cb = (void*) this;
            std::function<void(const std_msgs::msg::String::SharedPtr msg)> msgCb =
                [&, cb] (const std_msgs::msg::String::SharedPtr msg)
                {
                    GstRos2MsgListener::SentenceNotify(msg, cb);
                };

            auto qos = rclcpp::QoS(rclcpp::KeepAll(), rmw_qos_profile_default);
            m_pStrSubscriper =
                m_pRos2Node->create_subscription<std_msgs::msg::String>(topic.topicName, qos, msgCb);

            break;
        }
        default:
        {
            break;
        }
    }

    m_pNotify = notify;
    //m_topic   = topic;

    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GstRos2Msg::GstRos2Msg(GstRos2MsgCreateInfo *info)
{
    for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        m_pGstRos2ImgPublisher[i]   =  NULL;
        m_pGstRos2ImgListener[i]    =  NULL;
    }

    m_pGstRos2MsgPublisher = NULL;
    m_pGstRos2MsgListener  = NULL;
}

GstRos2Msg::~GstRos2Msg()
{
    Deinit();
}

GstRos2Msg* GstRos2Msg::CreateInstance(GstRos2MsgCreateInfo *info)
{
    static GstRos2Msg* g_gstRos2Msg = NULL;

    if (g_gstRos2Msg || info == NULL)
    {
        return g_gstRos2Msg;
    }

    g_gstRos2Msg = new GstRos2Msg(info);
    if (g_gstRos2Msg == NULL)
    {
        return g_gstRos2Msg;
    }

    if (g_gstRos2Msg->Init(info))
    {
        g_gstRos2Msg->Destroy();
        g_gstRos2Msg = NULL;
    }

    return  g_gstRos2Msg;
}

void GstRos2Msg::Destroy()
{
    delete this;
}

int32_t GstRos2Msg::Init(GstRos2MsgCreateInfo *info)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (info == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    m_createInfo = *info;

    return 0;
}

void GstRos2Msg::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);

    for (int32_t i = 0; i < MAX_GST_ROS2_PIPELINES; i++)
    {
        if (m_pGstRos2ImgPublisher[i])
        {
            m_pGstRos2ImgPublisher[i]->Destroy();
            m_pGstRos2ImgPublisher[i] = NULL;
        }
        if (m_pGstRos2ImgListener[i])
        {
            m_pGstRos2ImgListener[i]->Destroy();
            m_pGstRos2ImgListener[i] = NULL;
        }
    }

    if (m_pGstRos2MsgPublisher)
    {
        m_pGstRos2MsgPublisher->Destroy();
        m_pGstRos2MsgPublisher = NULL;
    }

    if (m_pGstRos2MsgListener)
    {
        m_pGstRos2MsgListener->Destroy();
        m_pGstRos2MsgListener  = NULL;
    }
}

int64_t GstRos2Msg::CalLatency(int64_t ts)
{
    int64_t diff = GstRos2Msg::GetCurRosTime() - ts;

    return diff;
}

int64_t GstRos2Msg::GetCurRosTime()
{
    rclcpp::Clock rosClock(RCL_ROS_TIME);
    rclcpp::Time  rosTime = rosClock.now();

    // rclcpp::Time  rosTime = ros2Node->now(); // unexpected crash issue in librclcpp.so

    return rosTime.nanoseconds();
}

void GstRos2Msg::SyncTimestamp(int64_t gstTime)
{
    int64_t curTime = GstRos2Msg::GetCurRosTime();
    m_createInfo.timeNsOffset = curTime - gstTime;

    GSTROS2_DBG("ros_time %ld, gstTime %ld, offset %ld",
        curTime, gstTime, m_createInfo.timeNsOffset);
}

int32_t GstRos2Msg::AdvertiseTopic(GstRos2MsgTopic topic)
{
    std::lock_guard<std::mutex> lock(m_lock);

    int32_t rc = 0;
    if (topic.direction != GstRos2MsgDirectionPublish)
    {
        GSTROS2_ERR("invalid direction");
        return -EINVAL;
    }

    GstRos2MsgPublisher **pub = NULL;
    if (topic.type == GstRos2MsgStr)
    {
        pub = &m_pGstRos2MsgPublisher;
    }
    else
    {
        uint32_t idx = topic.idx;
        if (idx >= MAX_GST_ROS2_PIPELINES)
        {
            GSTROS2_ERR("invalid idx");
            return -EINVAL;
        }
        pub = &m_pGstRos2ImgPublisher[idx];
    }

    if ((*pub) == NULL)
    {
        *pub = GstRos2MsgPublisher::CreateInstance(&m_createInfo);
    }

    if (*pub)
    {
        rc = (*pub)->AdvertiseTopic(topic);
    }

    return rc;
}

int32_t GstRos2Msg::CancelImgTopic(GstRos2MsgTopic topic)
{
    std::lock_guard<std::mutex> lock(m_lock);

    uint32_t idx = topic.idx;
    if (idx >= MAX_GST_ROS2_PIPELINES)
    {
        GSTROS2_ERR("invalid idx");
        return -EINVAL;
    }

    if (m_pGstRos2ImgPublisher[idx])
    {
        m_pGstRos2ImgPublisher[idx]->Destroy();
        m_pGstRos2ImgPublisher[idx] = NULL;
    }

    return 0;
}

int32_t GstRos2Msg::RegisterMsgNotify(GstRos2MsgTopic topic, GstRos2MsgNotifyCb notify)
{
    std::lock_guard<std::mutex> lock(m_lock);

    int32_t rc = 0;
    if (topic.direction != GstRos2MsgDirectionListen)
    {
        GSTROS2_ERR("invalid direction");
        return -EINVAL;
    }

    GstRos2MsgListener **listener = NULL;
    if (topic.type == GstRos2MsgStr)
    {
        listener = &m_pGstRos2MsgListener;
    }
    else
    {
        uint32_t idx = topic.idx;
        if (idx >= MAX_GST_ROS2_PIPELINES)
        {
            GSTROS2_ERR("invalid idx");
            return -EINVAL;
        }
        listener = &m_pGstRos2ImgListener[idx];
    }

    /// un-registerMsgNotify
    if (*listener)
    {
        (*listener)->Destroy();
        *listener = NULL;
    }

    if (notify == NULL)
    {
        return rc;
    }

    *listener = GstRos2MsgListener::CreateInstance(&m_createInfo);

    if (*listener)
    {
        rc = (*listener)->RegisterMsgNotify(topic, notify);
    }

    return rc;
}

void GstRos2Msg::PublishImage(GstRos2Frame *frame, int32_t topicIdx)
{
    if (m_pGstRos2ImgPublisher[topicIdx])
    {
        m_pGstRos2ImgPublisher[topicIdx]->PublishImage(frame);
    }
}

void GstRos2Msg::PublishSentence(std::string &sentence, int32_t topicIdx)
{
    if (m_pGstRos2MsgPublisher)
    {
        m_pGstRos2MsgPublisher->PublishSentence(sentence);
    }
}

