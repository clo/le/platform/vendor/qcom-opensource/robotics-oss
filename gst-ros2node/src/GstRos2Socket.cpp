/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gst-ros2node/GstRos2Socket.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GstRos2Socket::GstRos2Socket(GstRos2SocketCreateInfo *info)
{
    m_pid        = -1;
    m_pipelineId = -1;
    m_type       = GstRos2SocketDirectionServer;
    m_pSyncCb    = NULL;
}

GstRos2Socket::~GstRos2Socket()
{

}

GstRos2Socket* GstRos2Socket::CreateInstance(GstRos2SocketCreateInfo *info)
{
    GstRos2Socket* g_GstRos2Socket = NULL;

    if (info->type == GstRos2SocketDirectionServer)
    {
        g_GstRos2Socket = new GstRos2SocketServer(info);
    }
    else if (info->type == GstRos2SocketDirectionClient)
    {
        g_GstRos2Socket = new GstRos2SocketClient(info);
    }

    if (g_GstRos2Socket == NULL)
    {
        return g_GstRos2Socket;
    }

    if (g_GstRos2Socket->Init(info) < 0)
    {
        g_GstRos2Socket->Destroy();
        g_GstRos2Socket = NULL;
    }

    return  g_GstRos2Socket;
}

void GstRos2Socket::Destroy()
{
    delete this;
}

int32_t GstRos2Socket::SendFd(std::vector<int32_t> &fdList, int32_t sendFd)
{
    return -1;
}

int32_t GstRos2Socket::RecvFd(int32_t *recvFd, int32_t *mapFd)
{
    return -1;
}


bool GstRos2Socket::CheckFdInClientMap(int32_t fd)
{
    bool rc = false;
    if (fd <= 0)
    {
        GSTROS2_ERR("invalid fd");
        return rc;
    }

    for (auto iter = m_clientMap.begin(); iter != m_clientMap.end(); )
    {
        if (iter->second == fd)
        {
            rc = true;
            break;
        }
        iter++;
    }
    return rc;
}

std::vector<int32_t> GstRos2Socket::GetClientFdFromPid(std::vector<int32_t> &pidList)
{
    std::vector<int32_t> clientFdList;
    for (int32_t i = 0; i < (int32_t)pidList.size(); i++)
    {
        auto iterMap = m_clientMap.find(pidList[i]);
        if (iterMap == m_clientMap.end())
        {
            GSTROS2_ERR("Not found pid %d in map", pidList[i]);
            continue;
        }

        clientFdList.push_back(iterMap->second);
    }
    return clientFdList;
}

int32_t GstRos2Socket::Init(GstRos2SocketCreateInfo *info)
{
    if (info == NULL)
    {
        return -EINVAL;
    }

    m_pid        = getpid();
    m_pipelineId = info->pipelineId;
    m_type       = info->type;
    m_pSyncCb    = info->pSyncCb;

    return 0;
}

void GstRos2Socket::Deinit()
{
    for (auto iter = m_clientMap.begin(); iter != m_clientMap.end(); )
    {
        if (iter->second > 0)
        {
            close(iter->second);
        }
        m_clientMap.erase(iter++);
    }
}

int32_t GstRos2Socket::Setup(int pipelineId)
{
    return 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GstRos2SocketServer::GstRos2SocketServer(GstRos2SocketCreateInfo *info) :
    GstRos2Socket(info)
{
    m_serverFd = -1;
}

GstRos2SocketServer::~GstRos2SocketServer()
{
    Deinit();
}

int32_t GstRos2SocketServer::Init(GstRos2SocketCreateInfo *info)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (info == NULL)
    {
        return -EINVAL;
    }

    if (GstRos2Socket::Init(info))
    {
        return -EINVAL;
    }

    return 0;
}

void GstRos2SocketServer::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_serverFd)
    {
        close(m_serverFd);
        m_serverFd = -1;
    }

    m_thread.isRunning = false;
    pthread_join(m_thread.pid, NULL);

    unlink(m_serverUn.sun_path);

    GstRos2Socket::Deinit();
}

void* GstRos2SocketServer::DetectThread(void *data)
{
    if (data == NULL)
    {
        GSTROS2_ERR("invalid data");
        return NULL;
    }

    PthreadInfo *pme = (PthreadInfo*) (data);
    GstRos2SocketServer* server = (GstRos2SocketServer*) pme->prvData;
    if (NULL == server)
    {
        GSTROS2_ERR("Got server failed");
        return NULL;
    }

    int32_t serverFd = server->m_serverFd;
    if(serverFd < 0)
    {
        GSTROS2_ERR("invalid serverFd");
        return NULL;
    }

    GSTROS2_DBG("Waiting clients");

    int32_t            clientFd = -1;
    fd_set             rfds;

    while (pme->isRunning)
    {
        FD_ZERO(&rfds);
        FD_SET(serverFd, &rfds);
        struct timeval       timeout;
        timeout.tv_sec     = 0;
        timeout.tv_usec    = 500 * 1000; //500ms

        int32_t maxFd      = serverFd;
        for (auto iter = server->m_clientMap.begin(); iter != server->m_clientMap.end(); iter++)
        {
            FD_SET(iter->second, &rfds);
            maxFd = (iter->second > maxFd ? iter->second : maxFd);
        }

        if (select(maxFd + 1, &rfds, NULL, NULL, &timeout) > 0)
        {
            /// Check whether exiting
            if (false == pme->isRunning)
            {
                break;
            }

            /// Check client disconnect
            {
                std::lock_guard<std::mutex> lock(server->m_lock);

                for (auto iter = server->m_clientMap.begin(); iter != server->m_clientMap.end(); )
                {
                    if (!FD_ISSET(iter->second, &rfds))
                    {
                        iter++;
                        continue;
                    }

                    if (iter->second <= 0)
                    {
                        GSTROS2_ERR("invalid clientFd %d", iter->second);
                        server->m_clientMap.erase(iter++);
                        continue;
                    }

                    int8_t recvBuf = -1;
                    int32_t count = recv(iter->second, &recvBuf, sizeof(recvBuf), 0);
                    if (INVALID_SOCKET == count || 0 == count)
                    {
                        GSTROS2_DBG("Disconnect client: fd %d", iter->second);
                        close(iter->second);
                        iter->second = -1;

                        if (server->m_pSyncCb)
                        {
                            GSTROS2_DBG("Notify disconnect event");
                            server->m_pSyncCb(GstRos2SocketSyncDisconnect, iter->first);
                        }
                        server->m_clientMap.erase(iter++);
                        continue;
                    }
                    iter++;
                }
            }

            /// Check new client connect
            if(FD_ISSET(serverFd, &rfds))
            {
                //GSTROS2_DBG("Detected client connect");

                clientFd = accept(serverFd, NULL, NULL);
                if (clientFd <= 0)
                {
                    if (clientFd == INVALID_SOCKET)
                    {
                        pme->isRunning = false;
                        break;
                    }

                    GSTROS2_ERR("accept failed: serverFd %d, clientFd %d, err %s",
                        serverFd, clientFd, strerror(errno));
                    continue;
                }

                GSTROS2_INFO("Accept client: fd %d", clientFd);

                struct sockaddr_storage ss;
                socklen_t sslen = sizeof(struct sockaddr_storage);
                if (getpeername(clientFd, (struct sockaddr *)&ss, &sslen))
                {
                    GSTROS2_ERR("unable to get socket name from %d", clientFd);
                }

                struct sockaddr_un *un = (struct sockaddr_un *)&ss;
                int32_t pipelineId     = -1;
                int32_t clientPid      = -1;
                sscanf(un->sun_path, GSTROS2_CLIENT_NAME, &pipelineId, &clientPid);
                if (pipelineId < 0 || clientPid <= 0)
                {
                    GSTROS2_ERR("Cannot get pid from sun_path:%s, pipelineId %d, clientPid %d",
                        un->sun_path, pipelineId, clientPid);
                    break;
                }

                server->m_clientMap.insert(std::pair<int32_t, int32_t>(clientPid, clientFd));

                if (server->m_pSyncCb)
                {
                    GSTROS2_DBG("Notify connect event");
                    server->m_pSyncCb(GstRos2SocketSyncConnect, clientPid);
                }
            }
        }
        else
        {
            /// time out
        }
    }
    GSTROS2_DBG("exit");

    return NULL;
}

int32_t GstRos2SocketServer::Setup(int pipelineId)
{
    if (m_serverFd > 0)
    {
        return -EINVAL;
    }

    int32_t serverFd;

    memset(&m_serverUn, 0, sizeof(struct sockaddr_un));

    m_serverUn.sun_family = AF_UNIX;
    snprintf(m_serverUn.sun_path,
             UNIX_PATH_MAX, GSTROS2_SERVER_NAME, pipelineId);

    /* remove the socket path if it already exists, otherwise bind might fail */
    unlink(m_serverUn.sun_path);

    serverFd = socket(m_serverUn.sun_family, SOCK_STREAM, 0);
    if (serverFd <= 0)
    {
        GSTROS2_ERR("error create serverFd =%d %s", serverFd, strerror(errno));
        return -EPERM;
    }

    if (bind(serverFd, (struct sockaddr *)&m_serverUn, sizeof(m_serverUn)))
    {
        close(serverFd);
        serverFd = -1;
        GSTROS2_ERR("bind failed serverFd=%d %s", serverFd, strerror(errno));
        return -EPERM;
    }
    GSTROS2_DBG("bind serverFd=%d %s", serverFd, m_serverUn.sun_path);

    if (listen(serverFd, MAX_CLIENTS_PER_PIPELIEN) < 0)
    {
        close(serverFd);
        serverFd = -1;
        GSTROS2_ERR("listen failed serverFd=%d %s", serverFd, strerror(errno));
        return -EPERM;
    }
    GSTROS2_DBG("listen serverFd=%d %s", serverFd, m_serverUn.sun_path);

    m_serverFd         = serverFd;
    m_thread.isRunning = true;
    m_thread.prvData   = (void*) this;

    int32_t ret = pthread_create(&m_thread.pid,
                             NULL,
                             DetectThread,
                             (void *)&m_thread);
    if (ret)
    {
        GSTROS2_ERR("create thread failed!");
        return ret;
    }

    return 0;
}

int32_t GstRos2SocketServer::SendFd(std::vector<int32_t> &fdList, int32_t sendFd)
{
    if (fdList.empty())
    {
        GSTROS2_ERR("invalid fdList");
        return -EINVAL;
    }

    if (sendFd <= 0)
    {
        GSTROS2_ERR("invalid sendFd %d", sendFd);
        return -EINVAL;
    }

    if (m_serverFd < 0)
    {
        GSTROS2_ERR("setup server first");
        return -EINVAL;
    }

    if (m_clientMap.empty())
    {
        GSTROS2_ERR("no clients connected");
        return -EINVAL;
    }

    char control[CMSG_SPACE(sizeof(int))];

    struct cmsghdr *cmsghp = NULL;
    struct iovec    iov[1];
    iov[0].iov_base = (int8_t *)&sendFd;
    iov[0].iov_len  = sizeof(int32_t);

    struct msghdr   msgh;
    memset(&msgh, 0, sizeof(msgh));
    msgh.msg_name       = NULL;
    msgh.msg_namelen    = 0;
    msgh.msg_iov        = iov;
    msgh.msg_iovlen     = 1;
    msgh.msg_control    = control;
    msgh.msg_controllen = sizeof(control);
    cmsghp              = CMSG_FIRSTHDR(&msgh);
    if (cmsghp == NULL)
    {
        GSTROS2_ERR("invalid cmsghp");
        return -EINVAL;
    }

    cmsghp->cmsg_level              = SOL_SOCKET;
    cmsghp->cmsg_type               = SCM_RIGHTS;
    cmsghp->cmsg_len                = CMSG_LEN(sizeof(int32_t));
    *((int32_t *)CMSG_DATA(cmsghp)) = sendFd;

    for (int32_t i = 0; i < (int32_t)fdList.size(); i++)
    {
        uint8_t tryMaxCnt = 3;
        int32_t len       = 0;
        bool    isSkip    = false;

        while(len <= 0 && tryMaxCnt > 0)
        {
            std::lock_guard<std::mutex> lock(m_lock);

            len = sendmsg(fdList[i], &msgh, 0);

            if (len <= 0 && tryMaxCnt > 0)
            {
                tryMaxCnt--;

                if (!CheckFdInClientMap(fdList[i]))
                {
                    GSTROS2_WARN("clientFd=%d disconnected, skip to send.", fdList[i]);
                    isSkip = true;
                    break;
                }

                /// wait for 5ms and re-try.
                GSTROS2_WARN("failed to send %d to clientFd=%d, try again.", sendFd, fdList[i]);
                usleep(5 * 1000);
                continue;
            }

            break;
        }

        if (isSkip)
        {
            continue;
        }

        if (len <= 0)
        {
            GSTROS2_ERR("failed to send %d to client fd %d, %s", sendFd, fdList[i], strerror(errno));
        }
        else
        {
            GSTROS2_INFO("Send %d to client(%d) successfully", sendFd, fdList[i]);
        }
    }

    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GstRos2SocketClient::GstRos2SocketClient(GstRos2SocketCreateInfo *info) :
    GstRos2Socket(info)
{
    m_clientFd = -1;
}

GstRos2SocketClient::~GstRos2SocketClient()
{
    Deinit();
}

int32_t GstRos2SocketClient::Init(GstRos2SocketCreateInfo *info)
{
    std::lock_guard<std::mutex> lock(m_lock);

    if (info == NULL)
    {
        return -EINVAL;
    }

    if (GstRos2Socket::Init(info))
    {
        return -EINVAL;
    }

    return 0;
}

void GstRos2SocketClient::Deinit()
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_clientFd)
    {
        close(m_clientFd);
        m_clientFd = -1;
    }
    unlink(m_clientUn.sun_path);

    if (m_pSyncCb)
    {
        m_pSyncCb(GstRos2SocketSyncDisconnect, m_pid);
    }

    GstRos2Socket::Deinit();
}

int32_t GstRos2SocketClient::Setup(int pipelineId)
{
    int32_t clientFd = -1;

    if ((clientFd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        GSTROS2_ERR("client socket error");
        return -EPERM;
    }

    {
        memset(&m_clientUn, 0, sizeof(struct sockaddr_un));
        m_clientUn.sun_family = AF_UNIX;
        snprintf(m_clientUn.sun_path,
            UNIX_PATH_MAX, GSTROS2_CLIENT_NAME, pipelineId, m_pid);

        unlink(m_clientUn.sun_path);
        if (bind(clientFd, (struct sockaddr *)&m_clientUn, sizeof(m_clientUn)) < 0)
        {
            close(clientFd);
            clientFd = -1;
            GSTROS2_ERR("Bind failed clientFd=%d %s", clientFd, strerror(errno));
            return -EPERM;
        }
        GSTROS2_DBG("Bind client fd %d, cli-path %s",
            clientFd, m_clientUn.sun_path);
    }

    {
        struct sockaddr_un server_un;
        memset(&server_un, 0, sizeof(struct sockaddr_un));
        server_un.sun_family = AF_UNIX;
        snprintf(server_un.sun_path,
            UNIX_PATH_MAX, GSTROS2_SERVER_NAME, pipelineId);

        uint8_t tryMaxCnt = 3;
        while (connect(clientFd, (struct sockaddr *)&server_un, sizeof(server_un)) < 0)
        {
            if (tryMaxCnt > 0)
            {
                tryMaxCnt--;
                /// wait for 200ms and re-try.
                GSTROS2_WARN("Connect failed clientFd=%d, try again.", clientFd);
                usleep(200 * 1000);
                continue;
            }

            close(clientFd);
            clientFd = -1;
            GSTROS2_ERR("Connect failed clientFd=%d %s", clientFd, strerror(errno));
            return -EPERM;
        }
        GSTROS2_DBG("Connect client: fd %d, ser-path %s", clientFd, server_un.sun_path);
    }

    m_clientFd = clientFd;

    m_clientMap.insert(std::pair<int32_t, int32_t>(m_pid, clientFd));

    if (m_pSyncCb)
    {
        m_pSyncCb(GstRos2SocketSyncConnect, m_pid);
    }

    return 0;
}

int32_t GstRos2SocketClient::RecvFd(int32_t *recvFd, int32_t *mapFd)
{
    std::lock_guard<std::mutex> lock(m_lock);
    if (m_clientFd < 0)
    {
        GSTROS2_ERR("setup client first");
        return -EINVAL;
    }

    if(recvFd == NULL || mapFd == NULL)
    {
        GSTROS2_ERR("invalid params");
        return -EINVAL;
    }

    int32_t len = -1;

    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(m_clientFd, &rfds);

    struct timeval  timeout;
    timeout.tv_sec  = 0;
    timeout.tv_usec = 500 * 1000; //500ms

    if (select(m_clientFd + 1, &rfds, NULL, NULL, &timeout) > 0)
    {
        if (!FD_ISSET(m_clientFd, &rfds))
        {
            GSTROS2_ERR("invalid rfds");
            return len;
        }

        char control[CMSG_SPACE(sizeof(int))];

        struct cmsghdr *cmsghp = NULL;
        struct iovec iov[1];
        iov[0].iov_base = (int8_t *)recvFd;
        iov[0].iov_len  = sizeof(int32_t);

        struct msghdr msgh;
        memset(&msgh, 0, sizeof(msgh));
        msgh.msg_name = NULL;
        msgh.msg_namelen = 0;
        msgh.msg_control = control;
        msgh.msg_controllen = sizeof(control);
        msgh.msg_iov = iov;
        msgh.msg_iovlen = 1;

        len = recvmsg(m_clientFd, &(msgh), 0);
        if (len <= 0)
        {
            GSTROS2_ERR("recvmsg failed");
            return len;
        }

        //GSTROS2_DBG("msg_ctrl %p len %zd", msgh.msg_control,
        //    msgh.msg_controllen);
        cmsghp = CMSG_FIRSTHDR(&msgh);
        if((cmsghp == NULL)                                   ||
           (cmsghp->cmsg_len   != CMSG_LEN(sizeof(int32_t)))  ||
           (cmsghp->cmsg_level != SOL_SOCKET)                 ||
           (cmsghp->cmsg_type  != SCM_RIGHTS))
        {
            GSTROS2_ERR("Unexpected Control Msg");
            return -EINVAL;
        }

        int32_t _mapFd = *((int32_t *) CMSG_DATA(cmsghp));
        if (_mapFd > 0)
        {
            *mapFd = _mapFd;
        }
        else
        {
            *mapFd = -1;
            len    = 0;
        }

        GSTROS2_INFO("Receieved fd = %d --> %d, len %d", *recvFd, *mapFd, len);
    }
    else
    {
        GSTROS2_WARN("timeout: m_clientFd %d", m_clientFd);
    }

    return len;
}
