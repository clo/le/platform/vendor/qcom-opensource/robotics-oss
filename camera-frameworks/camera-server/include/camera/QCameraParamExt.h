/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef QCAMERA_PARAM_EXT_H
#define QCAMERA_PARAM_EXT_H

namespace android
{
const char VALUE_ENABLE[]                                 = "enable";
const char VALUE_DISABLE[]                                = "disable";
const char VALUE_OFF[]                                    = "off";
const char VALUE_ON[]                                     = "on";
const char VALUE_TRUE[]                                   = "true";
const char VALUE_FALSE[]                                  = "false";

// ISO and exposure time for hal1
const char KEY_QC_GAIN_MANUAL[]                           = "qc-gain-manual"; //discarded
const char KEY_QC_ISO_MODE[]                              = "iso";
const char KEY_QC_SUPPORTED_ISO_MODES[]                   = "iso-values";
const char ISO_AUTO[]                                     = "auto";
const char ISO_HJR[]                                      = "ISO_HJR";
const char ISO_100[]                                      = "ISO100";
const char ISO_200[]                                      = "ISO200";
const char ISO_400[]                                      = "ISO400";
const char ISO_800[]                                      = "ISO800";
const char ISO_1600[]                                     = "ISO1600";
const char ISO_3200[]                                     = "ISO3200";
const char KEY_QC_EXPOSURE_MANUAL[]                       = "qc-exposure-manual"; //discarded

// ISO and exposure time for camx
const char KEY_QC_ISO_VALUE[]                             = "iso-value";
const char KEY_QC_EXPOSURE_TIME[]                         = "exposure-time";
const char META_QC_ISO_EXP_SELECT_PRIORITY[]              = "org.codeaurora.qcamera3.iso_exp_priority.select_priority";
const char META_QC_ISO_EXP_VALUE[]                        = "org.codeaurora.qcamera3.iso_exp_priority.use_iso_exp_priority";
const char META_QC_ABSOLUTE_ISO_VALUE[]                   = "org.codeaurora.qcamera3.iso_exp_priority.use_iso_value";
enum ISOExposureTimePriorityMode
{
    DisablePriority = -1,       ///< Disable ISO/ExpTime Priority
    ISOPriority,                ///< ISO priority
    ExposureTimePriority        ///< Exposure time Priority
};
enum ISOMode
{
    ISOModeAuto,                ///< ISO mode Auto
    ISOModeDeblur,              ///< ISO mode Deblur
    ISOMode100,                 ///< ISO value 100
    ISOMode200,                 ///< ISO value 200
    ISOMode400,                 ///< ISO value 400
    ISOMode800,                 ///< ISO value 800
    ISOMode1600,                ///< ISO value 1600
    ISOMode3200,                ///< ISO value 3200
    ISOModeAbsolute             ///< Absolute ISO Value
};

// Exposure metering mode
const char KEY_QC_EXPOSURE_METERING_MODE[]                = "exposure-metering";
const char META_QC_EXP_METERING_VALUE[]                   = "org.codeaurora.qcamera3.exposure_metering.exposure_metering_mode";
enum
{
    AECAlgoMeteringModeFrameAverage,                    ///< Frame average
    AECAlgoMeteringModeCenterWeighted,                  ///< Center weighted
    AECAlgoMeteringModeSpot,                            ///< Spot metering
    AECAlgoMeteringModeMax             = 0x7FFFFFFF     ///< Anchor to indicate the last item in the defines
};

// ZSL
const char KEY_QC_ZSL[]                                   = "zsl";
const char KEY_QC_SUPPORTED_ZSL_MODES[]                   = "zsl-values";

// RAW size
const char KEY_QC_SUPPORTED_RAW_FORMATS[]                 = "raw-format-values";
const char KEY_QC_RAW_FORMAT[]                            = "raw-format";
const char KEY_QC_RAW_PICUTRE_SIZE[]                      = "raw-size";

// Livesnapshot
const char KEY_QC_SUPPORTED_LIVESNAPSHOT_SIZES[]          = "supported-live-snapshot-sizes";

// Zoom
const char KEY_QC_OPTI_ZOOM[]                             = "opti-zoom";
const char KEY_QC_SUPPORTED_OPTI_ZOOM_MODES[]             = "opti-zoom-values";

// TOF range mode
const char KEY_QC_SHORT_RANGE_MODE[]                      = "short-range";
const char KEY_QC_LONG_RANGE_MODE[]                       = "long-range";
const char KEY_QC_RANGE_MODE[]                            = "range-mode";
const char KEY_QC_RANGE_MODE_AVAIABLE[]                   = "range-mode-avaiable";

// HFR
const char KEY_QC_VIDEO_HIGH_FRAME_RATE[]                 = "video-hfr";
const char KEY_QC_SUPPORTED_VIDEO_HIGH_FRAME_RATE_MODES[] = "video-hfr-values";
const char KEY_QC_SUPPORTED_HFR_SIZES[]                   = "hfr-size-values";

// frame duration
const char KEY_QC_FRAME_DURATION[]                        = "frame-duration";
const char KEY_QC_PREVIEW_FRAME_RATE_MODE[]               = "preview-frame-rate-mode";
const char KEY_QC_SUPPORTED_PREVIEW_FRAME_RATE_MODES[]    = "preview-frame-rate-modes";
const char KEY_QC_PREVIEW_FRAME_RATE_AUTO_MODE[]          = "frame-rate-auto";
const char KEY_QC_PREVIEW_FRAME_RATE_FIXED_MODE[]         = "frame-rate-fixed";

// sharpeness
const char KEY_QC_SHARPNESS[]                             = "sharpness";
const char KEY_QC_MIN_SHARPNESS[]                         = "min-sharpness";
const char KEY_QC_MAX_SHARPNESS[]                         = "max-sharpness";
const char KEY_QC_SHARPNESS_STEP[]                        = "sharpness-step";

// contrast
const char KEY_QC_CONTRAST[]                              = "contrast";
const char KEY_QC_MIN_CONTRAST[]                          = "min-contrast";
const char KEY_QC_MAX_CONTRAST[]                          = "max-contrast";
const char KEY_QC_CONTRAST_STEP[]                         = "contrast-step";

// bringhtness
const char KEY_QC_MIN_BRIGHTNESS[]                        = "min-brightness";
const char KEY_QC_MAX_BRIGHTNESS[]                        = "max-brightness";
const char KEY_QC_BRIGHTNESS_STEP[]                       = "brightness-step";

// Formats for setPreviewFormat and setPictureFormat.
const char PIXEL_FORMAT_YUV420SP_ADRENO[]                 = "yuv420sp-adreno";
const char PIXEL_FORMAT_YV12[]                            = "yuv420p";
const char PIXEL_FORMAT_NV12[]                            = "nv12";
const char QC_PIXEL_FORMAT_NV12_VENUS[]                   = "nv12-venus";

// Flip and Mirror
const char KEY_QC_VERTICAL_FLIP[]                         = "qc-vertical-flip";
const char KEY_QC_HORIZONTAL_MIRROR[]                     = "qc-horizontal-mirror";

// Early PCR
const char META_QC_EARLY_PCR_VALUE[]                      = "org.quic.camera.EarlyPCRenable.EarlyPCRenable";

// Sensor Name
const char META_QC_SENSOR_NAME_VALUE[]                    = "org.codeaurora.qcamera3.sensor_meta_data.sensorName";

// Values for raw image formats
const char QC_PIXEL_FORMAT_YUV_RAW_8BIT_YUYV[]            = "yuv-raw8-yuyv";
const char QC_PIXEL_FORMAT_YUV_RAW_8BIT_YVYU[]            = "yuv-raw8-yvyu";
const char QC_PIXEL_FORMAT_YUV_RAW_8BIT_UYVY[]            = "yuv-raw8-uyvy";
const char QC_PIXEL_FORMAT_YUV_RAW_8BIT_VYUY[]            = "yuv-raw8-vyuy";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_8GBRG[]         = "bayer-qcom-8gbrg";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_8GRBG[]         = "bayer-qcom-8grbg";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_8RGGB[]         = "bayer-qcom-8rggb";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_8BGGR[]         = "bayer-qcom-8bggr";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_10GBRG[]        = "bayer-qcom-10gbrg";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_10GRBG[]        = "bayer-qcom-10grbg";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_10RGGB[]        = "bayer-qcom-10rggb";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_10BGGR[]        = "bayer-qcom-10bggr";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_12GBRG[]        = "bayer-qcom-12gbrg";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_12GRBG[]        = "bayer-qcom-12grbg";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_12RGGB[]        = "bayer-qcom-12rggb";
const char QC_PIXEL_FORMAT_BAYER_QCOM_RAW_12BGGR[]        = "bayer-qcom-12bggr";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_8GBRG[]         = "bayer-mipi-8gbrg";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_8GRBG[]         = "bayer-mipi-8grbg";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_8RGGB[]         = "bayer-mipi-8rggb";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_8BGGR[]         = "bayer-mipi-8bggr";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_10GBRG[]        = "bayer-mipi-10gbrg";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_10GRBG[]        = "bayer-mipi-10grbg";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_10RGGB[]        = "bayer-mipi-10rggb";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_10BGGR[]        = "bayer-mipi-10bggr";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_12GBRG[]        = "bayer-mipi-12gbrg";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_12GRBG[]        = "bayer-mipi-12grbg";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_12RGGB[]        = "bayer-mipi-12rggb";
const char QC_PIXEL_FORMAT_BAYER_MIPI_RAW_12BGGR[]        = "bayer-mipi-12bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_8GBRG[]       = "bayer-ideal-qcom-8gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_8GRBG[]       = "bayer-ideal-qcom-8grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_8RGGB[]       = "bayer-ideal-qcom-8rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_8BGGR[]       = "bayer-ideal-qcom-8bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_10GBRG[]      = "bayer-ideal-qcom-10gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_10GRBG[]      = "bayer-ideal-qcom-10grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_10RGGB[]      = "bayer-ideal-qcom-10rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_10BGGR[]      = "bayer-ideal-qcom-10bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_12GBRG[]      = "bayer-ideal-qcom-12gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_12GRBG[]      = "bayer-ideal-qcom-12grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_12RGGB[]      = "bayer-ideal-qcom-12rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_QCOM_12BGGR[]      = "bayer-ideal-qcom-12bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_8GBRG[]       = "bayer-ideal-mipi-8gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_8GRBG[]       = "bayer-ideal-mipi-8grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_8RGGB[]       = "bayer-ideal-mipi-8rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_8BGGR[]       = "bayer-ideal-mipi-8bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_10GBRG[]      = "bayer-ideal-mipi-10gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_10GRBG[]      = "bayer-ideal-mipi-10grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_10RGGB[]      = "bayer-ideal-mipi-10rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_10BGGR[]      = "bayer-ideal-mipi-10bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_12GBRG[]      = "bayer-ideal-mipi-12gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_12GRBG[]      = "bayer-ideal-mipi-12grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_12RGGB[]      = "bayer-ideal-mipi-12rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_MIPI_12BGGR[]      = "bayer-ideal-mipi-12bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN8_8GBRG[]     = "bayer-ideal-plain8-8gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN8_8GRBG[]     = "bayer-ideal-plain8-8grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN8_8RGGB[]     = "bayer-ideal-plain8-8rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN8_8BGGR[]     = "bayer-ideal-plain8-8bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_8GBRG[]    = "bayer-ideal-plain16-8gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_8GRBG[]    = "bayer-ideal-plain16-8grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_8RGGB[]    = "bayer-ideal-plain16-8rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_8BGGR[]    = "bayer-ideal-plain16-8bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_10GBRG[]   = "bayer-ideal-plain16-10gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_10GRBG[]   = "bayer-ideal-plain16-10grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_10RGGB[]   = "bayer-ideal-plain16-10rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_10BGGR[]   = "bayer-ideal-plain16-10bggr";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_12GBRG[]   = "bayer-ideal-plain16-12gbrg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_12GRBG[]   = "bayer-ideal-plain16-12grbg";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_12RGGB[]   = "bayer-ideal-plain16-12rggb";
const char QC_PIXEL_FORMAT_BAYER_IDEAL_PLAIN16_12BGGR[]   = "bayer-ideal-plain16-12bggr";

// stats logging mask
const char KEY_QC_STATS_LOGGING_MASK[] = "qc-stats-logging-mask";
enum StatsLoggingMask {
    STATS_NO_LOG             = 0,
    STATS_AEC_LOG_MASK       = (1 << 0),
    STATS_AWB_LOG_MASK       = (1 << 1),
    STATS_AF_LOG_MASK        = (1 << 2),
    STATS_ASD_LOG_MASK       = (1 << 3),
    STATS_AFD_LOG_MASK       = (1 << 4),
    STATS_ALL_LOG            = 0x1F,
};

}

#endif
