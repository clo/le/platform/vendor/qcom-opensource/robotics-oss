/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "Camera2-StreamingProcessor"
#define ATRACE_TAG ATRACE_TAG_CAMERA
#define LOG_NDEBUG 1
//#define LOG_NNDEBUG 0 // Per-frame verbose logging

#ifdef LOG_NNDEBUG
#define ALOGVV(...) ALOGV(__VA_ARGS__)
#else
#define ALOGVV(...) ((void)0)
#endif

#include <cutils/properties.h>
#include <utils/Log.h>
#include <utils/Trace.h>
#include <gui/BufferItem.h>
#include <gui/Surface.h>
#include <camera/ICameraRecordingProxy.h>
#include "media/hardware/HardwareAPI.h"

#include "common/CameraDeviceBase.h"
#include "api1/Camera2Client.h"
#include "api1/client2/StreamingProcessor.h"
#include "api1/client2/Camera2Heap.h"

namespace android {
namespace camera2 {

StreamingProcessor::StreamingProcessor(sp<Camera2Client> client):
        mClient(client),
        mDevice(client->getCameraDevice()),
        mId(client->getCameraId()),
        mActiveRequest(NONE),
        mPaused(false),
        mPreviewRequestId(Camera2Client::kPreviewRequestIdStart),
        mPreviewStreamId(NO_STREAM),
        mPreviewHeapCount(kDefaultPreviewHeapCount),
        mPreviewHeapFree(kDefaultPreviewHeapCount),
        mPreviewFormat(kDefaultPreviewFormat),
        mPreviewDataSpace(kDefaultPreviewDataSpace),
        mPreviewGrallocUsage(kDefaultPreviewGrallocUsage),
        mRecordingRequestId(Camera2Client::kRecordingRequestIdStart),
        mRecordingStreamId(NO_STREAM),
        mRecordingFrameAvailable(false),
        mRecordingHeapCount(kDefaultRecordingHeapCount),
        mRecordingHeapFree(kDefaultRecordingHeapCount),
        mRecordingFormat(kDefaultRecordingFormat),
        mRecordingDataSpace(kDefaultRecordingDataSpace),
        mRecordingGrallocUsage(kDefaultRecordingGrallocUsage),
        mPreviewBufferHandler(NULL),
        mVideoBufferHandler(NULL)
{

}

StreamingProcessor::~StreamingProcessor() {
    deletePreviewStream();
    deleteRecordingStream();
}

status_t StreamingProcessor::setPreviewWindow(sp<Surface> window) {
    ATRACE_CALL();
    status_t res;

    res = deletePreviewStream();
    if (res != OK) return res;

    Mutex::Autolock m(mMutex);

    mPreviewWindow = window;

    return OK;
}

bool StreamingProcessor::haveValidPreviewWindow() const {
    Mutex::Autolock m(mMutex);
    return mPreviewWindow != 0;
}

status_t StreamingProcessor::updatePreviewRequest(const Parameters &params) {
    ATRACE_CALL();
    status_t res;
    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    Mutex::Autolock m(mMutex);
    if (mPreviewRequest.entryCount() == 0) {
        sp<Camera2Client> client = mClient.promote();
        if (client == 0) {
            ALOGE("%s: Camera %d: Client does not exist", __FUNCTION__, mId);
            return INVALID_OPERATION;
        }

        // Use CAMERA3_TEMPLATE_ZERO_SHUTTER_LAG for ZSL streaming case.
        if (client->getCameraDeviceVersion() >= CAMERA_DEVICE_API_VERSION_3_0) {
            if (params.zslMode && !params.recordingHint) {
                /*
                    Align with SnapdroganCamera and camx-hal3-test,
                    set CAMERA3_TEMPLATE_PREVIEW instead of CAMERA3_TEMPLATE_ZERO_SHUTTER_LAG in HAL-zsl mode.
                    (CAMERA3_TEMPLATE_ZERO_SHUTTER_LAG is just for APP-zsl in SnapdroganCamera ?)
                */
                res = device->createDefaultRequest(CAMERA3_TEMPLATE_PREVIEW, // CAMERA3_TEMPLATE_ZERO_SHUTTER_LAG
                        &mPreviewRequest);
            } else {
                res = device->createDefaultRequest(CAMERA3_TEMPLATE_PREVIEW,
                        &mPreviewRequest);
            }
        } else {
            res = device->createDefaultRequest(CAMERA2_TEMPLATE_PREVIEW,
                    &mPreviewRequest);
        }

        if (res != OK) {
            ALOGE("%s: Camera %d: Unable to create default preview request: "
                    "%s (%d)", __FUNCTION__, mId, strerror(-res), res);
            return res;
        }
    }

    res = params.updateRequest(&mPreviewRequest);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to update common entries of preview "
                "request: %s (%d)", __FUNCTION__, mId,
                strerror(-res), res);
        return res;
    }

    res = mPreviewRequest.update(ANDROID_REQUEST_ID,
            &mPreviewRequestId, 1);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to update request id for preview: %s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }

    return OK;
}

status_t StreamingProcessor::updatePreviewStream(const Parameters &params) {
    ATRACE_CALL();
    Mutex::Autolock m(mMutex);

    status_t res;
    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }
    ALOGV("%s: Camera %d: enter", __FUNCTION__, mId);

    if (mPreviewStreamId != NO_STREAM) {
        // Check if stream parameters have to change
        uint32_t currentWidth, currentHeight;
        res = device->getStreamInfo(mPreviewStreamId,
                &currentWidth, &currentHeight, 0, 0);
        if (res != OK) {
            ALOGE("%s: Camera %d: Error querying preview stream info: "
                    "%s (%d)", __FUNCTION__, mId, strerror(-res), res);
            return res;
        }
        if (currentWidth != (uint32_t)params.previewWidth ||
                currentHeight != (uint32_t)params.previewHeight) {
            ALOGV("%s: Camera %d: Preview size switch: %d x %d -> %d x %d",
                    __FUNCTION__, mId, currentWidth, currentHeight,
                    params.previewWidth, params.previewHeight);
            res = device->waitUntilDrained();
            if (res != OK) {
                ALOGE("%s: Camera %d: Error waiting for preview to drain: "
                        "%s (%d)", __FUNCTION__, mId, strerror(-res), res);
                return res;
            }
            res = device->deleteStream(mPreviewStreamId);
            if (res != OK) {
                ALOGE("%s: Camera %d: Unable to delete old output stream "
                        "for preview: %s (%d)", __FUNCTION__, mId,
                        strerror(-res), res);
                return res;
            }
            mPreviewStreamId = NO_STREAM;
        }
    }

    if (mPreviewStreamId == NO_STREAM) {
        mPreviewFrameCount = 0;
        if (params.previewFormat == HAL_PIXEL_FORMAT_RAW16) {
            if (params.rangeModeAvaiable) {
                device->setForceResMode(params.rangeMode);
            }

            res = device->createStream(
                    params.previewWidth, params.previewHeight,
                    HAL_PIXEL_FORMAT_RAW16, HAL_DATASPACE_UNKNOWN, STREAM_DEPTH_RAWPLAINE16,
                    CAMERA3_STREAM_ROTATION_0, &mPreviewStreamId);
            if (res != OK) {
                ALOGE("%s: Camera %d: Unable to create preview stream: %s (%d)",
                        __FUNCTION__, mId, strerror(-res), res);
                return res;
            }
        } else {
           res = device->createStream(
                   params.previewWidth, params.previewHeight,
                   CAMERA2_HAL_PIXEL_FORMAT_OPAQUE, HAL_DATASPACE_UNKNOWN, STREAM_PREVIEW,
                   CAMERA3_STREAM_ROTATION_0, &mPreviewStreamId);
           if (res != OK) {
                ALOGE("%s: Camera %d: Unable to create preview stream: %s (%d)",
                      __FUNCTION__, mId, strerror(-res), res);
                return res;
           }
        }
    }

    ALOGV("%s: Camera %d: mPreviewStreamId %d", __FUNCTION__, mId,mPreviewStreamId);

    res = device->setStreamTransform(mPreviewStreamId,
            params.previewTransform);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to set preview stream transform: "
                "%s (%d)", __FUNCTION__, mId, strerror(-res), res);
        return res;
    }

    mPreviewBufferHandler = device->getbufferhandler(mPreviewStreamId);
    if (mPreviewBufferHandler == NULL) {
        ALOGE("%s camera %d preview stream %d get buffer handler failed",
            __FUNCTION__, mId, mPreviewStreamId);
        return INVALID_OPERATION;
    }

    ALOGI("%s: buffer handler %p preview stream %d",
        __FUNCTION__,mPreviewBufferHandler, mPreviewStreamId);
    mPreviewBufferHandler->setFrameAvailableListener(this);

    return OK;
}

status_t StreamingProcessor::deletePreviewStream() {
    ATRACE_CALL();
    status_t res;

    Mutex::Autolock m(mMutex);

    if (mPreviewStreamId != NO_STREAM) {
        sp<CameraDeviceBase> device = mDevice.promote();
        if (device == 0) {
            ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
            return INVALID_OPERATION;
        }

        ALOGV("%s: for cameraId %d on streamId %d",
            __FUNCTION__, mId, mPreviewStreamId);

        res = device->waitUntilDrained();
        if (res != OK) {
            ALOGE("%s: Error waiting for preview to drain: %s (%d)",
                    __FUNCTION__, strerror(-res), res);
            return res;
        }
        res = device->deleteStream(mPreviewStreamId);
        if (res != OK) {
            ALOGE("%s: Unable to delete old preview stream: %s (%d)",
                    __FUNCTION__, strerror(-res), res);
            return res;
        }

        ALOGV("%s: Camera %d: clear preview heap", __FUNCTION__, mId);
        mPreviewHeap.clear();
        mPreviewWindow.clear();

        mPreviewStreamId = NO_STREAM;
    }
    return OK;
}

int StreamingProcessor::getPreviewStreamId() const {
    Mutex::Autolock m(mMutex);
    ALOGV("%s: preview stream id %d",__FUNCTION__,mPreviewStreamId);

    return mPreviewStreamId;
}

status_t StreamingProcessor::setRecordingBufferCount(size_t count) {
    ATRACE_CALL();
    // Make sure we can support this many buffer slots
    if (count > BufferQueue::NUM_BUFFER_SLOTS) {
        ALOGE("%s: Camera %d: Too many recording buffers requested: %zu, max %d",
                __FUNCTION__, mId, count, BufferQueue::NUM_BUFFER_SLOTS);
        return BAD_VALUE;
    }

    Mutex::Autolock m(mMutex);

    ALOGV("%s: Camera %d: New recording buffer count from encoder: %zu",
            __FUNCTION__, mId, count);

    // Need to re-size consumer and heap
    if (mRecordingHeapCount != count) {
        ALOGV("%s: Camera %d: Resetting recording heap and consumer",
            __FUNCTION__, mId);

        if (isStreamActive(mActiveStreamIds, mRecordingStreamId)) {
            ALOGE("%s: Camera %d: Setting recording buffer count when "
                    "recording stream is already active!", __FUNCTION__,
                    mId);
            return INVALID_OPERATION;
        }

        releaseAllRecordingFramesLocked();

        if (mRecordingHeap != 0) {
            mRecordingHeap.clear();
        }
        mRecordingHeapCount = count;
        mRecordingHeapFree = count;

        mRecordingConsumer.clear();
    }

    return OK;
}

status_t StreamingProcessor::setRecordingFormat(int format,
        android_dataspace dataSpace) {
    ATRACE_CALL();

    Mutex::Autolock m(mMutex);

    ALOGV("%s: Camera %d: New recording format/dataspace from encoder: %X, %X",
            __FUNCTION__, mId, format, dataSpace);

    mRecordingFormat = format;
    mRecordingDataSpace = dataSpace;
    int prevGrallocUsage = mRecordingGrallocUsage;
    if (mRecordingFormat == HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED) {
        mRecordingGrallocUsage = GRALLOC_USAGE_HW_VIDEO_ENCODER;
    } else {
        mRecordingGrallocUsage = GRALLOC_USAGE_SW_READ_OFTEN;
    }

    ALOGV("%s: Camera %d: New recording gralloc usage: %08X", __FUNCTION__, mId,
            mRecordingGrallocUsage);

    if (prevGrallocUsage != mRecordingGrallocUsage) {
        ALOGV("%s: Camera %d: Resetting recording consumer for new usage",
            __FUNCTION__, mId);

        if (isStreamActive(mActiveStreamIds, mRecordingStreamId)) {
            ALOGE("%s: Camera %d: Changing recording format when "
                    "recording stream is already active!", __FUNCTION__,
                    mId);
            return INVALID_OPERATION;
        }

        releaseAllRecordingFramesLocked();

        mRecordingConsumer.clear();
    }

    return OK;
}

status_t StreamingProcessor::updateRecordingRequest(const Parameters &params) {
    ATRACE_CALL();
    status_t res;
    Mutex::Autolock m(mMutex);

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    if (mRecordingRequest.entryCount() == 0) {
        res = device->createDefaultRequest(CAMERA2_TEMPLATE_VIDEO_RECORD,
                &mRecordingRequest);
        if (res != OK) {
            ALOGE("%s: Camera %d: Unable to create default recording request:"
                    " %s (%d)", __FUNCTION__, mId, strerror(-res), res);
            return res;
        }
    }

    res = params.updateRequest(&mRecordingRequest);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to update common entries of recording "
                "request: %s (%d)", __FUNCTION__, mId,
                strerror(-res), res);
        return res;
    }

    res = mRecordingRequest.update(ANDROID_REQUEST_ID,
            &mRecordingRequestId, 1);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to update request id for request: %s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }

    return OK;
}

status_t StreamingProcessor::recordingStreamNeedsUpdate(
        const Parameters &params, bool *needsUpdate) {
    status_t res;

    if (needsUpdate == 0) {
        ALOGE("%s: Camera %d: invalid argument", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    if (mRecordingStreamId == NO_STREAM) {
        *needsUpdate = true;
        return OK;
    }

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    uint32_t currentWidth, currentHeight, currentFormat;
    android_dataspace currentDataSpace;
    res = device->getStreamInfo(mRecordingStreamId,
            &currentWidth, &currentHeight, &currentFormat, &currentDataSpace);
    if (res != OK) {
        ALOGE("%s: Camera %d: Error querying recording output stream info: "
                "%s (%d)", __FUNCTION__, mId,
                strerror(-res), res);
        return res;
    }

    if (mRecordingConsumer == 0 ||
            currentWidth != (uint32_t)params.videoWidth ||
            currentHeight != (uint32_t)params.videoHeight ||
            currentFormat != (uint32_t)mRecordingFormat ||
            currentDataSpace != mRecordingDataSpace) {
        *needsUpdate = true;
    }
    *needsUpdate = false;
    return res;
}

status_t StreamingProcessor::updateRecordingStream(const Parameters &params) {
    ATRACE_CALL();
    status_t res;
    Mutex::Autolock m(mMutex);

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    bool newConsumer = false;

    if (mRecordingStreamId != NO_STREAM) {
        // Check if stream parameters have to change
        uint32_t currentWidth, currentHeight;
        uint32_t currentFormat;
        android_dataspace currentDataSpace;
        res = device->getStreamInfo(mRecordingStreamId,
                &currentWidth, &currentHeight,
                &currentFormat, &currentDataSpace);
        if (res != OK) {
            ALOGE("%s: Camera %d: Error querying recording output stream info: "
                    "%s (%d)", __FUNCTION__, mId,
                    strerror(-res), res);
            return res;
        }
        if (currentWidth != (uint32_t)params.videoWidth ||
                currentHeight != (uint32_t)params.videoHeight ||
                currentFormat != (uint32_t)mRecordingFormat ||
                currentDataSpace != mRecordingDataSpace ||
                newConsumer) {
            // TODO: Should wait to be sure previous recording has finished
            res = device->deleteStream(mRecordingStreamId);

            if (res == -EBUSY) {
                ALOGV("%s: Camera %d: Device is busy, call "
                      "updateRecordingStream after it becomes idle",
                      __FUNCTION__, mId);
                return res;
            } else if (res != OK) {
                ALOGE("%s: Camera %d: Unable to delete old output stream "
                        "for recording: %s (%d)", __FUNCTION__,
                        mId, strerror(-res), res);
                return res;
            }
            mRecordingStreamId = NO_STREAM;
        }
    }

    if (mRecordingStreamId == NO_STREAM) {
        mRecordingFrameCount = 0;

        device->setHfrMode(params.hfrMode, params.batchSize);

        int streamType = STREAM_RECORD;
        if (params.batchSize > 1) {
            streamType = STREAM_RECORD_HFR;
        }
        res = device->createStream(
                params.videoWidth, params.videoHeight,
                mRecordingFormat, mRecordingDataSpace, streamType,
                CAMERA3_STREAM_ROTATION_0, &mRecordingStreamId);
        if (res != OK) {
            ALOGE("%s: Camera %d: Can't create output stream for recording: "
                    "%s (%d)", __FUNCTION__, mId,
                    strerror(-res), res);
            return res;
        }
    }

    mVideoBufferHandler = device->getbufferhandler(mRecordingStreamId);
    if (mVideoBufferHandler == NULL) {
        ALOGE("%s camera %d video stream %d get buffer handler failed",
            __FUNCTION__, mId, mRecordingStreamId);
        return INVALID_OPERATION;
    }

    ALOGI("%s: video buffer handler %p video stream %d",
        __FUNCTION__,mVideoBufferHandler, mRecordingStreamId);
    mVideoBufferHandler->setFrameAvailableListener(this);

    return OK;
}

status_t StreamingProcessor::deleteRecordingStream() {
    ATRACE_CALL();
    status_t res;

    Mutex::Autolock m(mMutex);

    if (mRecordingStreamId != NO_STREAM) {
        sp<CameraDeviceBase> device = mDevice.promote();
        if (device == 0) {
            ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
            return INVALID_OPERATION;
        }

        res = device->waitUntilDrained();
        if (res != OK) {
            ALOGE("%s: Error waiting for HAL to drain: %s (%d)",
                    __FUNCTION__, strerror(-res), res);
            return res;
        }
        res = device->deleteStream(mRecordingStreamId);
        if (res != OK) {
            ALOGE("%s: Unable to delete recording stream: %s (%d)",
                    __FUNCTION__, strerror(-res), res);
            return res;
        }

        ALOGV("%s: Camera %d: clear recording heap", __FUNCTION__, mId);
        mRecordingHeap.clear();
        mRecordingWindow.clear();
        mRecordingConsumer.clear();

        mRecordingStreamId = NO_STREAM;
    }
    return OK;
}

int StreamingProcessor::getRecordingStreamId() const {
    return mRecordingStreamId;
}

status_t StreamingProcessor::startHfrStream(
    const Vector<int32_t> &outputStreams, int32_t batchSize) {

    ATRACE_CALL();
    status_t res;
    ALOGE("Camera3Device::%s E, batchSize %d", __FUNCTION__, batchSize);

    if (batchSize < 1) return INVALID_OPERATION;

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    Mutex::Autolock m(mMutex);

    // If a recording stream is being started up and no recording
    // stream is active yet, free up any outstanding buffers left
    // from the previous recording session. There should never be
    // any, so if there are, warn about it.
    bool isRecordingStreamIdle = !isStreamActive(mActiveStreamIds, mRecordingStreamId);
    bool startRecordingStream = isStreamActive(outputStreams, mRecordingStreamId);
    if (startRecordingStream && isRecordingStreamIdle) {
        releaseAllRecordingFramesLocked();
    }

    ALOGV("%s: Camera %d: %s started, recording heap has %zu free of %zu",
            __FUNCTION__, mId, "recording", mRecordingHeapFree, mRecordingHeapCount);

    List<const CameraMetadata> requestList;
    CameraMetadata &request = mRecordingRequest;
    Vector<int32_t> singleStream;

    // First request with all streams
    res = request.update(
        ANDROID_REQUEST_OUTPUT_STREAMS,
        outputStreams);
    if (res != OK) {
        ALOGE("%s %d: Camera %d: Unable to set up request: %s (%d)",
                __FUNCTION__, __LINE__, mId, strerror(-res), res);
        return res;
    }

    res = request.sort();
    if (res != OK) {
        ALOGE("%s %d: Camera %d: Error sorting request: %s (%d)",
                __FUNCTION__, __LINE__, mId, strerror(-res), res);
        return res;
    }

    requestList.push_back(request);

    // Advance requests with single stream
    singleStream.push_back(mRecordingStreamId);

    res = request.update(
        ANDROID_REQUEST_OUTPUT_STREAMS,
        singleStream);
    if (res != OK) {
        ALOGE("%s %d: Camera %d: Unable to set up request: %s (%d)",
                __FUNCTION__, __LINE__, mId, strerror(-res), res);
        return res;
    }

    res = request.sort();
    if (res != OK) {
        ALOGE("%s %d: Camera %d: Error sorting request: %s (%d)",
                __FUNCTION__, __LINE__, mId, strerror(-res), res);
        return res;
    }

    for (uint32_t i = 1; i < batchSize; i++) {
        requestList.push_back(request);
    }

    // setStreamingRequestList
    res = device->setStreamingRequestList(requestList, NULL);

    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to set request list to start HFR stream: "
                "%s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }
    mActiveRequest = RECORD;
    mPaused = false;
    mActiveStreamIds = outputStreams;
    ALOGE("Camera3Device::%s X",__FUNCTION__);
    return OK;
}

status_t StreamingProcessor::startStream(StreamType type,
        const Vector<int32_t> &outputStreams) {
    ATRACE_CALL();
    status_t res;
    ALOGE("Camera3Device::%s E",__FUNCTION__);

    if (type == NONE) return INVALID_OPERATION;

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    ALOGV("%s: Camera %d: type = %d", __FUNCTION__, mId, type);

    Mutex::Autolock m(mMutex);

    // If a recording stream is being started up and no recording
    // stream is active yet, free up any outstanding buffers left
    // from the previous recording session. There should never be
    // any, so if there are, warn about it.
    bool isRecordingStreamIdle = !isStreamActive(mActiveStreamIds, mRecordingStreamId);
    bool startRecordingStream = isStreamActive(outputStreams, mRecordingStreamId);
    if (startRecordingStream && isRecordingStreamIdle) {
        releaseAllRecordingFramesLocked();
    }

    ALOGV("%s: Camera %d: %s started, heap has %zu free of %zu",
            __FUNCTION__, mId, (type == PREVIEW) ? "preview" : "recording",
            mRecordingHeapFree, mRecordingHeapCount);

    CameraMetadata &request = (type == PREVIEW) ?
            mPreviewRequest : mRecordingRequest;

    res = request.update(
        ANDROID_REQUEST_OUTPUT_STREAMS,
        outputStreams);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to set up preview request: %s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }

    res = request.sort();
    if (res != OK) {
        ALOGE("%s: Camera %d: Error sorting preview request: %s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }
    ALOGE("Camera3Device::%s before device->setStreamingRequest()",__FUNCTION__);
    res = device->setStreamingRequest(request);
    ALOGE("Camera3Device::%s done device->setStreamingRequest()",__FUNCTION__);
    if (res != OK) {
        ALOGE("%s: Camera %d: Unable to set preview request to start preview: "
                "%s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }
    mActiveRequest = type;
    mPaused = false;
    mActiveStreamIds = outputStreams;
    ALOGE("Camera3Device::%s X",__FUNCTION__);
    return OK;
}

status_t StreamingProcessor::togglePauseStream(bool pause) {
    ATRACE_CALL();
    status_t res;

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    ALOGV("%s: Camera %d: toggling pause to %d", __FUNCTION__, mId, pause);

    Mutex::Autolock m(mMutex);

    if (mActiveRequest == NONE) {
        ALOGE("%s: Camera %d: Can't toggle pause, streaming was not started",
              __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    if (mPaused == pause) {
        return OK;
    }

    if (pause) {
        res = device->clearStreamingRequest();
        if (res != OK) {
            ALOGE("%s: Camera %d: Can't clear stream request: %s (%d)",
                    __FUNCTION__, mId, strerror(-res), res);
            return res;
        }
    } else {
        CameraMetadata &request =
                (mActiveRequest == PREVIEW) ? mPreviewRequest
                                            : mRecordingRequest;
        res = device->setStreamingRequest(request);
        if (res != OK) {
            ALOGE("%s: Camera %d: Unable to set preview request to resume: "
                    "%s (%d)",
                    __FUNCTION__, mId, strerror(-res), res);
            return res;
        }
    }

    mPaused = pause;
    return OK;
}

status_t StreamingProcessor::stopStream() {
    ATRACE_CALL();
    status_t res;

    Mutex::Autolock m(mMutex);

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    res = device->clearStreamingRequest();
    if (res != OK) {
        ALOGE("%s: Camera %d: Can't clear stream request: %s (%d)",
                __FUNCTION__, mId, strerror(-res), res);
        return res;
    }

    mActiveRequest = NONE;
    mActiveStreamIds.clear();
    mPaused = false;

    return OK;
}

int32_t StreamingProcessor::getActiveRequestId() const {
    Mutex::Autolock m(mMutex);
    switch (mActiveRequest) {
        case NONE:
            return 0;
        case PREVIEW:
            return mPreviewRequestId;
        case RECORD:
            return mRecordingRequestId;
        default:
            ALOGE("%s: Unexpected mode %d", __FUNCTION__, mActiveRequest);
            return 0;
    }
}

status_t StreamingProcessor::incrementStreamingIds() {
    ATRACE_CALL();
    Mutex::Autolock m(mMutex);

    mPreviewRequestId++;
    if (mPreviewRequestId >= Camera2Client::kPreviewRequestIdEnd) {
        mPreviewRequestId = Camera2Client::kPreviewRequestIdStart;
    }
    mRecordingRequestId++;
    if (mRecordingRequestId >= Camera2Client::kRecordingRequestIdEnd) {
        mRecordingRequestId = Camera2Client::kRecordingRequestIdStart;
    }
    return OK;
}

void StreamingProcessor::onFrameAvailable() {
    ATRACE_CALL();
    Mutex::Autolock l(mMutex);
    if (!mRecordingFrameAvailable) {
        mRecordingFrameAvailable = true;
        mRecordingFrameAvailableSignal.signal();
    }

}

bool StreamingProcessor::threadLoop() {
    status_t res;

    {
        Mutex::Autolock l(mMutex);
        while (!mRecordingFrameAvailable) {
            res = mRecordingFrameAvailableSignal.waitRelative(
                mMutex, kWaitDuration);
            if (res == TIMED_OUT) return true;
        }
        mRecordingFrameAvailable = false;
    }

    do {
        //res = processRecordingFrame();
        res = processNewRecordingFrame();
    } while (res == OK);

    return true;
}

status_t StreamingProcessor::processNewRecordingFrame() {
    ATRACE_CALL();
    sp<Camera2Heap> previewHeap;
    sp<Camera2Heap> recordingHeap;
    size_t heapIdx = 0;
    nsecs_t timestamp;
    sp<Camera2Client> client = mClient.promote();
    BufferHandlerInfo* bufferInfo;
    //BufferHandlerInfo* videoBufferInfo;
    //BufferHandler* bufferHandler;
    //sp<Camera2Client> consumerclient;

    // Discard frames during shutdown or stream deleted
    if (client == 0) {
        ALOGV("%s: Camera %d discarding frames during shutdown %p %p",
            __FUNCTION__, mId,mPreviewBufferHandler,mVideoBufferHandler);
        if (mVideoBufferHandler)
            mVideoBufferHandler->QueueAllFreeBuffer();
        if (mPreviewBufferHandler)
            mPreviewBufferHandler->QueueAllFreeBuffer();

        return UNKNOWN_ERROR;
    }

    {
        /* acquire SharedParameters before mMutex so we don't dead lock
           with Camera2Client code calling into StreamingProcessor */
        SharedParameters::Lock l(client->getParameters());
        Mutex::Autolock m(mMutex);

        // Discard frames after stream deleted
        if ((mRecordingStreamId == NO_STREAM) && (mPreviewStreamId == NO_STREAM)) {
            ALOGV("%s: Camera %d discarding frames after stream deleted %p %p",
                __FUNCTION__, mId,mPreviewBufferHandler,mVideoBufferHandler);
            if (mVideoBufferHandler != NULL) {
                mVideoBufferHandler->QueueAllFreeBuffer();
            }
            if (mPreviewBufferHandler != NULL)
                mPreviewBufferHandler->QueueAllFreeBuffer();

            return UNKNOWN_ERROR;
        }

        if ((mRecordingStreamId != NO_STREAM) && (mVideoBufferHandler != NULL)) {
            buffer_handle_t* buffer = mVideoBufferHandler->GetOneBusyBuffer();
            if (NULL == buffer) {
                ALOGV("%s: Camera %d video stream %d : No aviable busy buffer",
                    __FUNCTION__, mId,mRecordingStreamId);
                return NOT_ENOUGH_DATA;
            }
            bufferInfo = mVideoBufferHandler->getBufferHandlerInfo(buffer);
            if (NULL == bufferInfo) {
                ALOGV("%s: Camera %d stream %d : No aviable busy buffer info",
                    __FUNCTION__, mId,mRecordingStreamId);
                return NOT_ENOUGH_DATA;
            }

            if (bufferInfo->streamType == STREAM_RECORD ||
                bufferInfo->streamType == STREAM_RECORD_HFR) {
                mRecordingFrameCount++;

                timestamp = bufferInfo->timestamp;
                ALOGV("%s: camera %d,  video stream %d, Get buffer info done, timestamp %llu",
                     __FUNCTION__,mId, mRecordingStreamId, timestamp);

                if ( l.mParameters.state != Parameters::PREVIEW
                     && l.mParameters.state != Parameters::RECORD
                     && l.mParameters.state != Parameters::VIDEO_SNAPSHOT) {
                     ALOGI("%s: Camera %d: No longer streaming",
                           __FUNCTION__, mId);
                    mVideoBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                    return OK;
                }

                size_t frameSize = bufferInfo->frameInfo.size;
                size_t bufferSize = frameSize + sizeof(FrameInfo);
                size_t currentBufferSize = (mRecordingHeap == 0) ?
                    0 : (mRecordingHeap->mHeap->getSize() / mRecordingHeapCount);

                if (bufferSize > currentBufferSize) {
                     ALOGV("%s: Camera %d: Creating recording heap with %zu buffers of "
                            "frame size %zu bytes & frame info size %zu bytes", __FUNCTION__, mId,
                            mRecordingHeapCount, frameSize, sizeof(FrameInfo));
                     mRecordingHeap.clear();
                     mRecordingHeap = new Camera2Heap(bufferSize, mRecordingHeapCount,
                            "Camera2Client::RecordingHeap");
                     if (mRecordingHeap->mHeap->getSize() == 0) {
                          ALOGE("%s: Camera %d: Unable to allocate memory for recording",
                                __FUNCTION__, mId);
                          mVideoBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                          raise(SIGSEGV); //trigger dump
                          return NO_MEMORY;
                     }

                     mRecordingHeapHead = 0;
                     mRecordingHeapFree = mRecordingHeapCount;
                }

                if (mRecordingHeapFree == 0) {
                     ALOGE("%s: Camera %d: No free recording buffers, dropping frame",
                           __FUNCTION__, mId);
                     mVideoBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                     return NO_MEMORY;
                }
                mRecordingHeapFree--;

                heapIdx = mRecordingHeapHead;
                mRecordingHeapHead = (mRecordingHeapHead + 1) % mRecordingHeapCount;
                recordingHeap = mRecordingHeap;

                ssize_t offset;
                size_t size;
                sp<IMemoryHeap> heap =
                    mRecordingHeap->mBuffers[heapIdx]->getMemory(&offset, &size);
                uint8_t *data = (uint8_t*)heap->getBase() + offset;

                ALOGV("%s: camera %d, stream %d,copy the frame data %p size %d into Imomery dataspace",
                       __FUNCTION__,mId,mRecordingStreamId,bufferInfo->vaddr,bufferSize);

                /*copy the frame data to Imomery dataspace*/
                memcpy(data, &bufferInfo->frameInfo, sizeof(FrameInfo));
                memcpy(data + sizeof(FrameInfo), bufferInfo->vaddr, frameSize);

                mVideoBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
            }
        } else if ((mPreviewStreamId != NO_STREAM) && (mPreviewBufferHandler != NULL)) {

            buffer_handle_t* buffer = mPreviewBufferHandler->GetOneBusyBuffer();
            if (NULL == buffer) {
                ALOGV("%s: Camera %d preview stream %d : No aviable busy buffer",
                    __FUNCTION__, mId,mRecordingStreamId);
                return NOT_ENOUGH_DATA;
            }
            bufferInfo = mPreviewBufferHandler->getBufferHandlerInfo(buffer);
            if (NULL == bufferInfo) {
                ALOGV("%s: Camera %d stream %d : No aviable busy buffer info",
                    __FUNCTION__, mId,mRecordingStreamId);
                return NOT_ENOUGH_DATA;
            }

            if (bufferInfo->streamType == STREAM_DEPTH_RAWPLAINE16) {
                mPreviewFrameCount++;

                timestamp = bufferInfo->timestamp;
                ALOGV("%s: camera %d, preview stream %d, Get buffer info done, timestamp %llu",
                     __FUNCTION__,mId, mPreviewStreamId, timestamp);

                if ( l.mParameters.state != Parameters::PREVIEW
                     && l.mParameters.state != Parameters::RECORD
                     && l.mParameters.state != Parameters::VIDEO_SNAPSHOT) {
                     ALOGI("%s: Camera %d: No longer streaming",
                           __FUNCTION__, mId);
                    mPreviewBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                    return OK;
                }

                size_t frameSize = bufferInfo->frameInfo.size;
                size_t bufferSize = frameSize + sizeof(FrameInfo);
                size_t currentBufferSize = (mPreviewHeap == 0) ?
                    0 : (mPreviewHeap->mHeap->getSize() / mPreviewHeapCount);

                if (bufferSize > currentBufferSize) {
                     ALOGV("%s: Camera %d: Creating preview heap with %zu buffers of "
                            "frame size %zu bytes & frame info size %zu bytes", __FUNCTION__, mId,
                            mPreviewHeapCount, frameSize, sizeof(FrameInfo));

                     mPreviewHeap.clear();
                     mPreviewHeap = new Camera2Heap(bufferSize, mPreviewHeapCount,
                            "Camera2Client::PreviewHeap");
                     if (mPreviewHeap->mHeap->getSize() == 0) {
                          ALOGE("%s: Camera %d: Unable to allocate memory for preview",
                                __FUNCTION__, mId);
                          mPreviewBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                          return NO_MEMORY;
                     }

                     mPreviewHeapHead = 0;
                     mPreviewHeapFree = mPreviewHeapCount;
                }

                if (mPreviewHeapFree == 0) {
                     ALOGE("%s: Camera %d: No free preview buffers, dropping frame",
                           __FUNCTION__, mId);
                     mPreviewBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                     return NO_MEMORY;
                }
                mPreviewHeapFree--;

                heapIdx = mPreviewHeapHead;
                mPreviewHeapHead = (mPreviewHeapHead + 1) % mPreviewHeapCount;
                previewHeap = mPreviewHeap;

                ssize_t offset;
                size_t size;
                sp<IMemoryHeap> heap =
                    mPreviewHeap->mBuffers[heapIdx]->getMemory(&offset, &size);
                uint8_t *data = (uint8_t*)heap->getBase() + offset;

                ALOGV("%s: camera %d, preview stream %d,copy the frame data %p size %d into Imomery dataspace",
                       __FUNCTION__,mId,mPreviewStreamId,bufferInfo->vaddr,bufferSize);
                /*copy the frame data to Imomery dataspace*/
                memcpy(data, &bufferInfo->frameInfo, sizeof(FrameInfo));
                memcpy(data + sizeof(FrameInfo), bufferInfo->vaddr, frameSize);

                mPreviewBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
            }
        } else {
            ALOGV("%s: Camera %d stream discarding frames since invalid data %p %p",
                __FUNCTION__, mId, mPreviewBufferHandler, mVideoBufferHandler);
            if (mPreviewBufferHandler != NULL)
                mPreviewBufferHandler->QueueAllFreeBuffer();
            if (mVideoBufferHandler != NULL)
                mVideoBufferHandler->QueueAllFreeBuffer();

            return UNKNOWN_ERROR;
        }

    }

    // Call outside parameter lock to allow re-entrancy from notification
    {
        Camera2Client::SharedCameraCallbacks::Lock
            l(client->mSharedCameraCallbacks);

        /* rawplaine16 used to meet special sensor, such as tof.
        */
        if ((bufferInfo->streamType == STREAM_PREVIEW)
             ||(bufferInfo->streamType == STREAM_DEPTH_RAWPLAINE16)) {
            if (client->getStatus() == Camera2Client::State::PREVIEW_ACTIVE) {
                l.mRemoteCallback->dataCallbackTimestamp(timestamp,
                    CAMERA_MSG_PREVIEW_FRAME,
                    mPreviewHeap->mBuffers[heapIdx]);
                mPreviewHeapFree++;
                ALOGV("%s: Camera %d msg type %d Invoking producer client(pid %d) data callback timestamp %llu",
                    __FUNCTION__, mId, CAMERA_MSG_PREVIEW_FRAME, client->getClientPid(), timestamp);
            } else {
                ALOGV("%s: Camera %d msg type %d skip producer client(pid %d) data callback timestamp %llu",
                    __FUNCTION__, mId, CAMERA_MSG_PREVIEW_FRAME, client->getClientPid(), timestamp);
            }
        } else if (bufferInfo->streamType == STREAM_RECORD ||
                   bufferInfo->streamType == STREAM_RECORD_HFR) {
            if (client->getStatus() == Camera2Client::State::VIDEO_ACTIVE) {
                l.mRemoteCallback->dataCallbackTimestamp(timestamp,
                    CAMERA_MSG_VIDEO_FRAME,
                    mRecordingHeap->mBuffers[heapIdx]);
                mRecordingHeapFree++;
                ALOGV("%s: Camera %d msg type %d Invoking producer client(pid %d) data callback timestamp %llu",
                    __FUNCTION__, mId, CAMERA_MSG_VIDEO_FRAME, client->getClientPid(), timestamp);
            } else {
                ALOGV("%s: Camera %d msg type %d skip producer client(pid %d) data callback timestamp %llu",
                    __FUNCTION__, mId, CAMERA_MSG_VIDEO_FRAME, client->getClientPid(),timestamp);
            }
        }
    }

    {
        Mutex::Autolock lock(client->mConsumerClientLock);
        ALOGV("%s: consumer client size %d",__FUNCTION__, client->mConsumerClient.size());
        for (auto& i : client->mConsumerClient) {
            if (i == 0) {
                ALOGV("%s: get invalid consumer client NULL in producer",__FUNCTION__);
                continue;
            } else if (i->isProducerClient()) {
                    // skip to transfer the frame to producer again
                    continue;
            }

            {
                // Call outside parameter lock to allow re-entrancy from notification
                Camera2Client::SharedCameraCallbacks::Lock
                    l(i->mSharedCameraCallbacks);

               /* rawplaine16 used to meet special sensor, such as tof.
               */
               int msgType = CAMERA_MSG_PREVIEW_FRAME;
               if ((bufferInfo->streamType == STREAM_PREVIEW)
                   ||(bufferInfo->streamType == STREAM_DEPTH_RAWPLAINE16)) {
                   msgType = CAMERA_MSG_PREVIEW_FRAME;

               } else if (bufferInfo->streamType == STREAM_RECORD
                   ||bufferInfo->streamType== STREAM_RECORD_HFR) {
                   msgType = CAMERA_MSG_VIDEO_FRAME;

               }
               if (msgType == CAMERA_MSG_PREVIEW_FRAME) {
                   if (i->getStatus() == Camera2Client::State::PREVIEW_ACTIVE) {
                       l.mRemoteCallback->dataCallbackTimestamp(timestamp,
                           msgType,
                           mPreviewHeap->mBuffers[heapIdx]);
                       mPreviewHeapFree++;
                       ALOGV("%s: Camera %d msg type %d Invoking consumer client(pid %d) data callback timestamp %llu",
                           __FUNCTION__, mId, msgType, i->getClientPid(), timestamp);
                   } else {
                       ALOGV("%s: Camera %d msg type %d skip consumer client(pid %d) data callback timestamp %llu",
                           __FUNCTION__, mId, msgType, i->getClientPid(), timestamp);
                   }
               } else if (msgType == CAMERA_MSG_VIDEO_FRAME) {
                   if (i->getStatus() == Camera2Client::State::VIDEO_ACTIVE) {
                       l.mRemoteCallback->dataCallbackTimestamp(timestamp,
                           msgType,
                           mRecordingHeap->mBuffers[heapIdx]);
                       mRecordingHeapFree++;
                       ALOGV("%s: Camera %d msg type %d Invoking consumer client(pid %d) data callback timestamp %llu",
                           __FUNCTION__, mId, msgType, i->getClientPid(), timestamp);
                   } else {
                       ALOGV("%s: Camera %d msg type %d skip consumer client(pid %d) data callback timestamp %llu",
                           __FUNCTION__, mId, msgType, i->getClientPid(), timestamp);
                   }
               }
          }
       }
    }
    return OK;
}

status_t StreamingProcessor::processRecordingFrame() {

    return OK;
}

void StreamingProcessor::releaseAllRecordingFramesLocked() {
    mRecordingHeapHead = 0;
    mRecordingHeapFree = mRecordingHeapCount;
}

bool StreamingProcessor::isStreamActive(const Vector<int32_t> &streams,
        int32_t recordingStreamId) {
    for (size_t i = 0; i < streams.size(); i++) {
        if (streams[i] == recordingStreamId) {
            return true;
        }
    }
    return false;
}


status_t StreamingProcessor::dump(int fd, const Vector<String16>& /*args*/) {
    String8 result;

    result.append("  Current requests:\n");
    if (mPreviewRequest.entryCount() != 0) {
        result.append("    Preview request:\n");
        write(fd, result.string(), result.size());
        mPreviewRequest.dump(fd, 2, 6);
        result.clear();
    } else {
        result.append("    Preview request: undefined\n");
    }

    if (mRecordingRequest.entryCount() != 0) {
        result = "    Recording request:\n";
        write(fd, result.string(), result.size());
        mRecordingRequest.dump(fd, 2, 6);
        result.clear();
    } else {
        result = "    Recording request: undefined\n";
    }

    const char* streamTypeString[] = {
        "none", "preview", "record"
    };
    result.append(String8::format("   Active request: %s (paused: %s)\n",
                                  streamTypeString[mActiveRequest],
                                  mPaused ? "yes" : "no"));

    write(fd, result.string(), result.size());

    return OK;
}

}; // namespace camera2
}; // namespace android
