/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "Camera2-CallbackProcessor"
#define ATRACE_TAG ATRACE_TAG_CAMERA
#define LOG_NDEBUG 1

#include <utils/Log.h>
#include <utils/Trace.h>
#include <gui/Surface.h>

#include "common/CameraDeviceBase.h"
#include "api1/Camera2Client.h"
#include "api1/client2/CallbackProcessor.h"
#include "device3/Camera3StreamInterface.h"
#define ALIGN(x, mask) ( ((x) + (mask) - 1) & ~((mask) - 1) )

namespace android {
namespace camera2 {

CallbackProcessor::CallbackProcessor(sp<Camera2Client> client):
        Thread(false),
        mClient(client),
        mDevice(client->getCameraDevice()),
        mId(client->getCameraId()),
        mCallbackAvailable(false),
        mCallbackToApp(false),
        mCallbackStreamId(NO_STREAM) {
}

CallbackProcessor::~CallbackProcessor() {
    ALOGV("%s: Exit", __FUNCTION__);
    deleteStream();
}

void CallbackProcessor::onFrameAvailable() {
    Mutex::Autolock l(mInputMutex);
    if (!mCallbackAvailable) {
        mCallbackAvailable = true;
        mCallbackAvailableSignal.signal();
    }
}

status_t CallbackProcessor::setCallbackWindow(
        sp<Surface> callbackWindow) {
    ATRACE_CALL();
    status_t res;
    Mutex::Autolock l(mInputMutex);

    sp<Camera2Client> client = mClient.promote();
    if (client == 0) return OK;
    sp<CameraDeviceBase> device = client->getCameraDevice();

    // If the window is changing, clear out stream if it already exists
    if (mCallbackWindow != callbackWindow && mCallbackStreamId != NO_STREAM) {
        res = device->deleteStream(mCallbackStreamId);
        if (res != OK) {
            ALOGE("%s: Camera %d: Unable to delete old stream "
                    "for callbacks: %s (%d)", __FUNCTION__,
                    client->getCameraId(), strerror(-res), res);
            return res;
        }
        mCallbackStreamId = NO_STREAM;
        mCallbackConsumer.clear();
    }
    mCallbackWindow = callbackWindow;
    mCallbackToApp = (mCallbackWindow != NULL);

    return OK;
}

status_t CallbackProcessor::updateStream(const Parameters &params) {
    ATRACE_CALL();
    status_t res;

    Mutex::Autolock l(mInputMutex);

    sp<CameraDeviceBase> device = mDevice.promote();
    if (device == 0) {
        ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    // If possible, use the flexible YUV format
    int32_t callbackFormat = params.previewFormat;
    if (mCallbackToApp) {
        // TODO: etalvala: This should use the flexible YUV format as well, but
        // need to reconcile HAL2/HAL3 requirements.
        callbackFormat = HAL_PIXEL_FORMAT_YV12;
    } else if(params.fastInfo.useFlexibleYuv &&
            (params.previewFormat == HAL_PIXEL_FORMAT_YCrCb_420_SP ||
             params.previewFormat == HAL_PIXEL_FORMAT_YV12) ) {
        callbackFormat = HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED;
    }
    
    ALOGV("%s: callbackFormat %d, params.previewFormat %d", __FUNCTION__,callbackFormat, params.previewFormat);
    
    if (!mCallbackToApp && mCallbackConsumer == 0) {
        // Create CPU buffer queue endpoint, since app hasn't given us one
        // Make it async to avoid disconnect deadlocks
        sp<IGraphicBufferProducer> producer;
        sp<IGraphicBufferConsumer> consumer;
    }

    if (mCallbackStreamId != NO_STREAM) {
        // Check if stream parameters have to change
        uint32_t currentWidth, currentHeight, currentFormat;
        res = device->getStreamInfo(mCallbackStreamId,
                &currentWidth, &currentHeight, &currentFormat, 0);
        if (res != OK) {
            ALOGE("%s: Camera %d: Error querying callback output stream info: "
                    "%s (%d)", __FUNCTION__, mId,
                    strerror(-res), res);
            return res;
        }
        if (currentWidth != (uint32_t)params.previewWidth ||
                currentHeight != (uint32_t)params.previewHeight ||
                currentFormat != (uint32_t)callbackFormat) {
            // Since size should only change while preview is not running,
            // assuming that all existing use of old callback stream is
            // completed.
            ALOGV("%s: Camera %d: Deleting stream %d since the buffer "
                    "parameters changed", __FUNCTION__, mId, mCallbackStreamId);
            res = device->deleteStream(mCallbackStreamId);
            if (res != OK) {
                ALOGE("%s: Camera %d: Unable to delete old output stream "
                        "for callbacks: %s (%d)", __FUNCTION__,
                        mId, strerror(-res), res);
                return res;
            }
            mCallbackStreamId = NO_STREAM;
        }
    }

    if (mCallbackStreamId == NO_STREAM) {
        ALOGV("Creating callback stream: %d x %d, format 0x%x, API format 0x%x",
                params.previewWidth, params.previewHeight,
                callbackFormat, params.previewFormat);
        StreamType streamType;
        android_dataspace dataSpace;
        if (callbackFormat == HAL_PIXEL_FORMAT_Y16) {
            streamType = STREAM_DEPTH_Y16;
            dataSpace  = HAL_DATASPACE_DEPTH;
        } else if (callbackFormat == HAL_PIXEL_FORMAT_RAW16) {
            streamType = STREAM_DEPTH_RAWPLAINE16;
            dataSpace  = HAL_DATASPACE_JFIF;
        } else if (callbackFormat == HAL_PIXEL_FORMAT_RAW10) {
            streamType = STREAM_PREVIEW_RAW10;
            dataSpace  = HAL_DATASPACE_JFIF;
        } else {
            streamType = STREAM_PREVIEW;
            dataSpace  = HAL_DATASPACE_JFIF;
        }

        res = device->createStream(
              params.previewWidth, params.previewHeight,
              callbackFormat, dataSpace, streamType,
              CAMERA3_STREAM_ROTATION_0, &mCallbackStreamId);

        if (res != OK) {
            ALOGE("%s: Camera %d: Can't create output stream for callbacks: "
                  "%s (%d)", __FUNCTION__, mId,
                  strerror(-res), res);
            return res;
        }

        mBufferHandler = device->getbufferhandler(mCallbackStreamId);
        if (mBufferHandler == NULL) {
            ALOGE("%s camera %d callback stream %d get buffer handler failed",
                __FUNCTION__, mId, mCallbackStreamId);
            return INVALID_OPERATION;
        }

        ALOGV("%s: buffer handler %p mCallbackStreamId %d", __FUNCTION__,mBufferHandler, mCallbackStreamId);
        mBufferHandler->setFrameAvailableListener(this);
    }

    return OK;
}

status_t CallbackProcessor::deleteStream() {
    ATRACE_CALL();
    sp<CameraDeviceBase> device;
    status_t res;
    {
        Mutex::Autolock l(mInputMutex);

        if (mCallbackStreamId == NO_STREAM) {
            return OK;
        }
        device = mDevice.promote();
        if (device == 0) {
            ALOGE("%s: Camera %d: Device does not exist", __FUNCTION__, mId);
            return INVALID_OPERATION;
        }
    }
    res = device->waitUntilDrained();
    if (res != OK) {
        ALOGE("%s: Error waiting for HAL to drain: %s (%d)",
                __FUNCTION__, strerror(-res), res);
        return res;
    }

    res = device->deleteStream(mCallbackStreamId);
    if (res != OK) {
        ALOGE("%s: Unable to delete callback stream: %s (%d)",
                __FUNCTION__, strerror(-res), res);
        return res;
    }

    {
        Mutex::Autolock l(mInputMutex);

        ALOGV("%s: Camera %d: clear callback heap", __FUNCTION__, mId);
        mCallbackHeap.clear();
        mCallbackWindow.clear();
        mCallbackConsumer.clear();

        mCallbackStreamId = NO_STREAM;
    }
    return OK;
}

int CallbackProcessor::getStreamId() const {
    Mutex::Autolock l(mInputMutex);
    return mCallbackStreamId;
}

void CallbackProcessor::dump(int /*fd*/, const Vector<String16>& /*args*/) const {
}

bool CallbackProcessor::threadLoop() {
    status_t res;
    {
        Mutex::Autolock l(mInputMutex);
        while (!mCallbackAvailable) {
            res = mCallbackAvailableSignal.waitRelative(mInputMutex,
                    kWaitDuration);
            if (res == TIMED_OUT) {
                return true;
            }
        }
        mCallbackAvailable = false;
    }

    do {
        sp<Camera2Client> client = mClient.promote();
        if (client == 0) {
            res = discardNewCallback();
        } else {
            res = processNewCallback(client);
        }
    } while (res == OK);
    return true;
}

status_t CallbackProcessor::discardNewCallback() {
    ATRACE_CALL();
    status_t res;

    ALOGV("%s:", __FUNCTION__);
    if (mCallbackStreamId == NO_STREAM) {
        ALOGE("%s: Camera %d:No stream is available"
                , __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    Mutex::Autolock m(mInputMutex);
    buffer_handle_t* buffer = mBufferHandler->GetOneBusyBuffer();
    if (NULL == buffer) {
        ALOGE("%s: Camera %d stream %d : No aviable busy buffer",
            __FUNCTION__, mId,mCallbackStreamId);
        return NOT_ENOUGH_DATA;
    }

    BufferHandlerInfo* bufferInfo = mBufferHandler->getBufferHandlerInfo(buffer);
    if (NULL == bufferInfo) {
        ALOGE("%s: Camera %d stream %d : No aviable busy buffer info",
            __FUNCTION__, mId,mCallbackStreamId);
        return NOT_ENOUGH_DATA;
    }

    if (mBufferHandler) {
        mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
    } else {
        ALOGE("%s camera %d callback stream %d buffer handler is null",
            __FUNCTION__, mId, mCallbackStreamId);
        return INVALID_OPERATION;
    }

    return OK;
}

status_t CallbackProcessor::processNewCallback(sp<Camera2Client> &client) {
    ATRACE_CALL();
    status_t res;

    sp<Camera2Heap> callbackHeap;
    size_t heapIdx;
    nsecs_t timestamp; 
    BufferHandlerInfo* bufferInfo;

    if (mCallbackStreamId == NO_STREAM) {
        ALOGE("%s: Camera %d:No stream is available"
                , __FUNCTION__, mId);
        return INVALID_OPERATION;
    }

    {
        /* acquire SharedParameters before mMutex so we don't dead lock
            with Camera2Client code calling into StreamingProcessor */
        SharedParameters::Lock l(client->getParameters());
        Mutex::Autolock m(mInputMutex);
        if (mBufferHandler == NULL) {
            ALOGE("%s camera %d callback stream %d buffer handler is null",
                        __FUNCTION__, mId, mCallbackStreamId);
            return INVALID_OPERATION;
        }

        buffer_handle_t* buffer = mBufferHandler->GetOneBusyBuffer();
        if (NULL == buffer) {
            ALOGV("%s: Camera %d stream %d : No aviable busy buffer",
                  __FUNCTION__, mId,mCallbackStreamId);
            return NOT_ENOUGH_DATA;
        }
        bufferInfo = mBufferHandler->getBufferHandlerInfo(buffer);
        if ( NULL == bufferInfo ) {
            ALOGV("%s: Camera %d stream %d : No aviable busy buffer info",
                  __FUNCTION__, mId,mCallbackStreamId);

            mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
            return NOT_ENOUGH_DATA;
        }

        BufferHandlerInfo imgBuffer = *bufferInfo;
        timestamp = bufferInfo->timestamp;

        if ( l.mParameters.state != Parameters::PREVIEW
                && l.mParameters.state != Parameters::RECORD
                && l.mParameters.state != Parameters::STILL_CAPTURE
                && l.mParameters.state != Parameters::VIDEO_SNAPSHOT) {
            ALOGI("%s: Camera %d: No longer streaming",
                    __FUNCTION__, mId);

            mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
            return INVALID_OPERATION;
        }

        if (imgBuffer.frameInfo.width != static_cast<uint32_t>(l.mParameters.previewWidth) ||
                imgBuffer.frameInfo.height != static_cast<uint32_t>(l.mParameters.previewHeight)) {
            ALOGW("%s: The preview size has changed to %d x %d from %d x %d, this buffer is"
                    " no longer valid, dropping",__FUNCTION__,
                    l.mParameters.previewWidth, l.mParameters.previewHeight,
                    imgBuffer.frameInfo.width, imgBuffer.frameInfo.height);

            mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);

            return INVALID_OPERATION;
        }

        size_t frameSize = bufferInfo->frameInfo.size;
        size_t bufferSize = frameSize + sizeof(FrameInfo);
        size_t currentBufferSize = (mCallbackHeap == 0) ?
            0 : (mCallbackHeap->mHeap->getSize() / kCallbackHeapCount);

        if (bufferSize > currentBufferSize) {
            ALOGV("%s: Camera %d: Creating callback heap with %zu buffers of "
                "frame size %zu bytes & frame info size %zu bytes", __FUNCTION__, mId,
                kCallbackHeapCount, frameSize, sizeof(FrameInfo));
            mCallbackHeap.clear();
            mCallbackHeap = new Camera2Heap(bufferSize, kCallbackHeapCount,
                    "Camera2Client::CallbackHeap");
            if (mCallbackHeap->mHeap->getSize() == 0) {
                ALOGE("%s: Camera %d: Unable to allocate memory for callbacks",
                        __FUNCTION__, mId);

                mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
                raise(SIGSEGV); //trigger dump
                return NO_MEMORY;
            }

            mCallbackHeapHead = 0;
            mCallbackHeapFree = kCallbackHeapCount;
        }

        if (mCallbackHeapFree == 0) {
            ALOGE("%s: Camera %d: No free callback buffers, dropping frame",
                    __FUNCTION__, mId);

            mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
            return OK;
        }
        /*get the heapidx from callback heap head*/
        heapIdx = mCallbackHeapHead;
        /*callback heap head increase*/
        mCallbackHeapHead = (mCallbackHeapHead + 1) % kCallbackHeapCount;
        mCallbackHeapFree--;

        // TODO: Get rid of this copy by passing the gralloc queue all the way
        // to app

        ssize_t offset;
        size_t size;
        /*get the Imomery by heapidx*/
        sp<IMemoryHeap> heap =
                mCallbackHeap->mBuffers[heapIdx]->getMemory(&offset,
                        &size);
        uint8_t *data = (uint8_t*)heap->getBase() + offset;

        {
            ALOGV("%s: Camera %d, stream %d,copy the frame data %p size %d into Imomery dataspace",
                  __FUNCTION__,mId,mCallbackStreamId,imgBuffer.vaddr,bufferSize);
            memcpy(data, &bufferInfo->frameInfo, sizeof(FrameInfo));
            memcpy(data + sizeof(FrameInfo), imgBuffer.vaddr, frameSize);
        }


        mBufferHandler->PutOneFreeBuffer(bufferInfo->bufHandle);
        // mCallbackHeap may get freed up once input mutex is released
        callbackHeap = mCallbackHeap;
    }

    {
        // Call outside parameter lock to allow re-entrancy from notification
        Camera2Client::SharedCameraCallbacks::Lock
            l(client->mSharedCameraCallbacks);

        int msgType = CAMERA_MSG_PREVIEW_FRAME;
        if ((bufferInfo->streamType == STREAM_DEPTH_Y16)
             || (bufferInfo->streamType == STREAM_PREVIEW_RAW10)
             || (bufferInfo->streamType == STREAM_DEPTH_RAWPLAINE16)) {
            msgType = CAMERA_MSG_RAW_IMAGE;
        }

        if ((client->getStatus() == Camera2Client::State::PREVIEW_ACTIVE)
            ||(client->getStatus() == Camera2Client::State::VIDEO_ACTIVE)) {
            l.mRemoteCallback->dataCallbackTimestamp(timestamp,
                msgType,
                callbackHeap->mBuffers[heapIdx]);
            ALOGV("%s: Camera %d: Invoking producer client(pid %d) data callback timestamp %llu",
                __FUNCTION__, mId, client->getClientPid(), timestamp);
        } else {
            ALOGV("%s: Camera %d: skip producer client(pid %d) data callback timestamp %llu",
                __FUNCTION__, mId, client->getClientPid(), timestamp);
        }
    }

    {
        Mutex::Autolock lock(client->mConsumerClientLock);
        ALOGV("%s: consumer client size %d",__FUNCTION__, client->mConsumerClient.size());
        for (auto& i : client->mConsumerClient) {
            if (i == 0) {
                ALOGV("%s: get invalid consumer client NULL in producer",__FUNCTION__);
                continue;
            } else if (i->isProducerClient()) {
                    // skip to transfer the frame to producer again
                    continue;
            }

            // Call outside parameter lock to allow re-entrancy from notification
            Camera2Client::SharedCameraCallbacks::Lock
                l(i->mSharedCameraCallbacks);
            {
                int msgType = CAMERA_MSG_PREVIEW_FRAME;
                if ((bufferInfo->streamType == STREAM_DEPTH_Y16)
                    || (bufferInfo->streamType == STREAM_PREVIEW_RAW10)
                    || (bufferInfo->streamType == STREAM_DEPTH_RAWPLAINE16)) {
                    msgType = CAMERA_MSG_RAW_IMAGE;
                }

                if ((i->getStatus() == Camera2Client::State::PREVIEW_ACTIVE)
                    ||(i->getStatus() == Camera2Client::State::VIDEO_ACTIVE)) {
                    l.mRemoteCallback->dataCallbackTimestamp(timestamp,
                        msgType,
                        callbackHeap->mBuffers[heapIdx]);
                    ALOGV("%s: Camera %d: Invoking consumer client(pid %d) data callback timestamp %llu",
                        __FUNCTION__, mId, i->getClientPid(), timestamp);
                } else {
                    ALOGV("%s: Camera %d: skip consumer client(pid %d) data callback timestamp %llu",
                        __FUNCTION__, mId, i->getClientPid(), timestamp);
                }
            }
        }
    }

    // Only increment free if we're still using the same heap
    mCallbackHeapFree++;
    ALOGV("%s: Camera %d: exit consumer client data callback timestamp %llu",
                        __FUNCTION__, mId, timestamp);
    return OK;
}

status_t CallbackProcessor::convertFromFlexibleYuv(int32_t previewFormat,
        uint8_t *dst,
        const CpuConsumer::LockedBuffer &src,
        uint32_t dstYStride,
        uint32_t dstCStride) const {

    if (previewFormat != HAL_PIXEL_FORMAT_YCrCb_420_SP &&
            previewFormat != HAL_PIXEL_FORMAT_YV12) {
        ALOGE("%s: Camera %d: Unexpected preview format when using "
                "flexible YUV: 0x%x", __FUNCTION__, mId, previewFormat);
        return INVALID_OPERATION;
    }

    // Copy Y plane, adjusting for stride
    const uint8_t *ySrc = src.data;
    uint8_t *yDst = dst;
    for (size_t row = 0; row < src.height; row++) {
        memcpy(yDst, ySrc, src.width);
        ySrc += src.stride;
        yDst += dstYStride;
    }

    // Copy/swizzle chroma planes, 4:2:0 subsampling
    const uint8_t *cbSrc = src.dataCb;
    const uint8_t *crSrc = src.dataCr;
    size_t chromaHeight = src.height / 2;
    size_t chromaWidth = src.width / 2;
    ssize_t chromaGap = src.chromaStride -
            (chromaWidth * src.chromaStep);
    size_t dstChromaGap = dstCStride - chromaWidth;

    if (previewFormat == HAL_PIXEL_FORMAT_YCrCb_420_SP) {
        // Flexible YUV chroma to NV21 chroma
        uint8_t *crcbDst = yDst;
        // Check for shortcuts
        if (cbSrc == crSrc + 1 && src.chromaStep == 2) {
            ALOGV("%s: Fast NV21->NV21", __FUNCTION__);
            // Source has semiplanar CrCb chroma layout, can copy by rows
            for (size_t row = 0; row < chromaHeight; row++) {
                memcpy(crcbDst, crSrc, src.width);
                crcbDst += src.width;
                crSrc += src.chromaStride;
            }
        } else {
            ALOGV("%s: Generic->NV21", __FUNCTION__);
            // Generic copy, always works but not very efficient
            for (size_t row = 0; row < chromaHeight; row++) {
                for (size_t col = 0; col < chromaWidth; col++) {
                    *(crcbDst++) = *crSrc;
                    *(crcbDst++) = *cbSrc;
                    crSrc += src.chromaStep;
                    cbSrc += src.chromaStep;
                }
                crSrc += chromaGap;
                cbSrc += chromaGap;
            }
        }
    } else {
        // flexible YUV chroma to YV12 chroma
        ALOG_ASSERT(previewFormat == HAL_PIXEL_FORMAT_YV12,
                "Unexpected preview format 0x%x", previewFormat);
        uint8_t *crDst = yDst;
        uint8_t *cbDst = yDst + chromaHeight * dstCStride;
        if (src.chromaStep == 1) {
            ALOGV("%s: Fast YV12->YV12", __FUNCTION__);
            // Source has planar chroma layout, can copy by row
            for (size_t row = 0; row < chromaHeight; row++) {
                memcpy(crDst, crSrc, chromaWidth);
                crDst += dstCStride;
                crSrc += src.chromaStride;
            }
            for (size_t row = 0; row < chromaHeight; row++) {
                memcpy(cbDst, cbSrc, chromaWidth);
                cbDst += dstCStride;
                cbSrc += src.chromaStride;
            }
        } else {
            ALOGV("%s: Generic->YV12", __FUNCTION__);
            // Generic copy, always works but not very efficient
            for (size_t row = 0; row < chromaHeight; row++) {
                for (size_t col = 0; col < chromaWidth; col++) {
                    *(crDst++) = *crSrc;
                    *(cbDst++) = *cbSrc;
                    crSrc += src.chromaStep;
                    cbSrc += src.chromaStep;
                }
                crSrc += chromaGap;
                cbSrc += chromaGap;
                crDst += dstChromaGap;
                cbDst += dstChromaGap;
            }
        }
    }

    return OK;
}

}; // namespace camera2
}; // namespace android
