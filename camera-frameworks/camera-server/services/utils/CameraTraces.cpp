/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "CameraTraces"

#include "utils/CameraTraces.h"

namespace android {

//Mutex gTracesLock;
//Mutex gSignalLock;

status_t CameraTraces::dump(FILE *fd)
{
    //Mutex::Autolock al(gTracesLock);

    int j, nptrs;
    void *buffer[MAX_TRACES];
    char **strings;

    nptrs = backtrace(buffer, MAX_TRACES);

    TRACE_PRINT("backtrace() returned %d addresses\n", nptrs);

    strings = backtrace_symbols(buffer, nptrs);
    if (strings == NULL) {
        perror("backtrace_symbols");
        _exit(EXIT_FAILURE);
    }

    if (fd) {
        char temp[32] = {0x00};
        snprintf(temp, sizeof(temp), "\n[Trace Dump]\n");
        fwrite(temp, strlen(temp), 1, fd);
    }

    for (j = 0; j < nptrs; j++) {
        TRACE_PRINT("[%02d] %s", j, strings[j]);
        if (fd) {
            fwrite(strings[j], strlen(strings[j]), 1, fd);
            fwrite("\n", 1, 1, fd);
        }
    }

    free(strings);

    return OK;
}

void CameraTraces::signal_handler(int signo)
{
    TRACE_PRINT("=========>>>catch signal %d <<<=========", signo);

    //Mutex::Autolock al(gSignalLock);

    /// Use static flag to avoid being called repeatedly.
    static volatile bool isHandling[SIGALRM + 1] = {false};
    if (true == isHandling[signo]) {
        return;
    }
    isHandling[signo] = true;

    if (SIGINT == signo) {
        _exit(0);
    }

    char fileName[128] = {0x00};
    char buff[256] = {0x00};

    /// Dump trace and r-xp maps
    snprintf(fileName, sizeof(fileName), "%s/trace_%d[%d].txt", TRACE_DUMP_DIR, getpid(), signo);
    FILE* fd = fopen(fileName, "wb");
    if (!fd) {
        TRACE_PRINT("fopen failed for %s", fileName);
        return;
    }

    snprintf(buff, sizeof(buff), "cat /proc/%d/maps | grep '/usr' | grep 'r-xp' | tee %s", getpid(), fileName);
    TRACE_PRINT("Run cmd: %s", buff);
    system((const char*) buff);

    fseek(fd, 0, SEEK_END);
    TRACE_PRINT("Dump stack start...");
    CameraTraces::dump(fd);
    TRACE_PRINT("Dump stack end...");
    fclose(fd);

    /// Dump fd
    snprintf(fileName, sizeof(fileName), "%s/fd_%d[%d].txt", TRACE_DUMP_DIR, getpid(), signo);
    snprintf(buff, sizeof(buff), "ls -l /proc/%d/fd > %s", getpid(), fileName);
    TRACE_PRINT("Run cmd: %s", buff);
    system((const char*) buff);

    /// Dump whole maps
    snprintf(fileName, sizeof(fileName), "%s/maps_%d[%d].txt", TRACE_DUMP_DIR, getpid(), signo);
    snprintf(buff, sizeof(buff), "cat /proc/%d/maps > %s", getpid(), fileName);
    TRACE_PRINT("Run cmd: %s", buff);
    system((const char*) buff);

    /// Dump meminfo
    snprintf(fileName, sizeof(fileName), "%s/meminfo_%d[%d].txt", TRACE_DUMP_DIR, getpid(), signo);
    snprintf(buff, sizeof(buff), "cat /proc/meminfo > %s", fileName);
    TRACE_PRINT("Run cmd: %s", buff);
    system((const char*) buff);

    /// Dump ps
    snprintf(fileName, sizeof(fileName), "%s/ps_%d[%d].txt", TRACE_DUMP_DIR, getpid(), signo);
    snprintf(buff, sizeof(buff), "ps -T > %s", fileName);
    TRACE_PRINT("Run cmd: %s", buff);
    system((const char*) buff);

    /// Dump top
    snprintf(fileName, sizeof(fileName), "%s/top_%d[%d].txt", TRACE_DUMP_DIR, getpid(), signo);
    snprintf(buff, sizeof(buff), "top -b -n 1 > %s", fileName);
    TRACE_PRINT("Run cmd: %s", buff);
    system((const char*) buff);

    _exit(0);
}

status_t CameraTraces::registerSigMonitor()
{
    int sigArray[SIG_MONITOR_SIZE] =
    {
        SIGINT,     // 2
        SIGILL,     // 4
        SIGABRT,    // 6
        SIGBUS,     // 7
        SIGFPE,     // 8
        SIGSEGV,    // 11
        SIGPIPE,    // 13
        SIGALRM,    // 14
    };

    /// Sig stack configure
    static stack_t    sigseg_stack;
    static char       stack_body[STACK_BODY_SIZE];
    sigseg_stack.ss_sp      = stack_body;
    sigseg_stack.ss_flags = SS_ONSTACK;
    sigseg_stack.ss_size  = sizeof(stack_body);
    assert(!sigaltstack(&sigseg_stack, NULL));

    /// Regiser handler of sigaction
    static struct  sigaction sig;
    sig.sa_handler   = CameraTraces::signal_handler;
    sig.sa_flags     = SA_ONSTACK;

    /// Add blocking signals
    /// TODO: still can not block signal SIGSEGV ?
    sigemptyset(&sig.sa_mask);
    for (int i = 0; i < SIG_MONITOR_SIZE; i++) {
        sigaddset(&sig.sa_mask, sigArray[i]);
    }

    /// Register handler for signals
    for (int i = 0; i < SIG_MONITOR_SIZE; i++) {
        if(0 != sigaction(sigArray[i], &sig, NULL)) {
            TRACE_PRINT("monitor signal %d failed", sigArray[i]);
        }
    }

    ALOGI("Monitoring...");

    return OK;
}

}; // namespace android
