/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ANDROID_SERVERS_CAMERA_TRACES_H_
#define ANDROID_SERVERS_CAMERA_TRACES_H_

#include <signal.h>
#include <execinfo.h>
#include <cassert>
#include <cstdlib>
#include <string>

#include <utils/Errors.h>
#include <utils/Mutex.h>
#include <utils/Log.h>
#include <string.h>

namespace android {

#define TRACE_PRINT(fmt, args...) do { \
    ALOGI("%s %d:" fmt, __func__, __LINE__, ##args);  \
    printf(fmt "\n", ##args); \
} while (0)

#define TRACE_DUMP_DIR "/data/misc/camera"

// Collect a list of the process's stack traces
class CameraTraces {
public:

    static status_t dump(FILE   *fd = NULL);
    static status_t registerSigMonitor();

private:
    enum {
        SIG_MONITOR_SIZE  = 8,
        MAX_TRACES        = 100,
        STACK_BODY_SIZE   = (64*1024),
    };

    CameraTraces();
    ~CameraTraces();
    static void signal_handler(int signo);
}; // class CameraTraces

}
#endif // ANDROID_SERVERS_CAMERA_TRACES_H_
