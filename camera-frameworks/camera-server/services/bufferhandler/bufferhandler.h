/* Copyright (c) 2019, The Linux Foundation. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __BUFFER_MANAGER__
#define __BUFFER_MANAGER__


#include <deque>
#include <mutex>

#include <stddef.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <linux/ion.h>
#include <linux/msm_ion.h>
#include <condition_variable>
#include <cutils/native_handle.h>
#include <utils/RefBase.h>
#include <utils/Log.h>
#include <utils/Timers.h>
#include <utils/Errors.h>
#include <hardware/camera_common.h>

#define LOG_NDEBUG 1
using namespace std;
using namespace android;

#define  BUFFER_QUEUE_DEPTH  24
#define  TIMEOUT_WAIT_MS(ms) (chrono::milliseconds((ms)))

typedef enum {
    STREAM_DEFAULT          = 0,
    STREAM_PREVIEW          = 1,
    STREAM_RECORD           = 2,
    STREAM_RECORD_HFR       = 3, // for batchSize >= 2
    STREAM_SNAPSHOT_NONZSL  = 4,
    STREAM_SNAPSHOT_ZSL     = 5,
    STREAM_DEPTH_Y16        = 6,
    STREAM_DEPTH_RAWPLAINE16= 7,
    STREAM_PREVIEW_RAW10    = 8,
} StreamType;

#define STREAM_PREVIEW_BUF_NUM           8
#define STREAM_VIDEO_BUF_NUM             8
#define STREAM_VIDEO_HFR_BUF_NUM         16
#define STREAM_NON_ZSL_BUF_NUM           2
#define STREAM_ZSL_BUF_NUM               2
#define STREAM_DEPTH_Y16_BUF_NUM         7
#define STREAM_DEPTH_RAWPLAINE16_BUF_NUM 7
#define STREAM_PREVIEW_RAW10_BUF_NUM     7
#define STREAM_BUF_NUM_DEFAULT           7

typedef const native_handle_t* buffer_handle_t;

typedef struct _frameInfo {
    uint32_t                   size;
    uint32_t                   width;
    uint32_t                   height;
    uint32_t                   stride;
    uint32_t                   slice;
    uint32_t                   format;
} FrameInfo;

typedef struct _BufferInfo{
    void*                      vaddr;
    buffer_handle_t*           bufHandle;
    int                        fd;
    uint32_t                   idx;
    uint32_t                   maxCount;
    FrameInfo                  frameInfo;
    bool                       useCache;
    bool                       isMetadataBuf;
    bool                       isUBWCBuf;
    StreamType                 streamType;
    nsecs_t                    timestamp;
    struct ion_allocation_data allocData;
} BufferHandlerInfo;

typedef struct _pthreadInfo{
    pthread_attr_t           attr;
    pthread_t                pid;
} PthreadInfo;

class BufferHandler {
public:
    struct CallbackFrameAvailableListener : public virtual RefBase {
    public:
        
        virtual void onFrameAvailable() = 0;
        virtual void onFrameReplaced() {}
    };
    BufferHandler();
    ~BufferHandler();

    // Initializes the instance
    int HandlerInitialize();
    // Destroys an instance of the class
    void HandlerDestroy();
    // Pre allocate the max number of buffers the buffer manager needs to manage
    int AllocateIONBufferList(BufferHandlerInfo bufferListInfo);

    // release all buffers
    void ReleaseAllBuffers();
    // Get a free buffer
    buffer_handle_t* GetOneFreeBuffer();

    void PutOneFreeBuffer(buffer_handle_t* buffer);
    void BufferHandler::QueueAllFreeBuffer();
    buffer_handle_t* GetOneBusyBuffer();

    void PutOneBusyBuffer(buffer_handle_t* buffer, nsecs_t timestamp);

    size_t FreeBufferListSize();

    BufferHandlerInfo* getBufferHandlerInfo(buffer_handle_t* buffer);

    int updateBufferInfo(buffer_handle_t* buffer, nsecs_t timestamp);

    void setFrameAvailableListener(
         const wp<CallbackFrameAvailableListener>& listener);

    int getTotalBufferNums() { return mNumTotalBuffers; }

private:
    static inline uint32_t ALIGN(uint32_t operand, uint32_t alignment)
    {
        uint32_t remainder = (operand % alignment);

        return (0 == remainder) ? operand : operand - remainder + alignment;
    }

    buffer_handle_t         mBuffersList[BUFFER_QUEUE_DEPTH];
    BufferHandlerInfo       mBufferinfo[BUFFER_QUEUE_DEPTH];
    BufferHandlerInfo       mAllocateInfo;
    PthreadInfo             mPthreadInfo;
    uint32_t                mNumTotalBuffers;
    deque<buffer_handle_t*> mFreeBuffersList;
    deque<buffer_handle_t*> mRequestBuffersList;
    deque<buffer_handle_t*> mBusyBuffersList;
    deque<buffer_handle_t*> mResultBuffersList;
    mutex                   mBufInfoMutex;
    mutex                   mFreeBufMutex;
    mutex                   mBusyBufMutex;
    mutex                   mFrameAvailableMutex;
    condition_variable      mFreeBufListCv;
    //condition_variable      mBusyBufListCv;
    wp<CallbackFrameAvailableListener> mFrameAvailableListener;

    int                     mIonFd;

    // Do not support the copy constructor or assignment operator
    BufferHandler(const BufferHandler&) = delete;
    BufferHandler& operator= (const BufferHandler&) = delete;

    static void *AllocateThread(void *data);

    // Allocate one ION buffer
    int AllocateOneIONBuffer(BufferHandlerInfo allocInfo,
                                       buffer_handle_t*   pAllocatedBuffer,
                                       uint32_t           index);

    int SyncCache(BufferHandlerInfo* info, unsigned int cmd);

};

#endif //__BUFFER_MANAGER__

