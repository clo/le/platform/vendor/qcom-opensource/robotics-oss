/* Copyright (c) 2019, The Linux Foundation. All rights reserved.

 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "bufferhandler.h"

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "BufferHandler"

#ifdef ALOGV
#undef ALOGV
#endif

#define ALOGV(...) (void(0))

using namespace std;
using namespace android;

BufferHandler::BufferHandler():\
mNumTotalBuffers(0)
{

    HandlerInitialize();
    mIonFd = -1;
}

BufferHandler::~BufferHandler()
{
    HandlerDestroy();
}

void BufferHandler::HandlerDestroy()
{
    ReleaseAllBuffers();

    close(mIonFd);

}

int BufferHandler::HandlerInitialize()
{
    int result = 0;
    for (int i = 0; i < BUFFER_QUEUE_DEPTH; i++) {
        mBuffersList[i] = NULL;
    }
    return result;
}

/* Buffer Sequence:
   GetOneFreeBuffer: free    --> request
   PutOneBusyBuffer: request --> busy
   GetOneBusyBuffer: busy    --> result
   PutOneFreeBuffer: result  --> free
   Total buffers = mFreeBuffersList + mRequestBuffersList + mBusyBuffersList + mResultBuffersList
*/
buffer_handle_t* BufferHandler::GetOneFreeBuffer()
{
    std::unique_lock<std::mutex> lock(mFreeBufMutex);
    if (mFreeBuffersList.empty()) {
        if (cv_status::timeout == mFreeBufListCv.wait_for(lock, TIMEOUT_WAIT_MS(200))
            || mFreeBuffersList.empty()) {
            ALOGW("%s waiting timeout for free buffers... streamType:%d free/req/busy/res:%d/%d/%d/%d",
                __FUNCTION__,
                mBufferinfo[0].streamType,
                mFreeBuffersList.size(),
                mRequestBuffersList.size(),
                mBusyBuffersList.size(),
                mResultBuffersList.size());
            return NULL;
        }
    }
    buffer_handle_t* buffer = mFreeBuffersList.front();
    mFreeBuffersList.pop_front();
    mRequestBuffersList.push_back(buffer);

    SyncCache(getBufferHandlerInfo(buffer), ION_IOC_CLEAN_INV_CACHES);

    ALOGV("%s: streamType:%d free/req/busy/res:%d/%d/%d/%d",
        __FUNCTION__,
        mBufferinfo[0].streamType,
        mFreeBuffersList.size(),
        mRequestBuffersList.size(),
        mBusyBuffersList.size(),
        mResultBuffersList.size());

    return buffer;
}

void BufferHandler::PutOneBusyBuffer(buffer_handle_t* buffer, nsecs_t timestamp)
{
    std::unique_lock<std::mutex> lockf(mFrameAvailableMutex);
    sp<CallbackFrameAvailableListener> listener = mFrameAvailableListener.promote();
    {
        // scope for the lock
        std::unique_lock<std::mutex> lock1(mBusyBufMutex);

        if (mRequestBuffersList.empty()) {
            ALOGE("%s error: empty for mRequestBuffersList", __func__);
        } else {
            mRequestBuffersList.pop_front();
        }

        mBusyBuffersList.push_back(buffer);
        updateBufferInfo(buffer, timestamp);

        ALOGV("%s: streamType:%d fd:%d free/req/busy/res:%d/%d/%d/%d",
            __FUNCTION__,
            mBufferinfo[0].streamType,
            (*buffer)->data[0],
            mFreeBuffersList.size(),
            mRequestBuffersList.size(),
            mBusyBuffersList.size(),
            mResultBuffersList.size());
    }

    if (listener != NULL) {
        listener->onFrameAvailable();
    } else {
        ALOGW("%s no listener, directly to free list", __func__);
        PutOneFreeBuffer(buffer);
    }

    SyncCache(getBufferHandlerInfo(buffer), ION_IOC_CLEAN_INV_CACHES);
}

buffer_handle_t* BufferHandler::GetOneBusyBuffer()
{
   std::unique_lock<std::mutex> lock(mBusyBufMutex);
   if (mBusyBuffersList.empty()) {
       /// Cannot wait here.
       /// Otherwise, all cb functions of CallbackProcessor/StreamingProcessor
       /// will be blocked at the same time because of accquiring SharedParameters::Lock.
       return NULL;
   }

   buffer_handle_t* buffer = mBusyBuffersList.front();
   mBusyBuffersList.pop_front();
   mResultBuffersList.push_back(buffer);

   ALOGV("%s: streamType:%d free/req/busy/res:%d/%d/%d/%d",
       __FUNCTION__,
       mBufferinfo[0].streamType,
       mFreeBuffersList.size(),
       mRequestBuffersList.size(),
       mBusyBuffersList.size(),
       mResultBuffersList.size());

   return buffer;
}

void BufferHandler::PutOneFreeBuffer(buffer_handle_t* buffer)
{
    std::unique_lock<std::mutex> lock(mFreeBufMutex);
    mFreeBuffersList.push_back(buffer);

    if (mResultBuffersList.empty()) {
        ALOGE("%s error: empty for mResultBuffersList", __func__);
    } else {
        mResultBuffersList.pop_front();
    }

    mFreeBufListCv.notify_all();

    ALOGV("%s: streamType:%d free/req/busy/res:%d/%d/%d/%d",
       __FUNCTION__,
       mBufferinfo[0].streamType,
       mFreeBuffersList.size(),
       mRequestBuffersList.size(),
       mBusyBuffersList.size(),
       mResultBuffersList.size());
}

void BufferHandler::QueueAllFreeBuffer()
{
    std::unique_lock<std::mutex> lock(mBusyBufMutex);
    if (!mBusyBuffersList.empty()) {
        for (uint32_t i = 0; i < mBusyBuffersList.size(); i++ ) {
            buffer_handle_t* buffer = mBusyBuffersList.front();
            mBusyBuffersList.pop_front();
            {
                std::unique_lock<std::mutex> lock(mFreeBufMutex);
                mFreeBuffersList.push_back(buffer);
                mFreeBufListCv.notify_all();
            }
        }
    }
    ALOGV("%s: streamType:%d free/req/busy/res:%d/%d/%d/%d",
            __FUNCTION__,
            mBufferinfo[0].streamType,
            mFreeBuffersList.size(),
            mRequestBuffersList.size(),
            mBusyBuffersList.size(),
            mResultBuffersList.size());
}

size_t BufferHandler::FreeBufferListSize()
{
    std::unique_lock<std::mutex> lock(mFreeBufMutex);
    return mFreeBuffersList.size();
}

BufferHandlerInfo* BufferHandler::getBufferHandlerInfo(buffer_handle_t* buffer)
{
   std::unique_lock<std::mutex> lock(mBufInfoMutex);
   for (uint32_t i = 0;i < mNumTotalBuffers;i++){
       if (*buffer == *mBufferinfo[i].bufHandle){
          return &(mBufferinfo[i]);
       }
   }
   return NULL;
}

int BufferHandler::updateBufferInfo(buffer_handle_t* buffer, nsecs_t timestamp)
{
   int ret = NO_ERROR;
   
   std::unique_lock<std::mutex> lock(mBufInfoMutex);
   for (uint32_t i = 0;i < mNumTotalBuffers;i++){
       if (*buffer == mBuffersList[i]){
          mBufferinfo[i].timestamp = timestamp;
          return ret;
       }
   }
   return -1;
}

void BufferHandler::setFrameAvailableListener(
    const wp<CallbackFrameAvailableListener>& listener) {
    std::unique_lock<std::mutex> lock(mFrameAvailableMutex);
    mFrameAvailableListener = listener;
    ALOGE("%s:    mFrameAvailableListener %p", __FUNCTION__,mFrameAvailableListener);
}

/// TODO: dynamic allocation.
int BufferHandler::AllocateIONBufferList(BufferHandlerInfo allocInfo)
{
    int ret = NO_ERROR;

    mAllocateInfo = allocInfo;

    pthread_attr_init(&mPthreadInfo.attr);
    pthread_attr_setdetachstate(&mPthreadInfo.attr, PTHREAD_CREATE_DETACHED);

    ret = pthread_create(&mPthreadInfo.pid, &mPthreadInfo.attr, AllocateThread, (void *)this);
    if (ret) {
        ALOGE("%s, failed to create thread", __func__, ret);
    }

    pthread_attr_destroy(&mPthreadInfo.attr);

    return ret;
}

void* BufferHandler::AllocateThread(void *data)
{
    int result = NO_ERROR;
    BufferHandler *pme = reinterpret_cast<BufferHandler *>(data);
    ALOGV("%s, Enter, maxCount %d", __func__, pme->mAllocateInfo.maxCount);

    for (uint32_t i = 0; i < pme->mAllocateInfo.maxCount; i++)
    {
        result = pme->AllocateOneIONBuffer(pme->mAllocateInfo, &pme->mBuffersList[i], i);
        if (NO_ERROR != result) {
            ALOGE("%s, alloc failed %d and raise SIGSEGV for dumping", __func__, result);
            raise(SIGSEGV);
            return NULL;
        }
        pme->mNumTotalBuffers++;
        pme->mFreeBuffersList.push_back(&pme->mBuffersList[i]);
    }

    ALOGV("%s, EXIT", __func__);

    return NULL;
}

int BufferHandler::AllocateOneIONBuffer(
    BufferHandlerInfo  allocInfo,
    buffer_handle_t*   pAllocatedBuffer,
    uint32_t           index)
{
    int rc = NO_ERROR;
    struct ion_allocation_data alloc;
    struct ion_fd_data ion_info_fd;
    native_handle_t* nh = nullptr;
    StreamType streamType = allocInfo.streamType;
    uint32_t format       = allocInfo.frameInfo.format;
    uint32_t width        = allocInfo.frameInfo.width;
    uint32_t height       = allocInfo.frameInfo.height;
    bool useCache         = allocInfo.useCache;
    size_t buf_size;
    uint32_t stride = 0;
    uint32_t slice = 0;

    if (mIonFd <= 0) {
        mIonFd = open("/dev/ion", O_RDONLY);
    }
    if (mIonFd <= 0) {
        ALOGE("%s, Ion dev open failed %s\n", __func__, strerror(errno));
        return NO_MEMORY;
    }

    memset(&alloc, 0, sizeof(alloc));
    memset(&ion_info_fd, 0, sizeof(ion_info_fd));

    if ((streamType == STREAM_DEPTH_Y16) || (streamType == STREAM_DEPTH_RAWPLAINE16)) {
        stride   = width * 2;
        slice    = height;
        buf_size = (size_t)(stride * slice);
    } else if (streamType == STREAM_PREVIEW_RAW10) {
        stride   = ALIGN((width * 5 / 4), 16);
        slice    = height;
        buf_size = (size_t)(stride * slice);
    } else if (streamType == STREAM_PREVIEW) {
        // Preview
        if (format == HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED) {
            stride = ALIGN(width,  128);
            slice  = ALIGN(height, 32);
        } else {
            stride = ALIGN(width,  64);
            slice  = ALIGN(height, 64);
        }
        buf_size = (size_t)(stride * slice * 3 / 2);
    } else if (streamType == STREAM_RECORD ||
               streamType == STREAM_RECORD_HFR) {
        // Video
        stride = ALIGN(width, 128);
        slice  = ALIGN(height, 32);
        buf_size = (size_t)(stride * slice * 3 / 2);
    } else {
        // Blob
        buf_size = (size_t)(allocInfo.frameInfo.size);
    }

    alloc.len = (size_t)(buf_size);
    alloc.len = (alloc.len + 4095U) & (~4095U);
    alloc.align = 4096;
    alloc.heap_id_mask = ION_HEAP(ION_SYSTEM_HEAP_ID);

    if (true == useCache) {
        alloc.flags = ION_FLAG_CACHED;
    }

    rc = ioctl(mIonFd, ION_IOC_ALLOC, &alloc);
    if (rc < 0) {
        ALOGE("%s ION allocation failed %s with rc = %d fd:%d\n", __func__, strerror(errno), rc, mIonFd);
        return rc;
    }
    memset(&ion_info_fd, 0, sizeof(ion_info_fd));
    ion_info_fd.handle = alloc.handle;
    rc = ioctl(mIonFd, ION_IOC_SHARE, &ion_info_fd);
    if (rc < 0) {
        ALOGE("%s ION map failed %s\n", __func__, strerror(errno));
        return rc;
    }

    if (!allocInfo.isMetadataBuf) {
        nh = native_handle_create(1, 4);
        (nh)->data[0] = ion_info_fd.fd;
        (nh)->data[1] = 0;
        (nh)->data[2] = 0;
        (nh)->data[3] = 0;
        (nh)->data[4] = alloc.len;
        (nh)->data[5] = 0;
    } else {
        if (!allocInfo.isUBWCBuf) {
            nh = native_handle_create(1, 4);
            (nh)->data[0] = ion_info_fd.fd;
            (nh)->data[1] = 0;
            (nh)->data[2] = alloc.len;
        } else {
            /*UBWC Mode*/
            ALOGE("%s Not support UBWC yet", __func__);
            return -EINVAL;
        }
    }
    *pAllocatedBuffer = nh;

    mBufferinfo[index].vaddr = mmap( NULL,
                                     alloc.len,
                                     PROT_READ  | PROT_WRITE,
                                     MAP_SHARED,
                                     ion_info_fd.fd,
                                     0 );

    mBufferinfo[index].frameInfo.size       = alloc.len;
    mBufferinfo[index].frameInfo.width      = width;
    mBufferinfo[index].frameInfo.height     = height;
    mBufferinfo[index].frameInfo.stride     = stride;
    mBufferinfo[index].frameInfo.slice      = slice;
    mBufferinfo[index].frameInfo.format     = format;
    mBufferinfo[index].idx                  = index;
    mBufferinfo[index].allocData            = alloc;
    mBufferinfo[index].fd                   = ion_info_fd.fd;
    mBufferinfo[index].streamType           = streamType;
    mBufferinfo[index].bufHandle            = pAllocatedBuffer;
    mBufferinfo[index].useCache             = useCache;

    ALOGV("%s: streamType:%d ion-fd:%d vaddr:%p buf-handle:%p ion-handle:%d "
          "size:%d width:%d height:%d stride:%d slice:%d useCache:%d ",
        __FUNCTION__,
        mBufferinfo[index].streamType,
        mBufferinfo[index].fd,
        mBufferinfo[index].vaddr,
        *mBufferinfo[index].bufHandle,
        mBufferinfo[index].allocData.handle,
        mBufferinfo[index].allocData.len,
        mBufferinfo[index].frameInfo.width,
        mBufferinfo[index].frameInfo.height,
        mBufferinfo[index].frameInfo.stride,
        mBufferinfo[index].frameInfo.slice,
        mBufferinfo[index].useCache);

    return rc;

}

void BufferHandler::ReleaseAllBuffers()
{
    int ret;

    /// TODO: double check whether have gotten all free buffers before release.
    ALOGV("%s: streamType:%d free/req/busy/res:%d/%d/%d/%d",
            __FUNCTION__,
            mBufferinfo[0].streamType,
            mFreeBuffersList.size(),
            mRequestBuffersList.size(),
            mBusyBuffersList.size(),
            mResultBuffersList.size());

    for (uint32_t i = 0; i < mNumTotalBuffers; i++)
    {
        if (NULL != mBuffersList[i])
        {
            ALOGV("%s: streamType:%d ion-fd:%d vaddr:%p buf-handle:%p ion-handle:%d"
                  "    size:%d width:%d height:%d stride:%d slice:%d useCache:%d",
                __FUNCTION__,
                mBufferinfo[i].streamType,
                mBufferinfo[i].fd,
                mBufferinfo[i].vaddr,
                *mBufferinfo[i].bufHandle,
                mBufferinfo[i].allocData.handle,
                mBufferinfo[i].allocData.len,
                mBufferinfo[i].frameInfo.width,
                mBufferinfo[i].frameInfo.height,
                mBufferinfo[i].frameInfo.stride,
                mBufferinfo[i].frameInfo.slice,
                mBufferinfo[i].useCache);

            /// For snapshot buffers, frameInfo.size will be changed in cb function.
            /// So we need to release the original allocData.len instead of frameInfo.size.
            ret = munmap(mBufferinfo[i].vaddr, mBufferinfo[i].allocData.len);
            if (ret != 0) {
                ALOGE("%s unmap buffer %p handle %d failed, size %d ret %d ",
                    __FUNCTION__, mBufferinfo[i].vaddr, mBufferinfo[i].allocData.handle, mBufferinfo[i].allocData.len, ret);
            }

            ret = ioctl(mIonFd, ION_IOC_FREE, &mBufferinfo[i].allocData.handle);
            if (ret != 0) {
                ALOGE("%s device fd %d, free buffer %p handle %d failed, ret %d ",
                    __FUNCTION__, mIonFd, mBufferinfo[i].vaddr, mBufferinfo[i].allocData.handle,ret);
            }

            native_handle_close((native_handle_t *)mBuffersList[i]);
            native_handle_delete((native_handle_t *)mBuffersList[i]);

            mBuffersList[i] = NULL;
        }
    }
}

/// Cache cmd:
/// - ION_IOC_CLEAN_INV_CACHES for CPU_HAS_READ_WRITTEN.
/// - ION_IOC_INV_CACHES       for CPU_HAS_READ.
/// - ION_IOC_CLEAN_CACHES     for CPU_HAS_WRITTEN.
int BufferHandler::SyncCache(BufferHandlerInfo* info, unsigned int cmd)
{
    struct ion_flush_data  cache_flush_data;
    struct ion_custom_data custom_data;
    int rc = NO_ERROR;

    if (false == info->useCache) {
        return rc;
    }

    memset(&cache_flush_data, 0, sizeof(cache_flush_data));
    memset(&custom_data, 0, sizeof(custom_data));
    cache_flush_data.vaddr  = info->vaddr;
    cache_flush_data.fd     = info->fd;
    cache_flush_data.handle = info->allocData.handle;
    cache_flush_data.length = info->allocData.len;
    custom_data.cmd         = cmd;
    custom_data.arg         = (unsigned long)&cache_flush_data;

    //ALOGV("%s: CMD = %u, addr = %p, fd = %d, handle = %lx length = %d, ION Fd = %d",
    //      __func__, cmd, cache_flush_data.vaddr, cache_flush_data.fd,
    //     (unsigned long)cache_flush_data.handle, cache_flush_data.length, mIonFd);

    rc = ioctl(mIonFd, ION_IOC_CUSTOM, &custom_data);
    if (rc < 0) {
        ALOGE("%s: cmd %d failed: %s\n", __func__, cmd, strerror(errno));
    }

    return rc;
}
