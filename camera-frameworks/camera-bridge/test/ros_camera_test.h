/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef ROS_CAMERA_TEST_H
#define ROS_CAMERA_TEST_H

#include "CameraAPPInterface.h"

class ROSCameraTest : public CameraAPPInterface, public AppCallbacks
{
public:
   ROSCameraTest();
   ~ROSCameraTest();
   virtual void onPreviewFrameAvaible(void *data, uint64_t size, uint64_t timestamp);
   int run();

private:
   CameraAPPInterface*   mROSCamera;
   cameraAppParameters   mParams;
   int                   mCameraId;
   uint64_t              mFrameCount;
   uint64_t              mTimeStampPrev;
   uint64_t              mTimeStampCurr;
   float                 mPreviewFpsAvg, mVideoFpsAvg;

   string getStringFromEnum(CamFunction e)
    {
        switch(e)
        {
            case CAM_FUNC_HIRES:        return "hires";
            case CAM_FUNC_TRACKING:     return "tracking";
            case CAM_FUNC_STEREO:       return "stereo";
            case CAM_FUNC_LEFT_SENSOR:  return "left";
            case CAM_FUNC_RIGHT_SENSOR: return "right";
            case CAM_FUNC_DEPTH:        return "depth";
            default:
               printf("error: unknown camera type \n");
            break;
        }
        return "unknown";
    }


};
#endif
