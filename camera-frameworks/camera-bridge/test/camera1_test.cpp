/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*************************************************************************
*
*Application Notes:  Refer readme.md
*
****************************************************************************/
#include "camera1_test.h"
using namespace std;
using namespace android;
using namespace camera1_if;

/**
 *
 * FUNCTION: onPreviewFrame
 *
 *  - This is called every preview frame.
 *  - In the test app, we save files only after every 30 frames.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Test::onPreviewFrame(CameraFrame frame)
{
    uint64_t diff;
    int ret;

    diff = frame.timeStamp - this->mPreviewTimeStampPrev;

    /* Skip the first frames with unstable timeStamp */
    if ((frame.timeStamp > 0) && (diff > 0) && (this->mPreviewFrameCount < 5)) {
        mPreviewFpsAvg = ((float)1e9 / diff);
    }

    if ((frame.timeStamp > 0)
        && (diff > 0)
        && (this->mPreviewFrameCount >= 5)
        && mPreviewOn) {
        this->mPreviewFpsAvg =
            ((this->mPreviewFpsAvg * this->mPreviewFrameCount) + ((float)1e9 / diff)) / (this->mPreviewFrameCount + 1);
    }

    this->mPreviewFrameCount++;
    this->mPreviewTimeStampPrev  = frame.timeStamp;

    if (this->mPreviewFrameCount == 1) {
        CAM_INFO("Camera %d get the first preview frame", this->getCameraId());
    }

    if (this->mConfig.dumpFrames == true
        && this->mPreviewFrameCount % this->mConfig.dumpFrameFreq == 0
        && false == mIsPicTaking) {
        char name[MAX_BUF_SIZE];
        if (this->mConfig.func == CAM_FUNC_TOF) {
            snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s_depth.raw",
                frame.frameinfo.width,
                frame.frameinfo.height,
                this->mPreviewFrameCount,
                frame.timeStamp,
                getStringFromEnum(this->mConfig.func).c_str());

        } else {
            snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.yuv",
                frame.frameinfo.width,
                frame.frameinfo.height,
                this->mPreviewFrameCount,
                frame.timeStamp,
                getStringFromEnum(this->mConfig.func).c_str());
        }

        dumpFrameToFile(name, frame);
        CAM_INFO("Camera[%d] Average FPS = %.2f for preview",
            this->getCameraId(), mPreviewFpsAvg);
    }
}

/**
 *
 * FUNCTION: onRawFrame
 *
 *  - This is called every raw frame.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Test::onRawFrame(CameraFrame frame)
{
    uint64_t diff;
    int ret;

    diff = frame.timeStamp - this->mRawTimeStampPrev;

    /* Skip the first frames with unstable timeStamp */
    if ((frame.timeStamp > 0) && (diff > 0) && (this->mRawFrameCount < 10)) {
        mRawFpsAvg = ((float)1e9 / diff);
    }

    if ((frame.timeStamp > 0)
        && (diff > 0)
        && (this->mRawFrameCount >= 10)
        && mPreviewOn) {
        this->mRawFpsAvg =
            ((this->mRawFpsAvg * this->mRawFrameCount) + ((float)1e9 / diff)) / (this->mRawFrameCount + 1);
    }

    this->mRawFrameCount++;
    this->mRawTimeStampPrev  = frame.timeStamp;

    if (this->mRawFrameCount == 1) {
        CAM_INFO("Camera %d get the first raw frame", this->getCameraId());
    }

    if (this->mConfig.dumpFrames == true && this->mRawFrameCount % this->mConfig.dumpFrameFreq == 0) {
        char name[MAX_BUF_SIZE];
        snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.raw",
            frame.frameinfo.width,
            frame.frameinfo.height,
            this->mRawFrameCount,
            frame.timeStamp,
            getStringFromEnum(this->mConfig.func).c_str());

        dumpFrameToFile(name, frame);
        CAM_INFO("Camera[%d] Average FPS = %.2f for raw preview",
            getCameraId(), mRawFpsAvg);
    }
}

/**
 *
 * FUNCTION: onVideoFrame
 *
 *  - This is called every video frame.
 *  - In the test app, we save files only after every 30 frames.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Test::onVideoFrame(CameraFrame frame)
{

    uint64_t diff = frame.timeStamp - this->mVideoTimeStampPrev;

    /* Skip the first frames with unstable timeStamp */
    if ((frame.timeStamp > 0) && (diff > 0) && (this->mVideoFrameCount < 5)) {
        mVideoFpsAvg = ((float)1e9 / diff);
    }

    if ((frame.timeStamp > 0)
        && (diff > 0)
        && (this->mVideoFrameCount >= 5)
        && mVideoOn) {
        this->mVideoFpsAvg =
            ((this->mVideoFpsAvg * this->mVideoFrameCount) + ((float)1e9 / diff)) / (this->mVideoFrameCount + 1);
    }

    this->mVideoFrameCount++;
    this->mVideoTimeStampPrev  = frame.timeStamp;

    if (this->mVideoFrameCount == 1) {
        CAM_INFO("Camera %d get the first video frame", this->getCameraId());
    }

    if (this->mConfig.vConfig.vFormat == YUV_FORMAT
        && this->mConfig.dumpFrames == true
        && this->mVideoFrameCount % this->mConfig.dumpFrameFreq == 0) {
        char name[MAX_BUF_SIZE];
        snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "V_%dx%d_%04d_%llu_%s.yuv",
                  frame.frameinfo.width,
                  frame.frameinfo.height,
                  (int)this->mVideoFrameCount,
                  frame.timeStamp,
                  getStringFromEnum(this->mConfig.func).c_str());

        dumpFrameToFile(name, frame);
        CAM_INFO("Camera[%d] Average FPS = %.2f for video",
            getCameraId(), mVideoFpsAvg);
    }

    if (this->mConfig.vConfig.vFormat == H264_FORMAT ||
        this->mConfig.vConfig.vFormat == H265_FORMAT) {
        this->mCameraIF->GenerateVideoByFrame(frame);
    }
}

/**
 *
 * FUNCTION: onPictureFrame
 *
 *  - This is called for every snapshot jpeg frame.
 *  - In the test app, we save all jpeg files.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Test::onPictureFrame(CameraFrame frame)
{
    char imageName[MAX_BUF_SIZE];

    if (this->mConfig.sConfig.sFormat == RAW_FORMAT) {
        snprintf(imageName, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "snapshot_mipi_raw10_%dx%d_%lld_%s.raw",
            frame.frameinfo.width,
            frame.frameinfo.height,
            frame.timeStamp,
            getStringFromEnum(this->mConfig.func).c_str());

    } else {
        snprintf(imageName, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "snapshot_%dx%d_%lld_%s.jpg",
            frame.frameinfo.width,
            frame.frameinfo.height,
            frame.timeStamp,
            getStringFromEnum(this->mConfig.func).c_str());
    }

    dumpFrameToFile(imageName, frame);
}

/**
 *
 * FUNCTION: onMetadataFrame
 *
 *  - This is called every metadata frame.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Test::onMetadataFrame(CameraFrame frame)
{

}

Camera1Test::Camera1Test(TestConfig config) :
    mCameraIF(NULL),
    mPreviewFrameCount(0),
    mVideoFrameCount(0),
    mRawFrameCount(0),
    mPreviewTimeStampPrev(0),
    mVideoTimeStampPrev(0),
    mRawTimeStampPrev(0),
    mRawFpsAvg(0.0f),
    mVideoFpsAvg(0.0f),
    mPreviewFpsAvg(0.0f),
    mIsPicTaking(false),
    mPreviewOn(false),
    mVideoOn(false)
{
    mConfig = config;
    char prop_val[PROPERTY_VALUE_MAX];
    property_get(PROP_DUMP_FRAME_FREQ, prop_val, DEFAULT_DUMP_FRAME_FREQ);
    mConfig.dumpFrameFreq = atoi(prop_val);
}

Camera1Test::~Camera1Test()
{

}

int Camera1Test::initialize()
{
    int rc = NO_ERROR;

    // initialize Camera1Callbacks
    this->Init(SYSTEMTIME_TYPE);

    // 1. returns the number of camera-modules connected on the board
    int numCamera = Camera1Interface::getNumberOfCameras(Camera1Interface::MODE_FWI);

    if (numCamera < 0) {
        CAM_PRINT("getNumberOfCameras() failed, rc = %d", numCamera);
        return EXIT_FAILURE;
    }

    CAM_PRINT("num_cameras = %d", numCamera);

    if (numCamera < 1) {
        CAM_PRINT("No cameras found.");
        return EXIT_FAILURE;
    }

    // 2. find camera id
    if ((mConfig.func == CAM_FUNC_INVALID || mConfig.func >= CAM_FUNC_MAX) &&
        (mConfig.camId < 0 || mConfig.camId > numCamera)) {
        mCamId = -1;
        CAM_ERR_PRINT("Camera function or id invalid");
        return EXIT_FAILURE;
    }

    CameraInfo camInfo;
    if (mConfig.camId >= 0) {
        // force-id mode
        CAM_PRINT("force-id mode: cam-id %d", mConfig.camId);
        rc = Camera1Interface::getCameraInfo(Camera1Interface::MODE_FWI, mConfig.camId, camInfo);
        if (rc != NO_ERROR) {
            CAM_ERR_PRINT("get the camera device info failed for camera %d", mConfig.camId);
            return EXIT_FAILURE;
        }
        mCamId = mConfig.camId;
    } else {
        for ( int i = 0; i < numCamera; i++ ) {
            Camera1Interface::getCameraInfo(Camera1Interface::MODE_FWI, i, camInfo);
            CAM_INFO("id = %d, facing = %d, name = %s", i, camInfo.facing, camInfo.sensorName);
            if (MatchCameraName(mConfig.func, camInfo.sensorName)) {
                mCamId = i;
                break;
            }
        }
    }

    if (mCamId < 0) {
        CAM_ERR_PRINT("Failed to find camera by func %d camId %d", mConfig.func, mConfig.camId);
        return EXIT_FAILURE;
    }
    CAM_PRINT("Selected camera success: id %d, name %s", mCamId, camInfo.sensorName);

    // 3. create camera instance
    rc = Camera1Interface::createInstance(Camera1Interface::MODE_FWI, mCamId, &mCameraIF);
    if (rc != NO_ERROR) {
        CAM_ERR_PRINT("could not open camera %d, rc %d", mCamId, rc);
        return rc;
    }

    CAM_PRINT("Testing camera id=%d", mCamId);

    // 4. set callback functions
    mCameraIF->setCallbacks(this);
    CAM_PRINT("camera %d set callback\n", mCamId);
    return NO_ERROR;
}

int Camera1Test::takePicture()
{
    int rc = NO_ERROR;
    CAM_PRINT("take picture");
    mIsPicTaking = true;
    rc = mCameraIF->takePicture(mConfig.sConfig.enableZSL);
    if (rc != NO_ERROR) {
        CAM_PRINT("takePicture failed");
        return rc;
    }
    mIsPicTaking = false;
    return rc;
}

void Camera1Test::queryCapabilities()
{
    /* query capabilities */
    mCameraIF->mParams.getSupportedPreviewSizes(mCaps.pSizes);
    mCameraIF->mParams.getSupportedVideoSizes(mCaps.vSizes);
    mCameraIF->mParams.getSupportedPictureSizes(mCaps.picSizes);
    mCameraIF->mParams.getSupportedPictureSizes(mCaps.rawPicSizes);

    /// TODO: Need to check the fps range values: why have not 60/90/120 fps?
    mCaps.previewFpsRanges   = mCameraIF->mParams.getSupportedPreviewFpsRanges();

    mCaps.previewFormats     = mCameraIF->mParams.getStringV(CameraParameters::KEY_SUPPORTED_PREVIEW_FORMATS);

    mCaps.videoFpsMode       = mCameraIF->mParams.getIntV(KEY_QC_SUPPORTED_VIDEO_HIGH_FRAME_RATE_MODES);

    mCaps.rawSize            = mCameraIF->mParams.getString(KEY_QC_RAW_PICUTRE_SIZE);
}

void Camera1Test::printCapabilities()
{
    CAM_PRINT("Camera capabilities");

    // Basical capabilities
    CAM_PRINT("available preview sizes:");
    for (size_t i = 0; i < mCaps.pSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d", i, mCaps.pSizes[i].width, mCaps.pSizes[i].height);
    }
    CAM_PRINT("available video sizes:");
    for (size_t i = 0; i < mCaps.vSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d", i, mCaps.vSizes[i].width, mCaps.vSizes[i].height);
    }
    CAM_PRINT("available jpeg picture sizes:");
    for (size_t i = 0; i < mCaps.picSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d", i, mCaps.picSizes[i].width, mCaps.picSizes[i].height);
    }
    CAM_PRINT("available raw picture sizes:");
    for (size_t i = 0; i < mCaps.rawPicSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d", i, mCaps.rawPicSizes[i].width, mCaps.rawPicSizes[i].height);
    }
    CAM_PRINT("available preview formats:");
    for (size_t i = 0; i < mCaps.previewFormats.size(); i++) {
        CAM_PRINT("%d: %s", i, mCaps.previewFormats[i].c_str());
    }
    CAM_PRINT("available preview fps ranges:");
    for (size_t i = 0; i < mCaps.previewFpsRanges.size(); i++) {
        CAM_PRINT("%d: [%d, %d]", i, mCaps.previewFpsRanges[i].min,
               mCaps.previewFpsRanges[i].max);
    }
    CAM_PRINT("available video fps values:");
    for (size_t i = 0; i < mCaps.videoFpsMode.size(); i++) {
        CAM_PRINT("%d: %d", i, mCaps.videoFpsMode[i]);
    }

    // Advance capabilities

    return;
}

/**
 * FUNCTION: setFPSindex
 *
 * scans through the supported fps values and returns index of
 * requested fps in the array of supported fps
 *
 * @param cfg      : configure information (input)
 * @param pFpsIdx  : preview fps index     (output)
 * @param vFpsIdx  : video fps index       (output)
 *
 *  */
int Camera1Test::setFPSindex(TestConfig & cfg, int &pFpsIdx, int &vFpsIdx)
{
    size_t i          =  0;
    int rc            =  NO_ERROR;
    int previewFps    =  cfg.fpsInfo.fps;
    int videoFps      =  cfg.fpsInfo.fps;

    /// Get preview FPS idx
    if (cfg.vConfig.testVideo == true) {
        previewFps = DEFAULT_CAMERA_FPS;
    }

    for (i = 0; i < mCaps.previewFpsRanges.size(); i++) {
        CAM_INFO("previewFpsRanges[%d] %d - %d, previewFps %d, fpsInfo.isFixed %d",
            i,
            mCaps.previewFpsRanges[i].min/1000,
            mCaps.previewFpsRanges[i].max/1000,
            previewFps,
            cfg.fpsInfo.isFixed);
        if (mCaps.previewFpsRanges[i].max/1000 == previewFps) {
            if (cfg.fpsInfo.isFixed == false) {
                break;
            } else if ((cfg.fpsInfo.isFixed == true) &&
                       (mCaps.previewFpsRanges[i].max == mCaps.previewFpsRanges[i].min)) {
                break;
            }
        }
    }

    if (i >= mCaps.previewFpsRanges.size()) {
        pFpsIdx = -1;
        rc = -EINVAL;
    } else {
        pFpsIdx = i;
    }
    CAM_INFO("preview FPS: i %d, pFpsIdx %d", i, pFpsIdx);

    /// Get video FPS idx
    if (mCaps.videoFpsMode.size() > 0) {
        for (i = 0; i < mCaps.videoFpsMode.size(); i++) {
            CAM_INFO("videoFpsMode[%d] %d, video fps %d ",
                i,
                mCaps.videoFpsMode[i],
                videoFps);

            if (videoFps == 30 * mCaps.videoFpsMode[i]) {
                break;
            }
            if (DEFAULT_CAMERA_FPS == 30 * mCaps.videoFpsMode[i]) {
                break;
            }
        }

        if (i >= mCaps.videoFpsMode.size()) {
            vFpsIdx = -1;
            //rc = -EINVAL;
        } else {
            vFpsIdx = i;
        }
    } else {
        vFpsIdx = -1;
        //rc = -EINVAL;
    }

    CAM_INFO("video FPS: i %d, vFpsIdx %d", i, vFpsIdx);

    return rc;
}


void Camera1Test::setSnapshotParameters()
{
    size_t index;
    Vector<Size> supportedSnapshotSizes;

    if (mConfig.sConfig.sFormat == RAW_FORMAT) {
        CAM_PRINT("Camera[%d] Setting snapshot format : raw",getCameraId());
        mCameraIF->mParams.setPictureFormat(QC_PIXEL_FORMAT_BAYER_MIPI_RAW_10BGGR);

        CAM_PRINT("Camera[%d] raw picture size: %s",getCameraId(), mCaps.rawSize.c_str());
        supportedSnapshotSizes = mCaps.rawPicSizes;
        mCameraIF->mParams.set(KEY_QC_RAW_PICUTRE_SIZE, "640x480");
    } else {
        CAM_PRINT("Camera[%d] Setting snapshot format : jpeg", getCameraId());
        supportedSnapshotSizes = mCaps.picSizes;
    }

    // Check requested snapshot resolution is supported for requested format
    if (mConfig.sConfig.sSize.width == 99999 && mConfig.sConfig.sSize.height == 99999 ) {
        if (supportedSnapshotSizes.size() == 0) {
            CAM_PRINT("Camera[%d] Error: No snapshot resolution found for requested format",getCameraId());
            exit(1);
        }
        mConfig.sConfig.sSize = supportedSnapshotSizes[0];
    } else {
        for (index = 0 ; index < supportedSnapshotSizes.size() ; index++) {
            if ( mConfig.sConfig.sSize.width == supportedSnapshotSizes[index].width
                && mConfig.sConfig.sSize.height == supportedSnapshotSizes[index].height)
            {
                mConfig.sConfig.sSize = supportedSnapshotSizes[index];
                break;
            }
        }
        if (index >= supportedSnapshotSizes.size()) {
            CAM_PRINT("Camera[%d] Error: Snapshot resolution %d x %d not supported for requested format",
                getCameraId(),mConfig.sConfig.sSize.width,mConfig.sConfig.sSize.height);
            exit(1);
        }
    }

    mCameraIF->mParams.setPictureSize(mConfig.sConfig.sSize.width, mConfig.sConfig.sSize.height);
    CAM_PRINT("Camera[%d] Setting snapshot size : %d x %d", getCameraId(),
        mConfig.sConfig.sSize.width, mConfig.sConfig.sSize.height);

    if (mConfig.sConfig.sFormat == JPEG_FORMAT) {
        //mCameraIF->mParams.setPictureThumbNailSize(mConfig.sConfig.sSize);
    }
}

void Camera1Test::setPreviewVideoParameters()
{
    int pFpsIdx = 3;
    int vFpsIdx = 3;

    // Preview parameters
    if (mConfig.pConfig.pFormat == RAW_FORMAT) {
        /* Do not turn on videostream for tracking camera in RAW format */
        mConfig.vConfig.testVideo = false;
        CAM_PRINT("Camera[%d] Setting output = RAW_FORMAT for camera", getCameraId());
        if (mConfig.func == CAM_FUNC_TOF) {
            mCameraIF->mParams.set(CameraParameters::KEY_PREVIEW_FORMAT, CameraParameters::PIXEL_FORMAT_BAYER_RGGB);
        } else {
            mCameraIF->mParams.set(CameraParameters::KEY_PREVIEW_FORMAT, CameraParameters::PIXEL_FORMAT_BAYER_RAW10);
        }
    } else if (mConfig.pConfig.pFormat == DEPTH_Y16_FORMAT) {
        /* Do not turn on videostream for depth camera in RAW format */
        mConfig.vConfig.testVideo = false;
        mConfig.sConfig.testSnapshot= false;
        CAM_PRINT("Camera[%d] Setting output = DEPTH_Y16_FORMAT for camera    ", getCameraId());
        mCameraIF->mParams.set(CameraParameters::KEY_PREVIEW_FORMAT, CameraParameters::PIXEL_FORMAT_DEPTH_Y16);
    }

    CAM_PRINT("Camera[%d] setting preview size: %dx%d",
        getCameraId(), mConfig.pConfig.pSize.width, mConfig.pConfig.pSize.height);
    mCameraIF->mParams.setPreviewSize(mConfig.pConfig.pSize.width, mConfig.pConfig.pSize.height);

    /* Find index and set FPS  */
    int rc = setFPSindex(mConfig, pFpsIdx, vFpsIdx);
    if (NO_ERROR != rc) {
        CAM_PRINT("Camera[%d] get fps index failed !" , getCameraId());
        return;
    }

    CAM_PRINT("Camera[%d] setting preview fps range: %d, %d ( idx = %d ) ",
        getCameraId(), mCaps.previewFpsRanges[pFpsIdx].min,
        mCaps.previewFpsRanges[pFpsIdx].max, pFpsIdx);
    mCameraIF->mParams.setPreviewFrameRate(mCaps.previewFpsRanges[pFpsIdx].max/1000);
    mCameraIF->mParams.setPreviewFpsRange(mCaps.previewFpsRanges[pFpsIdx].min,
                               mCaps.previewFpsRanges[pFpsIdx].max);

    if (mConfig.vConfig.testVideo  == true) {
        CAM_PRINT("Camera[%d] setting video size: %dx%d", getCameraId(), mConfig.vConfig.vSize.width, mConfig.vConfig.vSize.height);
        mCameraIF->mParams.setVideoSize(mConfig.vConfig.vSize.width, mConfig.vConfig.vSize.height);
        CAM_PRINT("Camera[%d] setting RECORDING_HINT: true ", getCameraId());
        mCameraIF->mParams.set(CameraParameters::KEY_RECORDING_HINT, CameraParameters::TRUE);
        if (mConfig.vConfig.vFps >= 60) {
            CAM_PRINT("Camera[%d] setting video fps: %d ( idx = %d )", getCameraId(), mConfig.vConfig.vFps, vFpsIdx );
            mCameraIF->mParams.set(KEY_QC_VIDEO_HIGH_FRAME_RATE, mConfig.vConfig.vFps);
        }
    }
}

void Camera1Test::setAdvanceParameters()
{
    if (mConfig.sConfig.enableZSL) {
        CAM_PRINT("Camera[%d] setting ZSL: true ", getCameraId());
        mCameraIF->mParams.set(KEY_QC_ZSL, CameraParameters::TRUE);
    }

    // Antibanding
    CAM_PRINT("Camera[%d] setting antibanding: %s ", getCameraId(),mConfig.antibanding.c_str());
    mCameraIF->mParams.setString(CameraParameters::KEY_ANTIBANDING, mConfig.antibanding);

    // Exposure metering mode
    CAM_PRINT("Camera[%d] setting exposure metering mode: %d ", getCameraId(),mConfig.expMeteringMode);
    mCameraIF->mParams.set(KEY_QC_EXPOSURE_METERING_MODE, mConfig.expMeteringMode);
}

void Camera1Test::setHiresCameraParameters()
{

}

void Camera1Test::setTrackingCameraParameters()
{

}

void Camera1Test::setDepthCameraParameters()
{

}

void Camera1Test::setTOFCameraParameters()
{
    if (mConfig.func == CAM_FUNC_TOF) {
       if (mConfig.tofMode == TOF_SHORT_RANGE_MODE) {
          mCameraIF->mParams.set(KEY_QC_RANGE_MODE, "short-range");
       } else if (mConfig.tofMode == TOF_LONG_RANGE_MODE){
          mCameraIF->mParams.set(KEY_QC_RANGE_MODE, "long-range");
       }

       mCameraIF->mParams.set(KEY_QC_RANGE_MODE_AVAIABLE, "true");
       CAM_PRINT("Camera[%d] TOF range mode: 0x%x ", getCameraId(), mConfig.tofMode);
    }
}

/**
 *  FUNCTION : setParameters
 *
 *  - When camera is opened, it is initialized with default set
 *    of parameters.
 *  - This function sets required parameters based on camera and
 *    usecase
 *  - params_setXXX and params_set  only updates parameter
 *    values in a local object.
 *  - mCameraIF->mParams.commit() function will update the hardware
 *    settings with the current state of the parameter object
 *  - Some functionality will not be application for all for
 *    sensor modules. for eg. tracking camera sensor does not support
 *    autofocus/focus mode.
 *  - Reference setting for different sensors and format are
 *    provided in this function.
 *
 *  */
int Camera1Test::setParameters()
{
    int rc = NO_ERROR;

    setPreviewVideoParameters();

    if (mConfig.sConfig.testSnapshot  == true) {
        setSnapshotParameters();
    }

    switch ( mConfig.func ) {
        case CAM_FUNC_HIRES:
            setHiresCameraParameters();
            break;
        case CAM_FUNC_TRACKING:
            setTrackingCameraParameters();
            break;
        case CAM_FUNC_DEPTH:
            setDepthCameraParameters();
            break;
        case CAM_FUNC_TOF:
            setTOFCameraParameters();
            break;
        default:
            break;
    }

    setAdvanceParameters();
    rc = mCameraIF->setParameters(mCameraIF->mParams);

    return rc;
}

/**
 *  FUNCTION : setPostParameters
 *
 *  - This function sets required parameters based on camera and
 *    usecase after starting preview
 *
 *  */
int Camera1Test::setPostParameters()
{
    switch(mConfig.func)
    {
        case CAM_FUNC_HIRES:
        case CAM_FUNC_TRACKING:
            {
                mCameraIF->mParams.set(KEY_QC_EXPOSURE_TIME, mConfig.expTimeNs);
                mCameraIF->mParams.set(KEY_QC_ISO_VALUE, mConfig.iso);
                CAM_PRINT("camera[%d] Setting exposure time value = %lld , iso value = %d",
                    getCameraId(),mConfig.expTimeNs, mConfig.iso);

                if (mConfig.zoom > 0) {
                    mCameraIF->mParams.set(CameraParameters::KEY_ZOOM, mConfig.zoom);
                    CAM_PRINT("camera[%d] set zoom value = %d.", getCameraId(), mConfig.zoom);
                }
            }
            break;
        case CAM_FUNC_DEPTH:
            break;
        case CAM_FUNC_TOF:
            return NO_ERROR;
        default:
            break;

    }

    return mCameraIF->setParameters(mCameraIF->mParams);
}

void Camera1Test::run()
{
    int rc = EXIT_SUCCESS;

    // 1. Camera initial
    rc = initialize();
    if (NO_ERROR != rc) {
        goto del_camera;
    }
    CAM_PRINT("Camera[%d] initialize done ",this->getCameraId());

    // 2. get initial parameters
    mCameraIF->mParams = mCameraIF->getParameters();
    //mCameraIF->mParams.dump();
    CAM_PRINT("Camera[%d] getParameters done ",this->getCameraId());

    // 3. Query and print Capabilities
    queryCapabilities();
    CAM_PRINT("Camera[%d] queryCapabilities done ",this->getCameraId());

    if (this->mConfig.infoMode) {
        printCapabilities();
        goto del_camera;
    }

    // 4. Set camera parameters
    rc = setParameters();
    if (NO_ERROR != rc) {
        CAM_PRINT("Camera[%d] setParameters failed ",this->getCameraId());
        printUsageExit(0);
        goto del_camera;
    }
    //mCameraIF->mParams = mCameraIF->getParameters();
    //mCameraIF->mParams.dump();
    CAM_PRINT("Camera[%d] setParameters done ",this->getCameraId());

    // 5. starts the preview stream. At every preview frame onPreviewFrame( ) callback is invoked
    CAM_PRINT("Camera[%d] starting preview ",this->getCameraId());
    rc = mCameraIF->startPreview();
    if (NO_ERROR != rc) {
        CAM_PRINT("Camera[%d] startPreview failed ",this->getCameraId());
        goto del_camera;
    }

    mPreviewOn = true;
    CAM_PRINT("Camera[%d] start preview done ",this->getCameraId());

    // 6. set camera parameters after preview is ready
    rc = setPostParameters();
    if (NO_ERROR != rc) {
        CAM_PRINT("setPostParameters failed ");
        printUsageExit(0);
        goto del_camera;
    } else {
       CAM_PRINT("Camera[%d] set post parameters done ",this->getCameraId());
    }

    // 7. starts video stream. At every video frame onVideoFrame( )  callback is invoked
    if (this->mConfig.vConfig.testVideo  == true) {
        CAM_PRINT("Camera[%d] start recording ",this->getCameraId());
        rc = mCameraIF->startRecording(mConfig.vConfig);
        if (NO_ERROR != rc) {
            CAM_PRINT("Camera[%d] recording failed ",this->getCameraId());
            goto del_camera;
        } else {
            mVideoOn = true;
            CAM_PRINT("Camera[%d] start recording done ",this->getCameraId());
        }
    }

    // 8. snapshot
    if (this->mConfig.sConfig.testSnapshot == true) {
        CAM_PRINT("Camera[%d] waiting for 2 seconds for exposure to settle... ",getCameraId());
        /* sleep required to settle the exposure before taking snapshot.
           This app does not provide interactive feedback to user
           about the exposure */
        sleep(2);

        CAM_PRINT("Camera[%d] taking picture ",this->getCameraId());
        rc = takePicture();
        if (NO_ERROR != rc) {
            CAM_PRINT("Camera[%d] takePicture failed ",this->getCameraId());
            goto del_camera;;
        }
    }

    /* Put the main/run thread to sleep and process the frames in the callbacks */
    CAM_PRINT("Camera[%d] waiting for %d seconds ... ",this->getCameraId(), this->mConfig.runTime);
    sleep(this->mConfig.runTime);

    // 9. After the sleep interval stop preview stream, stop video stream and end application
    if (mConfig.vConfig.testVideo  == true) {
        CAM_PRINT("Camera[%d] stoping recording ",this->getCameraId());
        mVideoOn = false;
        mCameraIF->stopRecording();
    }

    // 10. stop preview
    CAM_PRINT("Camera[%d] stopping preview ",getCameraId());
    mPreviewOn = false;
    mCameraIF->stopPreview();
    CAM_PRINT("Camera[%d] stop preview done ",getCameraId());

    CAM_PRINT("Camera[%d] Average preview FPS = %.2f ",getCameraId(), this->mPreviewFpsAvg);
    if (this->mConfig.vConfig.testVideo  == true)
        CAM_PRINT("Camera[%d] Average video FPS = %.2f ",getCameraId(), this->mVideoFpsAvg);
    if (mConfig.pConfig.pFormat == RAW_FORMAT ||
        mConfig.pConfig.pFormat == DEPTH_Y16_FORMAT)
        CAM_PRINT("Camera[%d] Average raw FPS = %.2f ",getCameraId(), this->mRawFpsAvg);
del_camera:
    // 11. release camera device
    Camera1Interface::deleteInstance(&(this->mCameraIF));
    CAM_PRINT("Camera[%d] close done ",getCameraId());
    //return rc;
}

/**
 *  FUNCTION: setDefaultConfig
 *
 *  set default config based on camera module
 *
 * */
static int setDefaultConfig(TestConfig *cfg) {

    cfg->pConfig.pFormat              = YUV_FORMAT;
    cfg->dumpFrames                   = false;
    cfg->runTime                      = 10;
    cfg->infoMode                     = false;
    cfg->vConfig.testVideo            = false;
    cfg->sConfig.testSnapshot         = false;
    cfg->sConfig.enableZSL            = false;
    cfg->expTimeNs                    = DEFAULT_EXPOSURE_TIME_VALUE;
    cfg->fpsInfo.fps                  = DEFAULT_CAMERA_FPS;
    cfg->fpsInfo.isFixed              = false;
    cfg->logLevel                     = CAM_LOG_DEBUG;
    cfg->sConfig.sFormat              = JPEG_FORMAT;
    cfg->sConfig.numSnapshot          = 1;
    cfg->antibanding                  = string(VALUE_OFF);
    cfg->iso                          = DEFAULT_ISO_VALUE;
    cfg->zoom                         = DEFAULT_ZOOM_VALUE;
    cfg->expMeteringMode              = AECAlgoMeteringModeFrameAverage;

    switch (cfg->func) {
    case CAM_FUNC_HIRES:
        cfg->pConfig.pSize      = HDSize;
        cfg->vConfig.vSize      = HDSize;
        cfg->sConfig.sSize      = HDSize;
        break;
    case CAM_FUNC_TRACKING:
        cfg->pConfig.pSize      = VGASize;
        cfg->vConfig.vSize      = VGASize;
        cfg->sConfig.sSize      = VGASize;
        break;
    case CAM_FUNC_DEPTH:
        cfg->pConfig.pSize      = HDSize;
        cfg->vConfig.vSize      = HDSize;
        cfg->sConfig.sSize      = HDSize;
        cfg->pConfig.pFormat    = DEPTH_Y16_FORMAT;
        break;
    case CAM_FUNC_TOF:
        cfg->pConfig.pSize      = VGASize;
        cfg->vConfig.vSize      = VGASize;
        cfg->sConfig.sSize      = VGASize;
        cfg->pConfig.pFormat    = RAW_FORMAT;
        cfg->tofMode            = 0; //long range mode by default.
        break;
    default:
        cfg->pConfig.pSize      = VGASize;
        cfg->vConfig.vSize      = VGASize;
        cfg->sConfig.sSize      = VGASize;
        break;
    }
    cfg->pConfig.pStride = ALIGN(cfg->pConfig.pSize.width,  ALIGN_PREVIEW_WIDTH);
    cfg->pConfig.pSlice  = ALIGN(cfg->pConfig.pSize.height, ALIGN_PREVIEW_HEIGHT);
    cfg->vConfig.vStride = ALIGN(cfg->vConfig.vSize.width,  ALIGN_VIDEO_WIDTH);
    cfg->vConfig.vSlice  = ALIGN(cfg->vConfig.vSize.height, ALIGN_VIDEO_HEIGHT);

    cfg->vConfig.bitrate            = 0;
    cfg->vConfig.targetBitrate      = 0;
    cfg->vConfig.vFps               = DEFAULT_CAMERA_FPS;
    cfg->vConfig.isBitRateConstant  = false;
    cfg->vConfig.vFormat            = YUV_FORMAT;

    return 0;
}

/**
 *  FUNCTION: setLogMask
 *
 *  set APP log level
 *
 *  */
static void setLogMask(TestConfig config)
{
    /* setup syslog level */
    if (config.logLevel == CAM_LOG_SILENT) {
        setlogmask(LOG_UPTO(LOG_EMERG));
    } else if (config.logLevel == CAM_LOG_DEBUG) {
        setlogmask(LOG_UPTO(LOG_DEBUG));
    } else if (config.logLevel == CAM_LOG_INFO) {
        setlogmask(LOG_UPTO(LOG_INFO));
    } else if (config.logLevel == CAM_LOG_ERROR) {
        setlogmask(LOG_UPTO(LOG_ERR));
    }
    openlog(NULL, LOG_NDELAY, LOG_DAEMON);
}

/**
 *  FUNCTION: MatchCameraName
 *
 *  */
static bool MatchCameraName(CamFunction func, const char* cameraName)
{
    if ((func == CAM_FUNC_INVALID)
        || (func >= CAM_FUNC_MAX)
        || (NULL == cameraName)
        || (strlen(cameraName) == 0)) {
        return false;
    }

    int sizeColumn = sizeof(gCameraModuleList[func]) / sizeof(gCameraModuleList[func][0]);
    for (int i = 0; i < sizeColumn; i++) {
        if (0 == strcmp(gCameraModuleList[func][i], cameraName)) {
            return true;
        }
    }
    return false;
}

/**
 *  FUNCTION: parseCommandline
 *
 *  parses commandline options and populates the config
 *  data structure
 *
 *  */
static TestConfig parseCommandline(int argc, char* argv[])
{
    TestConfig cfg;
    int c;

    //memset((void *)&cfg, 0, sizeof(cfg));
    cfg.func  = CAM_FUNC_INVALID;
    cfg.camId = -1;
    while ((c = getopt(argc, argv, "f:F:t:p:P:v:V:s:S:b:r:A:e:E:I:m:z:l:dZih?")) != -1) {
        switch (c) {
        case 'f':
            {
                string str(optarg);
                if (str == "hires") {
                    cfg.func = CAM_FUNC_HIRES;
                } else if (str == "tracking") {
                    cfg.func = CAM_FUNC_TRACKING;
                } else if (str == "depth") {
                    cfg.func = CAM_FUNC_DEPTH;
                } else if (str == "tof") {
                    cfg.func = CAM_FUNC_TOF;
                }
                break;
            }
        case 'F':
            {
                cfg.camId = atoi(optarg);
                break;
            }
        case 'h':
        case '?':
            printUsageExit(0);
            break;
        default:
            break;
        }
    }
    if (cfg.func == CAM_FUNC_INVALID && cfg.camId < 0) {
        CAM_ERR_PRINT("Camera function or id invalid");
        printUsageExit(0);
    }

    setDefaultConfig(&cfg);

    optind = 1;
    while ((c = getopt(argc, argv, "f:F:t:p:P:v:V:s:S:b:r:A:e:E:I:m:z:l:dZih?")) != -1) {
        switch (c) {
        /// Parameters with optarg.
        case 'f':
            break;
        case 'F':
            break;
        case 't':
            cfg.runTime = atoi(optarg);
            break;
        case 'p':
            {
                string str(optarg);
                if (str == "21M") {
                    cfg.pConfig.pSize = TwentyOneMegaSize;
                } else if (str == "4k") {
                    cfg.pConfig.pSize = UHDSize;
                } else if (str == "5M") {
                    cfg.pConfig.pSize = FIVEMegaSize;
                } else if (str == "2M") {
                    cfg.pConfig.pSize = TWOMegaSize;
                }else if (str == "1080p") {
                    cfg.pConfig.pSize = FHDSize;
                } else if (str == "720p") {
                    cfg.pConfig.pSize = HDSize;
                } else if (str == "VGA") {
                    cfg.pConfig.pSize = VGASize;
                } else if (str == "QVGA") {
                    cfg.pConfig.pSize = QVGASize;
                }
                cfg.pConfig.pStride = ALIGN(cfg.pConfig.pSize.width, ALIGN_PREVIEW_WIDTH);
                cfg.pConfig.pSlice  = ALIGN(cfg.pConfig.pSize.height, ALIGN_PREVIEW_HEIGHT);
                break;
            }
        case 'P':
            {
                string str(optarg);
                if (str == "yuv") {
                    cfg.pConfig.pFormat = YUV_FORMAT;
                } else if (str == "raw") {
                    cfg.pConfig.pFormat = RAW_FORMAT;
                    cfg.vConfig.testVideo = false;
                } else if (str == "y16") {
                    cfg.pConfig.pFormat = DEPTH_Y16_FORMAT;
                    cfg.vConfig.testVideo = false;
                } else {
                    CAM_PRINT("Invalid format. Setting to default YUV_FORMAT");
                    cfg.pConfig.pFormat = YUV_FORMAT;
                }
            }
            break;
        case 'v':
            {
                string str(optarg);
                cfg.vConfig.testVideo = true;
                if (str == "4k") {
                    cfg.vConfig.vSize = UHDSize;
                } else if (str == "2M") {
                    cfg.vConfig.vSize = TWOMegaSize;
                } else if (str == "1080p") {
                    cfg.vConfig.vSize = FHDSize;
                } else if (str == "720p") {
                    cfg.vConfig.vSize = HDSize;
                } else if (str == "VGA") {
                    cfg.vConfig.vSize = VGASize;
                } else if (str == "QVGA") {
                    cfg.vConfig.vSize = QVGASize;
                } else if (str == "disable"){
                    cfg.vConfig.testVideo = false;
                }
                cfg.vConfig.vStride = ALIGN(cfg.vConfig.vSize.width, ALIGN_VIDEO_WIDTH);
                cfg.vConfig.vSlice  = ALIGN(cfg.vConfig.vSize.height, ALIGN_VIDEO_HEIGHT);

                break;
            }
        case 'V':
            {
                string str(optarg);
                if (str == "h264") {
                    cfg.vConfig.vFormat = H264_FORMAT;
                } else if (str == "h265") {
                    cfg.vConfig.vFormat = H265_FORMAT;
                }else {
                    CAM_PRINT("invalid video format \"%s\", using default", optarg);
                }
                break;
            }
        case 's':
            {
                string str(optarg);
                if (str == "MAX") {
                    cfg.sConfig.sSize.height = 99999;
                    cfg.sConfig.sSize.width  = 99999;
                } else if (str == "4k") {
                    cfg.sConfig.sSize = UHDSize;
                } else if (str == "5M") {
                    cfg.sConfig.sSize = FIVEMegaSize;
                } else if (str == "2M") {
                    cfg.sConfig.sSize = TWOMegaSize;
                } else if (str == "1080p") {
                    cfg.sConfig.sSize = FHDSize;
                } else if (str == "720p") {
                    cfg.sConfig.sSize = HDSize;
                } else if (str == "VGA") {
                    cfg.sConfig.sSize = VGASize;
                } else if (str == "QVGA") {
                    cfg.sConfig.sSize = QVGASize;
                }
                cfg.sConfig.testSnapshot = true;
                break;
            }
        case 'S':
            {
                string str(optarg);
                if (str == "jpeg") {
                    cfg.sConfig.sFormat = JPEG_FORMAT;
                } else if (str == "raw") {
                    cfg.sConfig.sFormat = RAW_FORMAT;
                } else {
                    CAM_PRINT("invalid snapshot format \"%s\", using default", optarg);
                }
                break;
            }
        case 'b':
            cfg.sConfig.numSnapshot = atoi(optarg);
            CAM_PRINT("Not support setting numbers of snapshot now ");
            break;
        case 'r':
            cfg.fpsInfo.fps = atoi(optarg);
            cfg.fpsInfo.isFixed = true;
            if (!(cfg.fpsInfo.fps    == 15
                  || cfg.fpsInfo.fps == 30
                  || cfg.fpsInfo.fps == 60
                  || cfg.fpsInfo.fps == 90
                  || cfg.fpsInfo.fps == 120)) {
                cfg.fpsInfo.fps = DEFAULT_CAMERA_FPS;
                CAM_PRINT("Invalid fps values. Using default = %d ", cfg.fpsInfo.fps);
            }
            cfg.vConfig.vFps = cfg.fpsInfo.fps;

            break;
        case 'A':
            {
                cfg.antibanding = optarg;
            }
            break;
        case 'e':
            {
                string str(optarg);
                if (str == "average") {
                    cfg.expMeteringMode = AECAlgoMeteringModeFrameAverage;
                } else if (str == "center") {
                    cfg.expMeteringMode = AECAlgoMeteringModeCenterWeighted;
                } else if (str == "spot") {
                    cfg.expMeteringMode = AECAlgoMeteringModeSpot;
                } else {
                    CAM_PRINT("invalid exposure metering mode \"%s\", using default", optarg);
                }
                break;
            }
        case 'E':
            {
                uint32_t expTimeMs = atoi(optarg);
                if (expTimeMs == 0 || expTimeMs > 1000) {
                    CAM_PRINT("Invalid exposure time value. Using default");
                    cfg.expTimeNs = DEFAULT_EXPOSURE_TIME_VALUE;
                } else {
                    cfg.expTimeNs = expTimeMs * NS_PER_MS;
                }
                break;
            }
        case 'I':
            cfg.iso = atoi(optarg);
            if (cfg.iso < MIN_ISO_VALUE || cfg.iso > MAX_ISO_VALUE) {
                CAM_PRINT("Invalid iso value. Using default");
                cfg.iso = DEFAULT_ISO_VALUE;
            }
            break;
        case 'm':
            cfg.tofMode = atoi(optarg);
            break;
        case 'z':
            cfg.zoom = atoi(optarg);
            if (cfg.zoom < 0 || cfg.zoom > MAX_ZOOM_VALUE) {
                CAM_PRINT("Invalid zoom value. Using default");
                cfg.zoom = DEFAULT_ZOOM_VALUE;
            }
            break;
        case 'l':
            cfg.logLevel = (AppLoglevel)atoi(optarg);
            break;
        /// Parameters without optarg.
        case 'd':
            cfg.dumpFrames = true;
            break;
        case 'Z':
            cfg.sConfig.enableZSL = true;
            break;
        case 'i':
            cfg.infoMode = true;
            break;
        case 'h':
        case '?':
            break;
        default:
            CAM_ERR_PRINT("parse command line abort! ");
            abort();
            break;
        }
    }
    return cfg;
}

static void PrintCameraConfig(TestConfig *config)
{
    CAM_PRINT("=======================start======================");
    CAM_PRINT("camera %s: func %d", getStringFromEnum(config->func).c_str(), config->func);
    CAM_PRINT("camera %s: testvideo %d", getStringFromEnum(config->func).c_str(), config->vConfig.testVideo);
    CAM_PRINT("camera %s: testSnapshot %d", getStringFromEnum(config->func).c_str(), config->sConfig.testSnapshot);
    CAM_PRINT("camera %s: previewSize %dx%d", getStringFromEnum(config->func).c_str(), config->pConfig.pSize.width, config->pConfig.pSize.height);
    CAM_PRINT("camera %s: previewFormat 0x%x", getStringFromEnum(config->func).c_str(), config->pConfig.pFormat);
    CAM_PRINT("camera %s: videoSize %dx%d", getStringFromEnum(config->func).c_str(), config->vConfig.vSize.width, config->vConfig.vSize.height);
    CAM_PRINT("camera %s: videoFormat 0x%x", getStringFromEnum(config->func).c_str(), config->vConfig.vFormat);
    CAM_PRINT("camera %s: snapshotSize %dx%d", getStringFromEnum(config->func).c_str(), config->sConfig.sSize.width, config->sConfig.sSize.height);
    CAM_PRINT("camera %s: snapshotFormat 0x%x", getStringFromEnum(config->func).c_str(), config->sConfig.sFormat);
    CAM_PRINT("camera %s: ZSL enable %d", getStringFromEnum(config->func).c_str(), config->sConfig.enableZSL);
    CAM_PRINT("camera %s: dumpframes %d", getStringFromEnum(config->func).c_str(), config->dumpFrames);
    CAM_PRINT("camera %s: runTime %ds", getStringFromEnum(config->func).c_str(), config->runTime);
    CAM_PRINT("=======================-end-=======================");
}

static int32_t ParseConfig(char *fileName, TestConfig *camconfig) {

    FILE *fp;

    bool isStreamReadCompleted = false;
    const int MAX_LINE = 128;
    char line[MAX_LINE];
    char *ptr = line;
    char value[MAX_LINE];
    char key[50];
    uint32_t id = 0;
    int32_t camera_index = -1;

    if (!(fp = fopen(fileName, "r"))) {
         ALOGE("failed to open config file: %s", fileName);
         return -1;
    }

    while (fgets(line, MAX_LINE - 1, fp)) {
        //limitations: the first char in the line must not be \n, / or NULL.
        if ((line[0] == '\n') || (line[0] == '/') || line[0] == ' ')
            continue;
        strtok_r(line, "\n", &ptr);
        memset(value, 0x0, sizeof(value));
        memset(key, 0x0, sizeof(key));
        if (isStreamReadCompleted) {
            isStreamReadCompleted = false;
        }
        int len = strlen(line);
        int i, j = 0;

        // This assumes new stream params always start with #
        if (!strcspn(line, "#")) {
            id++;
            continue;
        }

        int pos = strcspn(line, ":");
        if (pos >= len) {
            CAM_PRINT("failed to get the chars of colon in config line ");
            break;
        }
        for (i = 0; i < pos; i++) {
            if (line[i] != ' ') {
                key[j] = line[i];
                j++;
            }
        }

        key[j] = '\0';
        j = 0;
        for (i = pos + 1; i < len; i++) {
             if (line[i] != ' ') {
                 value[j] = line[i];
                 j++;
             }
        }
        value[j] = '\0';
        CAM_PRINT("%s: key = %s value = %s",__FUNCTION__, key, value);
        if (!strncmp("CameraName", key, strlen("CameraName"))) {
            if (!strncmp("hires", value, strlen("hires"))) {
                camconfig->func = CAM_FUNC_HIRES;
            } else if (!strncmp("tracking", value, strlen("tracking"))) {
                camconfig->func = CAM_FUNC_TRACKING;
            } else if (!strncmp("depth", value, strlen("depth"))) {
                camconfig->func = CAM_FUNC_DEPTH;
            } else if (!strncmp("tof", value, strlen("of"))) {
                camconfig->func = CAM_FUNC_TOF;
            } else {
                ALOGE("Unknown CameraName %s found in %s", key, fileName);
                goto READ_FAILED;
            }
            setDefaultConfig(camconfig);
        } else if (!strncmp("CameraId", key, strlen("CameraId"))) {
            camconfig->camId= atoi(value);
        } else if (!strncmp("PreviewWidth", key, strlen("PreviewWidth"))) {
            camconfig->pConfig.pSize.width  = atoi(value);
            camconfig->pConfig.pStride = ALIGN(camconfig->pConfig.pSize.width, ALIGN_PREVIEW_WIDTH);
        } else if (!strncmp("PreviewHeight", key, strlen("PreviewHeight"))) {
            camconfig->pConfig.pSize.height = atoi(value);
            camconfig->pConfig.pSlice = ALIGN(camconfig->pConfig.pSize.height, ALIGN_PREVIEW_HEIGHT);
        } else if (!strncmp("PreviewFormat", key, strlen("PreviewFormat"))) {
            if (!strncmp("YUV", value, strlen("YUV"))) {
                camconfig->pConfig.pFormat = YUV_FORMAT;
            } else if (!strncmp("RAW", value, strlen("RAW"))) {
                camconfig->pConfig.pFormat = RAW_FORMAT;
            } else if (!strncmp("Y16", value, strlen("Y16"))) {
                camconfig->pConfig.pFormat = DEPTH_Y16_FORMAT;
            } else {
                camconfig->pConfig.pFormat = YUV_FORMAT;
                CAM_PRINT("Invalid format for sensor,Setting to default YUV_FORMAT");
                goto READ_FAILED;
            }
       } else if (!strncmp("VideoFormat", key, strlen("VideoFormat"))) {
            camconfig->vConfig.testVideo = true;
            if (!strncmp("YUV", value, strlen("YUV"))) {
                camconfig->vConfig.vFormat = YUV_FORMAT;
            } else if (!strncmp("H264", value, strlen("H264"))) {
                camconfig->vConfig.vFormat = H264_FORMAT;
            } else if (!strncmp("H265", value, strlen("H265"))) {
                camconfig->vConfig.vFormat = H265_FORMAT;
            } else {
                camconfig->vConfig.vFormat = YUV_FORMAT;
                CAM_PRINT("Invalid format for sensor,Setting to default format");
                goto READ_FAILED;
            }
       } else if (!strncmp("VideoWidth", key, strlen("VideoWidth"))) {
           camconfig->vConfig.vSize.width= atoi(value);
           camconfig->vConfig.vStride = ALIGN(camconfig->vConfig.vSize.width, ALIGN_VIDEO_WIDTH);
       } else if (!strncmp("VideoHeight", key, strlen("VideoHeight"))) {
           camconfig->vConfig.vSize.height= atoi(value);
           camconfig->vConfig.vSlice = ALIGN(camconfig->vConfig.vSize.height, ALIGN_VIDEO_HEIGHT);
       } else if (!strncmp("SnapshotFormat", key, strlen("SnapshotFormat"))) {
            camconfig->sConfig.testSnapshot = true;
            if (!strncmp("JPEG", value, strlen("JPEG"))) {
                camconfig->sConfig.sFormat = JPEG_FORMAT;
            } else if (!strncmp("RAW", value, strlen("RAW"))) {
                camconfig->sConfig.sFormat = RAW_FORMAT;
            } else {
                CAM_PRINT("Invalid snapshot format for sensor");
                goto READ_FAILED;
            }
       } else if (!strncmp("SnapshotWidth", key, strlen("SnapshotWidth"))) {
           camconfig->sConfig.sSize.width= atoi(value);
       } else if (!strncmp("SnapshotHeight", key, strlen("SnapshotHeight"))) {
           camconfig->sConfig.sSize.height= atoi(value);
       } else if (!strncmp("FPS", key, strlen("FPS"))) {
           camconfig->fpsInfo.fps = atoi(value);
           camconfig->fpsInfo.isFixed = true;
           if (!(camconfig->fpsInfo.fps	 == 15
               || camconfig->fpsInfo.fps == 30
               || camconfig->fpsInfo.fps == 60
               || camconfig->fpsInfo.fps == 90
               || camconfig->fpsInfo.fps == 120)) {
               camconfig->fpsInfo.fps = DEFAULT_CAMERA_FPS;
               CAM_PRINT("Invalid fps values. Using default = %d ", camconfig->fpsInfo.fps);
               goto READ_FAILED;
           }
           camconfig->vConfig.vFps = camconfig->fpsInfo.fps;
       } else if (!strncmp("ExposureValue", key, strlen("ExposureValue"))) {
           camconfig->expTimeNs= atoi(value);
       } else if (!strncmp("ISOValue", key, strlen("ISOValue"))) {
           camconfig->iso = atoi(value);
       } else if (!strncmp("AntibandingMode", key, strlen("AntibandingMode"))) {
           camconfig->antibanding= atoi(value);
       } else if (!strncmp("ZSLSnapshot", key, strlen("ZSLSnapshot"))) {
           if (!strncmp("true", value, strlen("true"))) {
               camconfig->sConfig.enableZSL = true;
           } else {
               camconfig->sConfig.enableZSL = false;
           }
       } else if (!strncmp("RangeMode", key, strlen("RangeMode"))) {
           camconfig->tofMode= atoi(value);
       } else if (!strncmp("DumpFrames", key, strlen("DumpFrames"))) {
           if (!strncmp("true", value, strlen("true"))) {
               camconfig->dumpFrames = true;
           } else {
               camconfig->dumpFrames = false;
           }
       } else if (!strncmp("RunningTime", key, strlen("RunningTime"))) {
           //The configure of RunningTime must be exist in the configure file
           //since it be identified as the last line of the configure file.
           camconfig->runTime = atoi(value);
           isStreamReadCompleted = true;
       } else {
           ALOGE("Unknown Key %s found in %s", key, fileName);
           goto READ_FAILED;
       }

       if (isStreamReadCompleted) {
            PrintCameraConfig(camconfig);
       }
    }

    fclose(fp);
    return EXIT_SUCCESS;

READ_FAILED:
    fclose(fp);
    exit(EXIT_FAILURE);
}

static int32_t getCamera0Config(int argc, char *argv[],TestConfig *cam0config)
{
   ALOGD("%s: Enter ",__func__);

   int32_t ret = 0;

   if(strcmp(argv[3], "-cam0")) {
     ALOGD("Usage: %s n -cam0 camera0config.txt -cam1 camera1config.txt -cam2 camera2config.txt",argv[0]);
     return -1;
   }

   ret = ParseConfig(argv[4], cam0config);
   if(ret != 0) {
     ALOGD("%s: failed ",__func__);
   }

   return ret;
}

static int32_t getCamera1Config(int argc, char *argv[], TestConfig *cam1config)
{
   ALOGD("%s: Enter ",__func__);

   int32_t ret;

   if(strcmp(argv[5], "-cam1")) {
     ALOGD("Usage: %s n -cam0 camera0config.txt -cam1 camera1config.txt -cam2 camera2config.txt",argv[0]);
     return -1;
   }

   ret = ParseConfig(argv[6], cam1config);
   if(ret != 0) {
     ALOGD("%s: failed ",__func__);
   }

   return ret;
}

static int32_t getCamera2Config(int argc, char *argv[], TestConfig *cam1config)
{
   ALOGD("%s: Enter ",__func__);

   int32_t ret;

   if(strcmp(argv[7], "-cam2")) {
     ALOGD("Usage: %s n -cam0 camera0config.txt -cam1 camera1config.txt -cam2 camera2config.txt",argv[0]);
     return -1;
   }

   ret = ParseConfig(argv[8], cam1config);
   if(ret != 0) {
     ALOGD("%s: failed ",__func__);
   }

   return ret;
}

template <typename TYPE, void(TYPE::*run)()>
void *thread_rounter(void * param)
{
    TYPE *p=(TYPE*)param;

    p->run();

    return NULL;
}

int main(int argc, char* argv[])
{
    int32_t ret = 0;
    CAM_PRINT("Camera1-test is Running...");

    if (strcmp(argv[1], "--config") == 0) {
        static int camNums = 1;
        camNums = atoi(argv[2]);
        CAM_PRINT("%d camera devices  under testing...",camNums);

        if ((camNums <= 0) || (camNums > 3)) {
            CAM_PRINT("Error: invalid camera numbers(%d) found, abort...",camNums);
            return -1;
        }

        TestConfig camrea0config;
        TestConfig camrea1config;
        TestConfig camrea2config;

        Camera1Test *camera0Test = NULL;
        pthread_t camera0Thread;
        int camera0ThreadRet ;

        Camera1Test *camera1Test = NULL;
        pthread_t camera1Thread;
        int camera1ThreadRet ;

        Camera1Test *camera2Test = NULL;
        pthread_t camera2Thread;
        int camera2ThreadRet ;

        ret = getCamera0Config(argc, argv, &camrea0config);
        if (ret != 0) {
            CAM_ERR_PRINT("Error: get Camera0 Config failed, abort...");
            return EXIT_FAILURE;
        }
        setLogMask(camrea0config);

        if (camNums >= 2) {
            ret = getCamera1Config(argc, argv, &camrea1config);
            if (ret != 0) {
                CAM_ERR_PRINT("Error: get Camera1 Config failed, abort...");
                return EXIT_FAILURE;
            }
            setLogMask(camrea1config);
        }

        if (camNums >= 3) {
            ret = getCamera2Config(argc, argv, &camrea2config);
            if (ret != 0) {
                CAM_ERR_PRINT("Error: get Camera2 Config failed, abort...");
                return EXIT_FAILURE;
            }
            setLogMask(camrea2config);
        }

        camera0Test = new Camera1Test(camrea0config);
        camera0ThreadRet = pthread_create( &camera0Thread, NULL, thread_rounter<Camera1Test,&Camera1Test::run>, (void*)(camera0Test));

        if (camNums >= 2) {
            //make sure camera 0 work first.
            sleep(1);
            camera1Test = new Camera1Test(camrea1config);
            camera1ThreadRet = pthread_create( &camera1Thread, NULL, thread_rounter<Camera1Test,&Camera1Test::run>, (void*)(camera1Test));
        }

        if (camNums >= 3) {
            //make sure camera 0&1 work first.
            sleep(1);
            camera2Test = new Camera1Test(camrea2config);
            camera2ThreadRet = pthread_create( &camera2Thread, NULL, thread_rounter<Camera1Test,&Camera1Test::run>, (void*)(camera2Test));
        }

        CAM_PRINT("waiting the first camera thread exit");
        pthread_join(camera0Thread, (void**)&camera0ThreadRet);

        if (camNums >= 2) {
            CAM_PRINT("waiting the second camera thread exit");
            pthread_join(camera1Thread, (void**)&camera1ThreadRet);
        }

        if (camNums >= 3) {
            CAM_PRINT("waiting the third camera thread exit");
            pthread_join(camera2Thread, (void**)&camera2ThreadRet);
        }
        CAM_PRINT("camera1-test exit sucess");

        if (camera0Test) {
            delete camera0Test;
            camera0Test = NULL;
        }
        if (camera1Test) {
            delete camera1Test;
            camera1Test = NULL;
        }
        if (camera2Test) {
            delete camera2Test;
            camera2Test = NULL;
        }

        return EXIT_SUCCESS;
    } else {

        TestConfig config = parseCommandline(argc, argv);

        setLogMask(config);

        Camera1Test *test = new Camera1Test(config);
        pthread_t testthread;
        int testthread_ret ;

        testthread_ret = pthread_create( &testthread, NULL, thread_rounter<Camera1Test,&Camera1Test::run>, (void*)(test));

        pthread_join( testthread, (void**)&testthread_ret );

        return EXIT_SUCCESS;
    }
}
