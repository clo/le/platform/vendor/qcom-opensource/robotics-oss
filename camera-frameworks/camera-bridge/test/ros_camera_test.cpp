/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "ros_camera_test.h"

#define QCAMERA_DUMP_LOCATION       "/data/misc/camera/dumps/"
#define MAX_BUF_SIZE 128


void ROSCameraTest::onPreviewFrameAvaible(void *data, uint64_t size, uint64_t timestamp)
{

    uint64_t diff;
    int ret;
    FILE* fp;

    mTimeStampCurr = systemTime();
    diff = mTimeStampCurr - mTimeStampPrev;
    mPreviewFpsAvg = ((mPreviewFpsAvg * mFrameCount) + (1e9 / diff)) / (mFrameCount + 1);
    mFrameCount++;
    mTimeStampPrev  = mTimeStampCurr;

    if (mFrameCount % 30 == 0) {
        char name[MAX_BUF_SIZE];

        snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.yuv",
                 mParams.width,
                 mParams.height,
                 mFrameCount,
                 timestamp,
                 getStringFromEnum(mParams.cameraFunc).c_str());

         fp = fopen(name, "wb");
         if (!fp) {
             CAM_PRINT("error: fopen failed for %s\n", name);
         } else {
             fwrite(data, size, 1, fp);
             CAM_PRINT("Camera[%d] saved filename %s\n", mCameraId, name);

             fclose(fp);
         }
    }

}

ROSCameraTest::ROSCameraTest():
    mROSCamera(NULL),
    mCameraId(-1)
{
    mROSCamera = new CameraAPPInterface();
    if (NULL == mROSCamera) {
        CAM_PRINT("ERROR: new ros camera failed!\n");
    }
}

ROSCameraTest::~ROSCameraTest()
{

}

int ROSCameraTest::run()
{
    int ret = 0;

    mParams.cameraId   = -1;
    mParams.cameraFunc = CAM_FUNC_HIRES;
    mParams.streamType = APP_PREVIEW_STREAM;
    mParams.format     = APP_YUV_FORMAT;
    mParams.width      = 1280;
    mParams.height     = 720;
    mParams.targetFPS  = 30;

    CAM_PRINT("Start to subscribe camera, function is %d \n", mParams.cameraFunc);
    ret = mROSCamera->subscribecamera(mParams, this);
    if ( 0 != ret ) {
        CAM_PRINT("subscribecamera failed!\n");
        goto ERROR;
    }

    mCameraId = mROSCamera->getCameraId();
    mParams.cameraId = mCameraId;
    CAM_PRINT("Camera[%d] subscribed success, waiting for 10s...... \n",mCameraId);

    sleep(10);

    CAM_PRINT("Camera[%d] start to unsubscribe\n",mCameraId);
    ret = mROSCamera->unsubscribecamera(mCameraId);
    if ( 0 != ret ) {
        CAM_PRINT("Camera[%d] unsubscribe failed!\n",mCameraId);
        goto ERROR;
    }

    CAM_PRINT("Camera[%d] unsubscribed success\n",mCameraId);

    if (mParams.streamType == APP_PREVIEW_STREAM) {
        CAM_PRINT("Camera[%d] Average preview FPS = %.2f \n",mCameraId, mPreviewFpsAvg);
    }

    return EXIT_SUCCESS;

ERROR:
    CAM_PRINT("error happend, ret %d\n", ret);
    return ret;
}

int main(int argc, char* argv[])
{

    ROSCameraTest* roscamera = new  ROSCameraTest();

    roscamera->run();

    return EXIT_SUCCESS;


}

