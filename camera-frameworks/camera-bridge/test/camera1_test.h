/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*************************************************************************
*
*Application Notes:  Refer readme.md
*
****************************************************************************/
#ifndef CAMERA1_TEST_H
#define CAMERA1_TEST_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/stat.h>
#include <algorithm>

#include <syslog.h>
#include <cutils/properties.h>

// Note: need undefine LOG_PRI from syslog.h here,
//       as Log.h will have new definition for it.
#undef LOG_PRI
#include <utils/Log.h>

#include "Camera1Interface.h"

using namespace std;
using namespace android;
using namespace camera1_if;

#define QCAMERA_DUMP_LOCATION       "/data/misc/camera/"
// Prop to set frequency of YUV data dumping
#define PROP_DUMP_FRAME_FREQ        "persist.camera1.test.dumpfreq"
#define DEFAULT_DUMP_FRAME_FREQ     "200"

#define DEFAULT_EXPOSURE_TIME_VALUE 0
#define DEFAULT_ISO_VALUE           0
#define MIN_ISO_VALUE               100
#define MAX_ISO_VALUE               3200

#define DEFAULT_ZOOM_VALUE          0
#define MAX_ZOOM_VALUE              99

#define DEFAULT_CAMERA_FPS          30
#define MS_PER_SEC                  1000
#define NS_PER_MS                   1000000
#define NS_PER_US                   1000
#define MAX_BUF_SIZE                256

#define TAKEPICTURE_TIMEOUT_MS      5000

static Size TwentyOneMegaSize(5488,4112);
static Size UHDSize(3840,2160);
static Size FIVEMegaSize(2592,1944);
static Size TWOMegaSize(1600,1200);
static Size FHDSize(1920,1080);
static Size HDSize(1280,720);
static Size VGASize(640,480);
static Size QVGASize(320,240);

static const char usageStr[] =
    "Usage: camera1-test [options]                                                     \n"
    "                                                                                  \n"
    "  -f <type>       camera-type                                                     \n"
    "                    - hires                                                       \n"
    "                    - tracking                                                    \n"
    "                    - depth                                                       \n"
    "                    - tof                                                         \n"
    "  -F <id>         forcing camera-id (-f will become invalid in forcing-id mode)   \n"
    "                    - id (0 ~ [MAX-ID])                                           \n"
    "  -t <duration>   capture duration in seconds [10s]                               \n"
    "                    - N (s)                                                       \n"
    "  -p <size>       Set resolution for preview frame                                \n"
    "                    - 4k             ( 3840x2160 )                                \n"
    "                    - 5M             ( 2592x1944)                                 \n"
    "                    - 1080p          ( 1920x1080 )                                \n"
    "                    - 720p           ( 1280x720 )                                 \n"
    "                    - VGA            ( 640x480 )                                  \n"
    "                    - QVGA           ( 320x240 )                                  \n"
    "                    - stereoVGA      ( 1280x480 : Stereo only - Max resolution )  \n"
    "                    - stereoQVGA     ( 640x240  : Stereo only )                   \n"
    "  -P <format>     Preview format                                                  \n"
    "                    - yuv : YUV format (default)                                  \n"
    "                    - raw : RAW format                                            \n"
    "                    - y16 : Depth format                                          \n"
    "  -v <size>       Set resolution for video frame                                  \n"
    "                    - 4k             ( 3840x2160 )                                \n"
    "                    - 1080p          ( 1920x1080 )                                \n"
    "                    - 720p           ( 1280x720 )                                 \n"
    "                    - VGA            ( 640x480 )                                  \n"
    "                    - QVGA           ( 320x240 )                                  \n"
    "                    - stereoVGA      ( 1280x480 : Stereo only - Max resolution )  \n"
    "                    - stereoQVGA     ( 640x240  : Stereo only )                   \n"
    "                    - disable        ( do not start video stream )                \n"
    "  -V <format>     Video Format                                                    \n"
    "                    - yuv  : yuv  format (default)                                \n"
    "                    - h264 : h264 format                                          \n"
    "                    - h265 : h265 format                                          \n"
    "  -s <size>       take pickture at set resolution ( disabled by default)          \n"
    "                    - MAX            ( max picture resolution supported by sensor)\n"
    "                    - 4k             ( 3840x2160 )                                \n"
    "                    - 5M             ( 2592x1944)                                 \n"
    "                    - 1080p          ( 1920x1080 )                                \n"
    "                    - 720p           ( 1280x720 )                                 \n"
    "                    - VGA            ( 640x480 )                                  \n"
    "                    - QVGA           ( 320x240 )                                  \n"
    "                    - stereoVGA      ( 1280x480 : Stereo only - Max resolution )  \n"
    "                    - stereoQVGA     ( 640x240  : Stereo only )                   \n"
    "  -S <format>     Snapshot Format                                                 \n"
    "                    - jpeg : JPEG format (default)                                \n"
    "                    - raw  : Full-size MIPI RAW format                            \n"
    "  -b <num>        Snapshot burst mode                                             \n"
    "                    - num : is the number of burst snapshot (default = 1)         \n"
    "  -r <fps>        set fps value (Enter supported fps for requested resolution)    \n"
    "                    - 15                                                          \n"
    "                    - 30 (default)                                                \n"
    "                    - 60                                                          \n"
    "                    - 90                                                          \n"
    "                    - 120                                                         \n"
    "  -A <type>       set anti-banding                                                \n"
    "                    - auto                                                        \n"
    "                    - 50hz                                                        \n"
    "                    - 60hz                                                        \n"
    "                    - off (default)                                               \n"
    "  -e <type>       set exposure metering mode                                      \n"
    "                    - average (default)                                           \n"
    "                    - center                                                      \n"
    "                    - spot                                                        \n"
    "  -E <value>      set manual exposure time (ms)                                   \n"
    "                    - N (0 ~ 1000 ms)                                             \n"
    "  -I <value>      set manual iso value                                            \n"
    "                    - N (100 ~ 3200)                                              \n"
    "  -m <value>      TOF range mode                                                  \n"
    "                    - 0: short range mode                                         \n"
    "                    - 1: long range mode                                          \n"
    "  -z <level>      set zoom level                                                  \n"
    "                    - N (0 ~ 99)                                                  \n"
    "  -l <level>      syslog level [0]                                                \n"
    "                    - 0: silent                                                   \n"
    "                    - 1: error                                                    \n"
    "                    - 2: info                                                     \n"
    "                    - 3: debug                                                    \n"
    "  -d              dump frames                                                     \n"
    "  -Z              enable ZSL                                                      \n"
    "  -i              info mode (print camera capabilities)                           \n"
    "  -h              print this message                                              \n"
;

struct CameraCaps
{
    Vector<Size> pSizes, vSizes, picSizes,rawPicSizes;
    Vector<string> previewFormats;
    Vector<Range>  previewFpsRanges;
    Vector<int>    videoFpsMode;
    string rawSize;
};

struct CameraFpsInfo
{
    bool isFixed;
    int  fps;
};

enum CamFunction {
    CAM_FUNC_INVALID      = -1,  // invalid
    CAM_FUNC_HIRES        = 0,   // high-resolution
    CAM_FUNC_TRACKING     = 1,   // tracking
    CAM_FUNC_DEPTH        = 2,   // depth image module
    CAM_FUNC_TOF          = 3,   // TOF camera module
    CAM_FUNC_MAX,
};

static const char* gCameraModuleList[CAM_FUNC_MAX][5] =
{
  /// CAM_FUNC_HIRES
  {"imx318", "ov8856", "ov5695", "", ""},
  /// CAM_FUNC_TRACKING
  {"imx258", "ov7251", "", "", ""},
  /// CAM_FUNC_DEPTH
  {"al6100_ov9282", "", "", "", ""},
  /// CAM_FUNC_TOF
  {"mn34906", "", "", "", ""},
};

enum TOFCamMode {
    TOF_SHORT_RANGE_MODE    = 0,
    TOF_LONG_RANGE_MODE     = 1,
    TOF_MODE_MAX,
};

enum AppLoglevel {
    CAM_LOG_SILENT = 0,
    CAM_LOG_ERROR  = 1,
    CAM_LOG_INFO   = 2,
    CAM_LOG_DEBUG  = 3,
    CAM_LOG_MAX,
};

/**
*  Helper class to store all parameter settings
*/
typedef struct TestConfig
{
    CamFunction    func;
    int            camId;
    bool           dumpFrames;
    bool           infoMode;
    PreviewConfig  pConfig;
    SnapshotConfig sConfig;
    VideoConfig    vConfig;
    uint32_t       runTime;
    uint32_t       dumpFrameFreq;
    uint64_t       expTimeNs;
    uint32_t       iso;
    CameraFpsInfo  fpsInfo;
    AppLoglevel    logLevel;
    string         antibanding;
    uint32_t       tofMode;
    int32_t        zoom;
    uint32_t       expMeteringMode;
}TestConfig;

/**
 * CLASS  Camera1Test
 *
 * - inherits Camera1Callbacks which provides core functionality
 * - User must define onPreviewFrame (virtual) function. It is
 *    the callback function for every preview frame.
 * - If user is using VideoStream then the user must define
 *    onVideoFrame (virtual) function. It is the callback
 *    function for every video frame.
 * - If any error occurs,  onError() callback function is
 *    called. User must define onError if error handling is
 *    required.
 */
class Camera1Test : Camera1Callbacks
{
public:
    Camera1Test(TestConfig config);
    ~Camera1Test();
    void run();

    int initialize();
    int getCameraId() { return this->mCamId; }
    TestConfig getTestConfig() { return this->mConfig; }

    /* callback methods */
    virtual void onPreviewFrame(CameraFrame frame);
    virtual void onVideoFrame(CameraFrame frame);
    virtual void onPictureFrame(CameraFrame frame);
    virtual void onMetadataFrame(CameraFrame frame);
    virtual void onRawFrame(CameraFrame frame);

private:
    void queryCapabilities();
    void printCapabilities();
    void setSnapshotParameters();
    void setPreviewVideoParameters();
    void setAdvanceParameters();
    void setHiresCameraParameters();
    void setTrackingCameraParameters();
    void setDepthCameraParameters();
    void setTOFCameraParameters();
    int setParameters();
    int setPostParameters();
    int takePicture();
    int setFPSindex(TestConfig& cfg, int &pFpsIdx, int &vFpsIdx);

    struct timespec getNextTime(int ms)
    {
        struct timespec time;
        struct timeval now;
        gettimeofday(&now, NULL);
        time.tv_sec = now.tv_sec + TAKEPICTURE_TIMEOUT_MS / MS_PER_SEC;
        time.tv_nsec = now.tv_usec * NS_PER_US + (TAKEPICTURE_TIMEOUT_MS % MS_PER_SEC) * NS_PER_MS;
        return time;
    }

    uint32_t align_size(uint32_t size, uint32_t align)
    {
        return ((size + align - 1) & ~(align-1));
    }

private:
    Camera1Interface*     mCameraIF;
    TestConfig            mConfig;
    CameraCaps            mCaps;
    volatile uint64_t     mPreviewFrameCount,mVideoFrameCount,mRawFrameCount;
    volatile uint64_t     mPreviewTimeStampPrev,mVideoTimeStampPrev,mRawTimeStampPrev;
    volatile float        mPreviewFpsAvg, mVideoFpsAvg,mRawFpsAvg;
    bool                  mIsPicTaking;
    bool                  mPreviewOn;
    bool                  mVideoOn;
};

inline void printUsageExit(int code);
static int setDefaultConfig(TestConfig *cfg);
static void setLogMask(TestConfig config);
static bool MatchCameraName(CamFunction func, const char* cameraName);

static string getStringFromEnum(CamFunction e)
{
    switch(e)
    {
        case CAM_FUNC_HIRES:        return "hires";
        case CAM_FUNC_TRACKING:     return "tracking";
        case CAM_FUNC_DEPTH:        return "depth";
        case CAM_FUNC_TOF:          return "tof";
        default:
             break;
    }
    return "unknown";
}

inline void printUsageExit(int code)
{
    CAM_PRINT("[Camera1 API test application]\n");
    CAM_PRINT("Supported module list:");

    int sizeColumn = sizeof(gCameraModuleList[0]) / sizeof(gCameraModuleList[0][0]);
    int sizeRow = sizeof(gCameraModuleList) / sizeof(gCameraModuleList[0][0]) / sizeColumn;
    for (int i = 0; (i < sizeRow) && (i < CAM_FUNC_MAX); i++) {
        char buf[MAX_BUF_SIZE];
        int offset = 0;
        for (int j = 0; (j < sizeColumn) && ('\0' != *gCameraModuleList[i][j]); j++) {
            offset += snprintf((buf + offset), (MAX_BUF_SIZE - offset), " %s", gCameraModuleList[i][j]);
            if (offset >= MAX_BUF_SIZE) {
                break;
            }
        }
        CAM_PRINT("[%d]%s:\n %s", i, getStringFromEnum((CamFunction)i).c_str(), buf);
    }

    CAM_PRINT("\n %s", usageStr);
    exit(code);
}

#endif
