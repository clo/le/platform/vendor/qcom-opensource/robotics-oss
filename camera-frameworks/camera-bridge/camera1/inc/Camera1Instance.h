/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*************************************************************************
*
*Application Notes:  Refer readme.md
*
****************************************************************************/
#ifndef CAMERA1_INSTANCE_H
#define CAMERA1_INSTANCE_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <inttypes.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#include <errno.h>
#include <sys/stat.h>
#include <algorithm>

#include <syslog.h>
// Note: need undefine LOG_PRI from syslog.h here,
//       as Log.h will have new definition for it.
#undef LOG_PRI
#include <utils/Log.h>

#include "Camera1Interface.h"

using namespace std;
using namespace android;
using namespace camera1_if;

#define QCAMERA_DUMP_LOCATION       "/data/misc/camera/dumps/"

#define DEFAULT_EXPOSURE_VALUE      250
#define MIN_EXPOSURE_VALUE          1
#define MAX_EXPOSURE_VALUE          65535
#define DEFAULT_GAIN_VALUE          300
#define MIN_GAIN_VALUE              256
#define MAX_GAIN_VALUE              2048
#define DEFAULT_EXPOSURE_TIME_VALUE 0

#define DEFAULT_CAMERA_FPS          30
#define MS_PER_SEC                  1000
#define NS_PER_MS                   1000000
#define NS_PER_US                   1000
#define MAX_BUF_SIZE                128

#define SNAPSHOT_WIDTH_ALIGN        64
#define SNAPSHOT_HEIGHT_ALIGN       64
#define TAKEPICTURE_TIMEOUT_MS      5000

Size UHDSize(3840,2160);
Size FIVEMegaSize(2592,1944);
Size FHDSize(1920,1080);
Size HDSize(1280,720);
Size VGASize(640,480);
Size QVGASize(320,240);
Size stereoVGASize(1280, 480);
Size stereoQVGASize(640,240);

enum CamFunction {
    CAM_FUNC_HIRES        = 0,   //hi-res
    CAM_FUNC_TRACKING     = 1,   //tracking ov
    CAM_FUNC_STEREO       = 2,   // _todo, stereo
    CAM_FUNC_LEFT_SENSOR  = 3,   //left ov
    CAM_FUNC_RIGHT_SENSOR = 4,   //right ov
    CAM_FUNC_DEPTH        = 5,   //depth image module
    CAM_FUNC_MAX,
};

struct cameraAppParameters {
   CamFunction cameraFunc;
   int cameraId;
   int streamType;
   int width;
   int height;
   int format;
   int targetFPS;
};

struct CameraCaps
{
    Vector<Size> pSizes, vSizes, picSizes,rawPicSizes;
    Vector<string> focusModes, wbModes, isoModes;
    //vector<string> sharpnessEdgeModes, tonemap_modes;
    Range brightness, sharpness, contrast, gainRange;
    Range64 exposureRange;
    Vector<string> previewFormats;
    Vector<Range>  previewFpsRanges;
    Vector<int>    videoFpsMode;
    string rawSize;
};

struct CameraFpsInfo
{
    bool isFixed;
    int  fps;
};

struct cameraOutputSizeInfo
{
    int width;
    int height;
};

enum OutputStreamType{
    CAM_INSTANCE_PREVIEW_STREAM     = 0,
    CAM_INSTANCE_VIDEO_STREAM       = 1,
    CAM_INSTANCE_SNAPSHOT_STREAM    = 2,
    CAM_INSTANCE_RAW_STREAM         = 3,
    CAM_INSTANCE_MAX_STREAM,
};

enum AppLoglevel {
    CAM_LOG_SILENT = 0,
    CAM_LOG_ERROR  = 1,
    CAM_LOG_INFO   = 2,
    CAM_LOG_DEBUG  = 3,
    CAM_LOG_MAX,
};

/**
*  Helper class to store all parameter settings
*/
struct CameraInstanceConfig
{
    CamFunction func;
    bool dumpFrames;
    bool infoMode;
    bool testSnapshot;
    bool testVideo;
    bool testContrastToneMap;
    int runTime;
    int exposureValue;
    int gainValue;
    int expTimeValue;
    int storagePath;
    int statsLogMask;
    int numSnapshot;
    int sharpnessValue;
    OutputFormatType outputFormat;
    OutputFormatType snapshotFormat;
    Size pSize;
    Size vSize;
    Size picSize;
    cameraOutputSizeInfo previewSize;
    cameraOutputSizeInfo videoSize;
    cameraOutputSizeInfo snapshotSize;
    CameraFpsInfo fpsInfo;
    AppLoglevel logLevel;
    string focusModeStr;
    string antibanding;
    string isoMode;
    string wbMode;
};

/**
 * Interface for application callbacks object. Application needs
 * to implement this interface to get access to camera data and
 * control events. The methods in listener can be invoked from
 * multiple different threads. Client needs to make sure that
 * implementation is thread-safe.
 */

class AppCallbacks: virtual public RefBase
{
public:
    AppCallbacks() {};
    ~AppCallbacks() {};
    virtual void onPreviewFrameAvaible(void *data, uint64_t size, uint64_t timestamp) = 0;
};

/**
 * CLASS  Camera1Instance
 *
 * - inherits Camera1Callbacks which provides core functionality
 * - User must define onPreviewFrame (virtual) function. It is
 *    the callback function for every preview frame.
 * - If user is using VideoStream then the user must define
 *    onVideoFrame (virtual) function. It is the callback
 *    function for every video frame.
 * - If any error occurs,  onError() callback function is
 *    called. User must define onError if error handling is
 *    required.
 */
class Camera1Instance : Camera1Callbacks
{
public:

    Camera1Instance();
    Camera1Instance(cameraAppParameters appParam);
    Camera1Instance(CameraInstanceConfig config);
    ~Camera1Instance();
    void setCameraInstanceConfig(cameraAppParameters param);
    int startCameraInstance(AppCallbacks* cb);
    int stopCameraInstance();
    int initialize();
    int getCameraId() { return mCamId; }
    void getCameraParametres();
    CameraInstanceConfig getTestConfig() { return mConfig; }
    int setApplicationCallback(AppCallbacks *cb);
    /* callback methods */
    virtual void onError();
    virtual void onPreviewFrame(CameraFrame frame);
    virtual void onVideoFrame(CameraFrame frame);
    virtual void onPictureFrame(CameraFrame frame);
    virtual void onMetadataFrame(CameraFrame frame);

private:

    int dumpToFile(void* data, uint32_t size, char* name, uint64_t timestamp);
    void queryCapabilities();
    void printCapabilities();
    void setSnapshotParameters();
    int setHiresCameraParameters();
    int setTrackingCameraParameters();
    int setDepthCameraParameters();
    int setParameters();
    int setPostParameters();
    int takePicture();
    int setFPSindex(CameraInstanceConfig& cfg, int &pFpsIdx, int &vFpsIdx);
    int setDefaultConfig(CameraInstanceConfig &cfg);
    void setLogMask(CameraInstanceConfig config);

    struct timespec getNextTime(int ms)
    {
        struct timespec time;
        struct timeval now;
        gettimeofday(&now, NULL);
        time.tv_sec = now.tv_sec + TAKEPICTURE_TIMEOUT_MS / MS_PER_SEC;
        time.tv_nsec = now.tv_usec * NS_PER_US + (TAKEPICTURE_TIMEOUT_MS % MS_PER_SEC) * NS_PER_MS;
        return time;
    }

    string getStringFromEnum(CamFunction e)
    {
        switch(e)
        {
            case CAM_FUNC_HIRES:        return "hires";
            case CAM_FUNC_TRACKING:     return "tracking";
            case CAM_FUNC_STEREO:       return "stereo";
            case CAM_FUNC_LEFT_SENSOR:  return "left";
            case CAM_FUNC_RIGHT_SENSOR: return "right";
            case CAM_FUNC_DEPTH:        return "depth";
            default:
                printf("error: unknown camera type \n");
            break;
        }
        return "unknown";
    }

    uint32_t align_size(uint32_t size, uint32_t align)
    {
        return ((size + align - 1) & ~(align-1));
    }

private:
    Camera1Interface*     mCameraIF;
    CameraInstanceConfig  mConfig;
    CameraCaps            mCaps;
    pthread_cond_t        cvPicDone;
    pthread_mutex_t       mLock;
    uint64_t              mFrameCount;
    uint64_t              mTimeStampPrev;
    uint64_t              mTimeStampCurr;
    float                 mPreviewFpsAvg, mVideoFpsAvg;
    bool                  mIsPicTaking;
    AppCallbacks*         mCB;
    bool                  mUseAppCallbacks;
};

#endif
