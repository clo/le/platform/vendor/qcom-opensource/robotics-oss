
/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*************************************************************************
*
*Application Notes
*
****************************************************************************/
#ifndef CAMERA_APP_INTERFACE_H
#define CAMERA_APP_INTERFACE_H

#include "Camera1Instance.h"

enum OutputStreamType_{
    APP_PREVIEW_STREAM     = 0,
    APP_VIDEO_STREAM       = 1,
    APP_SNAPSHOT_STREAM    = 2,
    APP_RAW_STREAM         = 3,
    APP_MAX_STREAM,
};

enum OutputFormatType_{
    APP_YUV_FORMAT       = 0,
    APP_RAW_FORMAT       = 1,
    APP_JPEG_FORMAT      = 2,
    APP_DEPTH_Y16_FORMAT = 3,
    APP_MAX_FORMAT,
};

struct CameraFpsInfo_
{
    bool isFixed;
    int  fps;
};

/**
 * CLASS  ROSNODECamera
 *
 */
class CameraAPPInterface
{
public:
    CameraAPPInterface();
    ~CameraAPPInterface();
    int subscribecamera(cameraAppParameters param,AppCallbacks* cb);
    int unsubscribecamera(int camID);
    int getCameraId() { return mCameraId; };

private:

    AppCallbacks*    mCb;
    Camera1Instance* mCamera1Instance;
    int              mCameraId;
};
#endif

