/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef __CAMERA1_FWI_H__
#define __CAMERA1_FWI_H__

#include "Camera1Common.h"
#include "Camera1Interface.h"
#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>

namespace camera1_if
{
static const int VALUE_SIZE_MAX = 32;

class Camera1FWI : public Camera1Interface
{
public:
    Camera1FWI(int cameraId);
    virtual ~Camera1FWI();
    /* Implementation of virtual methods of ICameraDevice interface */
    static int getNumberOfCameras();
    static int getCameraInfo(int cameraId, struct CameraInfo& cameraInfo);
    virtual int startPreview();
    virtual int stopPreview();
    virtual int takePicture(bool zsl);
    virtual int startRecording(VideoConfig videoConfig);
    virtual int stopRecording();
    virtual const Camera1Params getParameters();
    virtual int setParameters(Camera1Params& params);
    virtual int setCallbacks(Camera1Callbacks* cb);
    virtual int sendCommand(int32_t cmd, int32_t arg1, int32_t arg2);
    virtual int ErrorNotify();

private:
    int init(int cameraId);
    void deinit();
    int getID() { return mCameraId; }
    sp<Camera> get_native_camera() { return mCamera;}

private:
    sp<Camera> mCamera;

};

} /* namespace camera */

#endif

