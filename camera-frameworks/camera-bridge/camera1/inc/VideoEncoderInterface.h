/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// @file  VideoEncoderInterface.h
/// @brief mid objetct for video encoder test
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _VIDEO_ENCODER_INTERFACE_
#define _VIDEO_ENCODER_INTERFACE_

#include <OMX_Video.h>
#include <OMX_Core.h>
#include <OMX_Component.h>
#include <OMX_QCOMExtns.h>
#include <OMX_VideoExt.h>
#include <QComOMXMetadata.h>

#include "QCamxHAL3TestOMXEncoder.h"
#include "Camera1Common.h"

using namespace std;
using namespace android;

namespace camera1_if
{

#define TIME_TO_WAIT    (1)

class VideoEncoderInterface: public QCamxHAL3TestBufferHolder
{
public:
    VideoEncoderInterface(VideoConfig config);
   ~VideoEncoderInterface();
    void run();
    void stop();
    int Read(OMX_BUFFERHEADERTYPE *buf);
    OMX_ERRORTYPE Write(OMX_BUFFERHEADERTYPE *buf);
    OMX_ERRORTYPE EmptyDone(OMX_BUFFERHEADERTYPE *buf);
    void EnqueueFrameBuffer(CameraFrame frame);

    bool ExistInFrameQueue (CameraFrame item, list<CameraFrame> * list)
    {
        /// std::list
        if (list == NULL) {
            ALOGE("%s: frame list %p ", __FUNCTION__, list);
            return false;
        }

        std::list<CameraFrame>::iterator itr = list->begin();
        for (; itr != list->end(); itr++) {
            if (itr->data == item.data) {
                return true;
            }
        }
        return false;
    }

private:
    QCamxHAL3TestOMXEncoder *       mCoder;
    omx_config_t                    mConfig;
    FILE*                           mOutFd;
    uint64_t                        mTimeOffset;
    uint32_t                        mTimeOffsetInc;
    list<CameraFrame> *             mFrameQueue;
    pthread_mutex_t                 mLock;
    pthread_mutex_t                 mBufferLock;
    pthread_cond_t                  mCondBufSync;
    pthread_cond_t                  mCondBufDone;
    bool                            mIsRunning;
};

}
#endif
