/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __CAMERA_COMMON_H__
#define __CAMERA_COMMON_H__

#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <sstream>
#include <vector>
#include <pthread.h>
#include <queue>
#include <list>
#include <map>

#include <utils/Log.h>
#include <utils/Errors.h>
#include <utils/Timers.h>
#include <utils/Vector.h>
#include <utils/Mutex.h>
#include <utils/Condition.h>
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#include <camera/Camera.h>
#include <camera/CameraParameters.h>

using namespace std;
using namespace android;

namespace camera1_if
{

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "CAM_BRIDGE"

#ifdef LOG_NIDEBUG
#undef LOG_NIDEBUG
#endif
#define LOG_NIDEBUG 0

#define CAM_ERR(fmt, args...) do { \
    ALOGE("%s %d:" fmt, __func__, __LINE__, ##args); \
} while (0)

#define CAM_INFO(fmt, args...) do { \
    ALOGI("%s %d:" fmt, __func__, __LINE__, ##args); \
} while (0)

#define CAM_DBG(fmt, args...) do { \
    ALOGD("%s %d:" fmt, __func__, __LINE__, ##args); \
} while (0)

#define CAM_PRINT(fmt, args...) do { \
    ALOGI("%s %d:" fmt, __func__, __LINE__, ##args); \
    printf(fmt "\n", ##args); \
} while (0)

#define CAM_ERR_PRINT(fmt, args...) do { \
    ALOGE("%s %d:" fmt, __func__, __LINE__, ##args); \
    printf(fmt "\n", ##args); \
} while (0)

#define CAMERA_STORAGE_DIR "/data/misc/camera"

/**
 * Structure for storing values for ranged parameters such as
 * brightness, contrast, fps etc.
 */
typedef struct Range
{
    int min;    /*!< minimum value */
    int max;    /*!< maximum value */
    int step;    /*!< step increment for intermediate values */

    Range() : min(0), max(0), step(0) {}
    Range(int m, int x, int s) : min(m), max(x), step(s) {}
} Range;

typedef struct Range64
{
    uint64_t min;     /*!< minimum value */
    uint64_t max;     /*!< maximum value */
    uint64_t step;     /*!< step increment for intermediate values */

    Range64() : min(0), max(0), step(0) {}
    Range64(uint64_t m, uint64_t x, uint64_t s) : min(m), max(x), step(s) {}
} Range64;

typedef struct FrameInfo {
    uint32_t                   size;
    uint32_t                   width;
    uint32_t                   height;
    uint32_t                   stride;
    uint32_t                   slice;
    uint32_t                   format;
} FrameInfo;

/**
 * Structure for storing frame information
 */
typedef struct CameraFrame
{
    void*     data;
    int       size;
    uint64_t  timeStamp;
    FrameInfo frameinfo;
} CameraFrame;

typedef enum
{
    YUV_FORMAT           = 0,
    RAW_FORMAT           = 1,
    JPEG_FORMAT          = 2,
    DEPTH_Y16_FORMAT     = 3,
    H264_FORMAT          = 4,
    H265_FORMAT          = 5,
    OUTPUT_FORMAT_MAX    = 6,
} OutputFormatType;

typedef struct PreviewConfig {
    Size               pSize;
    uint32_t           pStride;
    uint32_t           pSlice;
    OutputFormatType   pFormat;
} PreviewConfig;

typedef struct SnapshotConfig {
    bool               testSnapshot;
    bool               enableZSL;
    Size               sSize;
    uint32_t           numSnapshot;
    OutputFormatType   sFormat;
} SnapshotConfig;

typedef struct VideoConfig {
    bool               testVideo;
    Size               vSize;
    uint32_t           bitrate;
    uint32_t           targetBitrate;
    uint32_t           vFps;
    uint32_t           vStride;
    uint32_t           vSlice;
    bool               isBitRateConstant;
    OutputFormatType   vFormat;
} VideoConfig;

typedef struct _pthreadInfo{
    pthread_t                pid;
    pthread_attr_t           attr;
    Mutex                    mutex; //pthread_mutex_t
    Condition                cond;  //pthread_cond_t
    volatile bool            isRunning;
} PthreadInfo;

// Align size of preview/video must keep the same with bufferhandler.cpp
#define ALIGN_PREVIEW_WIDTH  128
#define ALIGN_PREVIEW_HEIGHT 32
#define ALIGN_VIDEO_WIDTH    128
#define ALIGN_VIDEO_HEIGHT   32

static inline uint32_t ALIGN(uint32_t operand, uint32_t alignment)
{
    uint32_t remainder = (operand % alignment);

    return (0 == remainder) ? operand : operand - remainder + alignment;
}

}
#endif
