/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "VideoEncoderInterface.h"

namespace camera1_if
{

/************************************************************************
* name : QCamxTestVideoEncoderInterface
* function: init default setting
************************************************************************/
VideoEncoderInterface::VideoEncoderInterface(VideoConfig config)
{
    nsecs_t timestampCurr = systemTime();

    mCoder =  QCamxHAL3TestOMXEncoder::getInstance();
    mConfig = {
        .componentName = (char *)"OMX.qcom.video.encoder.avc",
        .inputcolorfmt = HAL_PIXEL_FORMAT_YCbCr_420_SP_VENUS,  //NV12
        /*
         * Support fmt: 0x7fa30c06 HAL_PIXEL_FORMAT_YCbCr_420_SP_VENUS_UBWC
         * Support fmt: 0x7fa30c04 HAL_PIXEL_FORMAT_YCbCr_420_SP_VENUS
         * Support fmt: 0x7fa30c00 HAL_PIXEL_FORMAT_NV21_ENCODEABLE or OMX_QCOM_COLOR_FormatYVU420SemiPlanar or QOMX_COLOR_FormatYVU420SemiPlanar
         * Support fmt: 0x7fa30c09 HAL_PIXEL_FORMAT_YCbCr_420_TP10_UBWC
         * Support fmt: 0x7fa30c0a HAL_PIXEL_FORMAT_YCbCr_420_P010_VENUS
         * Support fmt: 0x7fa30c08
         * Support fmt: 0x7fa30c07 QOMX_VIDEO_CodingMVC
         * Support fmt: 0x7f000789 OMX_COLOR_FormatAndroidOpaque
         * Support fmt: 0x15
         */
        .codec      = OMX_VIDEO_CodingAVC,
        /*avc config*/
        .npframe    = 29,
        .nbframes   = 0,
        .eprofile   = OMX_VIDEO_AVCProfileBaseline,
        .elevel     = OMX_VIDEO_AVCLevel3,
        .bcabac     = 0,
        .nframerate = 30,

        /*buf config*/
        .storemeta = (DISABLE_META_MODE == 1)? 0:1,

        /*input port param*/
        .input_w = 1920,
        .input_h = 1080,
        .input_buf_cnt = 20,

        /*output port param*/
        .output_w = 1920,
        .output_h = 1080,
        .output_buf_cnt = 20,
    };
    mConfig.input_w = mConfig.output_w = config.vSize.width;
    mConfig.input_h = mConfig.output_h = config.vSize.height;
    mConfig.bitrate           = config.bitrate;
    mConfig.targetBitrate     = config.targetBitrate;
    mConfig.isBitRateConstant = config.isBitRateConstant;
    mConfig.nframerate        = config.vFps;
    mTimeOffsetInc            = 30000;
    if (mConfig.nframerate > 60) {
        mTimeOffsetInc = 15000;
    }
    char path[256] = {0};
    if (config.vFormat == H265_FORMAT) {
        mConfig.componentName = (char *)"OMX.qcom.video.encoder.hevc",
        mConfig.codec    = OMX_VIDEO_CodingHEVC,
        mConfig.eprofile = OMX_VIDEO_HEVCProfileMain,
        mConfig.elevel   = OMX_VIDEO_HEVCHighTierLevel3,
        snprintf(path, sizeof(path), "%s/camera_0_%llu.h265", CAMERA_STORAGE_DIR,timestampCurr);
    } else {
        snprintf(path, sizeof(path), "%s/camera_0_%llu.h264", CAMERA_STORAGE_DIR,timestampCurr);
    }

    if (OMX_ErrorNone != mCoder->setConfig(&mConfig, this)) {
        raise(SIGSEGV); //trigger dump
    }

    mOutFd = fopen(path,"w+");

    mTimeOffset = 0;
    mFrameQueue= new list<CameraFrame>;
    mIsRunning = false;

    pthread_mutex_init(&mLock,       NULL);
    pthread_mutex_init(&mBufferLock, NULL);
    pthread_condattr_t cattr;
    pthread_condattr_init(&cattr);
    pthread_condattr_setclock(&cattr, CLOCK_MONOTONIC);
    pthread_cond_init(&mCondBufSync, &cattr);
    pthread_cond_init(&mCondBufDone, &cattr);
    pthread_condattr_destroy(&cattr);
}

/************************************************************************
* name : ~VideoEncoderInterface
* function: ~VideoEncoderInterface
************************************************************************/
VideoEncoderInterface::~VideoEncoderInterface()
{
    if (mOutFd != NULL){
        fclose(mOutFd);
        mOutFd = NULL;
    }

    CAM_DBG("waitting for frame queue to empty");
    while (mFrameQueue->size() > 0) {
        CAM_INFO("frame queue size %d\n", mFrameQueue->size());
        mFrameQueue->pop_front();
    }

    delete mFrameQueue;
    mFrameQueue = NULL;

    delete mCoder;
    mCoder = NULL;
    pthread_mutex_destroy(&mLock);
    pthread_mutex_destroy(&mBufferLock);
    pthread_cond_destroy(&mCondBufSync);
    pthread_cond_destroy(&mCondBufDone);
}

/************************************************************************
* name : run
* function: start encoder thread
************************************************************************/
void VideoEncoderInterface::run()
{
    CAM_DBG("E");
    mCoder->start();
    mIsRunning = true;
    CAM_DBG("X");
}

/************************************************************************
* name : stop
* function: stop encoder
************************************************************************/
void VideoEncoderInterface::stop()
{
    CAM_DBG("E");
    mCoder->stop();

    pthread_mutex_lock(&mLock);
    mIsRunning = false;
    pthread_cond_signal(&mCondBufSync);
    pthread_mutex_unlock(&mLock);

    pthread_mutex_lock(&mBufferLock);
    while (mFrameQueue->size() > 0) {
        CAM_INFO("VideoEncoderInterface::stop: mFrameQueue:%d\n", mFrameQueue->size());
        mFrameQueue->pop_front();
    }
    pthread_cond_signal(&mCondBufDone);
    pthread_mutex_unlock(&mBufferLock);

    CAM_DBG("X");
}

/************************************************************************
* name : Read
* function: handler to fill a buffer of handle to omx input port
************************************************************************/
int VideoEncoderInterface::Read(OMX_BUFFERHEADERTYPE *buf)
{
    int ret = 0;

    if (NULL == mFrameQueue) {
        CAM_ERR("fail: mFrameQueue is NULL");
        return -1;
    }

    if (false == mIsRunning) {
        CAM_DBG("X Recording is not running");
        return -1;
    }

    pthread_mutex_lock(&mLock);
    while (mFrameQueue->size() == 0 && true == mIsRunning) {
        struct timespec tv;
        clock_gettime(CLOCK_MONOTONIC,&tv);
        tv.tv_sec += TIME_TO_WAIT;//s
        ret = pthread_cond_timedwait(&mCondBufSync, &mLock, &tv);
        if (ret != 0) {
            /*CallStack e;
              e.update();
              e.log("VideoEncoderInterface");*/
            pthread_mutex_unlock(&mLock);
            CAM_ERR("waiting timeout");
            return -1;
        }
    }
    CameraFrame frame = mFrameQueue->front();
    void* data = frame.data;
    if (NULL == data) {
        CAM_ERR("frame data is NULL");
        pthread_mutex_unlock(&mLock);
        return -1;
    }

    int size = (frame.size < buf->nAllocLen ? frame.size : buf->nAllocLen);
    CAM_DBG("memcpy size = %d", size);
    memcpy(buf->pBuffer, data, size);

    buf->nFilledLen = buf->nAllocLen;
    buf->nTimeStamp = mTimeOffset;
    mTimeOffset += mTimeOffsetInc;
    pthread_mutex_lock(&mBufferLock);
    mFrameQueue->pop_front();
    pthread_cond_signal(&mCondBufDone);
    pthread_mutex_unlock(&mBufferLock);

    pthread_mutex_unlock(&mLock);

    return ret;
}

/************************************************************************
* name : Write
* function: handler to write omx output data
************************************************************************/
OMX_ERRORTYPE VideoEncoderInterface::Write(OMX_BUFFERHEADERTYPE *buf)
{
    fwrite(buf->pBuffer, 1, buf->nFilledLen, mOutFd);
    return OMX_ErrorNone;
}

/************************************************************************
* name : EmptyDone
* function: handler to put input buffer
************************************************************************/
OMX_ERRORTYPE VideoEncoderInterface::EmptyDone(OMX_BUFFERHEADERTYPE *buf)
{
    return OMX_ErrorNone;
}

/************************************************************************
* name : EnqueueFrameBuffer
* function: enq a buffer to input port queue
************************************************************************/
void VideoEncoderInterface::EnqueueFrameBuffer(CameraFrame frame)
{
    CAM_DBG("E data %p size %d", frame.data, frame.size);
    if (NULL == frame.data || NULL == mFrameQueue) {
        CAM_ERR("fail: No frames to Enqueue.");
        return;
    }

    if (false == mIsRunning) {
        CAM_DBG("X Recording is not running");
        return;
    }

    pthread_mutex_lock(&mLock);
    mFrameQueue->push_back(frame);
    pthread_cond_signal(&mCondBufSync);
    pthread_mutex_unlock(&mLock);


    pthread_mutex_lock(&mBufferLock);
    /// Need to wait for data copy done.
    while (ExistInFrameQueue(frame, mFrameQueue)) {
        struct timespec tv;
        clock_gettime(CLOCK_MONOTONIC,&tv);
        tv.tv_sec += TIME_TO_WAIT; //s
        if (pthread_cond_timedwait(&mCondBufDone, &mBufferLock, &tv)) {
            /*CallStack e;
              e.update();
              e.log("VideoEncoderInterface");*/
            CAM_ERR("waiting timeout");
            mFrameQueue->pop_front();
            break;
        }
    }
    pthread_mutex_unlock(&mBufferLock);
    CAM_DBG("X");
}
} /* namespace camera */
