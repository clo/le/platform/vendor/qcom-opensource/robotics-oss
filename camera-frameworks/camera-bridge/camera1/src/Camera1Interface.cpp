/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "Camera1Interface.h"
#include "Camera1FWI.h"

namespace camera1_if
{
///////////////////////////////////////////////CameraCallbacks//////////////////////////////////////////////////////////////
void Camera1Callbacks::Init(int timetype)
{
    mCamId = -1;

    mLastFeedTime = 0;
    if (timetype == SYSTEMTIME_TYPE)
    {
        mBootTimestamp   = GetBootTime();
        mSystemTimestamp = GetSystemTime();
        mTimestampOffset = mSystemTimestamp - mBootTimestamp;
        CAM_INFO("TimestampOffset:%llu for system time.", mTimestampOffset);
    } else {
        mBootTimestamp   = 0;
        mSystemTimestamp = 0;
        mTimestampOffset = 0;
        CAM_INFO("TimestampOffset:%llu for boot time.", mTimestampOffset);
    }
}

void Camera1Callbacks::dumpFrameToFile(char* fname, CameraFrame frame)
{
    char tempName[256];

    uint8_t* data   = (uint8_t*)frame.data;
    int size        = frame.frameinfo.size;
    int width       = frame.frameinfo.width;
    int height      = frame.frameinfo.height;
    int stride      = frame.frameinfo.stride;
    int slice       = frame.frameinfo.slice;
    uint32_t format = frame.frameinfo.format;
    int plane_cnt   = 1;
    int pixel_byte  = 1;

    snprintf(tempName, sizeof(tempName), "%s.temp", fname);

    FILE* fd = fopen(tempName, "wb");
    if (!fd) {
        CAM_ERR_PRINT("Camera[%d] fopen failed for %s", mCamId, tempName);
        return;
    }

    switch (format) {
        case HAL_PIXEL_FORMAT_RAW10:
        case HAL_PIXEL_FORMAT_BLOB:
        {
            fwrite(data, size, 1, fd);
            break;
        }
        case HAL_PIXEL_FORMAT_YCBCR_420_888:
        case HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED:
        case HAL_PIXEL_FORMAT_Y16:
        case HAL_PIXEL_FORMAT_RAW16:
        {
            if (format == HAL_PIXEL_FORMAT_Y16 || format == HAL_PIXEL_FORMAT_RAW16) {
                plane_cnt  = 1;
                pixel_byte = 2;
            } else {
                plane_cnt  = 2;
                pixel_byte = 1;
            }

            for (int idx = 1; idx <= plane_cnt; idx++) {
                for (int h = 0; h < height / idx; h++) {
                    fwrite(data, (width * pixel_byte), 1, fd);
                    data += stride;
                }
                data += stride * (slice - height);
            }
            break;
        }
        default:
        {
            CAM_ERR_PRINT("Cannot matched format %d", format);
            break;
        }
    }

    fclose(fd);

    if (rename(tempName, fname)) {
        CAM_ERR_PRINT("rename failed for %s", fname);
        return;
    }

    CAM_PRINT("Camera[%d] saved filename %s", mCamId, fname);
}

void Camera1Callbacks::notify(int32_t msgType, int32_t ext1, int32_t ext2)
{
    switch (msgType) {
        case CAMERA_MSG_ERROR:
        {
            onError();
            break;
        }
        default:
        {
            CAM_INFO("unknown msg %d", msgType);
            break;
        }
    }
}

void Camera1Callbacks::onError()
{
    CAM_PRINT("camera error! but Do Nothing now");
}

void Camera1Callbacks::postData(int32_t msgType, const sp<IMemory>& dataPtr,
                          camera_frame_metadata_t *metadata)
{
    if (dataPtr == NULL) {
        return onError();
    }

    // Camx sets meta tag of org.quic.camera.qtimer.timestamp.
    // But callback processer return NULL metadata without timestamp.
    // So just set system time as timestamp.
    CAM_DBG("msgType %d, just use the systemtime...", msgType);
    nsecs_t timestamp = systemTime();
    postDataTimestamp(timestamp, msgType, dataPtr);
}

void Camera1Callbacks::postDataTimestamp(nsecs_t timestamp,
                          int32_t msgType, const sp<IMemory>& dataPtr)
{
    if (dataPtr == NULL) {
        return onError();
    }

    ssize_t offset = 0;
    size_t size = 0;
    sp<IMemoryHeap> heap = dataPtr->getMemory(&offset, &size);
    uint8_t *heapBase = (uint8_t*)heap->base();
    if (NULL == heapBase) {
        return onError();
    }

    CameraFrame frame;
    frame.data = (void *)(heapBase + offset);
    if (IsSystemtime(timestamp)) {
        frame.timeStamp = (uint64_t)timestamp;
    } else {
        frame.timeStamp = (uint64_t)timestamp + mTimestampOffset;
    }

    memcpy(&frame.frameinfo, frame.data, sizeof(FrameInfo));

    frame.data += sizeof(FrameInfo);
    frame.size = frame.frameinfo.size;

    switch (msgType) {
        case CAMERA_MSG_PREVIEW_FRAME:
        {
            onPreviewFrame(frame);
            break;
        }
        case CAMERA_MSG_VIDEO_FRAME:
        {
            onVideoFrame(frame);
            break;
        }
        case CAMERA_MSG_COMPRESSED_IMAGE:
        {
            onPictureFrame(frame);
            {
                Mutex::Autolock l(mLock);
                mPictureSignal.signal();
            }
            break;
        }
        case CAMERA_MSG_RAW_IMAGE:
        {
            onRawFrame(frame);
            break;
        }
        default:
        {
            break;
        }
    }

    uint64_t diffSeconds = (frame.timeStamp - mLastFeedTime) / 1e9;

    if (frame.timeStamp > mLastFeedTime && diffSeconds >= 1) {
        Mutex::Autolock l(mLock);
        //CAM_DBG("feed per 1s");
        mLastFeedTime = frame.timeStamp;
        mCbMonitorSignal.signal();
    }
}

///////////////////////////////////////////////Camera1Params//////////////////////////////////////////////////////////////
/**
 * parses the comma separated values from a string and returns a
 * vector of individual strings
 *
 * @param valueStr [in] comma separated value string
 *
 * @return vector<string>
 */
Vector<string> Camera1Params::parseCSV(const char *valueStr)
{
    Vector<string> values;
    if (NULL == valueStr) {
        CAM_ERR("valueStr NULL!");
        return values;
    }

    CAM_DBG("valueStr = %s", valueStr);

    string input(valueStr);
    istringstream ss(input);
    string token;
    while(getline(ss, token, ',')) {
        values.push_back(token);
    }
    return values;
}

/**
 * parses the comma separated values from a string and returns a
 * vector of individual ints
 *
 * @param valueStr [in] comma separated value string
 *
 * @return vector<int>
 */
Vector<int> Camera1Params::parseCIV(const char *valueStr)
{
    Vector<int> values;
    if (NULL == valueStr) {
        CAM_ERR("valueStr NULL!");
        return values;
    }

    CAM_DBG("valueStr = %s", valueStr);

    char *curStr = (char*) valueStr;
    while (true) {
        int token = (int)strtol(curStr, &curStr, 10);
        if (*curStr != ',' && *curStr != '\0') {
            CAM_ERR("contains invalid character");
            break;
        }
        values.push_back(token);

        if (*curStr == '\0') {
            break;
        }

        curStr++;
    }
    
    return values;
}

/**
 * parses text within paranthesis and sends a vector of all such
 * text found.
 * For example with input  "(10, 20), (15, 30)", it returns
 * vector with elements "10, 20" and "15, 30"
 *
 * @param valueStr
 *
 * @return vector<string>
 */
Vector<string> Camera1Params::parseParanthesis(const char *valueStr)
{
    Vector<string> values;
    char *p = (char *) valueStr;
    char buf[VALUE_SIZE_MAX];
    int idx = 0;

    if (NULL == valueStr) {
        CAM_ERR("valueStr NULL!");
        return values;
    }

    while (true) {
        idx = 0;
        /* find opening paranthesis */
        while (*p != '(' && *p != '\0') {
            p++;
        }
        if (*p == '\0') {
            break;
        }
        p++;
        /* find closing parenthesis */
        while (*p != ')' && *p != '\0' && (idx < VALUE_SIZE_MAX - 1)) {
            buf[idx] = *p;
            idx++;
            p++;
        }
        if (*p == '\0') {
            CAM_ERR("parse error: valueStr=%s", valueStr);
            break;
        }
        buf[idx] = '\0';
        string str(buf);
        values.push_back(str);
    }
    return values;
}

Vector<string> Camera1Params::getStringV(const char *key) const
{
    return parseCSV(get(key));
}

Vector<int> Camera1Params::getIntV(const char *key) const
{
    return parseCIV(get(key));
}

void Camera1Params::setString(const char *key, string value)
{
    set(key, value.c_str());
}

string Camera1Params::getString(const char *key) const
{
    const char *valueString = get(key);

    if (NULL == valueString) {
        return string();
    } 

    return string(valueString);
}

void Camera1Params::setPreviewFpsRange(int min_fps, int max_fps)
{
    String8 str = String8::format("%d,%d", min_fps, max_fps);
    set(CameraParameters::KEY_PREVIEW_FPS_RANGE, str.string());
}

Vector<Range> Camera1Params::getSupportedPreviewFpsRanges() const
{
    Vector<Range> ranges;
    int rc = 0;

    Vector<string> strRanges = parseParanthesis(
        get(CameraParameters::KEY_SUPPORTED_PREVIEW_FPS_RANGE));

    Range range;
    range.step = 1;

    for (size_t i = 0; i < strRanges.size(); i++) {
        rc = sscanf(strRanges[i].c_str(), "%d,%d", &range.min, &range.max);
        /* sscanf returns number of arguments successfully assigned.
           On success, it would be 2 */
        if (rc != 2) {
            CAM_ERR("parse failed for value: %s", strRanges[i].c_str());
            break;
        }
        ranges.push_back(range);
    }
    return ranges;
}

///////////////////////////////////////////////Camera1Interface///////////////////////////////////////////////////////////
Camera1Interface::Camera1Interface(int cameraId) :
    mCameraId(cameraId),
    mStatus(CAMERA1STATUS_INVALID),
    mCb(NULL),
    mVideoEncoderIF(NULL)
{
    mCbMonitorThread.isRunning = false;
}

int Camera1Interface::getNumberOfCameras(Camera1BridgeMode mode)
{
    int rc = NO_ERROR;

    switch (mode) {
        case MODE_FWI:
        {
            rc = Camera1FWI::getNumberOfCameras();
            break;
        }
        case MODE_LEGACY:
        case MODE_QMMF:
        default:
            break;
    }

    return rc;
}

int Camera1Interface::getCameraInfo(Camera1BridgeMode mode, 
                                           int cameraId, struct CameraInfo& cameraInfo)
{
    int rc = NO_ERROR;
    switch (mode) {
        case MODE_FWI:
        {
            rc = Camera1FWI::getCameraInfo(cameraId, cameraInfo);
            break;
        }
        case MODE_LEGACY:
        case MODE_QMMF:
        default:
            break;
    }

    return rc;
}

int Camera1Interface::createInstance(Camera1BridgeMode mode,
                                            int camId, Camera1Interface** cameraIF)
{
    Camera1Interface *me = NULL; 
    int rc = NO_ERROR;

    CAM_DBG("Camera[%d] E", camId);

    if (NULL == cameraIF) {
        rc = -EINVAL;
        goto bail;
    }
    *cameraIF = NULL;

    switch (mode) {
        case MODE_FWI:
        {
            me = static_cast<Camera1Interface*>(new Camera1FWI(camId));
            break;
        }
        case MODE_LEGACY:
        {
            // TODO: Support legacy HAL1 API
            // me = static_cast<Camera1Interface*>(new Camera1LGI(camId));
            break;
        }
        case MODE_QMMF:
        {
            // TODO: Support QMMF API
            // me = static_cast<Camera1Interface*>(new Camera1QMMF(camId));
            break;
        }
        default:
        {
            break;
        }
    }

    if (me == NULL) {
        rc = -ENOMEM;
        goto bail;
    }

    me->mBridgeMode = mode;
    *cameraIF = me;

    if (CAMERA1STATUS_INVALID != (*cameraIF)->mStatus) {
        rc = NO_INIT;
    }

bail:
    CAM_DBG("X, rc = %d", rc);
    return rc;
}

void Camera1Interface::deleteInstance(Camera1Interface** cameraIF)
{
    if (cameraIF == NULL || *cameraIF == NULL) {
        return;
    }

    delete *cameraIF;
    *cameraIF = NULL;
}

void Camera1Interface::CreateVideoEncoder(VideoConfig config)
{
    if (NULL == mVideoEncoderIF) {
        mVideoEncoderIF = new VideoEncoderInterface(config);
    }
}

void Camera1Interface::ReleaseVideoEncoder()
{
    if (NULL != mVideoEncoderIF) {
        delete mVideoEncoderIF;
        mVideoEncoderIF = NULL;
    }
}

void Camera1Interface::GenerateVideoByFrame(CameraFrame frame)
{
    if (mVideoEncoderIF) {
        mVideoEncoderIF->EnqueueFrameBuffer(frame);
    }
}

int Camera1Interface::setCallbacks(Camera1Callbacks* cb)
{
    if (NULL == cb) {
        CAM_ERR("%s: failed", __FUNCTION__);
        return -EPERM;
    }

    mCb = cb;

    return NO_ERROR;
}

int Camera1Interface::StartCbMonitor()
{
    int ret = NO_ERROR;

    if (mCbMonitorThread.isRunning == true) {
        CAM_INFO("Already Monitoring");
        return ret;
    }

    CAM_INFO("Camera[%d] create cb monitor thread", mCameraId);

    mCbMonitorThread.isRunning = true;
    ret = pthread_create(&mCbMonitorThread.pid,
                         NULL,
                         CbMonitorThread,
                         (void *)this);
    if (ret) {
        CAM_ERR("%s, failed to create thread", __func__, ret);
    }

    return ret;
}

void Camera1Interface::StopCbMonitor()
{
    CAM_INFO("Camera[%d] E", mCameraId);

    if (NULL == mCb) {
        CAM_ERR("callback was not set!");
        return;
    }

    if (false == mCbMonitorThread.isRunning) {
        CAM_ERR("CbMonitorThread is not running X");
        return;
    }

    {
        Mutex::Autolock _l(mCb->mLock);
        mCbMonitorThread.isRunning = false;
        mCb->mCbMonitorSignal.signal();
    }

    pthread_join(mCbMonitorThread.pid, NULL);

    CAM_INFO("X");
}

void* Camera1Interface::CbMonitorThread(void *data)
{
    int ret = NO_ERROR;

    CAM_INFO("start");

    Camera1Interface *pme = reinterpret_cast<Camera1Interface *>(data);
    if (NULL == pme->mCb) {
        CAM_ERR("callback was not set!");
        return NULL;
    }

    while(true == pme->mCbMonitorThread.isRunning) {
        Mutex::Autolock l(pme->mCb->mLock);
        ret = pme->mCb->mCbMonitorSignal.waitRelative(pme->mCb->mLock, kTimeout);
        if (ret == TIMED_OUT) {
            CAM_ERR_PRINT("Error: Camera[%d] wait timeout for cbs!", pme->mCameraId);
            pme->ErrorNotify();
            pme->mCbMonitorThread.isRunning = false;
            break;
        }
    }

    CAM_INFO("exit");

    return NULL;
}

} /* namespace camera */
