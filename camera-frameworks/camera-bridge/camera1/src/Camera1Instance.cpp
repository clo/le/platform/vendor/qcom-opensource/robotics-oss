/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*************************************************************************
*
*Application Notes:  Refer readme.md
*
****************************************************************************/
#include "Camera1Instance.h"

static inline void printUsageExit(int code)
{
    CAM_PRINT("please check the parameters again\n");
    exit(code);
}

void Camera1Instance::onError()
{
    CAM_PRINT("camera error! but Do Nothing now\n");
    //exit(EXIT_FAILURE);
}

int Camera1Instance::dumpToFile(void* data, uint32_t size, char* name, uint64_t timestamp)
{
    FILE* fp;
    fp = fopen(name, "wb");
    if (!fp) {
        CAM_PRINT("fopen failed for %s\n", name);
        return -EPERM;
    }
    fwrite(data, size, 1, fp);
    CAM_PRINT("saved filename %s\n", name);
    fclose(fp);
    return NO_ERROR;
}

/**
 *
 * FUNCTION: onPreviewFrame
 *
 *  - This is called every preview frame.
 *  - In the test app, we save files only after every 30 frames.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Instance::onPreviewFrame(CameraFrame frame)
{
    uint64_t diff;
    int ret;

    ret = pthread_mutex_trylock(&mLock);
    if (EBUSY == ret) {
        CAM_PRINT("taking picture, return\n");
        return;
    } else if(true == mIsPicTaking) {
        CAM_PRINT("is taking picture, return\n");
        pthread_mutex_unlock(&mLock);
        return;
    }



    //CAM_PRINT("Camera[%d] onPreviewFrame: mFrameCount %" PRIu64 ", mTimeStampPrev %" PRIu64 ",diff %" PRIu64 ", FpsAvg %.2f, dump %d\n",
    //          this->getCameraId(),this->mFrameCount,mTimeStampPrev,diff,mPreviewFpsAvg, this->mConfig.dumpFrames);
    if (mUseAppCallbacks) {
        if (mConfig.outputFormat == DEPTH_Y16_FORMAT) {
            frame.size = mConfig.pSize.width * mConfig.pSize.height * 2;
        } else {
            frame.size = (mConfig.pSize.width * mConfig.pSize.height * 3) / 2;
        }

        /* send preview frame to app*/
        mCB->onPreviewFrameAvaible(frame.data, frame.size,frame.timeStamp);
    } else {
       mTimeStampCurr = systemTime();
       diff = mTimeStampCurr - mTimeStampPrev;
       mPreviewFpsAvg = ((mPreviewFpsAvg * mFrameCount) + (1e9 / diff)) / (mFrameCount + 1);
       mFrameCount++;
       mTimeStampPrev  = mTimeStampCurr;

       if (mFrameCount % 30 == 0) {
           char name[MAX_BUF_SIZE];
           if (mConfig.outputFormat == RAW_FORMAT) {
               if (mConfig.storagePath == 1) {
                   snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.raw",
                       mConfig.pSize.width,
                       mConfig.pSize.height,
                       mFrameCount,
                       frame.timeStamp,
                       getStringFromEnum(mConfig.func).c_str());
               } else {
                   snprintf(name, MAX_BUF_SIZE, "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.raw",
                       mConfig.pSize.width,
                       mConfig.pSize.height,
                       mFrameCount,
                       frame.timeStamp,
                       getStringFromEnum(mConfig.func).c_str());
               }
          } else {
              if (mConfig.storagePath == 1) {
                  snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.yuv",
                      mConfig.pSize.width,
                      mConfig.pSize.height,
                      mFrameCount,
                      frame.timeStamp,
                      getStringFromEnum(mConfig.func).c_str());
              } else {
                  snprintf(name, MAX_BUF_SIZE, "P_%dx%d_%" PRIu64 "_%" PRIu64 "_%s.yuv",
                      mConfig.pSize.width,
                      mConfig.pSize.height,
                      mFrameCount,
                      frame.timeStamp,
                      getStringFromEnum(mConfig.func).c_str());
              }

              if (mConfig.outputFormat == DEPTH_Y16_FORMAT) {
                  frame.size = mConfig.pSize.width * mConfig.pSize.height * 2;
              } else {
                  frame.size = (mConfig.pSize.width * mConfig.pSize.height * 3) / 2;
              }
        }

        dumpToFile(frame.data, frame.size, name, frame.timeStamp);
      }
    }
    pthread_mutex_unlock(&mLock);
}

/**
 *
 * FUNCTION: onVideoFrame
 *
 *  - This is called every video frame.
 *  - In the test app, we save files only after every 30 frames.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Instance::onVideoFrame(CameraFrame frame)
{
    static uint64_t vFrameCount     = 0;
    static uint64_t vTimeStampPrev  = 0;

    uint64_t diff = frame.timeStamp - vTimeStampPrev;
    mVideoFpsAvg = ((mVideoFpsAvg * vFrameCount) + (1e9 / diff)) / (vFrameCount + 1);
    vFrameCount++;
    vTimeStampPrev  = frame.timeStamp;

    if (mConfig.dumpFrames == true && vFrameCount % 30 == 0) {
        char name[MAX_BUF_SIZE];
        if(mConfig.storagePath == 1){
            snprintf(name, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "V_%dx%d_%04d_%llu_%s.yuv",
                      mConfig.vSize.width,
                      mConfig.vSize.height,
                      (int)vFrameCount,
                      frame.timeStamp,
                      getStringFromEnum(mConfig.func).c_str());
        } else {
            snprintf(name, MAX_BUF_SIZE, "V_%dx%d_%04d_%llu_%s.yuv",
                      mConfig.vSize.width,
                      mConfig.vSize.height,
                      (int)vFrameCount,
                      frame.timeStamp,
                      getStringFromEnum(mConfig.func).c_str());
        }

        dumpToFile(frame.data, frame.size, name, frame.timeStamp);
    }
}

/**
 *
 * FUNCTION: onPictureFrame
 *
 *  - This is called for every snapshot jpeg frame.
 *  - In the test app, we save all jpeg files.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Instance::onPictureFrame(CameraFrame frame)
{
    char imageName[MAX_BUF_SIZE];

    if (mConfig.snapshotFormat == RAW_FORMAT) {
        if (mConfig.storagePath == 1){
            snprintf(imageName, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "snapshot_mipi_raw10_%dx%d_%lld_%s.raw",
                mConfig.picSize.width,
                mConfig.picSize.height,
                frame.timeStamp,
                getStringFromEnum(mConfig.func).c_str());
        } else {
            snprintf(imageName, MAX_BUF_SIZE, "snapshot_mipi_raw10_%dx%d_%lld_%s.raw",
                mConfig.picSize.width,
                mConfig.picSize.height,
                frame.timeStamp,
                getStringFromEnum(mConfig.func).c_str());
        }
    } else {
        if(mConfig.storagePath == 1){
            snprintf(imageName, MAX_BUF_SIZE, QCAMERA_DUMP_LOCATION "snapshot_%dx%d_%lld_%s.jpg",
                mConfig.picSize.width,
                mConfig.picSize.height,
                frame.timeStamp,
                getStringFromEnum(mConfig.func).c_str());
        } else {
            snprintf(imageName, MAX_BUF_SIZE, "snapshot_%dx%d_%lld_%s.jpg",
                mConfig.picSize.width,
                mConfig.picSize.height,
                frame.timeStamp,
                getStringFromEnum(mConfig.func).c_str());
        }
    }

    dumpToFile(frame.data, frame.size, imageName, frame.timeStamp);
    /* notify the waiting thread about picture done */
    pthread_mutex_lock(&mLock);
    pthread_cond_signal(&cvPicDone);
    pthread_mutex_unlock(&mLock);
    CAM_PRINT("%s:%d\n", __func__, __LINE__);
}

/**
 *
 * FUNCTION: onMetadataFrame
 *
 *  - This is called every metadata frame.
 *  - In parameter frame (Camera1Frame) also has the timestamps
 *    field which is public.
 *
 * @param frame
 *
 */
void Camera1Instance::onMetadataFrame(CameraFrame frame)
{

}

Camera1Instance::Camera1Instance() :
    mCameraIF(NULL),
    mFrameCount(0),
    mTimeStampPrev(0),
    mTimeStampCurr(0),
    mVideoFpsAvg(0.0f),
    mPreviewFpsAvg(0.0f),
    mIsPicTaking(false),
    mUseAppCallbacks(false)
{
    pthread_cond_init(&cvPicDone, NULL);
    pthread_mutex_init(&mLock, NULL);
}

Camera1Instance::Camera1Instance(cameraAppParameters appParam) :
    mCameraIF(NULL),
    mFrameCount(0),
    mTimeStampPrev(0),
    mTimeStampCurr(0),
    mVideoFpsAvg(0.0f),
    mPreviewFpsAvg(0.0f),
    mIsPicTaking(false),
    mUseAppCallbacks(false)
{
    setCameraInstanceConfig(appParam);
    pthread_cond_init(&cvPicDone, NULL);
    pthread_mutex_init(&mLock, NULL);
}


Camera1Instance::Camera1Instance(CameraInstanceConfig config) :
    mCameraIF(NULL),
    mFrameCount(0),
    mTimeStampPrev(0),
    mTimeStampCurr(0),
    mVideoFpsAvg(0.0f),
    mPreviewFpsAvg(0.0f),
    mIsPicTaking(false),
    mUseAppCallbacks(false)
{
    mConfig = config;
    pthread_cond_init(&cvPicDone, NULL);
    pthread_mutex_init(&mLock, NULL);
}

Camera1Instance::~Camera1Instance()
{
    pthread_cond_destroy(&cvPicDone);
    pthread_mutex_destroy(&mLock);
}

/**
 *  FUNCTION: setDefaultConfig
 *
 *  set default config based on camera module
 *
 * */
int Camera1Instance::setDefaultConfig(CameraInstanceConfig &cfg) {

    cfg.outputFormat        = YUV_FORMAT;
    cfg.dumpFrames          = false;
    cfg.runTime             = 10;
    cfg.infoMode            = false;
    cfg.testVideo           = false;
    cfg.testSnapshot        = false;
    cfg.exposureValue       = DEFAULT_EXPOSURE_VALUE;  /* Default exposure value */
    cfg.gainValue           = DEFAULT_GAIN_VALUE;      /* Default gain value */
    cfg.expTimeValue        = DEFAULT_EXPOSURE_TIME_VALUE;
    cfg.fpsInfo.fps         = DEFAULT_CAMERA_FPS;
    cfg.fpsInfo.isFixed     = true;
    cfg.logLevel            = CAM_LOG_DEBUG;
    cfg.snapshotFormat      = JPEG_FORMAT;
    cfg.statsLogMask        = STATS_NO_LOG;
    cfg.storagePath         = 0;
    cfg.numSnapshot         = 1;
    cfg.sharpnessValue      = -1;
    cfg.testContrastToneMap = false;
    cfg.focusModeStr        = string(VALUE_OFF);
    cfg.antibanding         = string(VALUE_OFF);
    cfg.isoMode             = string(ISO_AUTO);
    cfg.wbMode              = string(CameraParameters::WHITE_BALANCE_AUTO);

    switch (cfg.func) {
    case CAM_FUNC_HIRES:
        cfg.pSize        = HDSize;
        cfg.vSize        = HDSize;
        cfg.picSize      = HDSize;
        break;
    case CAM_FUNC_TRACKING:
        cfg.pSize        = VGASize;
        cfg.vSize        = VGASize;
        cfg.picSize      = VGASize;
        cfg.outputFormat = RAW_FORMAT;
        break;
    case CAM_FUNC_STEREO:
        cfg.pSize       = stereoVGASize;
        cfg.vSize       = stereoVGASize;
        cfg.picSize     = stereoVGASize;
        break;
    case CAM_FUNC_RIGHT_SENSOR:
        cfg.pSize       = VGASize;
        cfg.vSize       = VGASize;
        cfg.picSize     = VGASize;
        break;
    case CAM_FUNC_LEFT_SENSOR:
        cfg.pSize       = VGASize;
        cfg.vSize       = VGASize;
        cfg.picSize     = VGASize;
        break;
    case CAM_FUNC_DEPTH:
        cfg.pSize       = HDSize;
        cfg.vSize       = HDSize;
        cfg.picSize     = HDSize;
        cfg.outputFormat= DEPTH_Y16_FORMAT;
        break;
    default:
        CAM_PRINT("invalid sensor function \n");
        break;
    }
    return 0;
}

void Camera1Instance::setCameraInstanceConfig(cameraAppParameters param)
{
    CameraInstanceConfig cfg;

    cfg.func = param.cameraFunc;

    setDefaultConfig(cfg);

    if (param.streamType & CAM_INSTANCE_VIDEO_STREAM) {
       cfg.testVideo = true;
       /* not implement of the video size control*/
    }

    if (param.streamType & CAM_INSTANCE_SNAPSHOT_STREAM) {
       cfg.testSnapshot= true;
       /* not implement of the snapshot size control*/
    }

    cfg.previewSize.width  = param.width;
    cfg.previewSize.height = param.height;
    cfg.pSize.width        = param.width;
    cfg.pSize.height       = param.height;
    cfg.fpsInfo.fps        = param.targetFPS;
    cfg.fpsInfo.isFixed    = true;
    cfg.logLevel           = CAM_LOG_INFO;

    mConfig = cfg;
}



int Camera1Instance::setApplicationCallback(AppCallbacks *cb)
{
    mCB = cb;
    mUseAppCallbacks = true;
    CAM_INFO("use app callbacks %d\n", mUseAppCallbacks);
    return NO_ERROR;
}

int Camera1Instance::initialize()
{
    int rc = NO_ERROR;

    // 1. returns the number of camera-modules connected on the board
    int numCamera = Camera1Interface::getNumberOfCameras(Camera1Interface::MODE_FWI);

    if (numCamera < 0) {
        CAM_PRINT("getNumberOfCameras() failed, rc = %d\n", numCamera);
        return EXIT_FAILURE;
    }

    CAM_PRINT("num_cameras = %d\n", numCamera);

    if (numCamera < 1) {
        CAM_PRINT("No cameras found.\n");
        return EXIT_FAILURE;
    }

    // 2. get camera information
    if ( mConfig.func < CAM_FUNC_MAX ) {
        /* find camera based on function */
        for ( int i = 0; i < numCamera; i++ ) {
            CameraInfo camInfo;
            Camera1Interface::getCameraInfo(Camera1Interface::MODE_FWI, i, camInfo);
            CAM_INFO(" i = %d , info.func = %d \n",i, camInfo.facing);
            if (mConfig.func == CAM_FUNC_DEPTH) {
               if (camInfo.facing == 0) {
                  mCamId = i;
                  CAM_INFO("mCamId = %d , camera func = %d \n",mCamId, mConfig.func);
                  break;
               }
            } else {
               if ( camInfo.facing == mConfig.func ) {
                  mCamId = i;
                  CAM_INFO("mCamId = %d , camera func = %d \n",mCamId, mConfig.func);
                  break;
               }
            }
        }
    } else {
        mCamId = -1;
        CAM_PRINT("Camera not found, ID be forced to -1\n");
        return EXIT_FAILURE;
    }

    CAM_PRINT("Selected camera success, id is %d\n", mCamId);
    // 3. create camera instance
    rc = Camera1Interface::createInstance(Camera1Interface::MODE_FWI, mCamId, &mCameraIF);
    if (rc != NO_ERROR) {
        CAM_PRINT("could not open camera %d, rc %d\n", mCamId, rc);
        return rc;
    }

    CAM_PRINT("Testing camera id=%d\n", mCamId);

    // 4. set callback functions
    mCameraIF->setCallbacks(this);

    return NO_ERROR;
}

int Camera1Instance::takePicture()
{
    int rc = NO_ERROR;
    pthread_mutex_lock(&mLock);
    mIsPicTaking = true;
    CAM_PRINT("take picture\n");
    rc = mCameraIF->takePicture(true);
    if (rc != NO_ERROR) {
        CAM_PRINT("takePicture failed\n");
        pthread_mutex_unlock(&mLock);
        return rc;
    }

    struct timespec waitTime = getNextTime(TAKEPICTURE_TIMEOUT_MS);

    /* wait for picture done */
    while (mIsPicTaking == true) {
        rc = pthread_cond_timedwait(&cvPicDone, &mLock, &waitTime);
        if (rc == ETIMEDOUT) {
            CAM_PRINT("error: takePicture timed out\n");
            break;
        } else {
            mIsPicTaking = false;
        }
    }
    pthread_mutex_unlock(&mLock);
    return rc;
}


void Camera1Instance::getCameraParametres()
{

}


void Camera1Instance::queryCapabilities()
{
    /* query capabilities */
    mCameraIF->mParams.getSupportedPreviewSizes(mCaps.pSizes);
    mCameraIF->mParams.getSupportedVideoSizes(mCaps.vSizes);
    mCameraIF->mParams.getSupportedPictureSizes(mCaps.picSizes);
    mCameraIF->mParams.getSupportedPictureSizes(mCaps.rawPicSizes);

    mCaps.previewFpsRanges   = mCameraIF->mParams.getSupportedPreviewFpsRanges();
    mCaps.previewFormats     = mCameraIF->mParams.getStringV(CameraParameters::KEY_SUPPORTED_PREVIEW_FORMATS);
    mCaps.focusModes         = mCameraIF->mParams.getStringV(CameraParameters::KEY_SUPPORTED_FOCUS_MODES);
    mCaps.wbModes            = mCameraIF->mParams.getStringV(CameraParameters::KEY_SUPPORTED_WHITE_BALANCE);
    mCaps.isoModes           = mCameraIF->mParams.getStringV(KEY_QC_SUPPORTED_ISO_MODES);
    mCaps.videoFpsMode       = mCameraIF->mParams.getIntV(KEY_QC_SUPPORTED_VIDEO_HIGH_FRAME_RATE_MODES);

    mCaps.brightness.min     = mCameraIF->mParams.getInt(KEY_QC_MIN_BRIGHTNESS);
    mCaps.brightness.max     = mCameraIF->mParams.getInt(KEY_QC_MAX_BRIGHTNESS);
    mCaps.brightness.step    = mCameraIF->mParams.getInt(KEY_QC_BRIGHTNESS_STEP);

    mCaps.sharpness.min      = mCameraIF->mParams.getInt(KEY_QC_MIN_SHARPNESS);
    mCaps.sharpness.max      = mCameraIF->mParams.getInt(KEY_QC_MAX_SHARPNESS);
    mCaps.sharpness.step     = mCameraIF->mParams.getInt(KEY_QC_SHARPNESS_STEP);

    mCaps.contrast.min       = mCameraIF->mParams.getInt(KEY_QC_MIN_CONTRAST);
    mCaps.contrast.max       = mCameraIF->mParams.getInt(KEY_QC_MAX_CONTRAST);
    mCaps.contrast.step      = mCameraIF->mParams.getInt(KEY_QC_CONTRAST_STEP);

    mCaps.rawSize            = mCameraIF->mParams.getString(KEY_QC_RAW_PICUTRE_SIZE);

    //mCaps.exposureRange      = mCameraIF->mParams.getManualExposureRange(mConfig.pSize, mConfig.fpsInfo.fps);
    //mCaps.gainRange          = mCameraIF->mParams.getManualGainRange(mConfig.pSize, mConfig.fpsInfo.fps);
}

void Camera1Instance::printCapabilities()
{
    CAM_PRINT("Camera capabilities\n");

    CAM_PRINT("available preview sizes:\n");
    for (size_t i = 0; i < mCaps.pSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d\n", i, mCaps.pSizes[i].width, mCaps.pSizes[i].height);
    }
    CAM_PRINT("available video sizes:\n");
    for (size_t i = 0; i < mCaps.vSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d\n", i, mCaps.vSizes[i].width, mCaps.vSizes[i].height);
    }
    CAM_PRINT("available jpeg picture sizes:\n");
    for (size_t i = 0; i < mCaps.picSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d\n", i, mCaps.picSizes[i].width, mCaps.picSizes[i].height);
    }
    CAM_PRINT("available raw picture sizes:\n");
    for (size_t i = 0; i < mCaps.rawPicSizes.size(); i++) {
        CAM_PRINT("%d: %d x %d\n", i, mCaps.rawPicSizes[i].width, mCaps.rawPicSizes[i].height);
    }
    CAM_PRINT("available preview formats:\n");
    for (size_t i = 0; i < mCaps.previewFormats.size(); i++) {
        CAM_PRINT("%d: %s\n", i, mCaps.previewFormats[i].c_str());
    }
    CAM_PRINT("available focus modes:\n");
    for (size_t i = 0; i < mCaps.focusModes.size(); i++) {
        CAM_PRINT("%d: %s\n", i, mCaps.focusModes[i].c_str());
    }
    CAM_PRINT("available whitebalance modes:\n");
    for (size_t i = 0; i < mCaps.wbModes.size(); i++) {
        CAM_PRINT("%d: %s\n", i, mCaps.wbModes[i].c_str());
    }
    CAM_PRINT("available ISO modes:\n");
    for (size_t i = 0; i < mCaps.isoModes.size(); i++) {
        CAM_PRINT("%d: %s\n", i, mCaps.isoModes[i].c_str());
    }
    CAM_PRINT("available brightness values:\n");
    CAM_PRINT("min=%d, max=%d, step=%d\n", mCaps.brightness.min,
           mCaps.brightness.max, mCaps.brightness.step);

    CAM_PRINT("available sharpness values:\n");
    CAM_PRINT("min=%d, max=%d, step=%d\n", mCaps.sharpness.min,
           mCaps.sharpness.max, mCaps.sharpness.step);

    CAM_PRINT("available contrast values:\n");
    CAM_PRINT("min=%d, max=%d, step=%d\n", mCaps.contrast.min,
           mCaps.contrast.max, mCaps.contrast.step);

    CAM_PRINT("available preview fps ranges:\n");
    for (size_t i = 0; i < mCaps.previewFpsRanges.size(); i++) {
        CAM_PRINT("%d: [%d, %d]\n", i, mCaps.previewFpsRanges[i].min,
               mCaps.previewFpsRanges[i].max);
    }
    CAM_PRINT("available video fps values:\n");
    for (size_t i = 0; i < mCaps.videoFpsMode.size(); i++) {
        CAM_PRINT("%d: %d\n", i, mCaps.videoFpsMode[i]);
    }

    CAM_PRINT("available manual exposure range: min = %lld , max = %lld \n", mCaps.exposureRange.min, mCaps.exposureRange.max);
    CAM_PRINT("available manual gain range: min = %d , max = %d \n", mCaps.gainRange.min, mCaps.gainRange.max);

    return;
}

/**
 * FUNCTION: setFPSindex
 *
 * scans through the supported fps values and returns index of
 * requested fps in the array of supported fps
 *
 * @param fps      : Required FPS  (Input)
 * @param pFpsIdx  : preview fps index (output)
 * @param vFpsIdx  : video fps index   (output)
 *
 *  */
int Camera1Instance::setFPSindex(CameraInstanceConfig & cfg, int &pFpsIdx, int &vFpsIdx)
{
    size_t i     =  0;
    int rc       =  NO_ERROR;
    int fps      =  cfg.fpsInfo.fps;

    if (cfg.testVideo == true) {
        fps = DEFAULT_CAMERA_FPS;
    }
    for (i = 0; i < mCaps.previewFpsRanges.size(); i++) {
        CAM_INFO("previewFpsRanges[%d] %d - %d, previewFps %d, fpsInfo.isFixed %d\n",
            i,
            mCaps.previewFpsRanges[i].min/1000,
            mCaps.previewFpsRanges[i].max/1000,
            fps,
            cfg.fpsInfo.isFixed);
        if ((mCaps.previewFpsRanges[i].max/1000 == fps) &&
            (mCaps.previewFpsRanges[i].max == mCaps.previewFpsRanges[i].min) &&
            (cfg.fpsInfo.isFixed == true)) {
            break;
        }
        if ((mCaps.previewFpsRanges[i].max/1000 == fps) &&
            (mCaps.previewFpsRanges[i].max != mCaps.previewFpsRanges[i].min)) {
            break;
        }
    }

    if (i >= mCaps.previewFpsRanges.size()) {
        pFpsIdx = -1;
        rc = -EINVAL;
    } else {
        pFpsIdx = i;
    }
    CAM_INFO("preview FPS: i %d, pFpsIdx %d", i, pFpsIdx);

    for (i = 0; i < mCaps.videoFpsMode.size(); i++) {
        CAM_INFO("videoFpsMode[%d] %d, video fps %d \n",
            i,
            mCaps.videoFpsMode[i],
            fps);

        if (fps == 30 * mCaps.videoFpsMode[i]) {
            break;
        }
        if (DEFAULT_CAMERA_FPS == 30 * mCaps.videoFpsMode[i]) {
            break;
        }
    }

    if (i >= mCaps.videoFpsMode.size()) {
        vFpsIdx = -1;
        // Currently do not support KEY_QC_VIDEO_HIGH_FRAME_RATE in C1H3.
        // So ingore the err of video HFR mdoe selecting.
        //rc = -EINVAL;
    } else {
        vFpsIdx = i;
    }
    CAM_INFO("video FPS: i %d, vFpsIdx %d", i, vFpsIdx);

    return rc;
}


void Camera1Instance::setSnapshotParameters()
{
    size_t index;
    Vector<Size> supportedSnapshotSizes;

    if (mConfig.snapshotFormat == RAW_FORMAT) {
        CAM_PRINT("Camera[%d] Setting snapshot format : raw \n",getCameraId());
        mCameraIF->mParams.setPictureFormat(QC_PIXEL_FORMAT_BAYER_MIPI_RAW_10BGGR);

        CAM_PRINT("Camera[%d] raw picture size: %s\n",getCameraId(), mCaps.rawSize.c_str());
        supportedSnapshotSizes = mCaps.rawPicSizes;
        mCameraIF->mParams.set(KEY_QC_RAW_PICUTRE_SIZE, "640x480");
    } else {
        CAM_PRINT("Camera[%d] Setting snapshot format : jpeg \n", getCameraId());
        supportedSnapshotSizes = mCaps.picSizes;
    }

    // Check requested snapshot resolution is supported for requested format
    if (mConfig.picSize.width == 99999 && mConfig.picSize.height == 99999 ) {
        if (supportedSnapshotSizes.size() == 0) {
            CAM_PRINT("Camera[%d] Error: No snapshot resolution found for requested format \n",getCameraId());
            exit(1);
        }
        mConfig.picSize = supportedSnapshotSizes[0];
    } else {
        for (index = 0 ; index < supportedSnapshotSizes.size() ; index++) {
            if ( mConfig.picSize.width == supportedSnapshotSizes[index].width
                && mConfig.picSize.height == supportedSnapshotSizes[index].height)
            {
                mConfig.picSize = supportedSnapshotSizes[index];
                break;
            }
        }
        if (index >= supportedSnapshotSizes.size()) {
            CAM_PRINT("Camera[%d] Error: Snapshot resolution %d x %d not supported for requested format \n",
                getCameraId(),mConfig.picSize.width,mConfig.picSize.height);
            exit(1);
        }
    }

    mCameraIF->mParams.setPictureSize(mConfig.picSize.width, mConfig.picSize.height);
    CAM_PRINT("Camera[%d] Setting snapshot size : %d x %d \n", getCameraId(),
        mConfig.picSize.width, mConfig.picSize.height);

    if (mConfig.snapshotFormat == JPEG_FORMAT) {
        //mCameraIF->mParams.setPictureThumbNailSize(mConfig.picSize);
    }
}

int Camera1Instance::setHiresCameraParameters()
{
    int focusModeIdx = 0;
    int pFpsIdx = 3;
    int vFpsIdx = 3;

    // focus mode
    for (size_t i = 0; i < mCaps.focusModes.size(); i++) {
        if (strcmp(mCaps.focusModes[i].c_str(),mConfig.focusModeStr.c_str()) == 0) {
           /*ignore focuse mode since no reqiurement*/
           //focusModeIdx=i;
        }
    }
    CAM_PRINT("Camera[%d] setting focus mode: idx = %d , str = %s\n",
              getCameraId(),focusModeIdx,mCaps.focusModes[focusModeIdx].c_str());
    if (std::find(mCaps.focusModes.begin(), mCaps.focusModes.end(), mConfig.focusModeStr.c_str()) != mCaps.focusModes.end()){
         mCameraIF->mParams.setString(CameraParameters::KEY_FOCUS_MODE, mConfig.focusModeStr);
         CAM_PRINT("Camera[%d] setting focusMode = %s \n", getCameraId(),mConfig.focusModeStr.c_str());
    } else {
       CAM_PRINT("Camera[%d] Requested focusMode = %s not available \n", getCameraId(),mConfig.focusModeStr.c_str());
    }

    // ISO
    if (std::find(mCaps.isoModes.begin(), mCaps.isoModes.end(), mConfig.isoMode.c_str()) != mCaps.isoModes.end()){
       mCameraIF->mParams.setString(KEY_QC_ISO_MODE, mConfig.isoMode);
       CAM_PRINT("Camera[%d] setting isoMode = %s \n", getCameraId(),mConfig.isoMode.c_str());
    } else {
       CAM_PRINT("Camera[%d] Requested isoMode = %s not available \n",getCameraId(),mConfig.isoMode.c_str());
    }

    // WhiteBalance mode
    if (std::find(mCaps.wbModes.begin(), mCaps.wbModes.end(), mConfig.wbMode.c_str()) != mCaps.wbModes.end()){
        mCameraIF->mParams.setString(CameraParameters::KEY_WHITE_BALANCE, mConfig.wbMode);
        CAM_PRINT("Camera[%d] setting wbMode = %s \n", getCameraId(), mConfig.wbMode.c_str());
    } else {
        CAM_PRINT("Camera[%d] Requested wbMode = %s not available \n", getCameraId(),mConfig.wbMode.c_str());
    }

    // Sharpness
    if (mConfig.sharpnessValue != -1) {
        if (mConfig.sharpnessValue >= mCaps.sharpness.min && mConfig.sharpnessValue <= mCaps.sharpness.max){
            mCameraIF->mParams.set(KEY_QC_SHARPNESS, mConfig.sharpnessValue);
            CAM_PRINT("Camera[%d] setting sharpness = %d \n", getCameraId(),mConfig.sharpnessValue);
        } else {
            CAM_PRINT("Camera[%d] Requested sharpness value = %d not in range \n", getCameraId(),mConfig.sharpnessValue);
        }
    }

    if (mConfig.outputFormat == RAW_FORMAT) {
        /* Do not turn on videostream for tracking camera in RAW format */
        mConfig.testVideo = false;
        CAM_PRINT("Camera[%d] Setting output = RAW_FORMAT for camera\n", getCameraId());
        mCameraIF->mParams.set(CameraParameters::KEY_PREVIEW_FORMAT, CameraParameters::PIXEL_FORMAT_BAYER_RAW10);
    }

    CAM_PRINT("Camera[%d] setting preview size: %dx%d\n", getCameraId(), mConfig.pSize.width, mConfig.pSize.height);
    mCameraIF->mParams.setPreviewSize(mConfig.pSize.width, mConfig.pSize.height);

    setSnapshotParameters();

    CAM_PRINT("Camera[%d] setting video size: %dx%d\n", getCameraId(), mConfig.vSize.width, mConfig.vSize.height);
    mCameraIF->mParams.setVideoSize(mConfig.vSize.width, mConfig.vSize.height);

    /* Find index and set FPS  */
    int rc = setFPSindex(mConfig, pFpsIdx, vFpsIdx);
    if (NO_ERROR != rc) {
        return rc;
    }

    CAM_PRINT("Camera[%d] setting preview fps range: %d, %d ( idx = %d ) \n",
        getCameraId(), mCaps.previewFpsRanges[pFpsIdx].min,
        mCaps.previewFpsRanges[pFpsIdx].max, pFpsIdx);
    mCameraIF->mParams.setPreviewFpsRange(mCaps.previewFpsRanges[pFpsIdx].min,
                               mCaps.previewFpsRanges[pFpsIdx].max);

    if (vFpsIdx >= 0) {
        CAM_PRINT("Camera[%d] setting video fps: %d ( idx = %d )\n", getCameraId(), mCaps.videoFpsMode[vFpsIdx], vFpsIdx );
        mCameraIF->mParams.set(KEY_QC_VIDEO_HIGH_FRAME_RATE, mCaps.videoFpsMode[vFpsIdx]);
    }

    CAM_PRINT("Camera[%d] setting antibanding: %s \n", getCameraId(),mConfig.antibanding.c_str());
    mCameraIF->mParams.setString(CameraParameters::KEY_ANTIBANDING, mConfig.antibanding);

    mCameraIF->mParams.set(KEY_QC_STATS_LOGGING_MASK, mConfig.statsLogMask);

    return mCameraIF->setParameters(mCameraIF->mParams);
}

int Camera1Instance::setTrackingCameraParameters()
{
    int focusModeIdx = 0;
    int pFpsIdx = 3;
    int vFpsIdx = 3;

    // focus mode
    for (size_t i = 0; i < mCaps.focusModes.size(); i++) {
        if (strcmp(mCaps.focusModes[i].c_str(),mConfig.focusModeStr.c_str()) == 0) {
           /*ignore focuse mode since no reqiurement*/
           //focusModeIdx=i;
        }
    }
    CAM_PRINT("Camera[%d] setting focus mode: idx = %d , str = %s\n",
              getCameraId(),focusModeIdx,mCaps.focusModes[focusModeIdx].c_str());
    if (std::find(mCaps.focusModes.begin(), mCaps.focusModes.end(), mConfig.focusModeStr.c_str()) != mCaps.focusModes.end()){
         mCameraIF->mParams.setString(CameraParameters::KEY_FOCUS_MODE, mConfig.focusModeStr);
         CAM_PRINT("Camera[%d] setting focusMode = %s \n", getCameraId(),mConfig.focusModeStr.c_str());
    } else {
       CAM_PRINT("Camera[%d] Requested focusMode = %s not available \n", getCameraId(),mConfig.focusModeStr.c_str());
    }

    // ISO
    if (std::find(mCaps.isoModes.begin(), mCaps.isoModes.end(), mConfig.isoMode.c_str()) != mCaps.isoModes.end()){
       mCameraIF->mParams.setString(KEY_QC_ISO_MODE, mConfig.isoMode);
       CAM_PRINT("Camera[%d] setting isoMode = %s \n", getCameraId(),mConfig.isoMode.c_str());
    } else {
       CAM_PRINT("Camera[%d] Requested isoMode = %s not available \n",getCameraId(),mConfig.isoMode.c_str());
    }

    // WhiteBalance mode
    if (std::find(mCaps.wbModes.begin(), mCaps.wbModes.end(), mConfig.wbMode.c_str()) != mCaps.wbModes.end()){
        mCameraIF->mParams.setString(CameraParameters::KEY_WHITE_BALANCE, mConfig.wbMode);
        CAM_PRINT("Camera[%d] setting wbMode = %s \n", getCameraId(), mConfig.wbMode.c_str());
    } else {
        CAM_PRINT("Camera[%d] Requested wbMode = %s not available \n", getCameraId(),mConfig.wbMode.c_str());
    }

    // Sharpness
    if (mConfig.sharpnessValue != -1) {
        if (mConfig.sharpnessValue >= mCaps.sharpness.min && mConfig.sharpnessValue <= mCaps.sharpness.max){
            mCameraIF->mParams.set(KEY_QC_SHARPNESS, mConfig.sharpnessValue);
            CAM_PRINT("Camera[%d] setting sharpness = %d \n", getCameraId(),mConfig.sharpnessValue);
        } else {
            CAM_PRINT("Camera[%d] Requested sharpness value = %d not in range \n", getCameraId(),mConfig.sharpnessValue);
        }
    }

    if (mConfig.outputFormat == RAW_FORMAT) {
        /* Do not turn on videostream for tracking camera in RAW format */
        mConfig.testVideo = false;
        CAM_PRINT("Camera[%d] Setting output = RAW_FORMAT for camera\n", getCameraId());
        mCameraIF->mParams.set(CameraParameters::KEY_PREVIEW_FORMAT, CameraParameters::PIXEL_FORMAT_BAYER_RAW10);
    }

    CAM_PRINT("Camera[%d] setting preview size: %dx%d\n", getCameraId(), mConfig.pSize.width, mConfig.pSize.height);
    mCameraIF->mParams.setPreviewSize(mConfig.pSize.width, mConfig.pSize.height);

    setSnapshotParameters();

    CAM_PRINT("Camera[%d] setting video size: %dx%d\n", getCameraId(), mConfig.vSize.width, mConfig.vSize.height);
    mCameraIF->mParams.setVideoSize(mConfig.vSize.width, mConfig.vSize.height);

    /* Find index and set FPS  */
    int rc = setFPSindex(mConfig, pFpsIdx, vFpsIdx);
    if (NO_ERROR != rc) {
        return rc;
    }

    CAM_PRINT("Camera[%d] setting preview fps range: %d, %d ( idx = %d ) \n",
        getCameraId(), mCaps.previewFpsRanges[pFpsIdx].min,
        mCaps.previewFpsRanges[pFpsIdx].max, pFpsIdx);
    mCameraIF->mParams.setPreviewFpsRange(mCaps.previewFpsRanges[pFpsIdx].min,
                               mCaps.previewFpsRanges[pFpsIdx].max);

    if (vFpsIdx >= 0) {
        CAM_PRINT("Camera[%d] setting video fps: %d ( idx = %d )\n", getCameraId(), mCaps.videoFpsMode[vFpsIdx], vFpsIdx );
        mCameraIF->mParams.set(KEY_QC_VIDEO_HIGH_FRAME_RATE, mCaps.videoFpsMode[vFpsIdx]);
    }

    CAM_PRINT("Camera[%d] setting antibanding: %s \n", getCameraId(),mConfig.antibanding.c_str());
    mCameraIF->mParams.setString(CameraParameters::KEY_ANTIBANDING, mConfig.antibanding);

    mCameraIF->mParams.set(KEY_QC_STATS_LOGGING_MASK, mConfig.statsLogMask);

    return mCameraIF->setParameters(mCameraIF->mParams);
}

int Camera1Instance::setDepthCameraParameters()
{
    int focusModeIdx = 0;
    int pFpsIdx = 3;
    int vFpsIdx = 3;

    // focus mode
    for (size_t i = 0; i < mCaps.focusModes.size(); i++) {
        if (strcmp(mCaps.focusModes[i].c_str(),mConfig.focusModeStr.c_str()) == 0) {
           /*ignore focuse mode since no reqiurement*/
           //focusModeIdx=i;
        }
    }
    CAM_PRINT("Camera[%d][Depth] setting focus mode: idx = %d , str = %s\n",
              getCameraId(),focusModeIdx,mCaps.focusModes[focusModeIdx].c_str());
    if (std::find(mCaps.focusModes.begin(), mCaps.focusModes.end(), mConfig.focusModeStr.c_str()) != mCaps.focusModes.end()){
         mCameraIF->mParams.setString(CameraParameters::KEY_FOCUS_MODE, mConfig.focusModeStr);
         CAM_PRINT("Camera[%d][Depth] setting focusMode = %s \n", getCameraId(),mConfig.focusModeStr.c_str());
    } else {
       CAM_PRINT("Camera[%d][Depth] Requested focusMode = %s not available \n", getCameraId(),mConfig.focusModeStr.c_str());
    }

    // ISO
    if (std::find(mCaps.isoModes.begin(), mCaps.isoModes.end(), mConfig.isoMode.c_str()) != mCaps.isoModes.end()){
       mCameraIF->mParams.setString(KEY_QC_ISO_MODE, mConfig.isoMode);
       CAM_PRINT("Camera[%d][Depth] setting isoMode = %s \n", getCameraId(),mConfig.isoMode.c_str());
    } else {
       CAM_PRINT("Camera[%d][Depth] Requested isoMode = %s not available \n",getCameraId(),mConfig.isoMode.c_str());
    }

    // WhiteBalance mode
    if (std::find(mCaps.wbModes.begin(), mCaps.wbModes.end(), mConfig.wbMode.c_str()) != mCaps.wbModes.end()){
        mCameraIF->mParams.setString(CameraParameters::KEY_WHITE_BALANCE, mConfig.wbMode);
        CAM_PRINT("Camera[%d][Depth] setting wbMode = %s \n", getCameraId(), mConfig.wbMode.c_str());
    } else {
        CAM_PRINT("Camera[%d][Depth] Requested wbMode = %s not available \n", getCameraId(),mConfig.wbMode.c_str());
    }

    // Sharpness
    if (mConfig.sharpnessValue != -1) {
        if (mConfig.sharpnessValue >= mCaps.sharpness.min && mConfig.sharpnessValue <= mCaps.sharpness.max){
            mCameraIF->mParams.set(KEY_QC_SHARPNESS, mConfig.sharpnessValue);
            CAM_PRINT("Camera[%d][Depth] setting sharpness = %d \n", getCameraId(),mConfig.sharpnessValue);
        } else {
            CAM_PRINT("Camera[%d][Depth] Requested sharpness value = %d not in range \n", getCameraId(),mConfig.sharpnessValue);
        }
    }

    if (mConfig.outputFormat == DEPTH_Y16_FORMAT) {
        /* Do not turn on videostream for tracking camera in RAW format */
        mConfig.testVideo = false;
        mConfig.testSnapshot= false;
        CAM_PRINT("Camera[%d][Depth] Setting output = DEPTH_Y16_FORMAT for camera	\n", getCameraId());
        mCameraIF->mParams.set(CameraParameters::KEY_PREVIEW_FORMAT, CameraParameters::PIXEL_FORMAT_DEPTH_Y16);
   }

    CAM_PRINT("Camera[%d][Depth] setting preview size: %dx%d\n", getCameraId(), mConfig.pSize.width, mConfig.pSize.height);
    mCameraIF->mParams.setPreviewSize(mConfig.pSize.width, mConfig.pSize.height);

    setSnapshotParameters();

    CAM_PRINT("Camera[%d][Depth] setting video size: %dx%d\n", getCameraId(), mConfig.vSize.width, mConfig.vSize.height);
    mCameraIF->mParams.setVideoSize(mConfig.vSize.width, mConfig.vSize.height);

    /* Find index and set FPS  */
    int rc = setFPSindex(mConfig, pFpsIdx, vFpsIdx);
    if (NO_ERROR != rc) {
        return rc;
    }

    CAM_PRINT("Camera[%d][Depth] setting preview fps range: %d, %d ( idx = %d ) \n",
        getCameraId(), mCaps.previewFpsRanges[pFpsIdx].min,
        mCaps.previewFpsRanges[pFpsIdx].max, pFpsIdx);
    mCameraIF->mParams.setPreviewFpsRange(mCaps.previewFpsRanges[pFpsIdx].min,
                               mCaps.previewFpsRanges[pFpsIdx].max);

    if (vFpsIdx >= 0) {
        CAM_PRINT("Camera[%d][Depth] setting video fps: %d ( idx = %d )\n", getCameraId(), mCaps.videoFpsMode[vFpsIdx], vFpsIdx );
        mCameraIF->mParams.set(KEY_QC_VIDEO_HIGH_FRAME_RATE, mCaps.videoFpsMode[vFpsIdx]);
    }

    CAM_PRINT("Camera[%d][Depth] setting antibanding: %s \n", getCameraId(),mConfig.antibanding.c_str());
    mCameraIF->mParams.setString(CameraParameters::KEY_ANTIBANDING, mConfig.antibanding);

    mCameraIF->mParams.set(KEY_QC_STATS_LOGGING_MASK, mConfig.statsLogMask);

    return mCameraIF->setParameters(mCameraIF->mParams);
}
/**
 *  FUNCTION : setParameters
 *
 *  - When camera is opened, it is initialized with default set
 *    of parameters.
 *  - This function sets required parameters based on camera and
 *    usecase
 *  - params_setXXX and params_set  only updates parameter
 *    values in a local object.
 *  - mCameraIF->mParams.commit() function will update the hardware
 *    settings with the current state of the parameter object
 *  - Some functionality will not be application for all for
 *    sensor modules. for eg. tracking camera sensor does not support
 *    autofocus/focus mode.
 *  - Reference setting for different sensors and format are
 *    provided in this function.
 *
 *  */
int Camera1Instance::setParameters()
{
    int focusModeIdx = 0;
    int pFpsIdx = 3;
    int vFpsIdx = 3;
    int ret = 0;

    switch ( mConfig.func ) {
        case CAM_FUNC_HIRES:
            ret = this->setHiresCameraParameters();
            break;
        case CAM_FUNC_TRACKING:
            ret = this->setTrackingCameraParameters();
            break;
        case CAM_FUNC_STEREO:
        case CAM_FUNC_LEFT_SENSOR:
        case CAM_FUNC_RIGHT_SENSOR:
            break;
        case CAM_FUNC_DEPTH:
            ret = this->setDepthCameraParameters();
            break;
        default:
            CAM_PRINT("invalid sensor function \n");
            ret = EXIT_FAILURE;
    }

    return ret;
}

/**
 *  FUNCTION : setPostParameters
 *
 *  - This function sets required parameters based on camera and
 *    usecase after starting preview
 *
 *  */
int Camera1Instance::setPostParameters()
{
    switch(mConfig.func)
    {
        case CAM_FUNC_HIRES:
            {
                mCameraIF->mParams.set(KEY_QC_EXPOSURE_TIME, mConfig.expTimeValue);
                mCameraIF->mParams.setString(KEY_QC_ISO_MODE, mConfig.isoMode);
                CAM_PRINT("Setting exposure time value = %d , iso value = %s\n",
                    mConfig.expTimeValue, mConfig.isoMode.c_str());
            }
            break;
        case CAM_FUNC_TRACKING:
            {
                 mCameraIF->mParams.set(KEY_QC_EXPOSURE_MANUAL, mConfig.exposureValue);
                 mCameraIF->mParams.set(KEY_QC_GAIN_MANUAL, mConfig.gainValue);
                 CAM_PRINT("Setting exposure value =  %d , gain value = %d \n", mConfig.exposureValue, mConfig.gainValue );
            }
            break;
        case CAM_FUNC_STEREO:
        case CAM_FUNC_LEFT_SENSOR:
        case CAM_FUNC_RIGHT_SENSOR:
            {
                 mCameraIF->mParams.set(KEY_QC_EXPOSURE_MANUAL, mConfig.exposureValue);
                 mCameraIF->mParams.set(KEY_QC_GAIN_MANUAL, mConfig.gainValue);
                 mCameraIF->mParams.set(KEY_QC_VERTICAL_FLIP, (int)true);
                 mCameraIF->mParams.set(KEY_QC_HORIZONTAL_MIRROR, (int)true);
                 CAM_PRINT("Setting exposure value =  %d , gain value = %d \n",
                     mConfig.exposureValue, mConfig.gainValue);
            }
            break;
        case CAM_FUNC_DEPTH:
            break;
        default:
            {
                CAM_PRINT("Invalid camera cofig type \n");
                return -EINVAL;
            }
            break;

    }

    return mCameraIF->setParameters(mCameraIF->mParams);;
}

int Camera1Instance::startCameraInstance(AppCallbacks* cb)
{
    int rc = EXIT_SUCCESS;

    // 1. Camera initial
    rc = initialize();
    if (NO_ERROR != rc) {
        goto del_camera;
    }
    CAM_PRINT("Camera[%d] initialize done \n",getCameraId());

    // 2. set app callbacks
    setApplicationCallback(cb);

    // 3. get initial parameters
    mCameraIF->mParams = mCameraIF->getParameters();
    mCameraIF->mParams.dump();
    CAM_PRINT("Camera[%d] getParameters done \n",getCameraId());

    // 4. Query and print Capabilities
    queryCapabilities();
    CAM_PRINT("Camera[%d] queryCapabilities done \n",getCameraId());

    if (mConfig.infoMode) {
        printCapabilities();
        goto del_camera;
    }

    // 5. Set camera parameters
    rc = setParameters();
    if (NO_ERROR != rc) {
        CAM_PRINT("Camera[%d] setParameters failed \n",getCameraId());
        printUsageExit(0);
        goto del_camera;
    }
    CAM_PRINT("Camera[%d] setParameters done \n",getCameraId());

    // 6. starts the preview stream. At every preview frame onPreviewFrame( ) callback is invoked
    CAM_PRINT("Camera[%d] starting preview \n",getCameraId());
    rc = mCameraIF->startPreview();
    if (NO_ERROR != rc) {
        CAM_PRINT("Camera[%d] startPreview failed \n",getCameraId());
        goto del_camera;
    }
    CAM_PRINT("Camera[%d] start preview done \n",getCameraId());

    // 7. starts video stream. At every video frame onVideoFrame( )  callback is invoked
    if (mConfig.testVideo  == true) {
        CAM_PRINT("Camera[%d] start recording \n",getCameraId());
        //mCameraIF->startRecording();
    }

    // 8. snapshot
    if (mConfig.testSnapshot == true) {
        CAM_PRINT("Camera[%d] waiting for 2 seconds for exposure to settle... \n",getCameraId());
        /* sleep required to settle the exposure before taking snapshot.
           This app does not provide interactive feedback to user
           about the exposure */
        sleep(2);

        CAM_PRINT("Camera[%d] taking picture \n",getCameraId());
        rc = takePicture();
        if (NO_ERROR != rc) {
            CAM_PRINT("Camera[%d] takePicture failed \n",getCameraId());
            goto del_camera;;
        }
    }

    CAM_PRINT("Camera[%d] camera instance start sucess, rc %d\n",getCameraId(), rc);
    return rc;

del_camera:
    // release camera device
    Camera1Interface::deleteInstance(&mCameraIF);

    CAM_PRINT("Camera[%d] camera instance start failed\n",getCameraId());
    return -1;
}

int Camera1Instance::stopCameraInstance()
{

    if (mConfig.testVideo  == true) {
        CAM_PRINT("Camera[%d] stop recording \n",getCameraId());
        mCameraIF->stopRecording();
    }

    CAM_PRINT("Camera[%d] stopping preview \n",getCameraId());
    mCameraIF->stopPreview();
    CAM_PRINT("Camera[%d] stop preview done \n",getCameraId());

    return NO_ERROR;
}

void Camera1Instance::setLogMask(CameraInstanceConfig config)
{
    /* setup syslog level */
    if (config.logLevel == CAM_LOG_SILENT) {
        setlogmask(LOG_UPTO(LOG_EMERG));
    } else if (config.logLevel == CAM_LOG_DEBUG) {
        setlogmask(LOG_UPTO(LOG_DEBUG));
    } else if (config.logLevel == CAM_LOG_INFO) {
        setlogmask(LOG_UPTO(LOG_INFO));
    } else if (config.logLevel == CAM_LOG_ERROR) {
        setlogmask(LOG_UPTO(LOG_ERR));
    }
    openlog(NULL, LOG_NDELAY, LOG_DAEMON);
}
