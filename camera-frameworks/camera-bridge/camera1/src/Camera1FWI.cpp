/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "Camera1FWI.h"

namespace camera1_if
{

Camera1FWI::Camera1FWI(int cameraId) :
    Camera1Interface(cameraId),
    mCamera(NULL)
{
    mStatus = CAMERA1STATUS_INVALID;
    int rc = init(cameraId);
    if (rc != NO_ERROR) {
        mStatus = CAMERA1STATUS_INITED;
    }
    CAM_INFO("init cameraId = %d. client = %p, rc = %d", cameraId, mCamera, rc);
}

Camera1FWI::~Camera1FWI()
{
    deinit();
}

int Camera1FWI::init(int cameraId)
{
    //start binder thread to receive request from camera service
    sp<ProcessState> ps(ProcessState::self());
    ps->startThreadPool();

    sp<Camera> camera = NULL;
    char rawClientName[VALUE_SIZE_MAX];
    snprintf(rawClientName, VALUE_SIZE_MAX, "camera1_bridge_id_%d", cameraId);
    String16 clientName(rawClientName, strlen(rawClientName) + 1);

    CAM_INFO("Camera[%d] connect to service, client name = %s", cameraId, rawClientName);
    camera = Camera::connect(cameraId, clientName, Camera::USE_CALLING_UID);
    if (camera == NULL) {
        CAM_ERR("Connect fail");
        return -EACCES;
    }

    // make sure camera hardware is alive
    int status = camera->getStatus();
    if (NO_ERROR !=  status) {
        CAM_ERR("getStatus err = %d", status);
        return NO_INIT;
    }

    mCamera = camera;

    CAM_INFO("Camera[%d] Connect success, client %p", cameraId, mCamera);

    return NO_ERROR;
}

void Camera1FWI::deinit()
{
    mCamera = NULL;
}

int Camera1FWI::getNumberOfCameras()
{
    return Camera::getNumberOfCameras();
}

int Camera1FWI::getCameraInfo(int cameraId, struct CameraInfo& cameraInfo)
{
    return Camera::getCameraInfo(cameraId, &cameraInfo);
}

int Camera1FWI::startPreview()
{
    StartCbMonitor();

    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return -EPERM;
    }

    return camera->startPreview();
}

int Camera1FWI::stopPreview()
{
    StopCbMonitor();

    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return -EPERM;
    }

    /// Set listener to NULL befere stopping stream
    camera->setListener(NULL);

    camera->stopPreview();

    return NO_ERROR;
}

int Camera1FWI::takePicture(bool zsl)
{
    int rc = NO_ERROR;
    sp<Camera> camera = get_native_camera();
    if (camera == NULL || mCb == NULL) {
        CAM_ERR("%s: failed camera %p cb %p", __FUNCTION__, camera, mCb);
        return -EPERM;
    }

    Mutex::Autolock _l(mCb->mLock);

    int msgType = CAMERA_MSG_COMPRESSED_IMAGE | CAMERA_MSG_SHUTTER;
    rc = camera->takePicture(msgType);
    if (NO_ERROR != rc) {
        return rc;
    }

    /// Wait for jpeg cb
    rc = mCb->mPictureSignal.waitRelative(mCb->mLock, kTimeout);
    if (rc == TIMED_OUT) {
        CAM_PRINT("error: takePicture timed out! \n");
        return rc;
    } else {
        CAM_INFO("takePicture wait for snapshot cb signal done \n");
    }

    if (false == zsl) {
       //camera->stopPreview();
       rc = camera->startPreview();
       if (NO_ERROR != rc) {
          CAM_PRINT("restart preview failed \n");
       }
       CAM_INFO("restart preview successfully for non-zsl \n");
    }

    return rc;
}

int Camera1FWI::startRecording(VideoConfig videoConfig)
{
    StartCbMonitor();

    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return -EPERM;
    }

    if (videoConfig.vFormat == H264_FORMAT ||
        videoConfig.vFormat == H265_FORMAT) {
        CreateVideoEncoder(videoConfig);
        if (mVideoEncoderIF) {
            mVideoEncoderIF->run();
        }
    }

    return camera->startRecording();;
}

int Camera1FWI::stopRecording()
{
    StopCbMonitor();

    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return -EPERM;
    }

    /// Set listener to NULL befere stopping stream
    camera->setListener(NULL);

    if (mVideoEncoderIF) {
        mVideoEncoderIF->stop();
        ReleaseVideoEncoder();
    }

    camera->stopRecording();

    return NO_ERROR;
}

const Camera1Params Camera1FWI::getParameters()
{
    Camera1Params param;

    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return param;
    }

    const String8 string = camera->getParameters();

    param.unflatten(string);

    return param;
}

int Camera1FWI::setParameters(Camera1Params& params)
{
    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return -EPERM;
    }

    return camera->setParameters(params.flatten());
}

int Camera1FWI::setCallbacks(Camera1Callbacks* cb)
{
    sp<Camera> camera = get_native_camera();

    if (camera == NULL || cb == NULL) {
        CAM_ERR("%s: failed camera %p cb %p", __FUNCTION__, camera, cb);
        return -EPERM;
    }

    Camera1Interface::setCallbacks(cb);

    camera->setListener(cb);

    return NO_ERROR;
}

int Camera1FWI::sendCommand(int32_t cmd, int32_t arg1, int32_t arg2)
{
    sp<Camera> camera = get_native_camera();
    if (camera == NULL) {
        CAM_ERR("%s: native camera is NULL",__FUNCTION__);
        return -EPERM;
    }

    return camera->sendCommand(cmd, arg1, arg2);
}

int Camera1FWI::ErrorNotify()
{
    CAM_ERR("send CAMERA_MSG_ERROR");
    int ret = sendCommand(CAMERA_CMD_PING, CAMERA_MSG_ERROR, CAMERA_ERROR_RELEASED);

    /// TODO: catch useful info for camera-bridge before stopping camera-server
    CAM_ERR("Raise error to dump");
    raise(SIGSEGV);

    return ret;
}

} /* namespace camera */
