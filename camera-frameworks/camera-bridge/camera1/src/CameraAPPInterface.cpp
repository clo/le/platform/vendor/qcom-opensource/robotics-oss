/* Copyright (c) 2019 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "CameraAPPInterface.h"

CameraAPPInterface::CameraAPPInterface():
    mCb(NULL),
    mCamera1Instance(NULL),
    mCameraId(-1)
{

}

CameraAPPInterface::~CameraAPPInterface()
{

}


int CameraAPPInterface::subscribecamera(cameraAppParameters param, AppCallbacks* cb)
{
    int ret = 0;

    cameraAppParameters param_;
    param_.cameraFunc = param.cameraFunc;
    param_.cameraId   = param.cameraId;
    param_.format     = param.format;
    param_.streamType = param.streamType;
    param_.width      = param.width;
    param_.height     = param.height;
    param_.targetFPS  = param.targetFPS;

    mCamera1Instance = new Camera1Instance(param_);
    if (NULL == mCamera1Instance){
        CAM_PRINT("mCamera1Instance is NULL\n");
    }

    ret = mCamera1Instance->startCameraInstance(cb);
    if (NO_ERROR != ret) {
        CAM_PRINT("subscribe camera %d faild\n", param_.cameraId);
    }

    mCameraId = mCamera1Instance->getCameraId();
    return ret;
}

int CameraAPPInterface::unsubscribecamera(int camID)
{
    int ret = NO_ERROR;

    ret = mCamera1Instance->stopCameraInstance();
    if (NO_ERROR != ret) {
        CAM_PRINT("unsubscribe camera %d faild\n", mCameraId);
    }

    return ret;
}
