find_path (SYSTEM_INC_PATH pthread.h)
if (SYSTEM_INC_PATH)
    include_directories (${SYSTEM_INC_PATH})
endif()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -lpthread -lrt -lm -lglib-2.0 -ldl -O0 -g -rdynamic -funwind-tables")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=c99 -lpthread -lrt -lm -lglib-2.0 -ldl")
