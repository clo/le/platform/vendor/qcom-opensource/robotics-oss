/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __GST_ROS2SINK_VIDEO_PAD_H__
#define __GST_ROS2SINK_VIDEO_PAD_H__

#include <stdio.h>
#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/base/gstdataqueue.h>
#include <gst/video/video.h>

G_BEGIN_DECLS

#define ROS2SINK_COMMON_VIDEO_CAPS       \
    "camera = (int) [ 0, 1 ], "            \
    "width = (int) [ 16, 1920 ], "         \
    "height = (int) [ 16, 1080 ], "        \
    "framerate = (fraction) [ 0/1, 30/1 ]"

#define ROS2SINK_VIDEO_CAPS(type, formats) \
    "video/" type ", "                       \
    "format = (string) " formats ", "        \
    ROS2SINK_COMMON_VIDEO_CAPS

#define ROS2SINK_VIDEO_CAPS_WITH_FEATURES(type, features, formats) \
    "video/" type "(" features "), "                                 \
    "format = (string) " formats ", "                                \
    ROS2SINK_COMMON_VIDEO_CAPS

// Boilerplate cast macros2 and type check macros2 for Ros2 Sink Video Pad.
#define GST_TYPE_ROS2SINK_VIDEO_PAD (ros2sink_video_pad_get_type())
#define GST_ROS2SINK_VIDEO_PAD(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ROS2SINK_VIDEO_PAD,GstRos2SinkVideoPad))
#define GST_ROS2SINK_VIDEO_PAD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ROS2SINK_VIDEO_PAD,GstRos2SinkVideoPadClass))
#define GST_IS_ROS2SINK_VIDEO_PAD(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ROS2SINK_VIDEO_PAD))
#define GST_IS_ROS2SINK_VIDEO_PAD_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ROS2SINK_VIDEO_PAD))

#define GST_ROS2SINK_VIDEO_PAD_GET_LOCK(obj) (&GST_ROS2SINK_VIDEO_PAD(obj)->lock)
#define GST_ROS2SINK_VIDEO_PAD_LOCK(obj) \
  g_mutex_lock(GST_ROS2SINK_VIDEO_PAD_GET_LOCK(obj))
#define GST_ROS2SINK_VIDEO_PAD_UNLOCK(obj) \
  g_mutex_unlock(GST_ROS2SINK_VIDEO_PAD_GET_LOCK(obj))

typedef struct _GstRos2SinkVideoPad GstRos2SinkVideoPad;
typedef struct _GstRos2SinkVideoPadClass GstRos2SinkVideoPadClass;

struct _GstRos2SinkVideoPad {
  /// Inherited parent structure.
  GstPad         parent;
  /// Synchronization segment
  GstSegment     segment;
  /// Global mutex lock.
  GMutex         lock;
  /// Index of the video pad.
  guint          index;
  /// camera ID, set by the pad capabilities.
  gint           camera;
  /// width, set by the pad capabilities.
  gint           width;
  /// height, set by the pad capabilities.
  gint           height;
  /// framerate, set by the pad capabilities.
  gfloat         framerate;
  /// GStreamer video pad output buffers format.
  GstVideoFormat format;
  /// buffers duration, calculated from framerate.
  guint64        duration;
  /// Timestamp base used to normalize buffer timestamps to running time.
  guint64        tsbase;
  /// Agnostic structure containing format specific parameters.
  GstStructure  *params;
  /// File handle
  FILE          *file;
  /// Frame ID
  gint           frameId;
};

struct _GstRos2SinkVideoPadClass {
  /// Inherited parent structure.
  GstPadClass parent;
};

GType ros2sink_video_pad_get_type(void);

/// Receive memory and send buffer to ROS.
/// It will also set custom functions for query, event and activatemode.
GstPad *ros2sink_request_video_pad (GstElement *element, GstPadTemplate *templ,
                                    const gchar *name, const guint index);

/// Deactivates for the sink video pad.
void ros2sink_release_video_pad (GstElement *element, GstPad *pad);

G_END_DECLS

#endif // __GST_ROS2SINK_VIDEO_PAD_H__
