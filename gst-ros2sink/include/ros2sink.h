/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __GST_ROS2_SINK_H__
#define __GST_ROS2_SINK_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gobject/gtype.h>
#include <glib/gtypes.h>

#include <gst/gstplugin.h>
#include <gst/gstpadtemplate.h>
#include <gst/gstelementfactory.h>
//#include <gst/allocators/allocators.h>

#include <ros2sink_video_pad.h>

using namespace std;

G_BEGIN_DECLS

/// Boilerplate cast macros2 and type check macros2.
#define GST_TYPE_ROS2SINK (ros2sink_get_type())
#define GST_ROS2SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_ROS2SINK,GstRos2Sink))
#define GST_ROS2SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_ROS2SINK,GstRos2SinkClass))
#define GST_IS_ROS2SINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_ROS2SINK))
#define GST_IS_ROS2SINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_ROS2SINK))

#define GST_ROS2SINK_GET_LOCK(obj)   (&GST_ROS2SINK(obj)->lock)
#define GST_ROS2SINK_LOCK(obj)       g_mutex_lock(GST_ROS2SINK_GET_LOCK(obj))
#define GST_ROS2SINK_UNLOCK(obj)     g_mutex_unlock(GST_ROS2SINK_GET_LOCK(obj))
#define ROS2SINK_MAX_CAMERA          (5)
#ifndef GST_CAPS_FEATURE_MEMORY_GBM
#define GST_CAPS_FEATURE_MEMORY_GBM "memory:GBM"
#endif

typedef struct _GstRos2Sink {
  /// Inherited parent structure.
  GstElement parent;
  /// Global mutex lock.
  GMutex      lock;
  /// Next available index for the source pads.
  guint       nextidx;
  /// List containing the indexes of existing video source pads.
  GHashTable *vidindexes;
  /// List of camera devices opened by this source.
  GHashTable *camera_ids;

} GstRos2Sink;

typedef struct _GstRos2SinkClass {
  /// Inherited parent structure.
  GstElementClass parent;
} GstRos2SinkClass;

GType ros2sink_get_type(void);

G_END_DECLS

#endif // __GST_ROS2_SINK_H__