/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "ros2sink_video_pad.h"

#include <gst/gstplugin.h>
#include <gst/gstelementfactory.h>
#include <gst/gstpadtemplate.h>

// Declare ros2sink_video_pad_class_init() and ros2sink_video_pad_init()
// functions, implement ros2sink_video_pad_get_type() function and set
// ros2sink_video_pad_parent_class variable.
G_DEFINE_TYPE(GstRos2SinkVideoPad, ros2sink_video_pad, GST_TYPE_PAD);

static gboolean
ros2sink_video_pad_query(GstPad *pad, GstObject *parent, GstQuery *query)
{
  gboolean success = TRUE;

  GST_INFO_OBJECT(parent, "Received QUERY %s", GST_QUERY_TYPE_NAME(query));

  switch (GST_QUERY_TYPE(query)) {
    case GST_QUERY_LATENCY:
    {
      GstClockTime min_latency, max_latency;

      // Minimum latency is the time to capture one video frame.
      min_latency = GST_ROS2SINK_VIDEO_PAD(pad)->duration;

      // TODO This will change once GstBufferPool is implemented.
      max_latency = GST_CLOCK_TIME_NONE;

      GST_INFO_OBJECT(parent, "Latency %" GST_TIME_FORMAT "/%" GST_TIME_FORMAT,
          GST_TIME_ARGS(min_latency), GST_TIME_ARGS(max_latency));

      // We are always live, the minimum latency is 1 frame and
      // the maximum latency is the complete buffer of frames.
      gst_query_set_latency(query, TRUE, min_latency, max_latency);
      break;
    }
    default:
      success = gst_pad_query_default(pad, parent, query);
      break;
  }

  return success;
}

static gboolean
ros2sink_video_pad_event(GstPad *pad, GstObject *parent, GstEvent *event)
{
  gboolean success = TRUE;

  GST_INFO_OBJECT(parent, "Received EVENT %s", GST_EVENT_TYPE_NAME(event));

  switch (GST_EVENT_TYPE(event)) {
    case GST_EVENT_FLUSH_START:
      gst_event_unref(event);
      break;
    case GST_EVENT_FLUSH_STOP:
      gst_event_unref(event);
      gst_segment_init(&GST_ROS2SINK_VIDEO_PAD(pad)->segment, GST_FORMAT_UNDEFINED);
      break;
    case GST_EVENT_EOS:
      gst_event_unref(event);
      gst_segment_init(&GST_ROS2SINK_VIDEO_PAD(pad)->segment, GST_FORMAT_UNDEFINED);
      break;
    default:
      success = gst_pad_event_default(pad, parent, event);
      break;
  }
  return success;
}

static gboolean
ros2sink_video_pad_activate_mode(GstPad *pad, GstObject *parent,
                        GstPadMode mode, gboolean active)
{
  gboolean success = TRUE;
  GstEvent *event;

  GST_INFO_OBJECT(parent, "Video Pad (%u) mode: %s",
      GST_ROS2SINK_VIDEO_PAD(pad)->index, active ? "ACTIVE" : "STOPED");

  // Call the default pad handler for activate mode.
  return gst_pad_activate_mode(pad, mode, active);
}

static GstFlowReturn
ros2sink_video_pad_chain(GstPad * pad, GstObject * parent, GstBuffer * buf)
{
  GstRos2SinkVideoPad *sinkPad = GST_ROS2SINK_VIDEO_PAD(pad);
  sinkPad->frameId++;

  #if 0
  if (sinkPad->frameId % 10 == 0)
  {
    gchar fname[256];
    snprintf(fname, sizeof(fname), "/data/misc/ros2/ros2sink_frame%d.yuv", sinkPad->frameId);
    GstMemory *memory = gst_buffer_get_memory(buf, 0);
    GstMapInfo info;
    gst_memory_map(memory, &info, GST_MAP_READ);
    gsize &buf_size = info.size;
    guint8* &buf_data = info.data;
    GST_INFO_OBJECT(parent, "ros2sink_chain buf %p size %lu", buf_data, buf_size);

    sinkPad->file = fopen (fname, "wb");
    fwrite(buf_data, buf_size, 1, sinkPad->file);
    fclose(sinkPad->file);
    sinkPad->file = NULL;

    gst_memory_unmap(memory, &info);
    gst_memory_unref(memory);
  }
  #endif

  gst_buffer_unref (buf);
  return GST_FLOW_OK;
}

GstPad*
ros2sink_request_video_pad(GstElement *element, GstPadTemplate *templ,
                          const gchar *name, const guint index)
{
  GstPad *sinkpad = nullptr;

  g_print("Requesting video pad %s (%d) \n", name, index);

  sinkpad = GST_PAD(g_object_new(
      GST_TYPE_ROS2SINK_VIDEO_PAD,
      "name", name,
      "direction", templ->direction,
      "template", templ,
      nullptr
  ));

  if (sinkpad == nullptr) {
    GST_ERROR_OBJECT(element, "Failed to create video pad!");
    return nullptr;
  }
  GST_ROS2SINK_VIDEO_PAD(sinkpad)->index = index;

  gst_pad_set_query_function(sinkpad, GST_DEBUG_FUNCPTR(ros2sink_video_pad_query));
  gst_pad_set_event_function(sinkpad, GST_DEBUG_FUNCPTR(ros2sink_video_pad_event));
  gst_pad_set_activatemode_function(
      sinkpad, GST_DEBUG_FUNCPTR(ros2sink_video_pad_activate_mode));
  gst_pad_set_chain_function (sinkpad, GST_DEBUG_FUNCPTR(ros2sink_video_pad_chain));
  gst_pad_use_fixed_caps(sinkpad);
  gst_pad_set_active(sinkpad, TRUE);
  gst_element_add_pad(element, sinkpad);

  return sinkpad;
}

void
ros2sink_release_video_pad(GstElement *element, GstPad *pad)
{
  gchar *padname = GST_PAD_NAME(pad);
  guint index = GST_ROS2SINK_VIDEO_PAD(pad)->index;

  GST_INFO_OBJECT(element, "Releasing video pad %s (%d)", padname, index);

  gst_object_ref(pad);
  gst_element_remove_pad(element, pad);
  gst_pad_set_active(pad, FALSE);
  gst_object_unref(pad);
}

static void
ros2sink_video_pad_set_property(GObject *object, guint property_id,
                       const GValue *value, GParamSpec *pspec)
{
  // To be realize
}

static void
ros2sink_video_pad_get_property(GObject *object, guint property_id,
                       GValue *value, GParamSpec *pspec)
{
  // To be realize
}

static void
ros2sink_video_pad_finalize(GObject *object)
{
  GstRos2SinkVideoPad *pad = GST_ROS2SINK_VIDEO_PAD(object);

  G_OBJECT_CLASS(ros2sink_video_pad_parent_class)->finalize(object);
}

static gboolean
ros2sink_queue_is_full_cb(GstDataQueue *queue, guint visible, guint bytes,
                 guint64 time, gpointer checkdata)
{
  // There won't be any condition limiting for the buffer queue size.
  return FALSE;
}

// ROS2 Sink video pad class initialization.
static void
ros2sink_video_pad_class_init(GstRos2SinkVideoPadClass* klass)
{
  GObjectClass *gobject = G_OBJECT_CLASS(klass);

  gobject->get_property = GST_DEBUG_FUNCPTR(ros2sink_video_pad_get_property);
  gobject->set_property = GST_DEBUG_FUNCPTR(ros2sink_video_pad_set_property);
  gobject->finalize     = GST_DEBUG_FUNCPTR(ros2sink_video_pad_finalize);
}

// ROS2 Sink video pad initialization.
static void
ros2sink_video_pad_init(GstRos2SinkVideoPad* pad)
{
  gst_segment_init(&pad->segment, GST_FORMAT_UNDEFINED);

  pad->index     = 0;
  pad->camera    = -1;
  pad->width     = -1;
  pad->height    = -1;
  pad->framerate = 0;
  pad->format    = GST_VIDEO_FORMAT_UNKNOWN;
  pad->duration  = 0;
  pad->tsbase    = 0;
  pad->file      = NULL;
  pad->frameId   = 0;
}
