/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include "ros2sink.h"

using namespace std;

#define ROS2SINK_CHECK(element, expr, error) \
  if (expr) { \
    GST_ERROR_OBJECT(element, "%s", error); \
    return FALSE; \
  }

// Declare static GstDebugCategory variable for ros2sink.
GST_DEBUG_CATEGORY_STATIC(ros2sink_debug);

// Define default gstreamer core debug log category for ros2sink.
#define GST_CAT_DEFAULT ros2sink_debug

// Declare ros2sink_class_init() and ros2sink_init() functions, implement
// ros2sink_get_type() function and set ros2sink_parent_class variable.
G_DEFINE_TYPE(GstRos2Sink, ros2sink, GST_TYPE_ELEMENT);

static GstStaticPadTemplate kVideoRequestTemplate =
    GST_STATIC_PAD_TEMPLATE("video_%u",
        GST_PAD_SINK,
        GST_PAD_REQUEST,
        GST_STATIC_CAPS_ANY
        #if 0
        GST_STATIC_CAPS (
            ROS2SINK_VIDEO_CAPS("x-raw",
                "{ YUY2 }"
            ) "; "
            ROS2SINK_VIDEO_CAPS_WITH_FEATURES("x-raw",
                GST_CAPS_FEATURE_MEMORY_GBM,
                "{ NV12 }"
            )
        )
        #endif
    );

static gboolean
ros2sink_pad_push_event(GstElement *element, GstPad *pad, GstEvent *event)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(element);
  GST_INFO_OBJECT(ros2sink, "Event: %s", GST_EVENT_TYPE_NAME(event));
  return gst_pad_push_event(pad, gst_event_copy(event));
}

static gboolean
ros2sink_pad_send_event(GstElement *element, GstPad *pad, GstEvent *event)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(element);
  GST_INFO_OBJECT(ros2sink, "Event: %s", GST_EVENT_TYPE_NAME(event));
  return gst_pad_send_event(pad, gst_event_copy(event));
}

static GstPad*
ros2sink_request_pad(GstElement *element, GstPadTemplate *templ,
                    const gchar *reqname, const GstCaps *caps)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(element);
  GstElementClass *klass = GST_ELEMENT_GET_CLASS(element);
  g_print("%s \n", __func__);

  gchar *padname = nullptr;
  guint index = 0;
  gboolean isvideo = FALSE;
  GstPad *sink = nullptr;

  isvideo = (templ == gst_element_class_get_pad_template(klass, "video_%u"));

  if (!isvideo) {
    GST_ERROR_OBJECT(ros2sink, "Invalid pad template");
    return nullptr;
  }

  GST_ROS2SINK_LOCK(ros2sink);

  index = ros2sink->nextidx;
  // Find an unused source pad index.
  while (g_hash_table_contains(ros2sink->vidindexes, GUINT_TO_POINTER(index))) {
    index++;
  }
  // Update the index for next video pad and set his name.
  ros2sink->nextidx = index + 1;

  if (isvideo) {
    padname = g_strdup_printf("video_%u", index);
    sink = ros2sink_request_video_pad(element, templ, padname, index);
    g_free(padname);
    g_hash_table_insert(ros2sink->vidindexes, GUINT_TO_POINTER(index), nullptr);
  }

  if (sink == nullptr) {
    GST_ERROR_OBJECT(element, "Failed to create pad!");
    GST_ROS2SINK_UNLOCK(ros2sink);
    return nullptr;
  }

  GST_ROS2SINK_UNLOCK(ros2sink);

  return sink;
}

static void
ros2sink_release_pad(GstElement *element, GstPad *pad)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(element);

  guint index = 0;

  GST_ROS2SINK_LOCK(ros2sink);

  if (GST_IS_ROS2SINK_VIDEO_PAD(pad)) {
    index = GST_ROS2SINK_VIDEO_PAD(pad)->index;
    ros2sink_release_video_pad(element, pad);
    g_hash_table_remove(ros2sink->vidindexes, GUINT_TO_POINTER(index));
  }

  GST_ROS2SINK_UNLOCK(ros2sink);
}

void
ros2sink_free_queue_item(GstDataQueueItem *item)
{
  g_slice_free(GstDataQueueItem, item);
}

static GstStateChangeReturn
ros2sink_change_state(GstElement *element, GstStateChange transition)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(element);
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GST_INFO_OBJECT(ros2sink, "change_state %d", transition);

  ret = GST_ELEMENT_CLASS(ros2sink_parent_class)->change_state(element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    GST_ERROR_OBJECT(ros2sink, "Failure");
    return ret;
  }

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      ret = GST_STATE_CHANGE_SUCCESS;
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      // Return NO_PREROLL to inform bin/pipeline we won't be able to
      // produce data in the PAUSED state, as this is a live source.
      ret = GST_STATE_CHANGE_NO_PREROLL;
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      ret = GST_STATE_CHANGE_SUCCESS;
      break;
    case GST_STATE_CHANGE_PLAYING_TO_PAUSED:
      break;
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      // Otherwise it's success, we don't want to return spurious
      // NO_PREROLL or ASYNC from internal elements as we care for
      // state changes ourselves here
      // This is to catch PAUSED->PAUSED and PLAYING->PLAYING transitions.
      ret = (GST_STATE_TRANSITION_NEXT(transition) == GST_STATE_PAUSED) ?
          GST_STATE_CHANGE_NO_PREROLL : GST_STATE_CHANGE_SUCCESS;
      break;
  }

  return ret;
}

static gboolean
ros2sink_send_event(GstElement *element, GstEvent *event)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(element);
  gboolean success = TRUE;

  GST_INFO_OBJECT(ros2sink, "Event: %s", GST_EVENT_TYPE_NAME(event));

  switch (GST_EVENT_TYPE(event)) {
    // Bidirectional events.
    case GST_EVENT_FLUSH_START:
      GST_INFO_OBJECT(ros2sink, "Pushing FLUSH_START event");
      success = gst_element_foreach_src_pad(
          element, (GstElementForeachPadFunc)ros2sink_pad_send_event, event
      );
      gst_event_unref(event);
      break;
    case GST_EVENT_FLUSH_STOP:
      GST_INFO_OBJECT(ros2sink, "Pushing FLUSH_STOP event");
      success = gst_element_foreach_src_pad(
          element, (GstElementForeachPadFunc)ros2sink_pad_send_event, event
      );
      gst_event_unref(event);
      break;

    // Downstream serialized events.
    case GST_EVENT_EOS:
      GST_INFO_OBJECT(ros2sink, "Pushing EOS event downstream");
      success = gst_element_foreach_src_pad(
          element, (GstElementForeachPadFunc)ros2sink_pad_push_event, event
      );
      gst_event_unref(event);
      break;
    default:
      success =
          GST_ELEMENT_CLASS(ros2sink_parent_class)->send_event(element, event);
      break;
  }

  return success;
}

// GstElement virtual method implementation. Sets the element's properties.
static void
ros2sink_set_property(GObject *object, guint property_id,
                     const GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

// GstElement virtual method implementation. Sets the element's properties.
static void
ros2sink_get_property(GObject *object, guint property_id,
                     GValue *value, GParamSpec *pspec)
{
  switch (property_id) {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

// GstElement virtual method implementation. Called when plugin is destroyed.
static void
ros2sink_finalize(GObject* object)
{
  GstRos2Sink *ros2sink = GST_ROS2SINK(object);

  if (ros2sink->vidindexes != nullptr) {
    g_hash_table_remove_all(ros2sink->vidindexes);
    g_hash_table_destroy(ros2sink->vidindexes);
    ros2sink->vidindexes = nullptr;
  }

  if (ros2sink->camera_ids != nullptr) {
    g_hash_table_remove_all(ros2sink->camera_ids);
    g_hash_table_destroy(ros2sink->camera_ids);
    ros2sink->camera_ids = nullptr;
  }

  G_OBJECT_CLASS(ros2sink_parent_class)->finalize(object);

  GST_INFO_OBJECT(ros2sink, "finalized");
}

// GObject element class initialization function.
static void
ros2sink_class_init(GstRos2SinkClass *klass)
{
  GObjectClass *gobject       = G_OBJECT_CLASS(klass);
  GstElementClass *gstelement = GST_ELEMENT_CLASS(klass);

  gobject->set_property = GST_DEBUG_FUNCPTR(ros2sink_set_property);
  gobject->get_property = GST_DEBUG_FUNCPTR(ros2sink_get_property);
  gobject->finalize     = GST_DEBUG_FUNCPTR(ros2sink_finalize);

  gst_element_class_add_static_pad_template(
      gstelement, &kVideoRequestTemplate
  );
  gst_element_class_set_static_metadata(
      gstelement, "ROS2 Video Sink", "Sink/Video",
      "Send frames to ROS2 system", "QTI"
  );

  gstelement->request_new_pad = GST_DEBUG_FUNCPTR(ros2sink_request_pad);
  gstelement->release_pad = GST_DEBUG_FUNCPTR(ros2sink_release_pad);

  gstelement->send_event = GST_DEBUG_FUNCPTR(ros2sink_send_event);
  gstelement->change_state = GST_DEBUG_FUNCPTR(ros2sink_change_state);

  // Initializes a new ros2sink GstDebugCategory with the given properties.
  GST_DEBUG_CATEGORY_INIT(ros2sink_debug, "ros2sink", 0, "QTI ROS2 Sink");
}

// GObject element initialization function.
static void
ros2sink_init(GstRos2Sink *ros2sink)
{
  g_print("%s \n", __func__);

  ros2sink->nextidx = 0;

  ros2sink->vidindexes  = g_hash_table_new(nullptr, nullptr);

  ros2sink->camera_ids  = g_hash_table_new(nullptr, nullptr);

  GST_OBJECT_FLAG_SET(ros2sink, GST_ELEMENT_FLAG_SOURCE);
}

static gboolean
plugin_init(GstPlugin* plugin)
{
  return gst_element_register(plugin, "ros2sink", GST_RANK_PRIMARY,
                              GST_TYPE_ROS2SINK);
}

GST_PLUGIN_DEFINE(
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    ros2sink,
    "QTI ROS2 plugin library",
    plugin_init,
    PACKAGE_VERSION,
    PACKAGE_LICENSE,
    PACKAGE_SUMMARY,
    PACKAGE_ORIGIN
)
