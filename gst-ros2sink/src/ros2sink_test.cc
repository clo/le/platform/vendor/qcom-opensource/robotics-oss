/*
* Copyright (c) 2020, The Linux Foundation. All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are
* met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above
*       copyright notice, this list of conditions and the following
*       disclaimer in the documentation and/or other materials provided
*       with the distribution.
*     * Neither the name of The Linux Foundation nor the names of its
*       contributors may be used to endorse or promote products derived
*       from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
* WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
* ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
* BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
* IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <gst/gst.h>

static GstPadProbeReturn
data_cb (GstPad *pad, GstPadProbeInfo *probe_info, gpointer user_data)
{
  GstBuffer *buffer = (GstBuffer*) probe_info->data;

  static gint frameId = 0;

  if (frameId % 10 == 0)
  {
    gchar fname[256];
    snprintf(fname, sizeof(fname), "/data/misc/camera/ros2sinktest_frame%d.yuv", frameId);

    GstMemory *memory = gst_buffer_get_memory(buffer, 0);
    GstMapInfo info;
    gst_memory_map(memory, &info, GST_MAP_READ);
    gsize   &buf_size = info.size;
    guint8* &buf_data = info.data;

    g_print ("data_cb: data %p size %lu \n", buf_data, buf_size);

    FILE *fd = fopen (fname, "wb");
    fwrite(buf_data, buf_size, 1, fd);
    fclose(fd);
    fd = NULL;

    gst_memory_unmap(memory, &info);
    gst_memory_unref(memory);
  }

  frameId++;
  return GST_PAD_PROBE_OK;
}

static gboolean
bus_cb (GstBus *bus, GstMessage *msg, gpointer user_data)
{
  GMainLoop *loop = (GMainLoop *)user_data;

  switch (GST_MESSAGE_TYPE (msg)) {
    case GST_MESSAGE_ERROR:{
      gchar  *debug;
      GError *error = NULL;

      gst_message_parse_error (msg, &error, &debug);
      g_print ("Error: bus_cb -%s\n", error->message);
      g_error_free (error);
      g_free (debug);
      g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }
  return TRUE;
}

int
main (int argc, char **argv)
{

  GMainLoop *loop;

  GstElement *pipeline;
  GstElement *src, *filter, *sink;
  GstPad *pad;

  /// gst init
  gst_init (&argc, &argv);

  /// setup pipelines
  pipeline = gst_pipeline_new ("pipeline");

  src = gst_element_factory_make ("v4l2src", NULL);

  filter = gst_element_factory_make ("capsfilter", NULL);
  gst_util_set_object_arg (G_OBJECT (filter), "caps",
      "video/x-raw, width=1920, height=1080, framerate=5/1, "
      "format=YUY2");

  sink = gst_element_factory_make ("ros2sink", NULL);

  gst_bin_add_many (GST_BIN (pipeline), src, filter, sink, NULL);

  gst_element_link_many (src, filter, sink, NULL);

  /// debug level
  gst_debug_set_default_threshold(GST_LEVEL_INFO); //4

  /// start loop
  g_print ("Running ...\n");
  gst_element_set_state (pipeline, GST_STATE_PLAYING);

  loop = g_main_loop_new (NULL, FALSE);

  /// set bus cb
  gst_bus_add_watch (GST_ELEMENT_BUS (pipeline), bus_cb, loop);

  /// set data cb
  pad = gst_element_get_static_pad (sink, "video_0");
  gst_pad_add_probe (pad, GST_PAD_PROBE_TYPE_BUFFER, data_cb, NULL, NULL);
  gst_object_unref (pad);

  /* wait until it's up and running or failed */
  if (gst_element_get_state (pipeline, NULL, NULL, -1) == GST_STATE_CHANGE_FAILURE) {
    g_error ("Failed to Go into PLAYING state");
  }
  g_main_loop_run (loop);

  /// exit
  gst_element_set_state (pipeline, GST_STATE_NULL);
  gst_object_unref (pipeline);

  return 0;
}
